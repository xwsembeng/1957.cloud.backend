﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ComboSimulator.WebJob.SimulatorCore.Devices;
using ComboSimulator.WebJob.SimulatorCore.Devices.Factory;
using ComboSimulator.WebJob.SimulatorCore.Logging;
using ComboSimulator.WebJob.SimulatorCore.Telemetry.Factory;
using ComboSimulator.WebJob.SimulatorCore.Transport.Factory;
using XWS.BusinessPlatform.Core.Configurations;
using XWS.ComBo.Common.Interfaces;

namespace ComboSimulator.WebJob
{
    /// <summary>
    /// Creates multiple devices with events for testing.
    /// </summary>
    public class BulkDeviceTester
    {
        private readonly ILogger _logger;
        private readonly ITransportFactory _transportFactory;
        private readonly IConfigurationProvider _configProvider;
        private readonly ITelemetryFactory _telemetryFactory;
        private readonly IDeviceFactory _deviceFactory;
        private readonly IVirtualDeviceStorage _virtualDeviceStorage;

        /// <summary>
        /// Timespan intervall for polling if new devices are available
        /// </summary>
        private readonly int _devicePollIntervalSeconds;

        private const int DEFAULT_DEVICE_POLL_INTERVAL_SECONDS = 120;

        public BulkDeviceTester(ITransportFactory transportFactory, ILogger logger, IConfigurationProvider configProvider,
            ITelemetryFactory telemetryFactory, IDeviceFactory deviceFactory, IVirtualDeviceStorage virtualDeviceStorage)
        {
            _transportFactory = transportFactory;
            _logger = logger;
            _configProvider = configProvider;
            _telemetryFactory = telemetryFactory;
            _deviceFactory = deviceFactory;
            _virtualDeviceStorage = virtualDeviceStorage;

            string pollingIntervalString = _configProvider.GetConfigurationSettingValueOrDefault(
                "DevicePollIntervalSeconds",
                DEFAULT_DEVICE_POLL_INTERVAL_SECONDS.ToString(CultureInfo.InvariantCulture));

            _devicePollIntervalSeconds = Convert.ToInt32(pollingIntervalString, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Retrieves a set of device configs from the repository and creates devices with this information
        /// Once the devices are built, they are started
        /// </summary>
        /// <param name="token"></param>
        public async Task ProcessDevicesAsync(CancellationToken token)
        {
            var dm = new DeviceManager(_logger, token);

            try
            {
                _logger.LogInfo("********** Starting Simulator **********");
                while (!token.IsCancellationRequested)
                {
                    var virtualDevicesConfig = await _virtualDeviceStorage.GetDeviceListAsync();
                    var liveDevicesIds = dm.GetLiveDevicesIds();

                    var newVirtualDevicesConfig = virtualDevicesConfig
                        .Where(virtualDevice => !liveDevicesIds.Contains(virtualDevice.DeviceId)).ToList();
                    var removedDevicesIds = liveDevicesIds
                        .Where(liveDevice => virtualDevicesConfig.All(x => x.DeviceId != liveDevice)).ToList();

                    if (removedDevicesIds.Any())
                    {
                        _logger.LogInfo("********** {0} DEVICES REMOVED ********** ", removedDevicesIds.Count);

                        dm.StopDevices(removedDevicesIds);
                    }

                    //begin processing any new devices that were retrieved
                    if (newVirtualDevicesConfig.Any())
                    {
                        _logger.LogInfo("********** {0} NEW DEVICES FOUND ********** ", newVirtualDevicesConfig.Count);

                        var devicesToProcess = new List<IDevice>();

                        foreach (var deviceConfig in newVirtualDevicesConfig)
                        {
                            _logger.LogInfo("********** SETTING UP NEW DEVICE : {0} ********** ",
                                deviceConfig.DeviceId);
                            devicesToProcess.Add(_deviceFactory.CreateDevice(_logger, _transportFactory,
                                _telemetryFactory, deviceConfig));
                        }

                        dm.StartDevices(devicesToProcess);
                    }

                    await Task.Delay(TimeSpan.FromSeconds(_devicePollIntervalSeconds), token);
                }
            }
            catch (TaskCanceledException)
            {
                //do nothing if task was cancelled
                _logger.LogInfo(
                    "********** Primary worker role cancellation token source has been cancelled. **********");
            }
            finally
            {
                //ensure that all devices have been stopped
                dm.StopAllDevices();
            }
        }
    }
}
