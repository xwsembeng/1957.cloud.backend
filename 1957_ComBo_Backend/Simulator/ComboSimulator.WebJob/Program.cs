﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ComboSimulator.WebJob.Combo.Devices.Factory;
using ComboSimulator.WebJob.Combo.Telemetry.Factory;
using ComboSimulator.WebJob.DataInitialization;
using ComboSimulator.WebJob.SimulatorCore.Devices.Factory;
using ComboSimulator.WebJob.SimulatorCore.Logging;
using ComboSimulator.WebJob.SimulatorCore.Transport;
using ComboSimulator.WebJob.SimulatorCore.Transport.Factory;
using XWS.BusinessPlatform.Core.Configurations;
using XWS.BusinessPlatform.Core.InversionOfControl;
using XWS.ComBo.Common.Interfaces;
using XWS.ComBo.Common.Repositories;

namespace ComboSimulator.WebJob
{
    public static class Program
    {
        private static readonly CancellationTokenSource CancellationTokenSource = new CancellationTokenSource();

        private const string SHUTDOWN_FILE_ENV_VAR = "WEBJOBS_SHUTDOWN_FILE";
        private static string _shutdownFile;
        private static Timer _timer;

        static void Main(string[] args)
        {
            try
            {
                #if DEBUG
                    //Task.Delay(30000).Wait();
                #endif


                // Cloud deploys often get staged and started to warm them up, then get a shutdown
                // signal from the framework before being moved to the production slot. We don't want 
                // to start initializing data if we have already gotten the shutdown message, so we'll 
                // monitor it. This environment variable is reliable
                // http://blog.amitapple.com/post/2014/05/webjobs-graceful-shutdown/#.VhVYO6L8-B4
                _shutdownFile = Environment.GetEnvironmentVariable(SHUTDOWN_FILE_ENV_VAR);
                bool shutdownSignalReceived = false;

                // Setup a file system watcher on that file's directory to know when the file is created
                // First check for null, though. This does not exist on a localhost deploy, only cloud
                if (!string.IsNullOrWhiteSpace(_shutdownFile))
                {
                    var fileSystemWatcher = new FileSystemWatcher(Path.GetDirectoryName(_shutdownFile));
                    fileSystemWatcher.Created += OnShutdownFileChanged;
                    fileSystemWatcher.Changed += OnShutdownFileChanged;
                    fileSystemWatcher.NotifyFilter = NotifyFilters.CreationTime | NotifyFilters.FileName | NotifyFilters.LastWrite;
                    fileSystemWatcher.IncludeSubdirectories = false;
                    fileSystemWatcher.EnableRaisingEvents = true;

                    // In case the file had already been created before we started watching it.
                    if (File.Exists(_shutdownFile))
                    {
                        shutdownSignalReceived = true;
                    }
                }

                if (!shutdownSignalReceived)
                {
                    ConfigureContainer();
               
                    //TODO: Implement
                    StartDataInitializationAsNeeded();

                    StartSimulator();
                    RunAsync().Wait();
                }
            }
            catch (Exception ex)
            {
                CancellationTokenSource.Cancel();
                Trace.TraceError("Webjob terminating: {0}", ex);

            }
        }

        private static void OnShutdownFileChanged(object sender, FileSystemEventArgs e)
        {
            if (e.FullPath.IndexOf(Path.GetFileName(_shutdownFile), StringComparison.OrdinalIgnoreCase) >= 0)
            {
                CancellationTokenSource.Cancel();
            }
        }

        private static void ConfigureContainer()
        {
           IoC.Configure("Simulator_Services.config");
        }

        private static void StartDataInitializationAsNeeded()
        {
            //We have observed that Azure reliably starts the web job twice on a fresh deploy. The second start
            //is reliably about 7 seconds after the first start (under current conditions -- this is admittedly
            //not a perfect solution, but absent visibility into the black box of Azure this is what works at
            //the time) with a shutdown command being received on the current instance in the interim. We want
            //to further bolster our guard against starting a data initialization process that may be aborted
            //in the middle of its work. So we want to delay the data initialization for about 10 seconds to
            //give ourselves the best chance of receiving the shutdown command if it is going to come in. After
            //this delay there is an extremely good chance that we are on a stable start that will remain in place.
            _timer = new Timer(CreateInitialDataAsNeeded, null, 10000, Timeout.Infinite);
        }
        private static void CreateInitialDataAsNeeded(object state)
        {
            _timer.Dispose();
            if (!CancellationTokenSource.Token.IsCancellationRequested)
            {
                Trace.TraceInformation("Preparing to add initial data");
                var dataInitializer = IoC.Resolve<IDataInitializer>();
                dataInitializer.CreateInitialDataIfNeeded();
                //Release the transient instance to end the tracking by the container
                IoC.Release(dataInitializer);
            }
        }

        private static void StartSimulator()
        {
            // Dependencies to inject into the Bulk Device Tester
            var logger = new TraceLogger();
            var configProvider = new ConfigurationProvider();


            IVirtualDeviceStorage deviceStorage = null;
            var useConfigforDeviceList = Convert.ToBoolean(configProvider.GetConfigurationSettingValueOrDefault("UseConfigForDeviceList", "False"), CultureInfo.InvariantCulture);

            if (useConfigforDeviceList)
            {
                //TODO: Implement if necessary
                //deviceStorage = new AppConfigRepository(configProvider, logger);
            }
            else
            {
                deviceStorage = new VirtualDeviceTableStorage(configProvider);
            }

            IDeviceFactory deviceFactory = new ComboDeviceFactory();

            // Start Simulator
            Trace.TraceInformation("Starting Simulator");

            var bulkDeviceTester = new BulkDeviceTester(new IotHubTransportFactory(logger, configProvider), logger,
                configProvider, new ComboTelemetryFactory(logger), deviceFactory, deviceStorage);
            Task.Run(() => bulkDeviceTester.ProcessDevicesAsync(CancellationTokenSource.Token), CancellationTokenSource.Token);
        }
        /// <summary>
        /// Checks how many simulated devices are down and forces a restart if down devices > 50% of all simulated devices
        /// </summary>
        /// <returns></returns>
        private static async Task RunAsync()
        {
            while (!CancellationTokenSource.Token.IsCancellationRequested)
            {
                try
                {
                    await Task.Delay(TimeSpan.FromMinutes(5), CancellationTokenSource.Token);
                }
                catch (TaskCanceledException)
                {
                }

                int downDevices, totalDevices;
                StateCollection<DeviceClientState>.GetRatio(DeviceClientState.Down, out downDevices, out totalDevices);
                Trace.TraceInformation($"{downDevices} of {totalDevices} devices down");
                //Forcing of simulator restart if more than 50% of the devices are down and the simulator is deployed as a webjob
                if (downDevices > totalDevices * 0.5 && Environment.GetEnvironmentVariable("WEBSITE_HOSTNAME") != null)
                {
                    Trace.TraceError("Too many devices down. Force restart");
                    break;
                }
            }
        }
    }
}
