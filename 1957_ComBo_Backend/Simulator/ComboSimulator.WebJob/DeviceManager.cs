﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ComboSimulator.WebJob.SimulatorCore.Devices;
using ComboSimulator.WebJob.SimulatorCore.Logging;

namespace ComboSimulator.WebJob
{
    /// <summary>
    /// Manages and coordinates all devices
    /// </summary>
    public class DeviceManager
    {

        private class TaskDetail
        {
            public CancellationTokenSource CancellationTokenSource { get; private set; }
            public Task Task { get; private set; }

            public TaskDetail(Func<CancellationToken, Task> entry)
            {
                CancellationTokenSource = new CancellationTokenSource();
                Task = entry(CancellationTokenSource.Token);
            }
        }

        private readonly ILogger _logger;
        private readonly CancellationToken _token;
        private readonly Dictionary<string, TaskDetail> _tasks;

        public DeviceManager(ILogger logger, CancellationToken token)
        {
            _logger = logger;
            _token = token;

            _tasks = new Dictionary<string, TaskDetail>();
        }

        /// <summary>
        /// Gets the ids of the virtual devices which are already started
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetLiveDevicesIds()
        {
            _logger.LogInfo($"{_tasks.Count} virtual devices were started");
            var completedTasks = _tasks
                .Where(pair => pair.Value.Task.IsCompleted)
                .ToList();
            //Check for completed task and remove them from the execution tasks list
            foreach (var pair in completedTasks)
            {
                if (pair.Value.Task.IsFaulted)
                {
                    _logger.LogWarning($"Device {pair.Key} shut down due to fault");
                }
                else
                {
                    _logger.LogInfo($"Device {pair.Key} shut down as expected");
                }

                _tasks.Remove(pair.Key);
            }

            var deviceIds = _tasks.Keys.ToList();
            foreach (var deviceId in deviceIds)
            {
                _logger.LogInfo($"Device {deviceId} is still running");
            }

            return deviceIds;
        }

        /// <summary>
        /// Starts all the devices in the list of devices in this class.
        /// </summary>
        public void StartDevices(List<IDevice> devices)
        {
            if (devices == null || !devices.Any())
            {
                return;
            }

            foreach (var device in devices)
            {
                _tasks.Add(device.DeviceId, new TaskDetail(device.StartAsync));
            }
        }

        /// <summary>
        /// Cancel the asynchronous tasks for the devices specified
        /// </summary>
        /// <param name="deviceIds"></param>
        public void StopDevices(List<string> deviceIds)
        {
            foreach (var deviceId in deviceIds)
            {
                TaskDetail task;
                if (_tasks.TryGetValue(deviceId, out task))
                {
                    task.CancellationTokenSource.Cancel();
                    _tasks.Remove(deviceId);

                    _logger.LogInfo("********** STOPPED DEVICE : {0} ********** ", deviceId);
                }
            }
        }

        /// <summary>
        /// Cancel the asynchronous tasks for all devices
        /// </summary>
        public void StopAllDevices()
        {
            foreach (var pair in _tasks)
            {
                pair.Value.CancellationTokenSource.Cancel();
                _logger.LogInfo("********** STOPPED DEVICE : {0} ********** ", pair.Key);
            }

            _tasks.Clear();
        }
    }
}
