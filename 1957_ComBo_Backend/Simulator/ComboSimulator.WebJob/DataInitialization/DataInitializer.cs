﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.ComBo.Modules.Services.Interfaces;

namespace ComboSimulator.WebJob.DataInitialization
{
    public class DataInitializer : IDataInitializer
    {
        //private readonly IActionMappingLogic _actionMappingLogic;
        private readonly IDeviceService _deviceService;
        //private readonly IDeviceRulesLogic _deviceRulesLogic;

        public DataInitializer(
            //IActionMappingLogic actionMappingLogic,
            IDeviceService deviceService
            //IDeviceRulesLogic deviceRulesLogic
            )
        {
            //if (actionMappingLogic == null)
            //{
            //    throw new ArgumentNullException("actionMappingLogic");
            //}

            if (deviceService == null)
            {
                throw new ArgumentNullException("deviceService");
            }

            //if (deviceRulesLogic == null)
            //{
            //    throw new ArgumentNullException("deviceRulesLogic");
            //}

            //_actionMappingLogic = actionMappingLogic;
            _deviceService = deviceService;
            //_deviceRulesLogic = deviceRulesLogic;
        }

        /// <summary>
        /// TODO: Implement
        /// </summary>
        public void CreateInitialDataIfNeeded()
        {
            try
            {
                bool initializationNeeded = false;

                // only create default data if the action mappings are missing

                // check if action mappings are there
                //Task<bool>.Run(async () => initializationNeeded = await _actionMappingLogic.IsInitializationNeededAsync()).Wait();

                //if (!initializationNeeded)
                //{
                //    Trace.TraceInformation("No initial data needed.");
                //    return;
                //}

                //Trace.TraceInformation("Beginning initial data creation...");

                //List<string> bootstrappedDevices = null;

                //// 1) create default devices
                //Task.Run(async () => bootstrappedDevices = await _deviceService.BootstrapDefaultDevices()).Wait();

                //// 2) create default rules
                //Task.Run(() => _deviceRulesLogic.BootstrapDefaultRulesAsync(bootstrappedDevices)).Wait();

                //// 3) create action mappings (do this last to ensure that we'll try to 
                ////    recreate if any of the above throws)
                //_actionMappingLogic.InitializeDataIfNecessaryAsync();

                Trace.TraceInformation("Initial data creation completed.");
            }
            catch (Exception ex)
            {
                Trace.TraceError("Failed to create initial default data: {0}", ex.ToString());
            }
        }
    }
}
