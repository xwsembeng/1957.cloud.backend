﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ComboSimulator.WebJob.Combo.Telemetry.Data;
using ComboSimulator.WebJob.SimulatorCore.Logging;
using ComboSimulator.WebJob.SimulatorCore.SampleDataGenerator;
using ComboSimulator.WebJob.SimulatorCore.Telemetry;

namespace ComboSimulator.WebJob.Combo.Telemetry
{
    /// <summary>
    /// The logic and events for sending remote monitor telemetry
    /// </summary>
    public class RemoteMonitorTelemetry : ITelemetry, ITelemetryWithInterval, ITelemetryWithTemperatureMeanValue, ITelemetryFactoryResetSupport
    {
        private readonly ILogger _logger;
        private readonly string _deviceId;

        private const uint REPORT_FREQUENCY_IN_SECONDS = 15;
        private const uint PEAK_FREQUENCY_IN_SECONDS = 90;

        private SampleDataGenerator _temperatureGenerator;
        private SampleDataGenerator _humidityGenerator;
        //Data generator for sending additional telemetry data (enabled/disabled by command)
        private SampleDataGenerator _externalTemperatureGenerator;

        public RemoteMonitorTelemetry(ILogger logger, string deviceId)
        {
            _logger = logger;
            _deviceId = deviceId;

            Reset();
        }

        public bool ActivateExternalTemperature { get; set; }

        private bool _telemetryActive;
        public bool TelemetryActive
        {
            get
            {
                return _telemetryActive;
            }

            set
            {
                _telemetryActive = value;
                _telemetryIntervalInSeconds = _telemetryActive ? REPORT_FREQUENCY_IN_SECONDS : 0;
            }
        }

        private uint _telemetryIntervalInSeconds;
        public uint TelemetryIntervalInSeconds
        {
            get
            {
                return _telemetryIntervalInSeconds;
            }

            set
            {
                _telemetryIntervalInSeconds = value;
                _telemetryActive = _telemetryIntervalInSeconds > 0;
            }
        }

        public double TemperatureMeanValue
        {
            get
            {
                return _temperatureGenerator.GetMidPointOfRange();
            }

            set
            {
                _temperatureGenerator.ShiftSubsequentData(value);
            }
        }

        public async Task SendEventsAsync(CancellationToken token, Func<object, Task> sendMessageAsync)
        {
            var monitorData = new RemoteMonitorTelemetryData();
            while (!token.IsCancellationRequested)
            {
                if (TelemetryActive)
                {
                    monitorData.DeviceId = _deviceId;
                    monitorData.Temperature = _temperatureGenerator.GetNextValue();
                    monitorData.Humidity = _humidityGenerator.GetNextValue();
                    var messageBody = "Temperature: " + Math.Round(monitorData.Temperature, 2)
                                         + " Humidity: " + Math.Round(monitorData.Humidity, 2);

                    if (ActivateExternalTemperature)
                    {
                        monitorData.ExternalTemperature = _externalTemperatureGenerator.GetNextValue();
                        messageBody += " External Temperature: " + Math.Round((double)monitorData.ExternalTemperature, 2);
                    }
                    else
                    {
                        monitorData.ExternalTemperature = null;
                    }

                    _logger.LogInfo("Sending " + messageBody + " for Device: " + _deviceId);

                    await sendMessageAsync(monitorData);
                }
                await Task.Delay(TimeSpan.FromSeconds(TelemetryIntervalInSeconds), token);
            }
        }

        public void FactoryReset()
        {
            Reset();
        }

        private void Reset()
        {
            ActivateExternalTemperature = false;
            TelemetryActive = true;

            int peakFrequencyInTicks = Convert.ToInt32(Math.Ceiling((double)PEAK_FREQUENCY_IN_SECONDS / REPORT_FREQUENCY_IN_SECONDS));

            _temperatureGenerator = new SampleDataGenerator(33, 36, 42, peakFrequencyInTicks);
            _humidityGenerator = new SampleDataGenerator(20, 50);
            _externalTemperatureGenerator = new SampleDataGenerator(-20, 120);

            TelemetryIntervalInSeconds = REPORT_FREQUENCY_IN_SECONDS;
        }
    }
}
