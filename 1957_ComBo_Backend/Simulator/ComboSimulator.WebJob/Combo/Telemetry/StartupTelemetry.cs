﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComboSimulator.WebJob.SimulatorCore.Devices;
using ComboSimulator.WebJob.SimulatorCore.Logging;
using ComboSimulator.WebJob.SimulatorCore.Telemetry;

namespace ComboSimulator.WebJob.Combo.Telemetry
{
    /// <summary>
    /// The logic and events for sending startup telemetry
    /// </summary>
    public class StartupTelemetry : ITelemetry
    {
        private readonly IDevice _device;

        public StartupTelemetry(IDevice device)
        {
            _device = device;
        }

        public async Task SendEventsAsync(System.Threading.CancellationToken token, Func<object, Task> sendMessageAsync)
        {
            if (!token.IsCancellationRequested)
            {
                await sendMessageAsync(_device.Twin.Properties.Reported);
            }
        }
    }
}
