﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComboSimulator.WebJob.SimulatorCore.Devices;
using ComboSimulator.WebJob.SimulatorCore.Logging;
using ComboSimulator.WebJob.SimulatorCore.Telemetry.Factory;

namespace ComboSimulator.WebJob.Combo.Telemetry.Factory
{
    public class ComboTelemetryFactory : ITelemetryFactory
    {
        private readonly ILogger _logger;

        public ComboTelemetryFactory(ILogger logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Populates a device with telemetry events or logic
        /// </summary>
        /// <param name="device">Device interface to populate</param>
        /// <returns>
        /// Returns object as a way to handle returning the instance that is generating telemetry data
        /// so that it can be used by the caller of this method
        /// </returns>
        public object PopulateDeviceWithTelemetryEvents(IDevice device)
        {
            var startupTelemetry = new StartupTelemetry(device);
            device.TelemetryEvents.Add(startupTelemetry);

            var monitorTelemetry = new RemoteMonitorTelemetry(_logger, device.DeviceId);
            device.TelemetryEvents.Add(monitorTelemetry);

            return monitorTelemetry;
        }
    }
}
