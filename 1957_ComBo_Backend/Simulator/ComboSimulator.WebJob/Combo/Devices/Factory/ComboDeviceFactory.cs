﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComboSimulator.WebJob.SimulatorCore.Devices;
using ComboSimulator.WebJob.SimulatorCore.Devices.Factory;
using ComboSimulator.WebJob.SimulatorCore.Logging;
using ComboSimulator.WebJob.SimulatorCore.Telemetry.Factory;
using ComboSimulator.WebJob.SimulatorCore.Transport.Factory;
using XWS.BusinessPlatform.Core.Configurations;
using XWS.ComBo.Modules.Domain;

namespace ComboSimulator.WebJob.Combo.Devices.Factory
{
    public class ComboDeviceFactory : IDeviceFactory
    {
        public IDevice CreateDevice(ILogger logger, ITransportFactory transportFactory,
            ITelemetryFactory telemetryFactory, InitialDeviceConfig config)
        {
            var device = new ComboDevice(logger, transportFactory, telemetryFactory);
            device.Init(config);
            return device;
        }
    }
}
