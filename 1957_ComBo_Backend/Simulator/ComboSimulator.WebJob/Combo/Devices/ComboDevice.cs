﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ComboSimulator.WebJob.Combo.CommandProcessors;
using ComboSimulator.WebJob.Combo.Telemetry;
using ComboSimulator.WebJob.SimulatorCore.CommandProcessors;
using ComboSimulator.WebJob.SimulatorCore.Devices;
using ComboSimulator.WebJob.SimulatorCore.Devices.DMTasks;
using ComboSimulator.WebJob.SimulatorCore.Logging;
using ComboSimulator.WebJob.SimulatorCore.Telemetry;
using ComboSimulator.WebJob.SimulatorCore.Telemetry.Factory;
using ComboSimulator.WebJob.SimulatorCore.Transport.Factory;
using Microsoft.Azure.Devices.Client;
using Microsoft.Azure.Devices.Shared;
using Newtonsoft.Json;
using XWS.ComBo.Common.Extensions;

namespace ComboSimulator.WebJob.Combo.Devices
{

    /// <summary>
    /// Implementation of a specific device type that extends the BaseDevice functionality
    /// </summary>
    public class ComboDevice : DeviceBase
    {
        /// <summary>
        /// Teh current device management task to execute
        /// </summary>
        private Task _deviceManagementTask;

        public ComboDevice(ILogger logger, ITransportFactory transportFactory, ITelemetryFactory telemetryFactory)
            : base(logger, transportFactory, telemetryFactory)
        {
            DesiredPropertyUpdateHandlers.Add(TemperatureMeanValuePropertyName, OnTemperatureMeanValueUpdate);
            DesiredPropertyUpdateHandlers.Add(TelemetryIntervalPropertyName, OnTelemetryIntervalUpdate);
        }

        /// <summary>
        /// Builds up the set of commands that are supported by this device
        /// </summary>
        protected override void InitCommandProcessors()
        {
            var pingDeviceProcessor = new PingDeviceProcessor(this);
            var startCommandProcessor = new StartCommandProcessor(this);
            var stopCommandProcessor = new StopCommandProcessor(this);
            var diagnosticTelemetryCommandProcessor = new DiagnosticTelemetryCommandProcessor(this);
            var changeSetPointTempCommandProcessor = new ChangeSetPointTempCommandProcessor(this);
            var changeDeviceStateCommmandProcessor = new ChangeDeviceStateCommandProcessor(this);

            pingDeviceProcessor.NextCommandProcessor = startCommandProcessor;
            startCommandProcessor.NextCommandProcessor = stopCommandProcessor;
            stopCommandProcessor.NextCommandProcessor = diagnosticTelemetryCommandProcessor;
            diagnosticTelemetryCommandProcessor.NextCommandProcessor = changeSetPointTempCommandProcessor;
            changeSetPointTempCommandProcessor.NextCommandProcessor = changeDeviceStateCommmandProcessor;

            RootCommandProcessor = pingDeviceProcessor;
        }

        #region Command-Messages
        public bool StartTelemetryData()
        {
            var remoteMonitorTelemetry = (RemoteMonitorTelemetry)TelemetryController;
            bool lastStatus = remoteMonitorTelemetry.TelemetryActive;
            remoteMonitorTelemetry.TelemetryActive = true;
            Logger.LogInfo("Device {0}: Telemetry has started", DeviceId);

            return lastStatus;
        }

        public bool StopTelemetryData()
        {
            var remoteMonitorTelemetry = (RemoteMonitorTelemetry)TelemetryController;
            bool lastStatus = remoteMonitorTelemetry.TelemetryActive;
            remoteMonitorTelemetry.TelemetryActive = false;
            Logger.LogInfo("Device {0}: Telemetry has stopped", DeviceId);

            return lastStatus;
        }
        /// <summary>
        /// Sets the temperature point around that the temperature data is generated
        /// </summary>
        /// <param name="setPointTemp"></param>
        public void ChangeSetPointTemp(double setPointTemp)
        {
            var remoteMonitorTelemetry = (RemoteMonitorTelemetry)TelemetryController;
            remoteMonitorTelemetry.TemperatureMeanValue = setPointTemp;
            Logger.LogInfo("Device {0} temperature changed to {1}", DeviceId, setPointTemp);
        }

        public async Task ChangeDeviceState(string deviceState)
        {
            await SetReportedPropertyAsync(DeviceStatePropertyName, deviceState);
            Logger.LogInfo("Device {0} in {1} state", DeviceId, deviceState);
        }
        /// <summary>
        /// Activates/Deactivates the sending of the additional telemetry data "External Temperature"
        /// </summary>
        /// <param name="active"></param>
        public void DiagnosticTelemetry(bool active)
        {
            var remoteMonitorTelemetry = (RemoteMonitorTelemetry)TelemetryController;
            remoteMonitorTelemetry.ActivateExternalTemperature = active;
            string externalTempActive = active ? "on" : "off";
            Logger.LogInfo("Device {0}: External Temperature: {1}", DeviceId, externalTempActive);
        }
        #endregion Command-Messages

        #region Command-Methods

        public async Task<MethodResponse> OnInitiateFirmwareUpdate(MethodRequest methodRequest, object userContext)
        {
            if (_deviceManagementTask != null && !_deviceManagementTask.IsCompleted)
            {
                return await Task.FromResult(BuildMethodRespose(new
                {
                    Message = "Device is busy"
                }, 409));
            }

            try
            {
                var operation = new FirmwareUpdate(methodRequest);
                _deviceManagementTask = operation.Run(Transport).ContinueWith(async task =>
                {
                    // after firmware completed, we reset telemetry for demo purpose
                    var telemetry = TelemetryController as ITelemetryWithTemperatureMeanValue;
                    if (telemetry != null)
                    {
                        //Default value for temperature
                        telemetry.TemperatureMeanValue = 34.5;
                    }

                    await UpdateReportedTemperatureMeanValue();
                });

                return await Task.FromResult(BuildMethodRespose(new
                {
                    Message = "FirmwareUpdate accepted",
                    Uri = operation.Uri
                }));
            }
            catch (Exception ex)
            {
                return await Task.FromResult(BuildMethodRespose(new
                {
                    Message = ex.Message
                }, 400));
            }
        }

        public async Task<MethodResponse> OnReboot(MethodRequest methodRequest, object userContext)
        {
            //No waiting until reboot is completed
            var task = RebootAsync();

            return await Task.FromResult(BuildMethodRespose(new
            {
                Message = "Reboot accepted"
            }));
        }

        private async Task RebootAsync()
        {
            await SetReportedPropertyAsync("Method.Reboot", null);
            var watch = Stopwatch.StartNew();

            await SetReportedPropertyAsync(new Dictionary<string, dynamic>
            {
                { "Device.LastRebootTime", DateTime.UtcNow.ToString(CultureInfo.InvariantCulture) },
                { "Method.Reboot.Status", "Running" },
                { "Method.Reboot.LastUpdate", DateTime.UtcNow.ToString(CultureInfo.InvariantCulture) }
            });

            await Task.Delay(TimeSpan.FromSeconds(20));
            await SetReportedPropertyAsync(new Dictionary<string, dynamic>
            {
                { StartupTimePropertyName, DateTime.UtcNow.ToString(CultureInfo.InvariantCulture) },
                { "Method.Reboot.Status", "Complete" },
                { "Method.Reboot.LastUpdate", DateTime.UtcNow.ToString(CultureInfo.InvariantCulture) },
                { "Method.Reboot.Duration-s", (int)watch.Elapsed.TotalSeconds }
            });
        }

        public async Task<MethodResponse> OnFactoryReset(MethodRequest methodRequest, object userContext)
        {
            //No waiting until reboot is completed
            var task = FactoryResetAsync();

            return await Task.FromResult(BuildMethodRespose(new
            {
                Message = "FactoryReset accepted"
            }));
        }

        private async Task FactoryResetAsync()
        {
            await SetReportedPropertyAsync("Method.FactoryReset", null);
            var watch = Stopwatch.StartNew();

            await SetReportedPropertyAsync(new Dictionary<string, dynamic>
            {
                { "Device.LastFactoryResetTime", DateTime.UtcNow.ToString(CultureInfo.InvariantCulture) },
                { "Method.FactoryReset.Status", "Running" },
                { "Method.FactoryReset.LastUpdate", DateTime.UtcNow.ToString(CultureInfo.InvariantCulture) }
            });

            await Task.Delay(TimeSpan.FromSeconds(10));

            var factoryResetSupport = TelemetryController as ITelemetryFactoryResetSupport;
            if (factoryResetSupport != null)
            {
                factoryResetSupport.FactoryReset();
                await UpdateReportedTemperatureMeanValue();
                await UpdateReportedTelemetryInterval();
            }

            await SetReportedPropertyAsync(new Dictionary<string, dynamic>
            {
                { StartupTimePropertyName, DateTime.UtcNow.ToString(CultureInfo.InvariantCulture) },
                { FirmwareVersionPropertyName, "1.0" },
                { "Method.FactoryReset.Status", "Complete" },
                { "Method.FactoryReset.LastUpdate", DateTime.UtcNow.ToString(CultureInfo.InvariantCulture) },
                { "Method.FactoryReset.Duration-s", (int)watch.Elapsed.TotalSeconds }
            });
        }

        private MethodResponse BuildMethodRespose(string responseInJSON, int status = 200)
        {
            return new MethodResponse(Encoding.UTF8.GetBytes(responseInJSON), status);
        }

        private MethodResponse BuildMethodRespose(object response, int status = 200)
        {
            return BuildMethodRespose(JsonConvert.SerializeObject(response), status);
        }


        #endregion Command-Methods


        #region Twin's desired properties update
        private async Task OnTemperatureMeanValueUpdate(object value)
        {
            // When set the temperature we clean up the firmware update status for demo purpose.
            var clear = new TwinCollection();
            clear.Set(FirmwareUpdate.ReportPrefix, null);
            await Transport.UpdateReportedPropertiesAsync(clear);

            var telemetry = TelemetryController as ITelemetryWithTemperatureMeanValue;
            if (telemetry != null)
            {
                telemetry.TemperatureMeanValue = Convert.ToDouble(value);
            }

            await UpdateReportedTemperatureMeanValue();
        }

        private async Task UpdateReportedTemperatureMeanValue()
        {
            var telemetry = TelemetryController as ITelemetryWithTemperatureMeanValue;
            if (telemetry != null)
            {
                await SetReportedPropertyAsync(TemperatureMeanValuePropertyName, telemetry.TemperatureMeanValue);
            }
        }

        private async Task OnTelemetryIntervalUpdate(object value)
        {
            var telemetry = TelemetryController as ITelemetryWithInterval;
            if (telemetry != null)
            {
                telemetry.TelemetryIntervalInSeconds = Convert.ToUInt32(value);
            }

            await UpdateReportedTelemetryInterval();
        }

        private async Task UpdateReportedTelemetryInterval()
        {
            var telemetry = TelemetryController as ITelemetryWithInterval;
            if (telemetry != null)
            {
                await SetReportedPropertyAsync(TelemetryIntervalPropertyName, telemetry.TelemetryIntervalInSeconds);
            }
        } 
        #endregion Twin's desired properties update
    }
}
