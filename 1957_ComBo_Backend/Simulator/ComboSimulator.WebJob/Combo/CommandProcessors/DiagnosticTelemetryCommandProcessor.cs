﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComboSimulator.WebJob.Combo.Devices;
using ComboSimulator.WebJob.SimulatorCore.CommandProcessors;
using ComboSimulator.WebJob.SimulatorCore.Transport;
using XWS.ComBo.Common.Helpers;
using XWS.ComBo.Modules.Domain.Commands;

namespace ComboSimulator.WebJob.Combo.CommandProcessors
{
    /// <summary>
    /// Command processor to handle activating external temperature
    /// </summary>
    public class DiagnosticTelemetryCommandProcessor : CommandProcessor
    {
        private const string DIAGNOSTIC_TELEMETRY = "DiagnosticTelemetry";

        public DiagnosticTelemetryCommandProcessor(ComboDevice device)
            : base(device)
        {

        }

        public override async Task<CommandProcessingResult> HandleCommandAsync(DeserializableCommand deserializableCommand)
        {
            if (deserializableCommand.CommandName == DIAGNOSTIC_TELEMETRY)
            {
                CommandHistory commandHistory = deserializableCommand.CommandHistory;

                try
                {
                    var device = Device as ComboDevice;
                    if (device != null)
                    {
                        dynamic parameters = commandHistory.CommandParameters;
                        if (parameters != null)
                        {
                            dynamic activeAsDynamic =
                                ReflectionHelper.GetNamedPropertyValue(
                                    parameters,
                                    "Active",
                                    usesCaseSensitivePropertyNameMatch: true,
                                    exceptionThrownIfNoMatch: true);

                            if (activeAsDynamic != null)
                            {
                                var active = Convert.ToBoolean(activeAsDynamic.ToString());

                                if (active != null)
                                {
                                    device.DiagnosticTelemetry(active);
                                    return CommandProcessingResult.Success;
                                }
                                // Active is not a boolean.
                                return CommandProcessingResult.CannotComplete;
                            }
                            // Active is a null reference.
                            return CommandProcessingResult.CannotComplete;
                        }
                        // parameters is a null reference.
                        return CommandProcessingResult.CannotComplete;
                    }
                }
                catch (Exception)
                {
                    return CommandProcessingResult.RetryLater;
                }
            }
            else if (NextCommandProcessor != null)
            {
                return await NextCommandProcessor.HandleCommandAsync(deserializableCommand);
            }

            return CommandProcessingResult.CannotComplete;
        }
    }
}
