﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComboSimulator.WebJob.Combo.Devices;
using ComboSimulator.WebJob.SimulatorCore.CommandProcessors;
using ComboSimulator.WebJob.SimulatorCore.Transport;
using XWS.ComBo.Common.Helpers;
using XWS.ComBo.Modules.Domain.Commands;

namespace ComboSimulator.WebJob.Combo.CommandProcessors
{
    /// <summary>
    /// Command processor to handle the change in device state.
    /// Currently this just changes the DeviceState string on the device.
    /// </summary>
    public class ChangeDeviceStateCommandProcessor : CommandProcessor
    {
        private const string CHANGE_DEVICE_STATE = "ChangeDeviceState";

        public ChangeDeviceStateCommandProcessor(ComboDevice device)
            : base(device)
        {

        }

        public override async Task<CommandProcessingResult> HandleCommandAsync(DeserializableCommand deserializableCommand)
        {
            if (deserializableCommand.CommandName == CHANGE_DEVICE_STATE)
            {
                CommandHistory commandHistory = deserializableCommand.CommandHistory;

                try
                {
                    var device = Device as ComboDevice;
                    if (device != null)
                    {
                        dynamic parameters = commandHistory.CommandParameters;
                        if (parameters != null)
                        {
                            dynamic deviceState = ReflectionHelper.GetNamedPropertyValue(
                                parameters,
                                "DeviceState",
                                usesCaseSensitivePropertyNameMatch: true,
                                exceptionThrownIfNoMatch: true);

                            if (deviceState != null)
                            {
                                await device.ChangeDeviceState(deviceState.ToString());

                                return CommandProcessingResult.Success;
                            }
                            // DeviceState is a null reference.
                            return CommandProcessingResult.CannotComplete;
                        }
                        // parameters is a null reference.
                        return CommandProcessingResult.CannotComplete;
                    }
                    // Unsupported Device type.
                    return CommandProcessingResult.CannotComplete;
                }
                catch (Exception)
                {
                    return CommandProcessingResult.RetryLater;
                }
            }
            else if (NextCommandProcessor != null)
            {
                return await NextCommandProcessor.HandleCommandAsync(deserializableCommand);
            }

            return CommandProcessingResult.CannotComplete;
        }
    }
}
