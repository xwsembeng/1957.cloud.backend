﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComboSimulator.WebJob.Combo.Devices;
using ComboSimulator.WebJob.SimulatorCore.CommandProcessors;
using ComboSimulator.WebJob.SimulatorCore.Transport;

namespace ComboSimulator.WebJob.Combo.CommandProcessors
{
    /// <summary>
    /// Command processor to stop sending telemetry data
    /// </summary>
    public class StopCommandProcessor : CommandProcessor
    {
        private const string STOP_TELEMETRY = "StopTelemetry";
        public StopCommandProcessor(ComboDevice device)
            : base(device)
        {

        }
        public override async Task<CommandProcessingResult> HandleCommandAsync(DeserializableCommand deserializableCommand)
        {
            if (deserializableCommand.CommandName == STOP_TELEMETRY)
            {
                try
                {
                    var device = Device as ComboDevice;
                    if (device != null)
                    {
                        device.StopTelemetryData();
                        return CommandProcessingResult.Success;
                    }
                    // Unsupported Device type.
                    return CommandProcessingResult.CannotComplete;

                }
                catch (Exception)
                {
                    return CommandProcessingResult.RetryLater;
                }
            }
            else if (NextCommandProcessor != null)
            {
                return await NextCommandProcessor.HandleCommandAsync(deserializableCommand);
            }

            return CommandProcessingResult.CannotComplete;
        }
    }
}
