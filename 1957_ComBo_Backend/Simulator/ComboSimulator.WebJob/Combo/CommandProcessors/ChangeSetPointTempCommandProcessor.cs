﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComboSimulator.WebJob.Combo.Devices;
using ComboSimulator.WebJob.SimulatorCore.CommandProcessors;
using ComboSimulator.WebJob.SimulatorCore.Transport;
using XWS.ComBo.Common.Helpers;
using XWS.ComBo.Modules.Domain.Commands;

namespace ComboSimulator.WebJob.Combo.CommandProcessors
{
    /// <summary>
    /// Command processor that changes the temperature range the device sends
    /// </summary>
    public class ChangeSetPointTempCommandProcessor : CommandProcessor
    {
        private const string CHANGE_SET_POINT_TEMP = "ChangeSetPointTemp";

        public ChangeSetPointTempCommandProcessor(ComboDevice device)
            : base(device)
        {

        }

        public override async Task<CommandProcessingResult> HandleCommandAsync(DeserializableCommand deserializableCommand)
        {
            if (deserializableCommand.CommandName == CHANGE_SET_POINT_TEMP)
            {
                CommandHistory commandHistory = deserializableCommand.CommandHistory;

                try
                {
                    var device = Device as ComboDevice;
                    if (device != null)
                    {
                        dynamic parameters = commandHistory.CommandParameters;
                        if (parameters != null)
                        {
                            dynamic setPointTempDynamic = ReflectionHelper.GetNamedPropertyValue(
                                parameters,
                                "SetPointTemp",
                                usesCaseSensitivePropertyNameMatch: true,
                                exceptionThrownIfNoMatch: true);

                            if (setPointTempDynamic != null)
                            {
                                double setPointTemp;
                                if (Double.TryParse(setPointTempDynamic.ToString(), out setPointTemp))
                                {
                                    device.ChangeSetPointTemp(setPointTemp);

                                    return CommandProcessingResult.Success;
                                }
                                // SetPointTemp cannot be parsed as a double.
                                return CommandProcessingResult.CannotComplete;
                            }
                            // setPointTempDynamic is a null reference.
                            return CommandProcessingResult.CannotComplete;
                        }
                        // parameters is a null reference.
                        return CommandProcessingResult.CannotComplete;
                    }
                    // Unsupported Device type.
                    return CommandProcessingResult.CannotComplete;
                }
                catch (Exception)
                {
                    return CommandProcessingResult.RetryLater;
                }
            }
            else if (NextCommandProcessor != null)
            {
                return await NextCommandProcessor.HandleCommandAsync(deserializableCommand);
            }

            return CommandProcessingResult.CannotComplete;
        }
    }
}
