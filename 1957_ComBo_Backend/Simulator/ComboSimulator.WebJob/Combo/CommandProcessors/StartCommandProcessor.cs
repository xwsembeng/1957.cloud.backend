﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComboSimulator.WebJob.Combo.Devices;
using ComboSimulator.WebJob.SimulatorCore.CommandProcessors;
using ComboSimulator.WebJob.SimulatorCore.Transport;

namespace ComboSimulator.WebJob.Combo.CommandProcessors
{
    /// <summary>
    /// Command processor to start sending telemetry data
    /// </summary>
    public class StartCommandProcessor : CommandProcessor
    {
        private const string START_TELEMETRY = "StartTelemetry";

        public StartCommandProcessor(ComboDevice device)
            : base(device)
        {

        }

        public override async Task<CommandProcessingResult> HandleCommandAsync(DeserializableCommand deserializableCommand)
        {
            if (deserializableCommand.CommandName == START_TELEMETRY)
            {
                try
                {
                    var device = Device as ComboDevice;
                    if (device != null)
                    {
                        device.StartTelemetryData();
                        return CommandProcessingResult.Success;
                    }
                    // Unsupported Device type.
                    return CommandProcessingResult.CannotComplete;
                }
                catch (Exception)
                {
                    return CommandProcessingResult.RetryLater;
                }

            }
            else if (NextCommandProcessor != null)
            {
                return await NextCommandProcessor.HandleCommandAsync(deserializableCommand);
            }

            return CommandProcessingResult.CannotComplete;
        }
    }
}
