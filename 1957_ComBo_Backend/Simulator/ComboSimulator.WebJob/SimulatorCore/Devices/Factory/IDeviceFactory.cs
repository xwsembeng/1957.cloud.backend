﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComboSimulator.WebJob.SimulatorCore.Logging;
using ComboSimulator.WebJob.SimulatorCore.Telemetry.Factory;
using ComboSimulator.WebJob.SimulatorCore.Transport.Factory;
using XWS.ComBo.Modules.Domain;

namespace ComboSimulator.WebJob.SimulatorCore.Devices.Factory
{
    public interface IDeviceFactory
    {
        IDevice CreateDevice(ILogger logger, ITransportFactory transportFactory,
            ITelemetryFactory telemetryFactory, InitialDeviceConfig config);
    }
}
