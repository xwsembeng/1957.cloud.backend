﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ComboSimulator.WebJob.SimulatorCore.Telemetry;
using Microsoft.Azure.Devices.Shared;
using XWS.ComBo.Modules.Domain;
using XWS.ComBo.Modules.Domain.Commands;

namespace ComboSimulator.WebJob.SimulatorCore.Devices
{
    /// <summary>
    /// Represents a simulated device
    /// </summary>
    public interface IDevice
    {
        List<ITelemetry> TelemetryEvents { get; }
        string DeviceId { get; set; }
        string HostName { get; set; }
        string PrimaryAuthKey { get; set; }
        //Twin Twin { get; set; }
        List<Command> Commands { get; set; }

        Twin Twin { get; set; }
        void Init(InitialDeviceConfig config);
        //Task SendDeviceInfo();

        /// <summary>
        /// Generates a DeviceInfo packet for a simulated device to send over the wire
        /// </summary>
        /// <returns></returns>
        DeviceModel GetDeviceInfo();

        /// <summary>
        /// Starts the send event loop and runs the receive loop in the background
        /// to listen for commands that are sent to the device
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        Task StartAsync(CancellationToken token);
    }
}
