﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ComboSimulator.WebJob.Combo.Telemetry;
using ComboSimulator.WebJob.SimulatorCore.CommandProcessors;
using ComboSimulator.WebJob.SimulatorCore.Logging;
using ComboSimulator.WebJob.SimulatorCore.Telemetry;
using ComboSimulator.WebJob.SimulatorCore.Telemetry.Factory;
using ComboSimulator.WebJob.SimulatorCore.Transport;
using ComboSimulator.WebJob.SimulatorCore.Transport.Factory;
using Microsoft.Azure.Devices.Client;
using Microsoft.Azure.Devices.Common.Exceptions;
using Microsoft.Azure.Devices.Shared;
using XWS.ComBo.Common.Extensions;
using XWS.ComBo.Common.Factory;
using XWS.ComBo.Common.Helpers;
using XWS.ComBo.Modules.Domain;
using XWS.ComBo.Modules.Domain.Commands;
using XWS.ComBo.Modules.Domain.Enums;

namespace ComboSimulator.WebJob.SimulatorCore.Devices
{
    /// <summary>
    /// Simulates a single IoT device that sends and recieves data from a transport
    /// </summary>
    public class DeviceBase : IDevice
    {

        #region Hard coded flattended device twin properties 

        public const string DeviceStatePropertyName = "Device.DeviceState";
        public const string StartupTimePropertyName = "Device.StartupTime";
        public const string FirmwareVersionPropertyName = "System.FirmwareVersion";
        public const string TemperatureMeanValuePropertyName = "Config.TemperatureMeanValue";
        public const string TelemetryIntervalPropertyName = "Config.TelemetryInterval";
        public const string LastDesiredPropertyChangePropertyName = "Device.LastDesiredPropertyChange";

        #endregion Hard coded flattended device twin properties 


        protected readonly ILogger Logger;
        protected readonly ITransportFactory TransportFactory;
        protected readonly ITelemetryFactory TelemetryFactory;
        protected ITransport Transport;
        /// <summary>
        /// The starting command processor of all available command processors
        /// </summary>
        protected CommandProcessor RootCommandProcessor;

        public string DeviceId
        {
            get { return Twin.DeviceId; }
            set { Twin.DeviceId = value; }
        }

        public string HostName { get; set; }
        public string PrimaryAuthKey { get; set; }

        public Twin Twin { get; set; }

        public List<Command> Commands { get; set; }

        public List<XWS.ComBo.Modules.Domain.Telemetry> Telemetry { get; set; }

        /// <summary>
        /// The different telemetry types of the simulator
        /// </summary>
        public List<ITelemetry> TelemetryEvents { get;}

        /// <summary>
        /// Contains the update handlers for updating reported properties by desired properties
        /// Key: Hard coded flattended property name of the device
        /// Value: Update handler for updating the reported property
        /// </summary>
        protected Dictionary<string, Func<object, Task>> DesiredPropertyUpdateHandlers = new Dictionary<string, Func<object, Task>>();

        /// <summary>
        /// Controls the events and logic for sending telemetry data to the cloud
        /// </summary>
        protected object TelemetryController;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger">Logger where this device will log information to</param>
        /// <param name="transportFactory">Factory which creates the transport where the device will send and receive data to/from</param>
        /// <param name="telemetryFactory">Factory which populates the devices the different telemetry events or logic</param>
        public DeviceBase(ILogger logger, ITransportFactory transportFactory, ITelemetryFactory telemetryFactory)
        {
            Logger = logger;
            TransportFactory = transportFactory;
            TelemetryFactory = telemetryFactory;
            TelemetryEvents = new List<ITelemetry>();
        }

        public void Init(InitialDeviceConfig config)
        {
            InitDeviceInfo(config);

            Transport = TransportFactory.CreateTransport(this);
            TelemetryController = TelemetryFactory.PopulateDeviceWithTelemetryEvents(this);

            InitCommandProcessors();
        }

        /// <summary>
        /// Builds up a set of commands supported by this device
        /// </summary>
        protected virtual void InitCommandProcessors()
        {
            var pingDeviceProcessor = new PingDeviceProcessor(this);
            RootCommandProcessor = pingDeviceProcessor;
        }

        //public virtual async Task SendDeviceInfo()
        //{
        //    Logger.LogInfo("Sending Device Info for device {0}...", DeviceId);
        //    await Transport.SendEventAsync(GetDeviceInfo());
        //}

        /// <summary>
        /// Generates a DeviceInfo packet for a simulated device to send over the wire
        /// </summary>
        /// <returns></returns>
        public virtual DeviceModel GetDeviceInfo()
        {
            DeviceModel device = DeviceCreatorHelper.BuildDeviceStructure(DeviceId, true);
            device.Twin = this.Twin;
            device.Commands = this.Commands?.Where(c => c.CommandDeliveryType == CommandDeliveryType.Message).ToList() ?? new List<Command>();
            device.Telemetry = this.Telemetry ?? new List<XWS.ComBo.Modules.Domain.Telemetry>();
            device.ObjectType = SampleDeviceFactory.OBJECT_TYPE_DEVICE_INFO;

            return device;
        }

        /// <summary>
        /// Starts the send event loop and runs the receive loop in the background
        /// to listen for commands that are sent to the device
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task StartAsync(CancellationToken token)
        {
            try
            {
                await InitializeAsync();

                var loopTasks = new List<Task>
                {
                    StartReceiveLoopAsync(token),
                    StartSendLoopAsync(token)
                };

                // Wait both the send and receive loops
                await Task.WhenAll(loopTasks.ToArray());
            }
            catch (Exception ex)
            {
                Logger.LogError($"Exception raise while starting device {DeviceId}: {ex}");
            }
            finally
            {
                // once the code makes it here the token has been canceled
                await Transport.CloseAsync();
            }
        }

        protected async Task SetReportedPropertyAsync(string name, dynamic value)
        {
            var collection = new TwinCollection();
            TwinCollectionExtension.Set(collection, name, value);
            await Transport.UpdateReportedPropertiesAsync(collection);
        }
        protected async Task SetReportedPropertyAsync(Dictionary<string, dynamic> pairs)
        {
            var collection = new TwinCollection();
            foreach (var pair in pairs)
            {
                TwinCollectionExtension.Set(collection, pair.Key, pair.Value);
            }
            await Transport.UpdateReportedPropertiesAsync(collection);
        }

        private void InitDeviceInfo(InitialDeviceConfig config)
        {
            DeviceModel initialDevice = SampleDeviceFactory.GetSampleSimulatedDevice(config.DeviceId);
            Twin = initialDevice.Twin;
            Commands = initialDevice.Commands ?? new List<Command>();
            Telemetry = initialDevice.Telemetry ?? new List<XWS.ComBo.Modules.Domain.Telemetry>();
            HostName = config.HostName;
            PrimaryAuthKey = config.Key;
        }
        /// <summary>
        /// Initializes the device to send and receive messages
        /// </summary>
        /// <returns></returns>
        private async Task InitializeAsync()
        {
            await Transport.OpenAsync();
            await SetupCallbacksAsync();

            var twin = await Transport.GetTwinAsync();
            await UpdateReportedPropertiesAsync(twin.Properties.Reported);
            await OnDesiredPropertyUpdate(twin.Properties.Desired, null);
        }
        /// <summary>
        /// Sets the callbacks for the direct methods of the device
        /// </summary>
        /// <returns></returns>
        private async Task SetupCallbacksAsync()
        {
            foreach (var method in Commands.Where(c => c.CommandDeliveryType == CommandDeliveryType.Method))
            {
                try
                {
                    var methodHandler = GetType().GetMethod(FormattableString.Invariant($"On{method.Name}"))
                        .CreateDelegate(typeof(MethodCallback), this) as MethodCallback;

                    await Transport.SetMethodHandlerAsync(method.Name, methodHandler);
                }
                catch (Exception ex)
                {
                    Logger.LogError(FormattableString.Invariant($"Exception raised while adding callback for method {method.Name} on device {DeviceId}: {ex.Message}"));
                }
            }

            await Transport.SetDesiredPropertyUpdateCallbackAsync(OnDesiredPropertyUpdate);
        }

        /// <summary>
        /// Adds the configurations to the reported properties
        /// </summary>
        /// <param name="patch"></param>
        protected void AddConfigs(TwinCollection patch)
        {
            var telemetryWithInterval = TelemetryController as ITelemetryWithInterval;
            if (telemetryWithInterval != null)
            {
                patch.Set(TelemetryIntervalPropertyName, telemetryWithInterval.TelemetryIntervalInSeconds);
            }

            var telemetryWithTemperatureMeanValue = TelemetryController as ITelemetryWithTemperatureMeanValue;
            if (telemetryWithTemperatureMeanValue != null)
            {
                patch.Set(TemperatureMeanValuePropertyName, telemetryWithTemperatureMeanValue.TemperatureMeanValue);
            }

            patch.Set(StartupTimePropertyName, DateTime.UtcNow.ToString(CultureInfo.CurrentCulture));
        }


        protected async Task UpdateReportedPropertiesAsync(TwinCollection reportedProperties)
        {
            var patch = new TwinCollection();
            SupportedMethodsHelper.CreateSupportedMethodsReport(patch, Commands, reportedProperties);
            AddConfigs(patch);

            // Update ReportedProperties to IoT Hub
            await Transport.UpdateReportedPropertiesAsync(patch);
        }

        /// <summary>
        /// Handler when changes of the desired property are triggered by the cloud
        /// </summary>
        /// <param name="desiredProperties"></param>
        /// <param name="userContext"></param>
        /// <returns></returns>
        private async Task OnDesiredPropertyUpdate(TwinCollection desiredProperties, object userContext)
        {
            await SetReportedPropertyAsync(LastDesiredPropertyChangePropertyName, desiredProperties.ToJson());
            Logger.LogInfo($"{DeviceId} received desired property update: {desiredProperties.ToJson()}");

            foreach (var pair in desiredProperties.AsEnumerableFlatten())
            {
                Func<object, Task> handler;
                if (DesiredPropertyUpdateHandlers.TryGetValue(pair.Key, out handler))
                {
                    try
                    {
                        await handler(pair.Value.Value.Value);
                        Logger.LogInfo($"Successfully called desired property update handler {handler.Method.Name} on {DeviceId}");
                    }
                    catch (Exception ex)
                    {
                        Logger.LogError($"Exception raised while processing desired property {pair.Key} change on device {DeviceId}: {ex.Message}");
                    }
                }
                else
                {
                    Logger.LogWarning($"Cannot find desired property update handler for {pair.Key} on {DeviceId}");
                }
            }
        }

        /// <summary>
        /// Iterates through the list of ITelemetry and fires off the events in a given event group before moving to the next.
        /// If RepeatEventListForever is true the device will continue to loop through each event group, if false
        /// once a single pass is made through all event groups the device will stop sending events
        /// </summary>
        /// <param name="token">Cancellation token to cancel out of the loop</param>
        /// <returns></returns>
        private async Task StartSendLoopAsync(CancellationToken token)
        {
            try
            {
                Logger.LogInfo("Booting device {0}...", DeviceId);

                do
                {
                    int currentTelemetryEvent = 0;

                    Logger.LogInfo("Starting events list for device {0}...", DeviceId);

                    while (currentTelemetryEvent < TelemetryEvents.Count && !token.IsCancellationRequested)
                    {
                        Logger.LogInfo("Device {0} starting ITelemetry {1}...", DeviceId, currentTelemetryEvent);

                        var telemetryEvent = TelemetryEvents[currentTelemetryEvent];

                        await telemetryEvent.SendEventsAsync(token, async (object eventData) =>
                        {
                            if (telemetryEvent is StartupTelemetry)
                            {
                                //Only send initial startup data if the device created time hasn't already been set before
                                if ((await Transport.GetTwinAsync()).Properties.Reported.Get("Device.CreatedTime") == null)
                                {
                                    Logger.LogInfo("Sending initial data for device {0}", DeviceId);
                                    await Transport.UpdateReportedPropertiesAsync((TwinCollection)eventData);
                                }
                                 
                            }
                            else
                                await Transport.SendEventAsync(eventData);
                        });

                        currentTelemetryEvent++;
                    }

                    Logger.LogInfo("Device {0} finished sending all events in list...", DeviceId);

                } while (!token.IsCancellationRequested);

                Logger.LogWarning("Device {0} sent all events and is shutting down send loop. (Set RepeatEventListForever = true on the device to loop forever.)", DeviceId);

            }
            catch (TaskCanceledException)
            {
                //do nothing if the task was cancelled
            }
            catch (Exception ex)
            {
                Logger.LogError($"Exception raised while starting device send loop {DeviceId}: {ex.Message}");
            }

            if (token.IsCancellationRequested)
            {
                Logger.LogInfo("********** Processing Device {0} has been cancelled - StartSendLoopAsync Ending. **********", DeviceId);
            }
        }


        /// <summary>
        /// Starts the loop that listens for message-commands (not methods) from the IoT Hub which are sent to this device
        /// </summary>
        /// <param name="token">Cancellation token that can stop the loop if needed</param>
        private async Task StartReceiveLoopAsync(CancellationToken token)
        {
            try
            {
                while (!token.IsCancellationRequested)
                {
                    DeserializableCommand command = null;
                    Exception exception = null;

                    // Pause before running through the receive loop
                    await Task.Delay(TimeSpan.FromSeconds(10), token);
                    Logger.LogInfo("Device {0} checking for commands...", DeviceId);

                    try
                    {
                        // Retrieve the message from the IoT Hub
                        command = await Transport.ReceiveAsync();

                        if (command == null)
                        {
                            continue;
                        }

                        CommandProcessingResult processingResult = await RootCommandProcessor.HandleCommandAsync(command);

                        switch (processingResult)
                        {
                            case CommandProcessingResult.CannotComplete:
                                await Transport.SignalRejectedCommand(command);
                                break;

                            case CommandProcessingResult.RetryLater:
                                await Transport.SignalAbandonedCommand(command);
                                break;

                            case CommandProcessingResult.Success:
                                await Transport.SignalCompletedCommand(command);
                                break;
                        }

                        Logger.LogInfo(
                            "Device: {1}{0}Command: {2}{0}Lock token: {3}{0}Result: {4}{0}",
                            Console.Out.NewLine,
                            DeviceId,
                            command.CommandName,
                            command.LockToken,
                            processingResult);
                    }
                    catch (IotHubException ex)
                    {
                        exception = ex;

                        Logger.LogInfo(
                            "Device: {1}{0}Command: {2}{0}Lock token: {3}{0}Error Type: {4}{0}Exception: {5}{0}",
                            Console.Out.NewLine,
                            DeviceId,
                            command?.CommandName,
                            command?.LockToken,
                            ex.IsTransient ? "Transient Error" : "Non-transient Error",
                            ex.ToString());
                    }
                    catch (Exception ex)
                    {
                        exception = ex;

                        Logger.LogInfo(
                            "Device: {1}{0}Command: {2}{0}Lock token: {3}{0}Exception: {4}{0}",
                            Console.Out.NewLine,
                            DeviceId,
                            command?.CommandName,
                            command?.LockToken,
                            ex.ToString());
                    }

                    if (command != null && exception != null)
                    {
                        await Transport.SignalAbandonedCommand(command);
                    }
                }
            }
            catch (TaskCanceledException)
            {
                //do nothing if the task was cancelled
            }
            catch (Exception ex)
            {
                Logger.LogError($"Exception raised while starting device receive loop {DeviceId}: {ex}");
            }

            Logger.LogInfo("********** Processing Device {0} has been cancelled - StartReceiveLoopAsync Ending. **********", DeviceId);
        }

    }
}
