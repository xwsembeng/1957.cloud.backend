﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComboSimulator.WebJob.SimulatorCore.Transport;

namespace ComboSimulator.WebJob.SimulatorCore.Devices.DMTasks
{

    enum DMTaskState
    {
        //DM = Device Management
        DM_NULL,
        DM_IDLE,

        //FU = Firmware Update
        FU_PENDING,
        FU_DOWNLOADING,
        FU_APPLYING,
        FU_REBOOTING
    }
    /// <summary>
    /// Represents the step a device management task runs through
    /// </summary>
    class DMTaskStep
    {
        public DMTaskState CurrentState { get; set; }
        public TimeSpan ExecuteTime { get; set; }
        public DMTaskState NextState { get; set; }
    }
    abstract class DMTaskBase
    {
        /// <summary>
        /// Processes the necessary tasks when entering the given state
        /// </summary>
        /// <param name="state">State of the process step</param>
        /// <param name="transport"></param>
        /// <returns></returns>
        protected abstract Task<bool> OnEnterStateProc(DMTaskState state, ITransport transport);
        /// <summary>
        /// Processes the necessary tasks after leaving the given state
        /// </summary>
        /// <param name="state">State of the process step</param>
        /// <param name="transport"></param>
        /// <returns></returns>
        protected abstract Task<bool> OnLeaveStateProc(DMTaskState state, ITransport transport);

        protected List<DMTaskStep> _dmTaskSteps = new List<DMTaskStep>();

        /// <summary>
        /// Runs the device management task and its different steps
        /// </summary>
        /// <param name="transport"></param>
        /// <returns></returns>
        public async Task Run(ITransport transport)
        {
            DMTaskState state = _dmTaskSteps.First().CurrentState;

            while (true)
            {
                if (!await OnEnterStateProc(state, transport))
                {
                    break;
                }

                var step = _dmTaskSteps.SingleOrDefault(dmTaskStep => dmTaskStep.CurrentState == state);
                if (step == null)
                {
                    break;
                }

                await Task.Delay(step.ExecuteTime);

                if (!await OnLeaveStateProc(state, transport))
                {
                    break;
                }

                state = step.NextState;
            }
        }
    }
}

