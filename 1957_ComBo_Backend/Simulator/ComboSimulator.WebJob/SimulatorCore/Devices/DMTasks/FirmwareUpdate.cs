﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ComboSimulator.WebJob.SimulatorCore.Transport;
using Microsoft.Azure.Devices.Client;
using Microsoft.Azure.Devices.Shared;
using Newtonsoft.Json;
using XWS.ComBo.Common.Extensions;

namespace ComboSimulator.WebJob.SimulatorCore.Devices.DMTasks
{
    class FirmwareUpdate : DMTaskBase
    {
        internal const string ReportPrefix = "Method.UpdateFirmware";
        private const string Status = "Status";
        private const string LastUpdate = "LastUpdate";
        private const string Duration = "Duration-s";
        private const string Running = "Running";
        private const string Failed = "Failed";
        private const string Complete = "Complete";

        private Stopwatch _masterWatch;
        private Stopwatch _stepWatch;

        public string Uri { get; private set; }
        private string FirmwareVersion { get; set; }

        public FirmwareUpdate(MethodRequest request)
        {
            var payload = JsonConvert.DeserializeObject<dynamic>(request.DataAsJson);

            var uri = (string)payload.FwPackageUri;
            if (string.IsNullOrWhiteSpace(uri))
            {
                throw new ArgumentException("Missing FwPackageUri");
            }

            Uri = uri;

            // State switch graph: pending -> downloading -> applying -> rebooting -> idle
            _dmTaskSteps = new List<DMTaskStep>
            {
                new DMTaskStep { CurrentState = DMTaskState.FU_PENDING, ExecuteTime = TimeSpan.Zero, NextState = DMTaskState.FU_DOWNLOADING },
                new DMTaskStep { CurrentState = DMTaskState.FU_DOWNLOADING, ExecuteTime = TimeSpan.FromSeconds(20), NextState = DMTaskState.FU_APPLYING },
                new DMTaskStep { CurrentState = DMTaskState.FU_APPLYING, ExecuteTime = TimeSpan.FromSeconds(20), NextState = DMTaskState.FU_REBOOTING },
                new DMTaskStep { CurrentState = DMTaskState.FU_REBOOTING, ExecuteTime = TimeSpan.FromSeconds(20), NextState = DMTaskState.DM_IDLE }
            };
        }

        protected override async Task<bool> OnEnterStateProc(DMTaskState state, ITransport transport)
        {
            bool succeed = true;
            var reportedProperties = new TwinCollection();

            switch (state)
            {
                case DMTaskState.FU_PENDING:
                    var clear = new TwinCollection();
                    clear.Set(ReportPrefix, null);
                    await transport.UpdateReportedPropertiesAsync(clear);

                    _masterWatch = Stopwatch.StartNew();
                    _stepWatch = Stopwatch.StartNew();
                    BuildReport(reportedProperties, Running);
                    break;

                case DMTaskState.FU_DOWNLOADING:
                    _stepWatch = Stopwatch.StartNew();

                    try
                    {
                        using (var client = new HttpClient())
                        {
                            FirmwareVersion = (await client.GetStringAsync(Uri)).Trim();
                        }
                    }
                    catch
                    {
                        succeed = false;
                    }

                    BuildReport(reportedProperties, "Download", succeed ? Running : Failed);
                    break;

                case DMTaskState.FU_APPLYING:
                    _stepWatch = Stopwatch.StartNew();
                    succeed = FirmwareVersion != "applyFail";
                    BuildReport(reportedProperties, "Applied", succeed ? Running : Failed);
                    break;

                case DMTaskState.FU_REBOOTING:
                    _stepWatch = Stopwatch.StartNew();
                    succeed = FirmwareVersion != "rebootFail";
                    BuildReport(reportedProperties, "Reboot", succeed ? Running : Failed);
                    break;

                case DMTaskState.DM_IDLE:
                    BuildReport(reportedProperties, Complete);

                    reportedProperties.Set(DeviceBase.StartupTimePropertyName, DateTime.UtcNow.ToString());
                    reportedProperties.Set(DeviceBase.FirmwareVersionPropertyName, FirmwareVersion);
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(state));
            }

            await transport.UpdateReportedPropertiesAsync(reportedProperties);

            return succeed;
        }

        protected override async Task<bool> OnLeaveStateProc(DMTaskState state, ITransport transport)
        {
            var reportedProperties = new TwinCollection();

            switch (state)
            {
                case DMTaskState.DM_IDLE:
                    reportedProperties = null;  // No report for leaving idle state
                    break;

                case DMTaskState.FU_PENDING:
                    reportedProperties = null;  // No report for leaving pending state
                    break;

                case DMTaskState.FU_DOWNLOADING:
                    BuildReport(reportedProperties, "Download", Complete);
                    break;

                case DMTaskState.FU_APPLYING:
                    BuildReport(reportedProperties, "Applied", Complete);
                    break;

                case DMTaskState.FU_REBOOTING:
                    BuildReport(reportedProperties, "Reboot", Complete);
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(state));
            }

            if (reportedProperties != null)
            {
                await transport.UpdateReportedPropertiesAsync(reportedProperties);
            }

            return true;
        }

        private void BuildReport(TwinCollection reportProperties, string status)
        {
            reportProperties.Set(FormattableString.Invariant($"{ReportPrefix}.{Status}"), status);
            reportProperties.Set(FormattableString.Invariant($"{ReportPrefix}.{LastUpdate}"), DateTime.UtcNow.ToString(CultureInfo.CurrentCulture));
            reportProperties.Set(FormattableString.Invariant($"{ReportPrefix}.{Duration}"), (int)_masterWatch.Elapsed.TotalSeconds);
        }

        private void BuildReport(TwinCollection reportedProperties, string stepName, string status)
        {
            reportedProperties.Set(FormattableString.Invariant($"{ReportPrefix}.{stepName}.{Status}"), status);
            reportedProperties.Set(FormattableString.Invariant($"{ReportPrefix}.{stepName}.{LastUpdate}"), DateTime.UtcNow.ToString(CultureInfo.CurrentCulture));
            reportedProperties.Set(FormattableString.Invariant($"{ReportPrefix}.{stepName}.{Duration}"), (int)_stepWatch.Elapsed.TotalSeconds);

            if (status == Failed)
            {
                BuildReport(reportedProperties, Failed);
            }
            else
            {
                BuildReport(reportedProperties, Running);
            }
        }
    }
}
