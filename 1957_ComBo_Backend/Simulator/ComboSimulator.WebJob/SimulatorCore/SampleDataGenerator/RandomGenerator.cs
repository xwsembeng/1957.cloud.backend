﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComboSimulator.WebJob.SimulatorCore.SampleDataGenerator
{
    public class RandomGenerator : IRandomGenerator
    {
        private static readonly Random Random = BuildRandomSource();

        public RandomGenerator()
        {
        }

        public double GetRandomDouble()
        {
            //Makes the generation of values thread save
            lock (Random)
            {
                return Random.NextDouble();
            }
        }

        private static Random BuildRandomSource()
        {
            return new Random(Guid.NewGuid().GetHashCode());
        }
    }
}
