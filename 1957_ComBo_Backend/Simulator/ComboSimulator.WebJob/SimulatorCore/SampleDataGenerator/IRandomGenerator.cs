﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComboSimulator.WebJob.SimulatorCore.SampleDataGenerator
{
    public interface IRandomGenerator
    {
        double GetRandomDouble();
    }
}
