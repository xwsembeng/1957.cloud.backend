﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Devices.Client;
using Microsoft.Azure.Devices.Shared;

namespace ComboSimulator.WebJob.SimulatorCore.Transport
{
    /// <summary>
    /// Interface to provide actions that can be performed against a cloud service such as IoT Hub
    /// </summary>
    public interface ITransport
    {
        /// <summary>
        /// Sends an event to the IoT Hub
        /// </summary>
        /// <param name="eventData"></param>
        /// <returns></returns>
        Task SendEventAsync(dynamic eventData);

        /// <summary>
        /// Sends an event to IoT Hub using the provided eventId GUID
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="eventData"></param>
        /// <returns></returns>
        Task SendEventAsync(Guid eventId, dynamic eventData);

        /// <summary>
        /// Adds the reported properties to update 
        /// </summary>
        /// <param name="reportedProperties"></param>
        /// <returns></returns>
        Task UpdateReportedPropertiesAsync(TwinCollection reportedProperties);

        /// <summary>
        /// Prepares the transport for sending messages
        /// </summary>
        /// <returns></returns>
        Task OpenAsync();

        /// <summary>
        /// Sets the handler for device methods
        /// </summary>
        /// <param name="methodName"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        Task SetMethodHandlerAsync(string methodName, MethodCallback callback);

        /// <summary>
        /// Sets the necessary handler when desired properties are changed by the cloud
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        Task SetDesiredPropertyUpdateCallbackAsync(DesiredPropertyUpdateCallback callback);

        Task<Twin> GetTwinAsync();

        /// <summary>
        /// Receives the messages from the cloud
        /// </summary>
        /// <returns></returns>
        Task<DeserializableCommand> ReceiveAsync();

        /// <summary>
        /// Signals that the command message is abandoned and processed at a later point of time
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        Task SignalAbandonedCommand(DeserializableCommand command);

        /// <summary>
        /// Signals that the command message is completed
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        Task SignalCompletedCommand(DeserializableCommand command);

        /// <summary>
        /// Signals that the command message is rejected
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        Task SignalRejectedCommand(DeserializableCommand command);

        /// <summary>
        /// Stops the sending and removes the device from the monitored state collection
        /// </summary>
        /// <returns></returns>
        Task CloseAsync();
    }
}
