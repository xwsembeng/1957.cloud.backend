﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ComboSimulator.WebJob.SimulatorCore.Devices;
using ComboSimulator.WebJob.SimulatorCore.Logging;
using Microsoft.Azure.Devices.Client;
using Microsoft.Azure.Devices.Shared;
using Newtonsoft.Json;
using XWS.BusinessPlatform.Core.Configurations;
using XWS.ComBo.Common.Helpers;
using XWS.ComBo.Modules.Domain;

namespace ComboSimulator.WebJob.SimulatorCore.Transport
{   
    /// <summary>
    /// The state of each device communicating with the cloud
    /// </summary>
    enum DeviceClientState
    {
        Up,
        Down
    }

    /// <summary>
    /// Provides actions that can be performed against a cloud service such as IoT Hub
    /// Workaround, because at the time of creation there is a issue with the IoT Hub Device SDK which causes random exceptions
    /// when updating reported properties => A workaround had to be implemented
    /// </summary>
    public class IoTHubWorkaroundTransport : ITransport
    {
        private readonly ILogger _logger;
        private readonly IConfigurationProvider _configurationProvider;
        private readonly IDevice _device;
        private readonly string _connectionString;
        private readonly TransportType _transportType;

        private Task _sendLoop;
        private CancellationTokenSource _cts;

        private DeviceClient _deviceClient;
        /// <summary>
        /// Handlers for device methods which are added at the preparing stage of the transport
        /// </summary>
        private Dictionary<string, MethodCallback> _savedMethodHandlers = new Dictionary<string, MethodCallback>();
        /// <summary>
        /// Handler for updating reported properties by desired properties
        /// </summary>
        private DesiredPropertyUpdateCallback _savedDesiredpropertyHandler = null;
        /// <summary>
        /// List with reported properties to update
        /// </summary>
        private LinkedList<TwinCollection> _reportedPropertiesList = new LinkedList<TwinCollection>();
        /// <summary>
        /// Messages to send
        /// </summary>
        private List<Message> _messages = new List<Message>();

        public IoTHubWorkaroundTransport(ILogger logger, IConfigurationProvider configurationProvider, IDevice device)
        {
            _logger = logger;
            _configurationProvider = configurationProvider;
            _device = device;
            _connectionString = IotHubConnectionStringBuilder.Create(
                _device.HostName,
                new DeviceAuthenticationWithRegistrySymmetricKey(
                    _device.DeviceId,
                    _device.PrimaryAuthKey)).ToString();
            //Environment variable to get the host url where the webjob is deployed
            var websiteHostName = Environment.GetEnvironmentVariable("WEBSITE_HOSTNAME");
            //Mqtt_Websocket_Only is not supported if the webjob isn't deployed to a webjobhost (MQTT does not support the rejection and the abondon of messages yet)
            //_transportType = websiteHostName == null ? TransportType.Mqtt : TransportType.Mqtt_WebSocket_Only;
            _transportType = TransportType.Amqp;
        }

        /// <summary>
        /// Prepares the transport for sending messages
        /// </summary>
        /// <returns></returns>
        public async Task OpenAsync()
        {
            if (string.IsNullOrWhiteSpace(_device.DeviceId))
            {
                throw new ArgumentException("DeviceID value cannot be missing, null, or whitespace");
            }

            _deviceClient = await CreateDeviceClient();

            _cts = new CancellationTokenSource();
            _sendLoop = SendLoop(_cts.Token);
        }
        /// <summary>
        /// Stops the sending and removes the device from the monitored state collection
        /// </summary>
        /// <returns></returns>
        public async Task CloseAsync()
        {
            if (_sendLoop != null)
            {
                _cts.Cancel();

                await _sendLoop;
                _sendLoop = null;

                _cts.Dispose();
                _cts = null;
            }

            await _deviceClient.CloseAsync();
            StateCollection<DeviceClientState>.Remove(_device.DeviceId);
        }

        /// <summary>
        /// Sends an event to the IoT Hub
        /// </summary>
        /// <param name="eventData"></param>
        /// <returns></returns>
        public async Task SendEventAsync(dynamic eventData)
        {
            var eventId = Guid.NewGuid();
            await SendEventAsync(eventId, eventData);
        }

        /// <summary>
        /// Sends an event to IoT Hub using the provided eventId GUID
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="eventData"></param>
        /// <returns></returns>
        public async Task SendEventAsync(Guid eventId, dynamic eventData)
        {
            /* Not necessary anymore because it is always telemetry data which is sent this way
             * (before it could be telemetry data or device info data)
            string objectType = this.GetObjectType(eventData);
            var objectTypePrefix = _configurationProvider.GetConfigurationSettingValue("ObjectTypePrefix");
            if (!string.IsNullOrWhiteSpace(objectType) && !string.IsNullOrEmpty(objectTypePrefix))
            {
                eventData.ObjectType = objectTypePrefix + objectType;
            }
            */

            var bytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(eventData));
            var message = new Message(bytes);
            message.Properties["EventId"] = eventId.ToString();

            lock (_messages)
            {
                _messages.Add(message);
            }

            await Task.FromResult(0);
        }
        /// <summary>
        /// Receives the messages from the cloud
        /// </summary>
        /// <returns></returns>
        public async Task<DeserializableCommand> ReceiveAsync()
        {
            Message message = await AzureRetryHelper.OperationWithBasicRetryAsync(
                async () =>
                {
                    try
                    {
                        return await _deviceClient.ReceiveAsync();
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"ReceiveAsync failed, device: {_device.DeviceId}, exception: {ex.Message}");
                        return null;
                    }
                });

            if (message != null)
            {
                return new DeserializableCommand(message);
            }

            return null;
        }
        /// <summary>
        /// Signals that the command message is abandoned and processed at a later point of time
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public async Task SignalAbandonedCommand(DeserializableCommand command)
        {
            if (command == null)
            {
                throw new ArgumentNullException("command");
            }

            Debug.Assert(
                !string.IsNullOrEmpty(command.LockToken),
                "command.LockToken is a null reference or empty string.");

            await AzureRetryHelper.OperationWithBasicRetryAsync(
                async () =>
                {
                    try
                    {
                        await _deviceClient.AbandonAsync(command.LockToken);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"Abandon Command failed, device: {_device.DeviceId}, exception: {ex.Message}");
                    }
                });
        }

        /// <summary>
        /// Signals that the command message is completed
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public async Task SignalCompletedCommand(DeserializableCommand command)
        {
            if (command == null)
            {
                throw new ArgumentNullException("command");
            }

            Debug.Assert(
                !string.IsNullOrEmpty(command.LockToken),
                "command.LockToken is a null reference or empty string.");

            await AzureRetryHelper.OperationWithBasicRetryAsync(
                async () =>
                {
                    try
                    {
                        await _deviceClient.CompleteAsync(command.LockToken);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"Complete Command failed, device: {_device.DeviceId}, exception: {ex.Message}");
                    }
                });
        }

        /// <summary>
        /// Signals that the command message is rejected
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public async Task SignalRejectedCommand(DeserializableCommand command)
        {
            if (command == null)
            {
                throw new ArgumentNullException("command");
            }

            Debug.Assert(
                !string.IsNullOrEmpty(command.LockToken),
                "command.LockToken is a null reference or empty string.");

            await AzureRetryHelper.OperationWithBasicRetryAsync(
                async () =>
                {
                    try
                    {
                        await _deviceClient.RejectAsync(command.LockToken);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"Reject Command failed, device: {_device.DeviceId}, exception: {ex.Message}");
                    }
                });
        }

        /// <summary>
        /// Adds the reported properties to update 
        /// </summary>
        /// <param name="reportedProperties"></param>
        /// <returns></returns>
        public async Task UpdateReportedPropertiesAsync(TwinCollection reportedProperties)
        {
            lock (_reportedPropertiesList)
            {
                _reportedPropertiesList.AddLast(reportedProperties);
            }
            await Task.FromResult(0);
        }

        public async Task<Twin> GetTwinAsync()
        {
            return await _deviceClient.GetTwinAsync();
        }



        #region Initialize device callbacks 
        /// <summary>
        /// Sets the handler for device methods
        /// </summary>
        /// <param name="methodName"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        public async Task SetMethodHandlerAsync(string methodName, MethodCallback callback)
        {
            await _deviceClient.SetMethodHandlerAsync(methodName, callback, null);

            if (!_savedMethodHandlers.ContainsKey(methodName))
            {
                _savedMethodHandlers.Add(methodName, callback);
            }
        }

        /// <summary>
        /// Sets the necessary handler when desired properties are changed by the cloud
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        public async Task SetDesiredPropertyUpdateCallbackAsync(DesiredPropertyUpdateCallback callback)
        {
            await _deviceClient.SetDesiredPropertyUpdateCallbackAsync(callback, null);
            _savedDesiredpropertyHandler = callback;
        }
        #endregion Initialize device callbacks 

        /// <summary>
        /// Gets the object type out of the event data
        /// </summary>
        /// <param name="eventData"></param>
        /// <returns></returns>
        private string GetObjectType(dynamic eventData)
        {
            if (eventData == null)
            {
                throw new ArgumentNullException("eventData");
            }

            var propertyInfo = eventData.GetType().GetProperty("ObjectType");
            if (propertyInfo == null)
            {
                return string.Empty;
            }

            var value = propertyInfo.GetValue(eventData, null);
            return value == null ? string.Empty : value.ToString();
        }

        private async Task<DeviceClient> CreateDeviceClient()
        {
            var deviceClient = DeviceClient.CreateFromConnectionString(_connectionString, _transportType);
            await deviceClient.OpenAsync();

            //Necessary if device client has to be recreated because of random exceptions
            //=> Method handlers don't have to be added again
            foreach (var handler in _savedMethodHandlers)
            {
                await deviceClient.SetMethodHandlerAsync(handler.Key, handler.Value, null);
            }
            //Necessary if device client has to be recreated because of random exceptions
            //=> Desired property handler don't have to be added again
            if (_savedDesiredpropertyHandler != null)
            {
                await deviceClient.SetDesiredPropertyUpdateCallbackAsync(_savedDesiredpropertyHandler, null);
            }

            return deviceClient;
        }
        /// <summary>
        /// Sends the necessary messages to the cloud
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        private async Task SendLoop(CancellationToken ct)
        {
            bool isDeviceClientAvailable = true;
            StateCollection<DeviceClientState>.Set(_device.DeviceId, DeviceClientState.Up);

            while (!ct.IsCancellationRequested)
            {
                await Task.Delay(TimeSpan.FromSeconds(1));

                // Re-create device client if it is not available (due to any exception)
                if (!isDeviceClientAvailable)
                {
                    try
                    {
                        _deviceClient?.Dispose();
                        _deviceClient = null;   // Avoid duplicated disposing the old device client, if the new one failed to be created

                        _deviceClient = await CreateDeviceClient();

                        _logger.LogInfo($"Transport opened for device {_device.DeviceId} with type {_transportType}");
                        isDeviceClientAvailable = true;
                        StateCollection<DeviceClientState>.Set(_device.DeviceId, DeviceClientState.Up);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"Exception raised while trying to open transport for device {_device.DeviceId}: {ex}");
                        continue;
                    }
                }

                if (_reportedPropertiesList.Any())
                {
                    try
                    {
                        await _deviceClient.UpdateReportedPropertiesAsync(_reportedPropertiesList.First.Value);
                        
                        lock (_reportedPropertiesList)
                        {
                            _reportedPropertiesList.RemoveFirst();
                        }
                        await SendReportedPropertiesChangedEventAsync();
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"Exception raised while device {_device.DeviceId} trying to update reported properties: {ex}");
                        isDeviceClientAvailable = false;
                        StateCollection<DeviceClientState>.Set(_device.DeviceId, DeviceClientState.Down);
                        continue;
                    }
                }

                if (_messages.Any())
                {
                    // Messages failed to be sent will be dropped
                    var sendingMessages = new List<Message>();
                    lock (_messages)
                    {
                        sendingMessages.AddRange(_messages);
                        _messages.Clear();
                    }

                    try
                    {
                        await _deviceClient.SendEventBatchAsync(sendingMessages);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"Exception raised while device {_device.DeviceId} trying to send events: {ex}");
                        isDeviceClientAvailable = false;
                        StateCollection<DeviceClientState>.Set(_device.DeviceId, DeviceClientState.Down);
                        continue;
                    }
                }
            }
        }
        /// <summary>
        /// Sends a notification message to the cloud 
        /// if reported properties are updated by the cloud
        /// </summary>
        /// <returns></returns>
        private async Task SendReportedPropertiesChangedEventAsync()
        {
            DeviceModel deviceModel = _device.GetDeviceInfo();
            //Overwrite the reported properties of the default twin with the updated reported properties
            //before sending the data over the wire
            deviceModel.Twin.Properties.Reported = (await GetTwinAsync()).Properties.Reported;
            var bytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(deviceModel));
            var reportedPropertiesMessage = new Message(bytes);
            reportedPropertiesMessage.Properties["EventId"] = Guid.NewGuid().ToString();
            await _deviceClient.SendEventAsync(reportedPropertiesMessage);
        }
    }
}
