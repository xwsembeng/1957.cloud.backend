﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Devices.Client;
using Newtonsoft.Json;
using XWS.ComBo.Modules.Domain.Commands;

namespace ComboSimulator.WebJob.SimulatorCore.Transport
{
    /// <summary>
    /// Wraps the byte array returned from the cloud so that it can be deserialized
    /// </summary>
    public class DeserializableCommand
    {
        private readonly CommandHistory _commandHistory;
        private readonly string _lockToken;

        public DeserializableCommand(Message message)
        {
            if (message == null)
            {
                throw new ArgumentNullException("message");
            }

            Debug.Assert(
                !string.IsNullOrEmpty(message.LockToken),
                "message.LockToken is a null reference or empty string.");
            _lockToken = message.LockToken;

            byte[] messageBytes = message.GetBytes(); // this needs to be saved if needed later, because it can only be read once from the original Message

            string jsonData = Encoding.UTF8.GetString(messageBytes);
            _commandHistory = JsonConvert.DeserializeObject<CommandHistory>(jsonData);
        }
        public string CommandName
        {
            get { return _commandHistory.Name; }
        }

           public CommandHistory CommandHistory
        {
            get { return _commandHistory; }
        }
        /// <summary>
        /// Unique identifier of a cloud-to-device-message
        /// </summary>
        public string LockToken
        {
            get
            {
                return _lockToken;
            }
        }
    }
}
