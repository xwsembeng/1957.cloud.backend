﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using XWS.Localization;

namespace XWS.ComBo.Exceptions
{
    [Serializable]
    public class UnsupportedCommandException : DeviceAdministrationExceptionBase
    {
        private string CommandName { get; }
        public UnsupportedCommandException(string deviceId, string commandName) : base(deviceId)
        {
            CommandName = commandName;
        }
        // protected constructor for deserialization
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected UnsupportedCommandException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }

            this.CommandName = info.GetString("CommandName");
        }
        public override string Message
        {
            get
            {
                return string.Format(
                    CultureInfo.CurrentCulture,
                    StaticResourceManager.GetString("Error.resx", "UnsupportedCommandExceptionMessage"),
                    DeviceId,
                    CommandName);
            }
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }

            info.AddValue("CommandName", CommandName);
            base.GetObjectData(info, context);
        }
    }
}
