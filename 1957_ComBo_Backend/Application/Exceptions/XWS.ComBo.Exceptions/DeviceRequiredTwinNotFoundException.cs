﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace XWS.ComBo.Exceptions
{
    /// <summary>
    /// Exception thrown when required device twin is not found.
    /// Note that this cannot inherit from the DeviceAdminExceptionBase as we 
    /// may not know the DeviceId in this case.
    /// </summary>
    [Serializable]
    public class DeviceRequiredTwinNotFoundException : Exception
    {
        public DeviceRequiredTwinNotFoundException(string message) : base(message)
        {
        }

        public DeviceRequiredTwinNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected DeviceRequiredTwinNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }

            base.GetObjectData(info, context);
        }
    }
}
