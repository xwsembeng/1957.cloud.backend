﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Core.Configurations;
using XWS.ComBo.Common.Extensions;
using XWS.ComBo.Common.Helpers;
using XWS.ComBo.Common.Interfaces;
using XWS.ComBo.Modules.Domain;
using XWS.ComBo.Modules.Repositories.Interfaces;
using XWS.ComBo.Modules.Services.Interfaces;

namespace XWS.ComBo.Modules.Services
{
    public class DeviceServiceWithIoTHubDM : DeviceService
    {
        private readonly IConfigurationProvider _configProvider;

        public DeviceServiceWithIoTHubDM(IConfigurationProvider configProvider, IDeviceRegistryRepository deviceRegistryRepository, IIotHubRepository iotHubRepository,
            IVirtualDeviceStorage virtualDeviceStorage, INameCacheService nameCacheService, ISecurityKeyGenerator securityKeyGenerator) :
            base(configProvider, deviceRegistryRepository, iotHubRepository, virtualDeviceStorage, nameCacheService, securityKeyGenerator)
        {
            _configProvider = configProvider;
        }

        /// <summary>
        /// Get the properties of the device twin
        /// </summary>
        /// <param name="deviceModel"></param>
        /// <returns></returns>
        public override IEnumerable<KeyValuePair<string, TwinCollectionExtension.TwinValue>> GetTwinProperties(DeviceModel deviceModel)
        {
            if (deviceModel == null)
            {
                throw new ArgumentNullException("device");
            }
            //Get tags
            IEnumerable<KeyValuePair<string, TwinCollectionExtension.TwinValue>> flattenedTwinTags = deviceModel.Twin.Tags
                .AsEnumerableFlatten("tags.").Where(tag => !tag.Key.IsReservedTwinName());

            //Get desired properties
            IEnumerable<KeyValuePair<string, TwinCollectionExtension.TwinValue>> flattenedTwinDesired = deviceModel.Twin.Properties.Desired
                .AsEnumerableFlatten("desired.").Where(desired => !desired.Key.IsReservedTwinName());

            //Get reported properties
            IEnumerable<KeyValuePair<string, TwinCollectionExtension.TwinValue>> flattenedTwinReported = deviceModel
                .Twin.Properties.Reported
                .AsEnumerableFlatten("reported.")
                .Where(reported => !reported.Key.IsReservedTwinName() &&
                                   !SupportedMethodsHelper.IsSupportedMethodProperty(reported.Key.Substring("reported.".Length)));

            IEnumerable<KeyValuePair<string, TwinCollectionExtension.TwinValue>> flattenedTwin = flattenedTwinTags
                .Concat(flattenedTwinDesired).Concat(flattenedTwinReported).OrderBy(pair => pair.Key);

            return flattenedTwin;

        }

    }
}
