﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Services;
using XWS.ComBo.Modules.Domain;
using XWS.ComBo.Modules.Repositories.Interfaces;
using XWS.ComBo.Modules.Services.Interfaces;

namespace XWS.ComBo.Modules.Services
{
    /// <summary>
    /// An IDeviceTelemetryLogic implementation that has business logic
    /// for Device telemetry-related functionality.
    /// </summary>
    public class DeviceTelemetryService : IDeviceTelemetryService
    {
        private readonly IDeviceTelemetryRepository _deviceTelemetryRepository;
    
        public DeviceTelemetryService(IDeviceTelemetryRepository deviceTelemetryRepository) 
        {
            _deviceTelemetryRepository = deviceTelemetryRepository;
        }

        /// <summary>
        /// Loads the most recent Device telemetry.
        /// </summary>
        /// <param name="deviceId">
        /// The ID of the Device for which telemetry should be returned.
        /// </param>
        /// <param name="telemetry">The different types of telemetry data</param>
        /// <param name="minTime">
        /// The minimum time of record of the telemetry that should be returned.
        /// </param>
        /// <returns>
        /// Telemetry for the Device specified by deviceId, inclusively since 
        /// minTime.
        /// </returns>
        public async Task<IEnumerable<DeviceTelemetryModel>> LoadLatestDeviceTelemetryAsync(
            string deviceId,
            IList<Telemetry> telemetry,
            DateTime minTime)
        {
            return await _deviceTelemetryRepository.LoadLatestDeviceTelemetryAsync(deviceId, telemetry, minTime);
        }


    }
}
