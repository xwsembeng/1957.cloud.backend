﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Runtime.Versioning;
using System.Security.Policy;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http.ModelBinding;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Shared;
using Newtonsoft.Json.Linq;
using XWS.BusinessPlatform.Core.Configurations;
using XWS.ComBo.Common.Extensions;
using XWS.ComBo.Common.Helpers;
using XWS.ComBo.Exceptions;
using XWS.ComBo.Modules.Domain;
using XWS.ComBo.Modules.Domain.FilterObjects;
using XWS.ComBo.Modules.Domain.FilterResult;
using XWS.ComBo.Modules.Repositories.Interfaces;
using XWS.ComBo.Modules.Services.Interfaces;
using XWS.ComBo.Common.Interfaces;
using XWS.ComBo.Modules.Domain.Commands;
using XWS.ComBo.Modules.Domain.Enums;
using XWS.RulesEngine;
using XWS.RulesEngine.Exceptions;
using XWS.RulesEngine.Rules;
using XWS.Localization;

namespace XWS.ComBo.Modules.Services
{
    public abstract class DeviceService : IDeviceService
    {
        private readonly IDeviceRegistryRepository _deviceRegistryRepository;
        private readonly IConfigurationProvider _configProvider;
        private readonly IIotHubRepository _iotHubRepository;
        private readonly IVirtualDeviceStorage _virtualDeviceStorage;
        private readonly INameCacheService _nameCacheService;
        private readonly ISecurityKeyGenerator _securityKeyGenerator;

        public DeviceService(IConfigurationProvider configProvider, IDeviceRegistryRepository deviceRegistryRepository, IIotHubRepository iotHubRepository,
            IVirtualDeviceStorage virtualDeviceStorage, INameCacheService nameCacheService, ISecurityKeyGenerator securityKeyGenerator)
        {
            _configProvider = configProvider;
            _deviceRegistryRepository = deviceRegistryRepository;
            _iotHubRepository = iotHubRepository;
            _virtualDeviceStorage = virtualDeviceStorage;
            _nameCacheService = nameCacheService;
            _securityKeyGenerator = securityKeyGenerator;
        }

        /// <summary>
        /// Get the properties of the device twin
        /// </summary>
        /// <param name="deviceModel"></param>
        /// <returns></returns>
        public abstract IEnumerable<KeyValuePair<string, TwinCollectionExtension.TwinValue>> GetTwinProperties(DeviceModel deviceModel);

        public async Task<ListDevicesFilterResult> GetDevicesAsync(ListDevicesFilter filter)
        {
            var listDevicesFilterResult = await _deviceRegistryRepository.GetDevicesAsync(filter);
           
            UpdateNameCache(listDevicesFilterResult.Results.Select(r => r.Twin));
            return listDevicesFilterResult;
        }

        /// <summary>
        /// Gets the devices whose device id is part of the given device id
        /// </summary>
        /// <param name="deviceId">The id of the device or a part of it</param>
        /// <param name="hubEnabledState">The state of the device</param>
        /// <returns></returns>
        public async Task<IList<DeviceModel>> GetDevicesAsync(string deviceId, HubEnabledState? hubEnabledState = null)
        {
            return await _deviceRegistryRepository.GetDevicesAsync(deviceId, hubEnabledState);
        }

        /// <summary>
        /// Adds a device to the Device Identity Store and Device Registry
        /// </summary>
        /// <param name="device">Device to add to the underlying repositories</param>
        /// <returns>Device created along with the device identity store keys</returns>
        public async Task<DeviceWithKeys> AddDeviceAsync(DeviceModel device)
        {
            await this.ValidateDevice(device);

            SecurityKeys generatedSecurityKeys = this._securityKeyGenerator.CreateRandomKeys();

            DeviceModel savedDevice = await this.AddDeviceToRepositoriesAsync(device, generatedSecurityKeys);
            return new DeviceWithKeys(savedDevice, generatedSecurityKeys);
        }

        /// <summary>
        /// Updates the device in the device registry with the exact device provided in this call.
        /// NOTE: The device provided here should represent the entire device that will be
        /// serialized into the device registry.
        /// </summary>
        /// <param name="device">Device to update in the device registry</param>
        /// <returns>Device that was saved into the device registry</returns>
        public async Task<DeviceModel> UpdateDeviceAsync(DeviceModel device)
        {
            return await _deviceRegistryRepository.UpdateDeviceAsync(device);
        }
        /// <summary>
        /// Updates the device inside the cloud with the device info sent by the device
        /// </summary>
        /// <param name="deviceModel"></param>
        /// <returns></returns>
        public async Task<DeviceModel> UpdateDeviceByDeviceInfoPacketAsync(DeviceModel deviceModel)
        {
            if (deviceModel == null)
            {
                throw new ArgumentNullException("deviceModel");
            }

            // Get original device document
            DeviceModel existingDevice = await this.GetDeviceAsync(deviceModel.Twin.DeviceId);
            SupportedMethodsHelper.AddSupportedMethodsFromReportedProperty(deviceModel, existingDevice.Twin);

            Twin iotHubTwin = deviceModel.Twin;
            if (existingDevice.Twin != null && iotHubTwin != null)
            {
                //Overwrite the reported properties of the device registry
                existingDevice.Twin.Properties.Reported = iotHubTwin.Properties.Reported;
            }

            // If there is Telemetry or Command objects from device, replace instead of merge
            if (deviceModel.Telemetry != null)
            {
                existingDevice.Telemetry = deviceModel.Telemetry;
            }
            if (deviceModel.Commands != null)
            {
                existingDevice.Commands = deviceModel.Commands;
            }

            return await _deviceRegistryRepository.UpdateDeviceAsync(existingDevice);
        }

        public async Task<DeviceModel> UpdateHubEnabledStateAsync(string deviceId, bool isEnabled)
        {
            // if an exception happens at this point pass it up the stack to handle it
            await _iotHubRepository.UpdateHubEnabledStateAsync(deviceId, isEnabled);

            DeviceModel registryDevice;
            try
            {
                registryDevice = await _deviceRegistryRepository.UpdateHubEnabledStateAsync(deviceId, isEnabled);
            }
            catch (Exception)
            {
                // This is a lazy attempt to revert the enabled status of the device in the IotHub.
                // If it fails the device status will still remain the same in the IotHub.
                // A more robust rollback may be needed in some scenarios.
                await _iotHubRepository.UpdateHubEnabledStateAsync(deviceId, !isEnabled);
                throw;

            }

            if (registryDevice == null || !registryDevice.IsSimulatedDevice)
                return registryDevice;

            //Add/Remove simulated devices from table storage
            return await this.AddOrRemoveSimulatedDevice(registryDevice, isEnabled);
        }

        /// <summary>
        /// Removes a device from the underlying repositories
        /// </summary>
        /// <param name="deviceId">ID of the device to remove</param>
        /// <returns></returns>
        public async Task RemoveDeviceAsync(string deviceId)
        {
            ExceptionDispatchInfo capturedException = null;
            Device iotHubDevice = await _iotHubRepository.GetIotHubDeviceAsync(deviceId);
            // if the device isn't already in the IotHub throw an exception and let the caller know
            if (iotHubDevice == null)
            {
                throw new DeviceNotRegisteredException(deviceId);
            }
            // Attempt to remove the device from the IotHub.  If this fails an exception will be thrown
            // and the remainder of the code not run, which is by design
            await _iotHubRepository.RemoveDeviceAsync(deviceId);

            try
            {
                await _deviceRegistryRepository.RemoveDeviceAsync(deviceId);
            }
            catch (Exception ex)
            {
                // if there is an exception while attempting to remove the device from the Device Registry
                // capture it so a rollback can be done on the Identity Registry
                capturedException = ExceptionDispatchInfo.Capture(ex);
            }

            if (capturedException == null)
            {
                try
                {
                    await _virtualDeviceStorage.RemoveDeviceAsync(deviceId);
                }
                catch (Exception ex)
                {
                    //if an exception occurs while attempting to remove the
                    //simulated device from table storage do not roll back the changes.
                    Trace.TraceError("Failed to remove simulated device : {0}", ex.Message);
                }

                //TODO: Implement when necessary
                //await _deviceRulesLogic.RemoveAllRulesForDeviceAsync(deviceId);
            }
            else
            {
                // The "rollback" is an attempt to add the device back in to the Identity Registry
                // It is assumed that if an exception has occured in the Device Registry, the device
                // is still in that store and this works to ensure that both repositories have the same
                // devices registered
                // A more robust rollback may be needed in some scenarios.
                await _iotHubRepository.TryAddDeviceAsync(iotHubDevice);
                capturedException.Throw();
            }

        }

        public async Task<DeviceModel> GetDeviceAsync(string deviceId)
        {
            return await _deviceRegistryRepository.GetDeviceAsync(deviceId);
        }

        /// <summary>
        /// Retrieves the IoT Hub keys for the given device
        /// </summary>
        /// <param name="deviceId">ID of the device to retrieve</param>
        /// <returns>Primary and Secondary keys from the IoT Hub</returns>
        public async Task<SecurityKeys> GetDeviceKeysAsync(string deviceId)
        {
            return await _iotHubRepository.GetDeviceKeysAsync(deviceId);
        }

        /// <summary>
        /// Send a command to a device based on the provided device id
        /// </summary>
        /// <param name="deviceId">The Device's ID</param>
        /// <param name="command"></param>
        /// <param name="commandDeliveryType">The type of the command</param>
        /// <param name="parameters">The parameters to send</param>
        /// <returns></returns>
        public async Task SendCommandAsync(string deviceId, Command command, CommandDeliveryType commandDeliveryType, dynamic parameters)
        {
            DeviceModel device = await this.GetDeviceAsync(deviceId);

            if (device == null)
            {
                throw new DeviceNotRegisteredException(deviceId);
            }
            CommandParameter commandParameter = command.CommandParameters.FirstOrDefault();
            if(commandParameter != null)
                ValidateCommandParameters(commandParameter, parameters);

            await SendCommandAsyncWithDevice(device, command.Name, commandDeliveryType, parameters);
        }


        /// <summary>
        /// Adds the device properties (twin, commands etc.) to the name cache
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        public async Task AddToNameCache(string deviceId)
        {
            var device = await this.GetDeviceAsync(deviceId);
            var twin = device.Twin;

            await _nameCacheService.AddNameAsync(nameof(device.Twin.DeviceId));

            await _nameCacheService.AddShortNamesAsync(
                NameCacheEntityType.Tag,
                twin.Tags
                    .AsEnumerableFlatten()
                    .Select(pair => pair.Key));

            await _nameCacheService.AddShortNamesAsync(
                NameCacheEntityType.DesiredProperty,
                twin.Properties.Desired
                    .AsEnumerableFlatten()
                    .Select(pair => pair.Key));

            await _nameCacheService.AddShortNamesAsync(
                NameCacheEntityType.ReportedProperty,
                twin.Properties.Reported
                    .AsEnumerableFlatten()
                    .Select(pair => pair.Key)
                    .Where(name => !SupportedMethodsHelper.IsSupportedMethodProperty(name)));

            foreach (var command in device.Commands.Where(c => c.CommandDeliveryType == CommandDeliveryType.Method))
            {
                await _nameCacheService.AddMethodAsync(command);
            }
        }

        /// <summary>
        /// Extracts the location data of the devices 
        /// </summary>
        /// <param name="deviceModels"></param>
        /// <returns></returns>
        public IList<DeviceLocationModel> ExtractLocationData(IList<DeviceModel>deviceModels)
        {
            IList<DeviceLocationModel>deviceLocationModels = new List<DeviceLocationModel>();
            foreach (var deviceModel in deviceModels)
            {
                if (deviceModel.Twin == null)
                {
                    throw new DeviceRequiredTwinNotFoundException(
                        StaticResourceManager.GetString("Error", "DeviceRequiredTwinNotFoundMessage"));

                }
                double latitude;
                double longitude;

                try
                {
                    latitude = (double)deviceModel.Twin.Properties.Reported.Get("Device.Location.Latitude");
                    longitude = (double)deviceModel.Twin.Properties.Reported.Get("Device.Location.Longitude");
                }
                catch
                {
                    continue;
                }

                DeviceLocationModel deviceLocationModel = new DeviceLocationModel
                {
                    DeviceId = deviceModel.Twin.DeviceId,
                    HubEnabledState = deviceModel.Twin.Tags["HubEnabledState"],
                    Latitude = latitude,
                    Longitude = longitude,
                };
                deviceLocationModels.Add(deviceLocationModel);
            }

             return deviceLocationModels;
        }

        #region Private-Methods
        /// <summary>
        /// Updates the twin properties inside table storage
        /// </summary>
        /// <param name="twins"></param>
        private void UpdateNameCache(IEnumerable<Twin> twins)
        {
            // Reminder: None of the tasks updating the namecache need to be waited for completion

            var tags = twins.GetNameList(twin => twin.Tags);
            _nameCacheService.AddShortNamesAsync(NameCacheEntityType.Tag, tags);

            var desiredProperties = twins.GetNameList(twin => twin.Properties.Desired);
            _nameCacheService.AddShortNamesAsync(NameCacheEntityType.DesiredProperty, desiredProperties);

            var reportedProperties = twins.GetNameList(twin => twin.Properties.Reported)
                .Where(name => !SupportedMethodsHelper.IsSupportedMethodProperty(name));
            _nameCacheService.AddShortNamesAsync(NameCacheEntityType.ReportedProperty, reportedProperties);

            // No need to update supported Methods inside the DeviceList-table here, since it will not change during device running
        }

        /// <summary>
        /// Adds the given device and assigned keys to the underlying repositories
        /// </summary>
        /// <param name="device">Device to add to repositories</param>
        /// <param name="securityKeys">Keys to assign to the device</param>
        /// <returns>Device that was added to the device registry</returns>
        private async Task<DeviceModel> AddDeviceToRepositoriesAsync(DeviceModel device, SecurityKeys securityKeys)
        {
            DeviceModel registryRepositoryDevice;

            // if an exception happens at this point pass it up the stack to handle it
            // (Making this call first then the call against the Registry removes potential issues
            // with conflicting rollbacks if the operation happens to still be in progress.)
            await _iotHubRepository.AddDeviceAsync(device, securityKeys);

            try
            {
                registryRepositoryDevice = await _deviceRegistryRepository.AddDeviceAsync(device);
            }
            catch (Exception)
            {
                // This is a lazy attempt to remove the device from the Iot Hub.  If it fails
                // the device will still remain in the Iot Hub.  A more robust rollback may be needed
                // in some scenarios.
                await _iotHubRepository.TryRemoveDeviceAsync(device.Twin.DeviceId);
                throw;

            }

            //Create a device in table storage if it is a simulated type of device
            //and the document was stored correctly without an exception
            if (device.IsSimulatedDevice)
            {
                try
                {
                    await _virtualDeviceStorage.AddOrUpdateDeviceAsync(new InitialDeviceConfig
                    {
                        DeviceId = device.Twin.DeviceId,
                        HostName = _configProvider.GetConfigurationSettingValue("iotHub.HostName"),
                        Key = securityKeys.PrimaryKey
                    });
                }
                catch (Exception ex)
                {
                    //if we fail adding to table storage for the device simulator just continue
                    Trace.TraceError("Failed to add simulated device : {0}", ex.Message);
                }
            }

            return registryRepositoryDevice;
        }

      
        /// <summary>
        /// Adds or removes a simulated device from the table storage
        /// </summary>
        /// <param name="deviceModel">The device to add or remove</param>
        /// <param name="isEnabled">The flag which indicates the device-status</param>
        /// <returns></returns>
        private async Task<DeviceModel> AddOrRemoveSimulatedDevice(DeviceModel deviceModel, bool isEnabled)
        {
            var deviceId = deviceModel.Twin.DeviceId;
            if (isEnabled)
            {
                try
                {
                    var securityKeys = await this.GetDeviceKeysAsync(deviceId);
                    await _virtualDeviceStorage.AddOrUpdateDeviceAsync(new InitialDeviceConfig
                    {
                        DeviceId = deviceId,
                        HostName = _configProvider.GetConfigurationSettingValue("iotHub.HostName"),
                        Key = securityKeys.PrimaryKey
                    });
                }
                catch (Exception ex)
                {
                    //if we fail adding to table storage for the device simulator just continue
                    Trace.TraceError("Failed to add enabled device to simulated device storage. Device telemetry is expected not to be sent. : {0}", ex.Message);
                }
            }
            else
            {
                try
                {
                    await _virtualDeviceStorage.RemoveDeviceAsync(deviceId);
                }
                catch (Exception ex)
                {
                    //if an exception occurs while attempting to remove the
                    //simulated device from table storage do not roll back the changes.
                    Trace.TraceError("Failed to remove disabled device from simulated device store. Device will keep sending telemetry data. : {0}", ex.Message);
                }
            }

            return deviceModel;
        }

        /// <summary>
        /// Sends a command to the provided device and updates the command history of the device
        /// </summary>
        /// <param name="device">Device to send the command to</param>
        /// <param name="commandName">Name of the command to send</param>
        /// <param name="commandDeliveryType">The type of the command</param>
        /// <param name="parameters">Parameters to send with the command</param>
        /// <returns></returns>
        private async Task SendCommandAsyncWithDevice(DeviceModel device, string commandName, CommandDeliveryType commandDeliveryType, dynamic parameters)
        {
            if (device == null)
            {
                throw new ArgumentNullException("device");
            }

            var deviceId = device.Twin.DeviceId;
            if (device.Commands.FirstOrDefault(x => x.Name == commandName) == null)
            {
                throw new UnsupportedCommandException(deviceId, commandName);
            }

            var commandHistory = new CommandHistory(commandName, commandDeliveryType, parameters);

            if (device.CommandHistory == null)
            {
                device.CommandHistory = new List<CommandHistory>();
            }

            device.CommandHistory.Add(commandHistory);

            await _iotHubRepository.SendCommand(deviceId, commandHistory);
            await _deviceRegistryRepository.UpdateDeviceAsync(device);
        }

        #region Validation
        private async Task ValidateDevice(DeviceModel deviceModel)
        {
            var rulesCollection = new RulesCollection();
            string deviceId = deviceModel.Twin.DeviceId;
            //Validate DeviceId
            if (deviceId.Length < 3 || deviceId.Length > 128)
            {
                rulesCollection.AddRule(new CustomRule("",
                    StaticResourceManager.GetString("StatusMessage", "DeviceIdCharactersCount"),
                    "DeviceIdCharactersCount", false));
            }
            else if (Regex.IsMatch(deviceId, "[^a-zA-Z0-9-:.+%_#*?!(),=@;$']"))
            {
                rulesCollection.AddRule(new CustomRule("",
                    StaticResourceManager.GetString("StatusMessage", "DeviceIdCharactersAllowed"),
                    "DeviceIdCharactersAllowed", false));
            }
            else
            {
                // Check if device with this Id exists
                if (await this.GetDeviceAsync(deviceModel.Twin.DeviceId) != null)
                {
                    rulesCollection.AddRule(new CustomRule("",
                        StaticResourceManager.GetString("StatusMessage", "DeviceExists"), "DeviceExists", false));
                }
            }

            BrokenRulesCollection brokenRulesCollection = rulesCollection.GetBrokenRules();
            if (!brokenRulesCollection.IsEmpty)
            {
                throw new BrokenRulesException(brokenRulesCollection);
            }
        }

        /// <summary>
        /// Validates the command parameters
        /// </summary>
        /// <param name="commandParameter"></param>
        /// <param name="parameters"></param>
        private void ValidateCommandParameters(CommandParameter commandParameter, dynamic parameters)
        {
            var rulesCollection = new RulesCollection();

            //TODO: Validate multiple command parameters, validate by specific command
            if (parameters.GetType() == typeof(JObject))
            {
                object parameterValue = ((JObject)parameters).GetValue(commandParameter.Name).Value<JValue>().Value;
                if (parameterValue == null)
                {
                    rulesCollection.AddRule(new CustomRule("",
                        StaticResourceManager.GetString("StatusMessage", "CommandParameterIsNull"),
                        "CommandParameterIsNull", false));
                }
            }
            else
            {
                throw new ArgumentException("Parameter type not supported");
            }

            BrokenRulesCollection brokenRulesCollection = rulesCollection.GetBrokenRules();
            if (!brokenRulesCollection.IsEmpty)
            {
                throw new BrokenRulesException(brokenRulesCollection);
            }
        }
        #endregion Validation

        #endregion Private-Methods
    }
}
