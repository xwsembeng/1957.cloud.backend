﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.ComBo.Modules.Domain;
using XWS.ComBo.Modules.Domain.Commands;
using XWS.ComBo.Modules.Domain.Enums;

namespace XWS.ComBo.Modules.Services.Interfaces
{
    public interface INameCacheService
    {
        Task AddShortNamesAsync(NameCacheEntityType type, IEnumerable<string> shortNames);
        Task<bool> AddNameAsync(string name);

        /// <summary>
        /// Gets a list of names of the name cache
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        Task<IEnumerable<NameCacheEntity>> GetNameListAsync(NameCacheEntityType type);

        /// <summary>
        /// Adds the supported device methods to the name cache
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        Task<bool> AddMethodAsync(Command method);
    }
}
