﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.ComBo.Common.Extensions;
using XWS.ComBo.Modules.Domain;
using XWS.ComBo.Modules.Domain.Commands;
using XWS.ComBo.Modules.Domain.Enums;
using XWS.ComBo.Modules.Domain.FilterObjects;
using XWS.ComBo.Modules.Domain.FilterResult;

namespace XWS.ComBo.Modules.Services.Interfaces
{
    public interface IDeviceService
    {
        Task<ListDevicesFilterResult> GetDevicesAsync(ListDevicesFilter filter);

        /// <summary>
        /// Removes a device from the underlying repositories
        /// </summary>
        /// <param name="deviceId">ID of the device to remove</param>
        /// <returns></returns>
        Task RemoveDeviceAsync(string deviceId);

        Task<DeviceModel> GetDeviceAsync(string deviceId);

        /// <summary>
        /// Adds a device to the Device Identity Store and Device Registry
        /// </summary>
        /// <param name="device">Device to add to the underlying repositories</param>
        /// <returns>Device created along with the device identity store keys</returns>
        Task<DeviceWithKeys> AddDeviceAsync(DeviceModel device);

        /// <summary>
        /// Retrieves the IoT Hub keys for the given device
        /// </summary>
        /// <param name="deviceId">ID of the device to retrieve</param>
        /// <returns>Primary and Secondary keys from the IoT Hub</returns>
        Task<SecurityKeys> GetDeviceKeysAsync(string deviceId);

        /// <summary>
        /// Updates the device in the device registry with the exact device provided in this call.
        /// NOTE: The device provided here should represent the entire device that will be
        /// serialized into the device registry.
        /// </summary>
        /// <param name="device">Device to update in the device registry</param>
        /// <returns>Device that was saved into the device registry</returns>
        Task<DeviceModel> UpdateDeviceAsync(DeviceModel device);

        Task<DeviceModel> UpdateHubEnabledStateAsync(string deviceId, bool isEnabled);

        /// <summary>
        /// Gets the devices whose device id is part of the given device id
        /// </summary>
        /// <param name="deviceId">The id of the device or a part of it</param>
        /// <param name="hubEnabledState">The state of the device</param>
        /// <returns></returns>
        Task<IList<DeviceModel>> GetDevicesAsync(string deviceId, HubEnabledState? hubEnabledState = null);

        /// <summary>
        /// Get the properties of the device twin
        /// </summary>
        /// <param name="deviceModel"></param>
        /// <returns></returns>
        IEnumerable<KeyValuePair<string, TwinCollectionExtension.TwinValue>> GetTwinProperties(DeviceModel deviceModel);

        /// <summary>
        /// Adds the device properties (twin, commands etc.) to the name cache
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        Task AddToNameCache(string deviceId);

        /// <summary>
        /// Updates the device inside the cloud with the device info sent by the device
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        Task<DeviceModel> UpdateDeviceByDeviceInfoPacketAsync(DeviceModel device);

        /// <summary>
        /// Send a command to a device based on the provided device id
        /// </summary>
        /// <param name="deviceId">The Device's ID</param>
        /// <param name="command"></param>
        /// <param name="commandDeliveryType">The type of the command</param>
        /// <param name="parameters">The parameters to send</param>
        /// <returns></returns>
        Task SendCommandAsync(string deviceId, Command command, CommandDeliveryType commandDeliveryType, dynamic parameters);

        /// <summary>
        /// Extracts the location data of the devices 
        /// </summary>
        /// <param name="deviceModels"></param>
        /// <returns></returns>
        IList<DeviceLocationModel> ExtractLocationData(IList<DeviceModel>deviceModels);
    }
}
