﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.ComBo.Common.Extensions;
using XWS.ComBo.Modules.Domain;
using XWS.ComBo.Modules.Domain.Commands;
using XWS.ComBo.Modules.Domain.Enums;
using XWS.ComBo.Modules.Repositories.Interfaces;
using XWS.ComBo.Modules.Services.Interfaces;

namespace XWS.ComBo.Modules.Services
{
    /// <summary>
    /// Business logic around different types of devices to store and 
    /// query the storage for device twin properties and methods
    /// </summary>
    public class NameCacheService : INameCacheService
    {
        private readonly INameCacheRepository _nameCacheRepository;

        public string PREFIX_REPORTED => "reported.";
        public string PREFIX_DESIRED => "desired.";
        public string PREFIX_TAGS => "tags.";

        public NameCacheService(INameCacheRepository nameCacheRepository)
        {
            _nameCacheRepository = nameCacheRepository;
        }

        public async Task AddShortNamesAsync(NameCacheEntityType type, IEnumerable<string> shortNames)
        {
            var names = shortNames.Select(shortName =>
            {
                switch (type)
                {
                    case NameCacheEntityType.Tag: return PREFIX_TAGS + shortName;
                    case NameCacheEntityType.DesiredProperty: return PREFIX_DESIRED + shortName;
                    case NameCacheEntityType.ReportedProperty: return PREFIX_REPORTED + shortName;
                    default: throw new ArgumentOutOfRangeException();
                }
            }).ToList();

            await _nameCacheRepository.AddNamesAsync(type, names);
        }

        public async Task<bool> AddNameAsync(string name)
        {
            var type = this.GetEntityType(name);
            var entity = new NameCacheEntity
            {
                Name = name
            };
            return await _nameCacheRepository.AddNameAsync(type, entity);
        }

        /// <summary>
        /// Gets a list of names of the name cache
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public async Task<IEnumerable<NameCacheEntity>> GetNameListAsync(NameCacheEntityType type)
        {
            var namelist = await _nameCacheRepository.GetNameListAsync(type);
            return namelist.Where(n => !n.Name.IsReservedTwinName());
        }

        /// <summary>
        /// Adds the supported device methods to the name cache
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        public async Task<bool> AddMethodAsync(Command method)
        {
            var parameterTypes = method.CommandParameters.Select(p => p.Type).ToList();
            string normalizedMethodName = $"{method.Name}({string.Join(",", parameterTypes)})";
            var entity = new NameCacheEntity
            {
                Name = normalizedMethodName,
                Description = method.Description,
                CommandParameters = method.CommandParameters
            };

            return await _nameCacheRepository.AddNameAsync(NameCacheEntityType.Method, entity);
        }


        private NameCacheEntityType GetEntityType(string name)
        {
            NameCacheEntityType type;

            if (name.StartsWith(PREFIX_REPORTED, StringComparison.Ordinal))
            {
                type = NameCacheEntityType.ReportedProperty;
            }
            else if (name.StartsWith(PREFIX_DESIRED, StringComparison.Ordinal))
            {
                type = NameCacheEntityType.DesiredProperty;
            }
            else if (name.StartsWith(PREFIX_TAGS, StringComparison.Ordinal))
            {
                type = NameCacheEntityType.Tag;
            }
            else
            {
                type = NameCacheEntityType.DeviceInfo;
            }

            return type;
        }
    }
}
