﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Devices.Shared;
using Newtonsoft.Json;
using XWS.ComBo.Modules.Domain.Commands;

namespace XWS.ComBo.Modules.Domain
{
    public class DeviceModel
    {
        /// <summary>
        /// Creates a new instance of a DeviceModel.
        /// </summary>
        public DeviceModel()
        {
            Commands = new List<Command>();
            CommandHistory = new List<CommandHistory>();
            Telemetry = new List<Telemetry>();
        }

        //public DeviceProperties DeviceProperties { get; set; }
        public bool IsSimulatedDevice { get; set; }
        /// <summary>
        /// User defined unique name of the resource (with the same partition key value). 
        /// If the user does not specify an id, an id will be system generated
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// System generated, unique and hierarchical identifier of the resource
        /// </summary>
        public string _rid { get; set; }
        /// <summary>
        /// Unique addressable URI of the resource
        /// </summary>
        public string _self { get; set; }
        /// <summary>
        /// etag of the resource required for optimistic concurrency control
        /// </summary>
        public string _etag { get; set; }
        /// <summary>
        /// Last updated timestamp of the resource
        /// </summary>
        public int _ts { get; set; }
        public string _attachments { get; set; }

        public List<Command> Commands { get; set; }
        public List<CommandHistory> CommandHistory { get; set; }
        public List<Telemetry> Telemetry { get; set; }

        public Twin Twin { get; set; }

        public string ObjectType { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

    }
}
