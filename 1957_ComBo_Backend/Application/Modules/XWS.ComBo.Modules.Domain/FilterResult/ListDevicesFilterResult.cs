﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.ComBo.Modules.Domain.FilterResult
{
    public class ListDevicesFilterResult
    {
        public List<DeviceModel> Results { get; set; }
    }
}
