﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.ComBo.Modules.Domain
{
    public class BlobContents
    {
        public Stream Data { get; set; }
        public DateTime? LastModifiedTime { get; set; }
    }
}
