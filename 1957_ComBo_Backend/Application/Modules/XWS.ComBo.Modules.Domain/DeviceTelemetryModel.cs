﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.ComBo.Modules.Domain
{
    /// <summary>
    /// A model that represents a Device's telemetry recording.
    /// </summary>
    public class DeviceTelemetryModel
    {
        /// <summary>
        /// Gets or sets the ID of the Device for which telemetry applies.
        /// </summary>
        public string DeviceId
        {
            get;
            set;
        }

        /// <summary>
        /// Values for telemetry data associated with individual fields
        /// </summary>
        public IDictionary<string, double> Values { get; set; } = new Dictionary<string, double>();

        /// <summary>
        /// Gets or sets the time of record for the represented telemetry 
        /// recording.
        /// </summary>
        public DateTime? Timestamp
        {
            get;
            set;
        }
    }
}
