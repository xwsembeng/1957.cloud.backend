﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.ComBo.Modules.Domain
{
    /// <summary>
    /// Contains the data for the telemetry at the dashboard
    /// </summary>
    public class DeviceDashboardTelemetryModel
    {
        public DeviceDashboardTelemetryModel()
        {
            DeviceTelemetryModels = new List<DeviceTelemetryModel>();
            Telemetry = new List<Telemetry>();
        }
        /// <summary>
        /// Gets or sets the ID of the Device for which telemetry should be 
        /// shown.
        /// </summary>
        public string DeviceId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a list of DeviceTelemetryModel for backing the 
        /// telemetry line graph.
        /// </summary>
        public IList<DeviceTelemetryModel> DeviceTelemetryModels
        {
            get;
            set;
        }

        /// <summary>
        /// The supported telemtry types
        /// </summary>
        public IList<Telemetry> Telemetry
        {
            get;
            set;
        }
    }
}
