﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.ComBo.Modules.Domain
{
    public class DeviceWithKeys
    {
        public DeviceModel Device { get; set; }
        public SecurityKeys SecurityKeys { get; set; }

        public DeviceWithKeys(DeviceModel device, SecurityKeys securityKeys)
        {
            Device = device;
            SecurityKeys = securityKeys;
        }
    }
}
