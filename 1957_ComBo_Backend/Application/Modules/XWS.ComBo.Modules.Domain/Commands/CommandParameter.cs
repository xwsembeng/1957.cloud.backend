﻿using System;
using System.Linq;
using Newtonsoft.Json;

namespace XWS.ComBo.Modules.Domain.Commands
{
    public class CommandParameter
    {
        /// <summary>
        /// Serialization deserialization constructor.
        /// </summary>
        [JsonConstructor]
        public CommandParameter()
        {
        }

        public CommandParameter(string name, string type)
        {
            Name = name;
            Type = type;
        }

        public string Name { get; set; }
        public string Type { get; set; }

        public string Serialize()
        {
            return FormattableString.Invariant($"{Name}-{Type}");
        }

        public static CommandParameter Deserialize(string serialized)
        {
            var parts = serialized.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);

            if (parts.Length < 2)
            {
                throw new ArgumentException(FormattableString.Invariant($"Invalidate serialized parameter format: {serialized}"));
            }

            return new CommandParameter
            {
                Name = string.Join("-", parts.Take(parts.Length - 1)),
                Type = parts.Last()
            };
        }
    }
}
