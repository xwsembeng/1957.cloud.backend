﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using XWS.ComBo.Modules.Domain.Enums;

namespace XWS.ComBo.Modules.Domain.Commands
{
    public class Command
    {
        public string Name { get; set; }
        public CommandDeliveryType CommandDeliveryType { get; set; }
        public List<CommandParameter> CommandParameters { get; set; }
        public string Description { get; set; }

        /// <summary>
        /// Serialization deserialization constructor.
        /// </summary>
        [JsonConstructor]
        public Command()
        {
            CommandParameters = new List<CommandParameter>();
        }

        public Command(string name, CommandDeliveryType commandDeliveryType, string description, IEnumerable<CommandParameter> parameters = null) : this()
        {
            Name = name;
            CommandDeliveryType = commandDeliveryType;
            Description = description;
            if (parameters != null)
            {
                CommandParameters.AddRange(parameters);
            }
        }

        public KeyValuePair<string, string> Serialize()
        {
            var parts = new string[] { Name }.Concat(CommandParameters.Select(p => FormattableString.Invariant($"{p.Name}-{p.Type}")));

            return new KeyValuePair<string, string>(string.Join("--", parts), Description);
        }

        public static Command Deserialize(string key, string value)
        {
            var parts = key.Split(new string[] { "--" }, StringSplitOptions.RemoveEmptyEntries);

            return new Command
            {
                Name = parts.First(),
                CommandDeliveryType = CommandDeliveryType.Method,
                Description = value,
                CommandParameters = parts.Skip(1).Select(CommandParameter.Deserialize).ToList()
            };
        }
    }
}
