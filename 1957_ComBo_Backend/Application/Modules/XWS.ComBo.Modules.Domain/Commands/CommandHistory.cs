﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using XWS.ComBo.Modules.Domain.Enums;

namespace XWS.ComBo.Modules.Domain.Commands
{
    public class CommandHistory
    {

        public string Name { get; set; }
        public CommandDeliveryType CommandDeliveryType { get; set; }
        public string MessageId { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime UpdatedTime { get; set; }
        private string _result;
        public string Result
        {
            get { return string.IsNullOrWhiteSpace(_result) ? "Pending" : _result; }
            set { _result = value; } 
        }
        public string ReturnValue { get; set; }
        public string ErrorMessage { get; set; }
        public dynamic CommandParameters { get; set; }

        /// <summary>
        /// Creates a new instance of a command history model.
        /// </summary>
        [JsonConstructor]
        internal CommandHistory()
        {
        }

        /// <summary>
        /// Creates a new instance of a command history model.
        /// </summary>
        /// <param name="name">The name of the command.</param>
        /// <param name="commandDeliveryType">The type of the command</param>
        /// <param name="commandParameters">Dynamic list of parameters issued with the command.</param>
        public CommandHistory(string name, CommandDeliveryType commandDeliveryType = CommandDeliveryType.Message, dynamic commandParameters = null)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            Name = name;
            CommandDeliveryType = commandDeliveryType;
            MessageId = Guid.NewGuid().ToString();
            CreatedTime = DateTime.UtcNow;
            SetParameters(commandParameters);
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        /// <summary>
        /// Adds the entire set of parameters to the command history.
        /// </summary>
        /// <param name="parameters">The parameters to set on the history. Must be either a JArray, JObject or a <see cref="Dictionary"/> instance.</param>
        private void SetParameters(dynamic parameters)
        {
            if (parameters == null)
            {
                return;
            }

            if (parameters.GetType() == typeof(Dictionary<string, object>))
            {
                var newParam = new JObject();
                foreach (var parameter in ((Dictionary<string, object>)parameters))
                {
                    newParam.Add(parameter.Key, new JValue(parameter.Value));
                }
                CommandParameters = newParam;
            }
            else
            {
                CommandParameters = parameters;
            }
        }

        /// <summary>
        /// Gets the string representation of the history parameters.
        /// </summary>
        /// <returns></returns>
        public string GetParameterString()
        {
            if (CommandParameters == null)
            {
                return string.Empty;
            }

            return CommandParameters.ToString();
        }
    }
}
