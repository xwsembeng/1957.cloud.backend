﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Table;
using XWS.ComBo.Modules.Domain.Enums;

namespace XWS.ComBo.Modules.Domain
{
    public class NameCacheTableEntity : TableEntity
    {
        public NameCacheTableEntity(NameCacheEntityType entityType, string name)
        {
            this.PartitionKey = entityType.ToString();
            this.RowKey = name;
        }

        /// <summary>
        /// Default constructor necessary for table storage
        /// </summary>
        public NameCacheTableEntity() { }

        [IgnoreProperty]
        public string Name
        {
            get { return this.RowKey; }
            set { this.RowKey = value; }
        }

        public string MethodParameters { get; set; }

        public string MethodDescription { get; set; }
    }
}
