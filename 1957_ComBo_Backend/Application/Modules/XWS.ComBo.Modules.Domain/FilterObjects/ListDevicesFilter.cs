﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.ComBo.Modules.Domain.Enums;

namespace XWS.ComBo.Modules.Domain.FilterObjects
{
    public class ListDevicesFilter
    {
        public ListDevicesFilter() { }


        public string DeviceID { get; set; }
        public HubEnabledState? HubEnabledState { get; set; }
        //public string DeviceState { get; set; }
        public string Manufacturer { get; set; }
        public string ModelNumber { get; set; }
        public string SerialNumber { get; set; }
        public string FirmwareVersion { get; set; }

        /// <summary>
        /// Translate the filters in current query to IoT Hub SQL query
        /// Full text searching, paging and sorting are not supported by the IoT Hub SQL query until now
        /// </summary>
        /// <returns>The full SQL query</returns>
        public string GetSQLQuery()
        {
            string queryWithoutCondition = "SELECT * FROM devices";
            string condition = GetSQLCondition();
            if (!string.IsNullOrWhiteSpace(condition))
            {
                return FormattableString.Invariant($"{queryWithoutCondition} WHERE {condition}");
            }

            return queryWithoutCondition;
        }

        /// <summary>
        /// Translate the filters in current query to IoT Hub SQL query condition
        /// Full text searching, paging and sorting are not supported by the IoT Hub SQL query until now
        /// </summary>
        /// <returns>The query condition, or empty string if no valid filter found</returns>
        public string GetSQLCondition()
        {
            var filterConditions = new List<KeyValuePair<string, string>>();
            if (!string.IsNullOrWhiteSpace(DeviceID))
            {
                filterConditions.Add(new KeyValuePair<string, string>("deviceId",
                    AddQuoteIfNeeded(TwinDataType.String, DeviceID)));

            }
            if (HubEnabledState.HasValue)
            {
                filterConditions.Add(new KeyValuePair<string, string>("tags.HubEnabledState",
                    AddQuoteIfNeeded(TwinDataType.String,
                        HubEnabledState == Enums.HubEnabledState.Running
                            ? Enums.HubEnabledState.Running.ToString()
                            : Enums.HubEnabledState.Disabled.ToString())));
            }
            //else if (string.IsNullOrWhiteSpace(DeviceState))
            //{
                
            //}
            if (!string.IsNullOrWhiteSpace(Manufacturer))
            {
                filterConditions.Add(new KeyValuePair<string, string>("reported.System.Manufacturer",
                    AddQuoteIfNeeded(TwinDataType.String, Manufacturer)));
            }
            if (!string.IsNullOrWhiteSpace(ModelNumber))
            {
                filterConditions.Add(new KeyValuePair<string, string>("reported.System.ModelNumber",
                    AddQuoteIfNeeded(TwinDataType.String, ModelNumber)));
            }
            if (!string.IsNullOrWhiteSpace(SerialNumber))
            {
                filterConditions.Add(new KeyValuePair<string, string>("reported.System.SerialNumber",
                    AddQuoteIfNeeded(TwinDataType.String, SerialNumber)));
            }
            if (!string.IsNullOrWhiteSpace(FirmwareVersion))
            {
                filterConditions.Add(new KeyValuePair<string, string>("reported.System.FirmwareVersion",
                    AddQuoteIfNeeded(TwinDataType.String, FirmwareVersion)));
            }

            var filters = new List<string>();
            foreach (var filterCondition in filterConditions)
            {
                var twinPropertyName = filterCondition.Key;
                if (this.IsProperties(twinPropertyName))
                {
                    twinPropertyName = FormattableString.Invariant($"properties.{filterCondition.Key}");
                }
                filters.Add(FormattableString.Invariant($"{twinPropertyName} = {filterCondition.Value}"));
            }
            return !filters.Any() ? string.Empty : string.Join(" AND ", filters);
        }

        private bool IsProperties(string name)
        {
            return name.StartsWith("reported.", StringComparison.Ordinal) || name.StartsWith("desired.", StringComparison.Ordinal);
        }

        private string AddQuoteIfNeeded(TwinDataType dataType, string value)
        {
            if (dataType == TwinDataType.String && !value.StartsWith("\'", StringComparison.Ordinal) && !value.EndsWith("\'", StringComparison.Ordinal))
            {
                value = FormattableString.Invariant($"\'{value}\'");
            }

            return value;
        }


    }
}
