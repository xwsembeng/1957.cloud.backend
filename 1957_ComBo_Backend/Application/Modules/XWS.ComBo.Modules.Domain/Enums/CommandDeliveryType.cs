﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.ComBo.Modules.Domain.Enums
{
    /// <summary>
    /// Type of command which is sent from the cloud to the device
    /// </summary>
    public enum CommandDeliveryType
    {
        /// <summary>
        /// Cloud-to-Device-Message
        /// </summary>
        Message = 0,
        /// <summary>
        /// Direct method on the device
        /// </summary>
        Method = 1
    }
}
