﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.ComBo.Modules.Domain.Enums
{
    [Flags]
    public enum NameCacheEntityType
    {
        DeviceInfo = 1,
        Tag = 2,
        DesiredProperty = 4,
        ReportedProperty = 8,
        Method = 16,
        Property = DesiredProperty | ReportedProperty,
        All = DeviceInfo | Tag | DesiredProperty | ReportedProperty | Method
    }
}
