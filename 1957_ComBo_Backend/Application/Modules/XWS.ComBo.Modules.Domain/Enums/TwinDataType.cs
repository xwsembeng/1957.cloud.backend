﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.ComBo.Modules.Domain.Enums
{
    public enum TwinDataType
    {
        String,
        Number,
        Boolean
    }
}
