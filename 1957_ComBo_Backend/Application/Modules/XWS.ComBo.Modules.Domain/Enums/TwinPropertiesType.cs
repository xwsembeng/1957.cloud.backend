﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.ComBo.Modules.Domain.Enums
{
    /// <summary>
    /// Type of the different properties of a device twin
    /// </summary>
    public enum TwinPropertiesType
    {
        Tag,
        Desired,
        Reported
    }
}
