﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.ComBo.Modules.Domain.Enums
{
    /// <summary>
    /// Indicates if the device can communicate with the cloud
    /// </summary>
    public enum HubEnabledState
    {
        Running,
        Disabled
    }
}
