﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace XWS.ComBo.Modules.Domain
{
    /// <summary>
    /// Reperesents the different telemetry types
    /// </summary>
    public class Telemetry
    {
        [JsonConstructor]
        public Telemetry()
        {
        }

        public Telemetry(string name, string displayName, string type)
        {
            Name = name;
            DisplayName = displayName;
            Type = type;
        }

        /// <summary>
        /// The name of the field
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The user-friendly name for the field
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// The type of the field, such as "double" or "integer"
        /// </summary>
        public string Type { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
