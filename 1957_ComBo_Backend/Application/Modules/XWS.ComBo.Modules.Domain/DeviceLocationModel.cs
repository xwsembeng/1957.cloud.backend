﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Devices.Shared;
using XWS.ComBo.Modules.Domain.Enums;

namespace XWS.ComBo.Modules.Domain
{
    public class DeviceLocationModel
    {
        public string DeviceId { get; set; }
        public HubEnabledState HubEnabledState { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
