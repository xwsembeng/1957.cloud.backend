﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.ComBo.Modules.Domain.Enums;

namespace XWS.ComBo.Modules.Domain
{
    /// <summary>
    /// Contains the twin properties data from the view
    /// </summary>
    public class TwinPropertyViewModel
    {
        public string Key { get; set; }
        public dynamic Value { get; set; }
        public TwinDataType DataType { get; set; }
        public bool IsDeleted { get; set; }
        public TwinPropertiesType TwinPropertiesType { get; set; }
    }
}
