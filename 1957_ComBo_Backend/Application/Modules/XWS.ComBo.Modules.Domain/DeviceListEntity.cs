﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.ComBo.Modules.Domain
{
    /// <summary>
    /// Represents an entity of the device list inside the table storage
    /// </summary>
    public class DeviceListEntity : TableEntity
    {
        public DeviceListEntity(string hostName, string deviceId)
        {
            this.PartitionKey = deviceId;
            this.RowKey = hostName;
        }

        /// <summary>
        /// Default constructor necessary for table storage
        /// </summary>
        public DeviceListEntity() { }

        [IgnoreProperty]
        public string HostName
        {
            get { return this.RowKey; }
            set { this.RowKey = value; }
        }

        [IgnoreProperty]
        public string DeviceId
        {
            get { return this.PartitionKey; }
            set { this.PartitionKey = value; }
        }

        public string Key { get; set; }
    }
}
