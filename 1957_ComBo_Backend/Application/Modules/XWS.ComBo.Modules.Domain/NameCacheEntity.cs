﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.ComBo.Modules.Domain.Commands;

namespace XWS.ComBo.Modules.Domain
{
    public class NameCacheEntity
    {
        public string Name { get; set; }
        public List<CommandParameter> CommandParameters { get; set; }
        public string Description { get; set; }
    }
}
