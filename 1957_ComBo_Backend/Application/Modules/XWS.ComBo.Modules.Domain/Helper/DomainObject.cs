﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.ComBo.Modules.Domain.Helper.Interfaces;

namespace XWS.ComBo.Modules.Domain.Helper
{
    [Serializable]
    public abstract class DomainObject : IDomainObject
    {
        private Guid _id;
        private int? _rowVersion;

        protected DomainObject()
        {
            _id = SequentialGuidGenerator.GenerateComb();
        }


        public virtual Guid Id
        {
            get { return _id; }
            protected set { _id = value; }
        }
        public virtual int? RowVersion
        {
            get { return _rowVersion; }
        }
        /// <summary>
        /// Gets a value indicating whether this instance was ever persisted.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is transient; otherwise, <c>false</c>.
        /// </value>
        public virtual bool IsTransient => _rowVersion == null;

        /// <summary>
        /// Sets the Id to a new Guid and the RowVersion to Null
        /// </summary>
        public void MakeTransient()
        {
            _id = SequentialGuidGenerator.GenerateComb();
            _rowVersion = null;
        }

        public override bool Equals(object obj)
        {
            return Id.Equals(((DomainObject)obj).Id);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
