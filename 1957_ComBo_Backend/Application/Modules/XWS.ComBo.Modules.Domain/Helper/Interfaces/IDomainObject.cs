﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.ComBo.Modules.Domain.Helper.Interfaces
{
    public interface IDomainObject
    {
        Guid Id { get; }
        int? RowVersion { get; }
        bool IsTransient { get; }
        void MakeTransient();

    }
}
