﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Devices.Shared;
using XWS.ComBo.Common.Extensions;
using XWS.ComBo.Modules.Domain;
using XWS.ComBo.Modules.Domain.Commands;
using XWS.ComBo.Modules.Domain.Enums;

namespace XWS.ComBo.Common.Helpers
{
    public static class SupportedMethodsHelper
    {
        private static string SupportedMethodsKey = "SupportedMethods";

        /// <summary>
        /// Adds/Removes the supported methods of the device to/from the reported properies of the twin
        /// </summary>
        /// <param name="patch"></param>
        /// <param name="commands"></param>
        /// <param name="reported"></param>
        public static void CreateSupportedMethodsReport(TwinCollection patch, IEnumerable<Command> commands,
            TwinCollection reported)
        {
            var existingMethods = new HashSet<string>();
            if (reported != null && reported.Contains("SupportedMethods"))
            {
                existingMethods.UnionWith(reported.AsEnumerableFlatten()
                    .Select(pair => pair.Key)
                    .Where(key => key.StartsWith("SupportedMethods.", StringComparison.Ordinal))
                    .Select(key => key.Split('.')[1]));
            }

            var supportedMethods = new TwinCollection();
            foreach (var method in commands.Where(c => c.CommandDeliveryType == CommandDeliveryType.Method))
            {
                if (string.IsNullOrWhiteSpace(method.Name))
                {
                    continue;
                }

                if (method.CommandParameters.Any(p =>
                    string.IsNullOrWhiteSpace(p.Name) || string.IsNullOrWhiteSpace(p.Type)))
                {
                    continue;
                }

                var pair = method.Serialize();
                supportedMethods[pair.Key] = pair.Value;

                existingMethods.Remove(pair.Key);
            }

            foreach (var method in existingMethods)
            {
                supportedMethods[method] = null;
            }

            patch["SupportedMethods"] = supportedMethods;
        }

        public static bool IsSupportedMethodProperty(string propertyName)
        {
            return propertyName == SupportedMethodsKey ||
                   propertyName.StartsWith(SupportedMethodsKey + ".", StringComparison.Ordinal);
        }

        public static void AddSupportedMethodsFromReportedProperty(DeviceModel device, Twin twin)
        {
            foreach (var pair in twin.Properties.Reported.AsEnumerableFlatten()
                .Where(pair => IsSupportedMethodProperty(pair.Key)))
            {
                try
                {
                    var command = Command.Deserialize(
                        pair.Key.Substring(SupportedMethodsKey.Length + 1), //SupportedMethodsKey.Length + 1 because of the "." after "SupportedMethods"
                        pair.Value.Value.Value.ToString());

                    if (!device.Commands.Any(c =>
                        c.Name == command.Name && c.CommandDeliveryType == CommandDeliveryType.Method))
                    {
                        device.Commands.Add(command);
                    }
                }
                catch (Exception ex)
                {
                    Trace.TraceError(
                        FormattableString.Invariant($"Exception raised while deserializing method {pair.Key}: {ex}"));
                    continue;
                }
            }
        }
    }
}
