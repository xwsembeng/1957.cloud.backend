﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Dynamitey;
using Microsoft.CSharp.RuntimeBinder;

namespace XWS.ComBo.Common.Helpers
{
    /// <summary>
    /// Contains methods related to reflection.
    /// </summary>
    public static class ReflectionHelper
    {
        /// <summary>
        /// Gets an empty object array.
        /// </summary>
        public static object[] EmptyArray { get; } = new object[0];


        /// <summary>
        /// Gets the value of an item's named property.
        /// </summary>
        /// <param name="item">
        /// The item from  which to extract a named property's value.
        /// </param>
        /// <param name="propertyName">
        /// The name of the property.
        /// </param>
        /// <param name="usesCaseSensitivePropertyNameMatch">
        /// A value indicating whether the property name match should be
        /// case-sensitive.
        /// </param>
        /// <param name="exceptionThrownIfNoMatch">
        /// A value indicating whether an exception should be thrown if
        /// no matching property can be found.
        /// </param>
        /// <returns>
        /// The value of the property named by propertyName on item,
        /// or a null reference if exceptionThrownIfNoMatch is false and
        /// propertyName does not name a property on item.
        /// </returns>
        public static object GetNamedPropertyValue(
            object item,
            string propertyName,
            bool usesCaseSensitivePropertyNameMatch,
            bool exceptionThrownIfNoMatch)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            if (string.IsNullOrEmpty("propertyName"))
            {
                throw new ArgumentException("propertyName is a null reference or empty string.", "propertyName");
            }

            ICustomTypeDescriptor customProvider;
            IDynamicMetaObjectProvider dynamicProvider;
            if ((dynamicProvider = item as IDynamicMetaObjectProvider) != null)
            {
                return GetNamedPropertyValue(
                    dynamicProvider,
                    propertyName,
                    usesCaseSensitivePropertyNameMatch,
                    exceptionThrownIfNoMatch);
            }
            else if ((customProvider = item as ICustomTypeDescriptor) != null)
            {
                return GetNamedPropertyValue(
                    customProvider,
                    propertyName,
                    usesCaseSensitivePropertyNameMatch,
                    exceptionThrownIfNoMatch);
            }

            StringComparison comparisonType = StringComparison.CurrentCultureIgnoreCase;
            if (usesCaseSensitivePropertyNameMatch)
            {
                comparisonType = StringComparison.CurrentCulture;
            }

            IEnumerable<PropertyInfo> matchingProps = item.GetType().GetProperties().Where(
                t => string.Equals(t.Name, propertyName, comparisonType));

            MethodInfo methodInfo = matchingProps.Select(t => t.GetGetMethod()).FirstOrDefault(u => u != null);

            if (methodInfo == default(MethodInfo))
            {
                if (exceptionThrownIfNoMatch)
                {
                    throw new ArgumentException("propertyName does not name a property on item", "propertyName");
                }

                return null;
            }

            return methodInfo.Invoke(item, EmptyArray);
        }

        /// <summary>
        /// Gets the value of an IDynamicMetaObjectProvider
        /// implementation's named property.
        /// </summary>
        /// <param name="item">
        /// The IDynamicMetaObjectProvider implementation, from
        /// which to extract a named property's value.
        /// </param>
        /// <param name="propertyName">
        /// The name of the property.
        /// </param>
        /// <param name="usesCaseSensitivePropertyNameMatch">
        /// A value indicating whether the property name match should be
        /// case-sensitive.
        /// </param>
        /// <param name="exceptionThrownIfNoMatch">
        /// A value indicating whether an exception should be thrown if
        /// no matching property can be found.
        /// </param>
        /// <returns>
        /// The value of the property named by propertyName on item,
        /// or a null reference if exceptionThrownIfNoMatch is false and
        /// propertyName does not name a property on item.
        /// </returns>
        private static object GetNamedPropertyValue(
            IDynamicMetaObjectProvider item,
            string propertyName,
            bool usesCaseSensitivePropertyNameMatch,
            bool exceptionThrownIfNoMatch)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            if (string.IsNullOrEmpty(propertyName))
            {
                throw new ArgumentException("propertyName is a null reference or empty string.", "propertyName");
            }

            if (!usesCaseSensitivePropertyNameMatch || exceptionThrownIfNoMatch)
            {
                StringComparison comparisonType = StringComparison.CurrentCultureIgnoreCase;
                if (usesCaseSensitivePropertyNameMatch)
                {
                    comparisonType = StringComparison.CurrentCulture;
                }

                propertyName = Dynamic.GetMemberNames(item, true).FirstOrDefault(
                    t => string.Equals(t, propertyName, comparisonType));

                if (string.IsNullOrEmpty(propertyName))
                {
                    if (exceptionThrownIfNoMatch)
                    {
                        throw new ArgumentException("propertyName does not name a property on item", "propertyName");
                    }

                    return null;
                }
            }

            try
            {
                return Dynamic.InvokeGet(item, propertyName);
            }
            catch (RuntimeBinderException)
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the value of an ICustomTypeDescriptor
        /// implementation's named property.
        /// </summary>
        /// <param name="item">
        /// The ICustomTypeDescriptor implementation, from
        /// which to extract a named property's value.
        /// </param>
        /// <param name="propertyName">
        /// The name of the property.
        /// </param>
        /// <param name="usesCaseSensitivePropertyNameMatch">
        /// A value indicating whether the property name match should be
        /// case-sensitive.
        /// </param>
        /// <param name="exceptionThrownIfNoMatch">
        /// A value indicating whether an exception should be thrown if
        /// no matching property can be found.
        /// </param>
        /// <returns>
        /// The value of the property named by propertyName on item,
        /// or a null reference if exceptionThrownIfNoMatch is false and
        /// propertyName does not name a property on item.
        /// </returns>
        private static object GetNamedPropertyValue(
            ICustomTypeDescriptor item,
            string propertyName,
            bool usesCaseSensitivePropertyNameMatch,
            bool exceptionThrownIfNoMatch)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            if (string.IsNullOrEmpty("propertyName"))
            {
                throw new ArgumentException("propertyName is a null reference or empty string.", "propertyName");
            }

            StringComparison comparisonType = StringComparison.CurrentCultureIgnoreCase;
            if (usesCaseSensitivePropertyNameMatch)
            {
                comparisonType = StringComparison.CurrentCulture;
            }

            PropertyDescriptor descriptor = default(PropertyDescriptor);
            foreach (PropertyDescriptor pd in item.GetProperties())
            {
                if (string.Equals(propertyName, pd.Name, comparisonType))
                {
                    descriptor = pd;
                    break;
                }
            }

            if (descriptor == default(PropertyDescriptor))
            {
                if (exceptionThrownIfNoMatch)
                {
                    throw new ArgumentException("propertyName does not name a property on item.", "propertyName");
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return descriptor.GetValue(item);
            }
        }

    }
}
