﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Devices.Shared;
using XWS.ComBo.Common.Extensions;
using XWS.ComBo.Modules.Domain;
using XWS.ComBo.Modules.Domain.Commands;
using XWS.ComBo.Modules.Domain.Enums;

namespace XWS.ComBo.Common.Helpers
{
    public static class DeviceCreatorHelper
    {
        /// <summary>
        /// Build a valid device representation used throughout the app.
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="isSimulated"></param>
        /// <param name="includeTwinDefaultProperties">if true default properties are added to the twin</param>
        /// <returns></returns>
        public static DeviceModel BuildDeviceStructure(string deviceId, bool isSimulated, bool includeTwinDefaultProperties = false)
        {
            DeviceModel deviceModel = new DeviceModel
            {
                IsSimulatedDevice = isSimulated,
                Twin = new Twin(deviceId),
                Commands = new List<Command>(),
                CommandHistory = new List<CommandHistory>()
            };
            if (includeTwinDefaultProperties)
            {
                deviceModel.Twin.Set("tags.HubEnabledState", HubEnabledState.Running.ToString());
            }
            return deviceModel;
        }

    }
}
