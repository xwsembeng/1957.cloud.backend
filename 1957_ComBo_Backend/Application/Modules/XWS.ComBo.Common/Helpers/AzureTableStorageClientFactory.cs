﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Core.InversionOfControl;
using XWS.ComBo.Common.Interfaces;
using XWS.ComBo.Common.Repositories;

namespace XWS.ComBo.Common.Helpers
{
    /// <summary>
    /// Factory to create the Storage Client for table storage access
    /// </summary>
    [Obsolete("Class not longer necessary to create Table Storage Clients")]
    public class AzureTableStorageClientFactory : IAzureTableStorageClientFactory
    {
        private IAzureTableStorageClient _tableStorageClient;

        public AzureTableStorageClientFactory() : this(null)
        {
        }

        public AzureTableStorageClientFactory(IAzureTableStorageClient customClient)
        {
            _tableStorageClient = customClient;
        }

        public IAzureTableStorageClient CreateClient(string storageConnectionString, string tableName)
        {
            if (_tableStorageClient == null)
            {
                //_tableStorageClient = new AzureTableStorageClient(storageConnectionString, tableName);
            }
            return _tableStorageClient;
        }
    }
}
