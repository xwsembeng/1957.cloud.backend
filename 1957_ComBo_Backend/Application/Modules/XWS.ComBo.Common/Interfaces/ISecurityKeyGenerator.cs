﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.ComBo.Modules.Domain;

namespace XWS.ComBo.Common.Interfaces
{
    public interface ISecurityKeyGenerator
    {
        /// <summary>
        /// Creates a random security key pair
        /// </summary>
        /// <returns>Populated SecurityKeys object</returns>
        SecurityKeys CreateRandomKeys();
    }
}
