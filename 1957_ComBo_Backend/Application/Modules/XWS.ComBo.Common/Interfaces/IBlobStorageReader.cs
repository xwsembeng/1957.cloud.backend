﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.ComBo.Modules.Domain;

namespace XWS.ComBo.Common.Interfaces
{
    public interface IBlobStorageReader : IEnumerable<BlobContents>
    {
    }
}
