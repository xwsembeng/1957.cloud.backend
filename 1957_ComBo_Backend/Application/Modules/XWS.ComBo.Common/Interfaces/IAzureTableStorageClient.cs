﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Table;
using XWS.ComBo.Modules.Domain;

namespace XWS.ComBo.Common.Interfaces
{
    public interface IAzureTableStorageClient
    {
        Task<IEnumerable<T>> ExecuteQueryAsync<T>(TableQuery<T> tableQuery) where T : TableEntity, new();
        Task<TableResult> ExecuteAsync(TableOperation operation);
        Task<IList<TableResult>> ExecuteBatchAsync(TableBatchOperation operation);

        Task<TableStorageResponse<TResult>> DoTableInsertOrReplaceAsync<TResult, TInput>(
            TInput incomingEntity,
            Func<TInput, TResult> tableEntityToModelConverter) where TInput : TableEntity;
    }
}
