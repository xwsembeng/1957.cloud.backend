﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.ComBo.Common.Interfaces
{
    public interface IBlobStorageClient
    {
        /// <summary>
        /// Gets the reader that contains the data of the blob storage
        /// </summary>
        /// <param name="prefix">The name of the folder which contains the data</param>
        /// <param name="minTime">The minimum time of record of the telemetry that should be returned.</param>
        /// <returns>Reader that contians the data from the blob storage</returns>
        Task<IBlobStorageReader> GetReader(string prefix, DateTime? minTime = null);
    }
}
