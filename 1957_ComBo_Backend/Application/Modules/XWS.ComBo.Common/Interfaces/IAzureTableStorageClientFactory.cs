﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.ComBo.Common.Interfaces
{
    public interface IAzureTableStorageClientFactory
    {
        IAzureTableStorageClient CreateClient(string storageConnectionString, string tableName);
    }
}
