﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.ComBo.Common.Extensions
{
    public static class StringExtension
    {
        /// <summary>
        /// Try to remove prefix from input string
        /// </summary>
        /// <param name="s">The raw string</param>
        /// <param name="prefix">The target prefix string</param>
        /// <param name="result">The prefix-removed string (or the raw string if target prefix was not found)</param>
        /// <returns>True if the target prefix was found</returns>
        public static bool TryTrimPrefix(this string s, string prefix, out string result)
        {
            if (s.StartsWith(prefix, StringComparison.Ordinal))
            {
                result = s.Substring(prefix.Length);
                return true;
            }
            else
            {
                result = s;
                return false;
            }
        }
        /// <summary>
        /// Try to remove prefix from input string
        /// </summary>
        /// <param name="s">The raw string</param>
        /// <param name="prefix">The target prefix string</param>
        /// <returns>prefix-removed string (or the raw string if target prefix was not found)</returns>
        public static string TryTrimPrefix(this string s, string prefix)
        {
            string result;
            s.TryTrimPrefix(prefix, out result);
            return result;
        }

        /// <summary>
        /// The twin's tag and property flat name start with "__" and end with "__" is reserved
        /// for internal usage purpose.
        /// e.g.
        ///     tags.HubEnabledState
        ///     tags.__icon__, tags.cpu.__version__
        ///     desired.__location__, desired.__location.latitude
        ///     reported.__location__, reported.__location__.__latitude__
        /// </summary>
        /// <param name="flatName"></param>
        /// <returns></returns>
        public static bool IsReservedTwinName(this string flatName)
        {
            if (string.IsNullOrEmpty(flatName)) return false;
            // this line should be removed once we change it to tags.__HubEnabledState__ in the future.
            if ("tags.HubEnabledState".Equals(flatName, StringComparison.Ordinal) || "HubEnabledState".Equals(flatName, StringComparison.Ordinal)) return true;
            string[] parts = flatName.Split('.');
            return parts.Any(p => p.StartsWith("__", StringComparison.Ordinal) && p.EndsWith("__", StringComparison.Ordinal));
        }
    }
}
