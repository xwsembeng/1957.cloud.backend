﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.ComBo.Common.Extensions;
using XWS.ComBo.Common.Helpers;
using XWS.ComBo.Exceptions;
using XWS.ComBo.Modules.Domain;
using XWS.ComBo.Modules.Domain.Commands;
using XWS.ComBo.Modules.Domain.Enums;

namespace XWS.ComBo.Common.Factory
{
    /// <summary>
    /// Creates sample devices
    /// </summary>
    public static class SampleDeviceFactory
    {

        private static readonly Random Rand = new Random();

        public const string OBJECT_TYPE_DEVICE_INFO = "DeviceInfo";

        private const bool IS_SIMULATED_DEVICE = true;

        private class Location
        {
            public double Latitude { get;}
            public double Longitude { get; }

            public Location(double latitude, double longitude)
            {
                Latitude = latitude;
                Longitude = longitude;
            }

        }

        private static readonly List<string> DefaultDeviceNames = new List<string>
        {
            "ComboSampleDevice001",
            "ComboSampleDevice002",
            "ComboSampleDevice003",
            "ComboSampleDevice004",
            "ComboSampleDevice005",
            "ComboSampleDevice006",
            "ComboSampleDevice007",
            "ComboSampleDevice008",
            "ComboSampleDevice009",
            "ComboSampleDevice010",
            "ComboSampleDevice011",
            "ComboSampleDevice012",
            "ComboSampleDevice013",
            "ComboSampleDevice014",
            "ComboSampleDevice015"
        };

        private static readonly List<string> FreeFirmwareDeviceNames = new List<string>
        {
            "ComboSampleDevice001",
            "ComboSampleDevice002",
            "ComboSampleDevice003",
            "ComboSampleDevice004",
            "ComboSampleDevice005",
            "ComboSampleDevice006",
            "ComboSampleDevice007",
            "ComboSampleDevice008"
        };

        //private static readonly List<string> HighTemperatureDeviceNames = new List<string>
        //{
        //    "ComboSampleDevice001",
        //    "ComboSampleDevice002",
        //    "ComboSampleDevice003",
        //    "ComboSampleDevice004"
        //};

        private static readonly List<Location> PossibleDeviceLocations = new List<Location>{
            new Location(48.125730, 11.502004),  
            new Location(47.662155, 9.503109),  
            new Location(47.996996, 7.865666), 
            new Location(48.750438, 9.145954), 
            new Location(49.973383, 8.244889),  
            new Location(50.939165, 6.924926), 
            new Location(51.491052, 7.512212),  
            new Location(53.538333, 9.979726), 
            new Location(52.488612, 13.353718), 
            new Location(51.332262, 12.373029)
        };

        public static DeviceModel GetSampleSimulatedDevice(string deviceId)
        {
            DeviceModel device = DeviceCreatorHelper.BuildDeviceStructure(deviceId, true);

            AssignDeviceProperties(device);
            device.ObjectType = OBJECT_TYPE_DEVICE_INFO;
            device.IsSimulatedDevice = IS_SIMULATED_DEVICE;

            AssignTelemetry(device);
            AssignCommands(device);

            return device;
        }

        private static void AssignDeviceProperties(DeviceModel device)
        {
            int randomId = Rand.Next(0, PossibleDeviceLocations.Count - 1);
            if (device?.Twin == null)
            {
                throw new DeviceRequiredTwinNotFoundException("Device Twin is missing");
            }

            device.Twin.Tags["HubEnabledState"] = HubEnabledState.Running.ToString();
            device.Twin.Properties.Reported.Set("Device.CreatedTime", DateTime.UtcNow.ToString(CultureInfo.InvariantCulture));
            device.Twin.Properties.Reported.Set("Device.DeviceState", "Normal");
            device.Twin.Properties.Reported.Set("System.Manufacturer", "XWS GmbH");
            device.Twin.Properties.Reported.Set("System.ModelNumber", "Combo-" + randomId);
            device.Twin.Properties.Reported.Set("System.SerialNumber", "SER-" + randomId);

            if (FreeFirmwareDeviceNames.Any(n => device.Twin.DeviceId.StartsWith(n, StringComparison.Ordinal)))
            {
                device.Twin.Properties.Reported.Set("System.FirmwareVersion", "1." + randomId);
            }
            else
            {
                device.Twin.Properties.Reported.Set("System.FirmwareVersion", "2.0");
            }
            device.Twin.Properties.Reported.Set("System.Platform", "Plat-" + randomId);
            device.Twin.Properties.Reported.Set("System.Processor", "i7- " + randomId);
            device.Twin.Properties.Reported.Set("System.InstalledRAM", randomId + " MB");

            // Choose a location among the 10 above and set Lat and Long for device properties
            device.Twin.Properties.Reported.Set("Device.Location.Latitude", PossibleDeviceLocations[randomId].Latitude);
            device.Twin.Properties.Reported.Set("Device.Location.Longitude", PossibleDeviceLocations[randomId].Longitude);
        }

        private static void AssignTelemetry(DeviceModel device)
        {
            device.Telemetry.Add(new Telemetry("Temperature", "Temperature", "double"));
            device.Telemetry.Add(new Telemetry("Humidity", "Humidity", "double"));
        }

        private static void AssignCommands(DeviceModel device)
        {
            // Device commands
            device.Commands.Add(new Command(
                "PingDevice",
                CommandDeliveryType.Message,
                "The device responds to this command with an acknowledgement. This is useful for checking that the device is still active and listening."
            ));
            device.Commands.Add(new Command(
                "StartTelemetry",
                CommandDeliveryType.Message,
                "Instructs the device to start sending telemetry."
            ));
            device.Commands.Add(new Command(
                "StopTelemetry",
                CommandDeliveryType.Message,
                "Instructs the device to stop sending telemetry."
            ));
            device.Commands.Add(new Command(
                "ChangeSetPointTemp",
                CommandDeliveryType.Message,
                "Controls the simulated temperature telemetry values the device sends. This is useful for testing back-end logic.",
                new[] { new CommandParameter("SetPointTemp", "double") }
            ));
            device.Commands.Add(new Command(
                "DiagnosticTelemetry",
                CommandDeliveryType.Message,
                "Controls if the device should send the external temperature as telemetry.",
                new[] { new CommandParameter("Active", "boolean") }
            ));
            device.Commands.Add(new Command(
                "ChangeDeviceState",
                CommandDeliveryType.Message,
                "Sets the device state metadata property that the device reports. This is useful for testing back-end logic.",
                new[] { new CommandParameter("DeviceState", "string") }
            ));

            // Device methods
            device.Commands.Add(new Command(
                "InitiateFirmwareUpdate",
                CommandDeliveryType.Method,
                "Updates device Firmware. Use parameter 'FwPackageUri' to specifiy the URI of the firmware file, e.g. https://iotrmassets.blob.core.windows.net/firmwares/FW20.bin",
                new[] { new CommandParameter("FwPackageUri", "string") }
            ));
            device.Commands.Add(new Command(
                "Reboot",
                CommandDeliveryType.Method,
                "Reboot the device"
            ));
            device.Commands.Add(new Command(
                "FactoryReset",
                CommandDeliveryType.Method,
                "Reset the device (including firmware and configuration) to factory default state"
            ));
        }


    }
}
