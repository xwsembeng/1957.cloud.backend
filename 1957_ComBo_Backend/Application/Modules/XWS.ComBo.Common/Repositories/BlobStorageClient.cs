﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Blob;
using XWS.ComBo.Common.Helpers;
using XWS.ComBo.Common.Interfaces;

namespace XWS.ComBo.Common.Repositories
{
    /// <summary>
    /// Methods, related to blob storage
    /// </summary>
    public abstract class BlobStorageClient : IBlobStorageClient
    {
        protected CloudBlobClient BlobClient;
        protected string ContainerName;
        private CloudBlobContainer _container;

        public BlobStorageClient() { }

        /// <summary>
        /// Gets the reader that contains the data of the blob storage
        /// </summary>
        /// <param name="prefix">The name of the folder which contains the data</param>
        /// <param name="minTime">The minimum time of record of the telemetry that should be returned.</param>
        /// <returns>Reader that contians the data from the blob storage</returns>
        public async Task<IBlobStorageReader> GetReader(string prefix, DateTime? minTime = null)
        {
            await CreateCloudBlobContainerAsync();

            var blobs = await LoadBlobItemsAsync(async token =>
            {
                return await _container.ListBlobsSegmentedAsync(
                    prefix,
                    true,
                    BlobListingDetails.None,
                    null,
                    token,
                    null,
                    null);
            });

            if (blobs != null)
            {
                blobs = blobs.OrderByDescending(ExtractBlobItemDate);
                if (minTime != null)
                {
                    blobs = blobs.Where(blobItem => FilterLessThanTime(blobItem, minTime.Value));
                }
            }

            return new BlobStorageReader(blobs);
        }


        #region Private-Methods

        private async Task CreateCloudBlobContainerAsync()
        {
            if (_container == null && ContainerName != null)
            {
                _container = BlobClient.GetContainerReference(ContainerName);
                await _container.CreateIfNotExistsAsync();
            }
        }

        /// <summary>
        ///     Load's a blob listing's items.
        /// </summary>
        /// <param name="segmentLoader">
        ///     A func for getting the blob listing's next segment.
        /// </param>
        /// <returns>
        ///     A concatenation of all the blob listing's resulting segments.
        /// </returns>
        private async Task<IEnumerable<IListBlobItem>> LoadBlobItemsAsync(
            Func<BlobContinuationToken, Task<BlobResultSegment>> segmentLoader)
        {
            if (segmentLoader == null)
            {
                throw new ArgumentNullException("segmentLoader");
            }

            IEnumerable<IListBlobItem> blobItems = new IListBlobItem[0];

            var segment = await segmentLoader(null);
            while (segment?.Results != null)
            {
                blobItems = blobItems.Concat(segment.Results);

                if (segment.ContinuationToken == null)
                {
                    break;
                }

                segment = await segmentLoader(segment.ContinuationToken);
            }

            return blobItems;
        }

        /// <summary>
        ///     Exctract's a blob item's last modified date.
        /// </summary>
        /// <param name="blobItem">
        ///     The blob item, for which to extract a last modified date.
        /// </param>
        /// <returns>
        ///     blobItem's last modified date, or null, of such could not be
        ///     extracted.
        /// </returns>
        private DateTime? ExtractBlobItemDate(IListBlobItem blobItem)
        {
            if (blobItem == null)
            {
                throw new ArgumentNullException("blobItem");
            }

            BlobProperties blobProperties;
            CloudBlockBlob blockBlob;
            CloudPageBlob pageBlob;

            if ((blockBlob = blobItem as CloudBlockBlob) != null)
            {
                blobProperties = blockBlob.Properties;
            }
            else if ((pageBlob = blobItem as CloudPageBlob) != null)
            {
                blobProperties = pageBlob.Properties;
            }
            else
            {
                blobProperties = null;
            }

            return blobProperties?.LastModified?.DateTime;
        }

        /// <summary>
        /// Indicates if the blobitem was modified before the passed minTime  
        /// </summary>
        /// <param name="blobItem"></param>
        /// <param name="minTime"></param>
        /// <returns></returns>
        private bool FilterLessThanTime(IListBlobItem blobItem, DateTime minTime)
        {
            CloudBlockBlob blockBlob;
            if ((blockBlob = blobItem as CloudBlockBlob) != null)
            {
                if (blockBlob.Properties?.LastModified != null &&
                    blockBlob.Properties.LastModified.Value.LocalDateTime >= minTime)
                {
                    return true;
                }
            }
            return false;
        }

        #endregion Private-Methods

    }
}
