﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using XWS.BusinessPlatform.Core.Configurations;
using XWS.ComBo.Common.Interfaces;
using XWS.ComBo.Modules.Domain;

namespace XWS.ComBo.Common.Repositories
{
    /// <summary>
    /// Wrapps calls to the table storage
    /// </summary>
    public abstract class AzureTableStorageClient : IAzureTableStorageClient
    {
        protected CloudTableClient TableClient;
        protected string TableName;
        private CloudTable _table;

        protected AzureTableStorageClient()
        {
        }


        public async Task<IEnumerable<T>> ExecuteQueryAsync<T>(TableQuery<T> tableQuery) where T : TableEntity, new()
        {
            var table = await this.GetCloudTableAsync();
            return table.ExecuteQuery(tableQuery);
        }

        public async Task<TableResult> ExecuteAsync(TableOperation operation)
        {
            var table = await GetCloudTableAsync();
            return await table.ExecuteAsync(operation);
        }

        public async Task<IList<TableResult>> ExecuteBatchAsync(TableBatchOperation operation)
        {
            var table = await GetCloudTableAsync();
            return await table.ExecuteBatchAsync(operation);
        }

        public async Task<TableStorageResponse<TResult>> DoTableInsertOrReplaceAsync<TResult, TInput>(
            TInput incomingEntity, Func<TInput, TResult> tableEntityToModelConverter) where TInput : TableEntity
        {
            var table = await GetCloudTableAsync();

            // Simply doing an InsertOrReplace will not do any concurrency checking, according to 
            // http://azure.microsoft.com/en-us/blog/managing-concurrency-in-microsoft-azure-storage-2/
            // So we will not use InsertOrReplace. Instead we will look to see if we have a rule like this
            // If so, then we'll do a concurrency-safe update, otherwise simply insert
            var retrieveOperation =
                TableOperation.Retrieve<TInput>(incomingEntity.PartitionKey, incomingEntity.RowKey);
            var retrievedEntity = await table.ExecuteAsync(retrieveOperation);

            TableOperation operation = null;
            if (retrievedEntity.Result != null)
            {
                operation = TableOperation.Replace(incomingEntity);
            }
            else
            {
                operation = TableOperation.Insert(incomingEntity);
            }

            return await PerformTableOperation(operation, incomingEntity, tableEntityToModelConverter);
        }
        #region Private-Methods

        private async Task<CloudTable> GetCloudTableAsync()
        {
            if (_table != null)
            {
                return _table;
            }
            _table = TableClient.GetTableReference(TableName);
            await _table.CreateIfNotExistsAsync();
            return _table;
        }

        private async Task<TableStorageResponse<TResult>> PerformTableOperation<TResult, TInput>(
            TableOperation operation, TInput incomingEntity, Func<TInput, TResult> tableEntityToModelConverter)
            where TInput : TableEntity
        {
            var table = await GetCloudTableAsync();
            var result = new TableStorageResponse<TResult>();

            try
            {
                await table.ExecuteAsync(operation);

                var nullModel = tableEntityToModelConverter(null);
                result.Entity = nullModel;
                result.Status = TableStorageResponseStatus.Successful;
            }
            catch (Exception ex)
            {
                var retrieveOperation = TableOperation.Retrieve<TInput>(incomingEntity.PartitionKey,
                    incomingEntity.RowKey);
                var retrievedEntity = table.Execute(retrieveOperation);

                if (retrievedEntity != null)
                {
                    // Return the found version of this rule in case it had been modified by someone else since our last read.
                    var retrievedModel = tableEntityToModelConverter((TInput)retrievedEntity.Result);
                    result.Entity = retrievedModel;
                }
                else
                {
                    // We didn't find an existing rule, probably creating new, so we'll just return what was sent in
                    result.Entity = tableEntityToModelConverter(incomingEntity);
                }

                if (ex.GetType() == typeof(StorageException)
                    &&
                    (((StorageException)ex).RequestInformation.HttpStatusCode ==
                     (int)HttpStatusCode.PreconditionFailed
                     || ((StorageException)ex).RequestInformation.HttpStatusCode == (int)HttpStatusCode.Conflict))
                {
                    result.Status = TableStorageResponseStatus.ConflictError;
                }
                else
                {
                    result.Status = TableStorageResponseStatus.UnknownError;
                }
            }

            return result;
        }



        #endregion Private-Methods
    }
}
