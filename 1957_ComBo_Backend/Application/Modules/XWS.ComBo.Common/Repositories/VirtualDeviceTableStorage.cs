﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using XWS.BusinessPlatform.Core.Configurations;
using XWS.ComBo.Common.Interfaces;
using XWS.ComBo.Modules.Domain;

namespace XWS.ComBo.Common.Repositories
{
    /// <summary>
    /// Contains methods for managing virtual (simulated) devices
    /// </summary>
    public class VirtualDeviceTableStorage : AzureTableStorageClient, IVirtualDeviceStorage
    {
        public VirtualDeviceTableStorage(IConfigurationProvider configurationProvider)
        {
            var storageAccount = CloudStorageAccount.Parse(configurationProvider.GetConfigurationSettingValue("device.StorageConnectionString"));
            TableClient = storageAccount.CreateCloudTableClient();
            TableName = configurationProvider.GetConfigurationSettingValue("device.TableName");
        }

        /// <summary>
        /// Gets a specific virtual device from storage based on deviceId
        /// </summary>
        /// <param name="deviceId">The deviceId to search for</param>
        /// <returns>InitialDeviceConfig for deviceId or null if not found</returns>
        public Task<InitialDeviceConfig> GetDeviceAsync(string deviceId)
        {
            var query = new TableQuery<DeviceListEntity>().Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, deviceId));
            return this.GetDeviceAsync(query);
        }
        /// <summary>
        /// Adds or updates a virtual device in storage
        /// </summary>
        /// <param name="deviceConfig">The device config to add or update</param>
        /// <returns>thows if fails</returns>
        public async Task AddOrUpdateDeviceAsync(InitialDeviceConfig deviceConfig)
        {
            var deviceEnity = new DeviceListEntity
            {
                DeviceId = deviceConfig.DeviceId,
                HostName = deviceConfig.HostName,
                Key = deviceConfig.Key
            };
            var operation = TableOperation.InsertOrReplace(deviceEnity);
            await ExecuteAsync(operation);
        }

        /// <summary>
        /// Deletes a virtual device from storage
        /// </summary>
        /// <param name="deviceId">The deviceId to search for</param>
        /// <returns>true if successfully deleted, false if not found, throws if delete fails</returns>
        public async Task<bool> RemoveDeviceAsync(string deviceId)
        {
            var device = await this.GetDeviceAsync(deviceId);
            if (device != null)
            {
                var operation = TableOperation.Retrieve<DeviceListEntity>(device.DeviceId, device.HostName);
                var result = await ExecuteAsync(operation);

                var deleteDevice = (DeviceListEntity)result.Result;
                if (deleteDevice != null)
                {
                    var deleteOperation = TableOperation.Delete(deleteDevice);
                    await ExecuteAsync(deleteOperation);
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Gets a list of stored virtual devices
        /// </summary>
        /// <returns>List of InitialDeviceConfig used for managing virtual devices</returns>
        public async Task<List<InitialDeviceConfig>> GetDeviceListAsync()
        {
            List<InitialDeviceConfig> devices = new List<InitialDeviceConfig>();
            TableQuery<DeviceListEntity> query = new TableQuery<DeviceListEntity>();
            var devicesResult = await ExecuteQueryAsync(query);
            foreach (var device in devicesResult)
            {
                var deviceConfig = new InitialDeviceConfig
                {
                    HostName = device.HostName,
                    DeviceId = device.DeviceId,
                    Key = device.Key
                };
                devices.Add(deviceConfig);
            }
            return devices;
        }

        #region Private-Methods

        private async Task<InitialDeviceConfig> GetDeviceAsync(TableQuery<DeviceListEntity> query)
        {
            var devicesResult = await ExecuteQueryAsync(query);
            foreach (var device in devicesResult)
            {
                // Always return first device found
                return new InitialDeviceConfig
                {
                    DeviceId = device.DeviceId,
                    HostName = device.HostName,
                    Key = device.Key
                };
            }
            return null;
        } 

        #endregion Private-Methods


    }
}
