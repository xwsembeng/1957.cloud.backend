﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.EventHubs.Processor;
using XWS.BusinessPlatform.Core.Configurations;
using XWS.ComBo.Modules.Services.Interfaces;

namespace XWS.ComBo.EventProcessor.WebJob.Processors.Factory
{
    public class DeviceAdministrationProcessorFactory : IEventProcessorFactory
    {
        private readonly IDeviceService _deviceService;
        private readonly IConfigurationProvider _configurationProvider;

        private readonly ConcurrentDictionary<string, DeviceAdministrationProcessor> _eventProcessors = new ConcurrentDictionary<string, DeviceAdministrationProcessor>();
        private readonly ConcurrentQueue<DeviceAdministrationProcessor> _closedProcessors = new ConcurrentQueue<DeviceAdministrationProcessor>();

        public DeviceAdministrationProcessorFactory(IDeviceService deviceService, IConfigurationProvider configurationProvider)
        {
            _deviceService = deviceService;
            _configurationProvider = configurationProvider;
        }

        /// <summary>
        /// Creates an event processor for every event hub partition
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public IEventProcessor CreateEventProcessor(PartitionContext context)
        {
            var processor = new DeviceAdministrationProcessor(_deviceService, _configurationProvider);
            processor.ProcessorClosed += this.ProcessorOnProcessorClosed;
            this._eventProcessors.TryAdd(context.PartitionId, processor);
            return processor;
        }

        private void ProcessorOnProcessorClosed(object sender, EventArgs eventArgs)
        {
            var processor = sender as DeviceAdministrationProcessor;
            if (processor != null)
            {
                this._eventProcessors.TryRemove(processor.Context.PartitionId, out processor);
                this._closedProcessors.Enqueue(processor);
            }
        }
    }
}
