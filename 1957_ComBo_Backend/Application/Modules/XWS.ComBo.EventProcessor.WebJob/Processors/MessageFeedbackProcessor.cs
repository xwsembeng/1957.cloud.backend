﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.Devices;
using XWS.BusinessPlatform.Core.Configurations;
using XWS.ComBo.EventProcessor.WebJob.Processors.Interfaces;
using XWS.ComBo.Modules.Services.Interfaces;

namespace XWS.ComBo.EventProcessor.WebJob.Processors
{
    public class MessageFeedbackProcessor : IMessageFeedbackProcessor, IDisposable
    {
        private CancellationTokenSource _cancellationTokenSource;

        private readonly IDeviceService _deviceService;
        private readonly IConfigurationProvider _configurationProvider;

        public MessageFeedbackProcessor(IDeviceService deviceService, IConfigurationProvider configurationProvider)
        {
            _configurationProvider = configurationProvider;
            _deviceService = deviceService;
        }

        public void Start(CancellationTokenSource cancellationTokenSource)
        {
            _cancellationTokenSource = cancellationTokenSource;

            Task.Run(
                () => this.StartProcessor(_cancellationTokenSource.Token),
                _cancellationTokenSource.Token);
        }

        private async Task StartProcessor(CancellationToken token)
        {
            if (token == null)
            {
                throw new ArgumentNullException("token");
            }

            ServiceClient serviceClient = null;
            try
            {
                serviceClient =
                    ServiceClient.CreateFromConnectionString(
                        _configurationProvider.GetConfigurationSettingValue("iotHub.ConnectionString"));
                await serviceClient.OpenAsync();

                while (!token.IsCancellationRequested)
                {
                    var batchReceiver = serviceClient.GetFeedbackReceiver();
                    var batch = await batchReceiver.ReceiveAsync(TimeSpan.FromSeconds(10.0));

                    IEnumerable<FeedbackRecord> records;
                    if (batch == null || (records = batch.Records) == null)
                    {
                        continue;
                    }

                    records = records.Where(record => record != null)
                        .Where(x => !string.IsNullOrEmpty(x.DeviceId))
                        .Where(x => !string.IsNullOrEmpty(x.OriginalMessageId));

                    foreach (FeedbackRecord record in records)
                    {
                        UpdateDeviceRecord(record, batch.EnqueuedTime);
                    }

                    await batchReceiver.CompleteAsync(batch);
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError("Error in MessageFeedbackProcessor.RunProcess, Exception: {0}", ex.Message);
            }
            finally
            {
                if (serviceClient != null)
                {
                    await serviceClient.CloseAsync();
                }
            }

        }

        private async void UpdateDeviceRecord(FeedbackRecord record, DateTime enqueuDateTime)
        {
            Trace.TraceInformation(
                "{0}{0}*** Processing Feedback Record ***{0}{0}DeviceId: {1}{0}OriginalMessageId: {2}{0}Result: {3}{0}{0}",
                Console.Out.NewLine,
                record.DeviceId,
                record.OriginalMessageId,
                record.StatusCode);

            var deviceModel = await _deviceService.GetDeviceAsync(record.DeviceId);
            var existingCommand = deviceModel?.CommandHistory.FirstOrDefault(x => x.MessageId == record.OriginalMessageId);
            if (existingCommand == null)
            {
                return;
            }
            var updatedTime = record.EnqueuedTimeUtc;
            if (updatedTime == default(DateTime))
            {
                updatedTime = enqueuDateTime == default(DateTime) ? DateTime.UtcNow : enqueuDateTime;
            }

            existingCommand.UpdatedTime = updatedTime;
            existingCommand.Result = record.StatusCode.ToString();

            if (record.StatusCode == FeedbackStatusCode.Success)
            {
                existingCommand.ErrorMessage = string.Empty;
            }

            await _deviceService.UpdateDeviceAsync(deviceModel);
        }


        #region IDisposable
        private bool _disposed;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                if (_cancellationTokenSource != null)
                {
                    _cancellationTokenSource.Dispose();
                }
            }

            _disposed = true;
        }

        ~MessageFeedbackProcessor()
        {
            Dispose(false);
        }

        #endregion IDisposable
    }
}
