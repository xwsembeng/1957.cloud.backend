﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace XWS.ComBo.EventProcessor.WebJob.Processors.Interfaces
{

    public interface IDeviceEventProcessor
    {
        void Start(CancellationTokenSource cancellationTokenSource);
    }
}
