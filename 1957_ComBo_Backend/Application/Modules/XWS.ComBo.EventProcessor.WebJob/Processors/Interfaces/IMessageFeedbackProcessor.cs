﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace XWS.ComBo.EventProcessor.WebJob.Processors.Interfaces
{
    /// <summary>
    /// Processes the feedback messages caused by calling commands on the devices
    /// </summary>
    public interface IMessageFeedbackProcessor
    {
        void Start(CancellationTokenSource cancellationTokenSource);
    }
}
