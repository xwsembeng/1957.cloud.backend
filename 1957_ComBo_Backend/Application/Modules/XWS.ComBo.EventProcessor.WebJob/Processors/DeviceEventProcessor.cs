﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.EventHubs;
using Microsoft.Azure.EventHubs.Processor;
using XWS.BusinessPlatform.Core.Configurations;
using XWS.ComBo.EventProcessor.WebJob.Processors.Factory;
using XWS.ComBo.EventProcessor.WebJob.Processors.Interfaces;
using XWS.ComBo.Modules.Services.Interfaces;

namespace XWS.ComBo.EventProcessor.WebJob.Processors
{
    public class DeviceEventProcessor : IDeviceEventProcessor, IDisposable
    {
        private CancellationTokenSource _cancellationTokenSource;

        private readonly IDeviceService _deviceService;
        private readonly IConfigurationProvider _configurationProvider;

        private DeviceAdministrationProcessorFactory _factory;
        private EventProcessorHost _eventProcessorHost;

        public DeviceEventProcessor(IDeviceService deviceService, IConfigurationProvider configurationProvider)
        {
            _configurationProvider = configurationProvider;
            _deviceService = deviceService;
        }

        public void Start(CancellationTokenSource cancellationTokenSource)
        {
            //_running = true;
            _cancellationTokenSource = cancellationTokenSource;
            Task.Run(() => this.StartProcessor(cancellationTokenSource.Token), cancellationTokenSource.Token);
        }

        //public void Stop()
        //{
        //    _cancellationTokenSource.Cancel();
        //    TimeSpan timeout = TimeSpan.FromSeconds(30);
        //    TimeSpan sleepInterval = TimeSpan.FromSeconds(1);
        //    while (_running)
        //    {
        //        if (timeout < sleepInterval)
        //        {
        //            break;
        //        }
        //        Thread.Sleep(sleepInterval);
        //    }
        //}

        private async Task StartProcessor(CancellationToken token)
        {
            try
            {
                // Initialize
                _eventProcessorHost = new EventProcessorHost(
                    Environment.MachineName,
                    _configurationProvider.GetConfigurationSettingValue("eventHub.HubName").ToLowerInvariant(),
                    PartitionReceiver.DefaultConsumerGroupName,
                    _configurationProvider.GetConfigurationSettingValue("eventHub.ConnectionString"),
                    _configurationProvider.GetConfigurationSettingValue("eventHub.StorageConnectionString"),
                    _configurationProvider.GetConfigurationSettingValue("eventHub.HubName").ToLowerInvariant());

                _factory = new DeviceAdministrationProcessorFactory(_deviceService, _configurationProvider);
                Trace.TraceInformation("DeviceEventProcessor: Registering host...");
                var options = new EventProcessorOptions();
                options.SetExceptionHandler(OptionsOnExceptionReceived);
                await _eventProcessorHost.RegisterEventProcessorFactoryAsync(_factory, options);

                // processing loop
                while (!token.IsCancellationRequested)
                {
                    Trace.TraceInformation("DeviceEventProcessor: Processing...");
                    await Task.Delay(TimeSpan.FromMinutes(5), token);

                    // Any additional incremental processing can be done here (like checking states, etc).
                }

                // cleanup
                await _eventProcessorHost.UnregisterEventProcessorAsync();
            }
            catch (Exception e)
            {
                Trace.TraceInformation("Error in DeviceEventProcessor.StartProcessor, Exception: {0}", e.Message);
            }
            //_running = false;
        }

        private static void OptionsOnExceptionReceived(ExceptionReceivedEventArgs exceptionReceivedEventArgs)
        {
            Trace.TraceError("Received exception, action: {0}, message: {1}", exceptionReceivedEventArgs.Action, exceptionReceivedEventArgs.Exception);
        }

        #region IDisposable

        private bool _disposed;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                if (_cancellationTokenSource != null)
                {
                    _cancellationTokenSource.Dispose();
                }
            }

            _disposed = true;
        }

        ~DeviceEventProcessor()
        {
            Dispose(false);
        }
        #endregion IDisposable
    }
}
