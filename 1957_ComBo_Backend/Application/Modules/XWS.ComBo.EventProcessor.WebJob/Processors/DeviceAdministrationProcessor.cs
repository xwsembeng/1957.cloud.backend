﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.EventHubs;
using Microsoft.Azure.EventHubs.Processor;
using Newtonsoft.Json;
using XWS.BusinessPlatform.Core.Configurations;
using XWS.ComBo.Common.Factory;
using XWS.ComBo.Exceptions;
using XWS.ComBo.Modules.Domain;
using XWS.ComBo.Modules.Services.Interfaces;

namespace XWS.ComBo.EventProcessor.WebJob.Processors
{
    /// <summary>
    /// Processes the device info sent from the devices
    /// </summary>
    public class DeviceAdministrationProcessor : IEventProcessor
    {
        private readonly IDeviceService _deviceService;
        private readonly IConfigurationProvider _configurationProvider;

        /// <summary>
        /// Offset of the last received message
        /// </summary>
        private int _lastMessageOffset;

        public DeviceAdministrationProcessor(IDeviceService deviceService, IConfigurationProvider configurationProvider)
        {
            _lastMessageOffset = -1;
            _deviceService = deviceService;
            _configurationProvider = configurationProvider;
        }

        public PartitionContext Context { get; private set; }

        public event EventHandler ProcessorClosed;

        public Task OpenAsync(PartitionContext context)
        {
            Trace.TraceInformation("DeviceAdministrationProcessor: Open. Partition : '{0}'", context.PartitionId);
            this.Context = context;

            return Task.CompletedTask;
        }

        public Task CloseAsync(PartitionContext context, CloseReason reason)
        {
            Trace.TraceInformation("DeviceAdministrationProcessor: Close. Partition : '{0}', Reason: '{1}'.", context.PartitionId, reason);
            this.OnProcessorClosed();
            try
            {
                return context.CheckpointAsync();
            }
            catch (Exception ex)
            {
                Trace.TraceError(
                    "{0}{0}*** CheckpointAsync Exception - DeviceAdministrationProcessor.CloseAsync ***{0}{0}{1}{0}{0}",
                    Console.Out.NewLine,
                    ex);

                return Task.CompletedTask;
            }
        }

        public async Task ProcessEventsAsync(PartitionContext context, IEnumerable<EventData> messages)
        {
            Trace.TraceInformation("DeviceAdministrationProcessor: In ProcessEventsAsync. Partition: '{0}'", context.PartitionId);

            foreach (EventData message in messages)
            {
                string jsonString = string.Empty;
                try
                {
                    // Write out message
                    Trace.TraceInformation("DeviceAdministrationProcessor: Offset: {0} - Partition: {1}", message.Body.Offset, context.PartitionId);
                    _lastMessageOffset = message.Body.Offset;

                    jsonString = Encoding.UTF8.GetString(message.Body.Array, message.Body.Offset, message.Body.Count);
                    IList<DeviceModel> results = JsonConvert.DeserializeObject<List<DeviceModel>>(jsonString);
                    if (results != null)
                    {
                        foreach (DeviceModel resultItem in results)
                        {
                            await ProcessEventItem(resultItem);
                        }
                    }
                }
                catch (Exception e)
                {
                    Trace.TraceInformation("DeviceAdministrationProcessor: Error in ProcessEventAsync -- " + e.Message);
                }
            }

            try
            {
                await context.CheckpointAsync();
            }
            catch (Exception ex)
            {
                Trace.TraceError(
                    "{0}{0}*** CheckpointAsync Exception - DeviceAdministrationProcessor.ProcessEventsAsync ***{0}{0}{1}{0}{0}",
                    Console.Out.NewLine,
                    ex);
            }
        }

        public Task ProcessErrorAsync(PartitionContext context, Exception error)
        {
            Trace.TraceError($"Error on Partition: {context.PartitionId}, Error: {error.Message}");
            return Task.CompletedTask;
        }

        #region Private-Methods


        private async Task ProcessEventItem(DeviceModel eventData)
        {
            if (eventData == null || eventData.ObjectType == null)
            {
                Trace.TraceWarning("Event has no ObjectType defined.  No action was taken on Event packet.");
                return;
            }

            string objectType = eventData.ObjectType;

            var objectTypePrefix = _configurationProvider.GetConfigurationSettingValue("ObjectTypePrefix");
            if (string.IsNullOrWhiteSpace(objectTypePrefix))
            {
                objectTypePrefix = "";
            }
            if (objectType == objectTypePrefix + SampleDeviceFactory.OBJECT_TYPE_DEVICE_INFO)
            {
                await ProcessDeviceInfo(eventData);
            }
            else
            {
                Trace.TraceWarning("Unknown ObjectType in event.");
            }
        }

        private async Task ProcessDeviceInfo(DeviceModel deviceModel)
        {
            if (deviceModel.Twin == null)
            {
                throw new DeviceRequiredTwinNotFoundException("Device Twin is missing");
            }
            string deviceId = deviceModel.Twin.DeviceId;
            Trace.TraceInformation("ProcessEventAsync -- DeviceInfo: {0}", deviceId);
            await _deviceService.UpdateDeviceByDeviceInfoPacketAsync(deviceModel);

            // Pick the task object rather than using await, since there is no need to wait until cache updated
            var task = _deviceService.AddToNameCache(deviceModel.Twin.DeviceId);
        }

        private void OnProcessorClosed()
        {
            ProcessorClosed?.Invoke(this, EventArgs.Empty);
        } 

        #endregion Private-Methods
    }
}
