﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Core.InversionOfControl;
using XWS.ComBo.EventProcessor.WebJob.Processors.Interfaces;

namespace XWS.ComBo.EventProcessor.WebJob
{
    public static class Program
    {
        private static CancellationTokenSource CancellationTokenSource = new CancellationTokenSource();

        private const string SHUTDOWN_FILE_ENV_VAR = "WEBJOBS_SHUTDOWN_FILE";
        private static string _shutdownFile;

        static void Main(string[] args)
        {
            try
            {
                // Cloud deploys often get staged and started to warm them up, then get a shutdown
                // signal from the framework before being moved to the production slot. We don't want 
                // to start initializing data if we have already gotten the shutdown message, so we'll 
                // monitor it. This environment variable is reliable
                // http://blog.amitapple.com/post/2014/05/webjobs-graceful-shutdown/#.VhVYO6L8-B4
                _shutdownFile = Environment.GetEnvironmentVariable(SHUTDOWN_FILE_ENV_VAR);
                bool shutdownSignalReceived = false;

                // Setup a file system watcher on that file's directory to know when the file is created
                // First check for null, though. This does not exist on a localhost deploy, only cloud
                if (!string.IsNullOrWhiteSpace(_shutdownFile))
                {
                    var fileSystemWatcher = new FileSystemWatcher(Path.GetDirectoryName(_shutdownFile));
                    fileSystemWatcher.Created += OnShutdownFileChanged;
                    fileSystemWatcher.Changed += OnShutdownFileChanged;
                    fileSystemWatcher.NotifyFilter = NotifyFilters.CreationTime | NotifyFilters.FileName | NotifyFilters.LastWrite;
                    fileSystemWatcher.IncludeSubdirectories = false;
                    fileSystemWatcher.EnableRaisingEvents = true;

                    // In case the file had already been created before we started watching it.
                    if (File.Exists(_shutdownFile))
                    {
                        shutdownSignalReceived = true;
                    }
                }

                if (!shutdownSignalReceived)
                {
                    ConfigureContainer();

                    StartEventProcessorHost();
                    //StartActionProcessorHost();
                    StartMessageFeedbackProcessorHost();

                    RunAsync().Wait();
                }
            }
            catch (Exception ex)
            {
                CancellationTokenSource.Cancel();
                Trace.TraceError("Webjob terminating: {0}", ex);
            }
        }

        private static void OnShutdownFileChanged(object sender, FileSystemEventArgs e)
        {
            if (e.FullPath.IndexOf(Path.GetFileName(_shutdownFile), StringComparison.OrdinalIgnoreCase) >= 0)
            {
                CancellationTokenSource.Cancel();
            }
        }

        private static void ConfigureContainer()
        {
            IoC.Configure("EventProcessor_Services.config");
        }

        private static void StartEventProcessorHost()
        {
            Trace.TraceInformation("Starting Event Processor");
            var eventProcessor = IoC.Resolve<IDeviceEventProcessor>();
            eventProcessor.Start(CancellationTokenSource);
        }

        private static void StartMessageFeedbackProcessorHost()
        {
            Trace.TraceInformation("Starting command feedback processor");
            var feedbackProcessor = IoC.Resolve<IMessageFeedbackProcessor>();
            feedbackProcessor.Start(CancellationTokenSource);
        }

        private static async Task RunAsync()
        {
            while (!CancellationTokenSource.Token.IsCancellationRequested)
            {
                Trace.TraceInformation("Running");
                try
                {
                    await Task.Delay(TimeSpan.FromMinutes(5), CancellationTokenSource.Token);
                }
                catch (TaskCanceledException) { }
            }
        }
    }
}
