﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.ComBo.Modules.Repositories.Interfaces
{
    public interface IDocumentDBClient<T> where T: new()
    {

        /// <summary>
        /// Returns a <see cref="IQueryable{T}"/> that can be used to query db.
        /// </summary>
        Task<IQueryable<T>> QueryAsync();

        /// <summary>
        /// Deletes a document from the db.
        /// </summary>
        /// <param name="id">The id of the document to delete</param>
        Task DeleteAsync(string id);

        /// <summary>
        /// Saves a document to the the db.
        /// </summary>
        /// <param name="data">The data of the document to save.</param>
        Task<T> SaveAsync(T data);
    }
}
