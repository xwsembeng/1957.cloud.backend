﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.ComBo.Modules.Domain;
using XWS.ComBo.Modules.Domain.Enums;

namespace XWS.ComBo.Modules.Repositories.Interfaces
{
    /// <summary>
    /// Interface for working with persistence of Device Twin (Tag and Property)
    /// and Method list defined for all devices. Note that we store Twin and
    /// Method list in the same table in separate columns. The list maintain
    /// a maximum set of unique tag names, property names and method names.
    /// </summary>
    public interface INameCacheRepository
    {
        /// <summary>
        /// Add a set of names in batch
        /// Reminder: Considering it will be called periodically to update the cache, it was
        /// not designed as a realiable routine to reduce complexity
        /// </summary>
        /// <param name="entityType">Type of adding names</param>
        /// <param name="names">Names to be added</param>
        /// <returns>The asychornize task</returns>
        Task AddNamesAsync(NameCacheEntityType entityType, IEnumerable<string> names);

        /// <summary>
        /// Save a new name into the repository. Update it if it already exists.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="nameCacheEntity"></param>
        /// <returns>true if succeeded, false if failed.</returns>
        Task<bool> AddNameAsync(NameCacheEntityType entityType, NameCacheEntity nameCacheEntity);

        /// <summary>
        /// Get name list for combined NameCacheEntityType flags
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns>a list of combined names</returns>
        Task<IEnumerable<NameCacheEntity>> GetNameListAsync(NameCacheEntityType entityType);
    }
}
