﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Shared;
using XWS.ComBo.Modules.Domain.FilterObjects;

namespace XWS.ComBo.Modules.Repositories.Interfaces
{
    /// <summary>
    /// Interface to expose methods that can be called against the underlying identity repository
    /// </summary>
    public interface IIoTHubDeviceManager
    {
        Task<IEnumerable<Twin>> QueryDevicesAsync(ListDevicesFilter filter, int maxDevices = 10000);
        Task CloseAsyncService();
        Task<Device> GetDeviceAsync(string deviceId);
        Task RemoveDeviceAsync(string deviceId);
        Task<Device> AddDeviceAsync(Device device);
        Task<Twin> UpdateTwinAsync(string deviceId, Twin twin);
        Task<Twin> GetTwinAsync(string deviceId);
        Task<Device> UpdateDeviceAsync(Device device);
        Task SendAsync(string deviceId, Message message);
        Task CloseAsyncDevice();
        Task<CloudToDeviceMethodResult> InvokeDeviceMethodAsync(string deviceId, CloudToDeviceMethod method);
    }
}
