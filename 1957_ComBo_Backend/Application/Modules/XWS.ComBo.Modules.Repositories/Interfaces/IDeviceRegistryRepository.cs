﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.ComBo.Modules.Domain;
using XWS.ComBo.Modules.Domain.Enums;
using XWS.ComBo.Modules.Domain.FilterObjects;
using XWS.ComBo.Modules.Domain.FilterResult;

namespace XWS.ComBo.Modules.Repositories.Interfaces
{
    public interface IDeviceRegistryRepository
    {
        /// <summary>
        /// Gets a list of type Device depending on search parameters, sort column, sort direction,
        /// starting point, and filters.
        /// </summary>
        /// <param name="filter">The device filter.</param>
        /// <returns></returns>
        Task<ListDevicesFilterResult> GetDevicesAsync(ListDevicesFilter filter);

        /// <summary>
        /// Removes a device asynchronously.
        /// </summary>
        /// <param name="deviceId">The device identifier.</param>
        /// <returns></returns>
        Task RemoveDeviceAsync(string deviceId);

        /// <summary>
        /// Queries the DocumentDB and retrieves the device based on its deviceId
        /// </summary>
        /// <param name="deviceId">DeviceID of the device to retrieve</param>
        /// <returns>Device instance if present, null if a device was not found with the provided deviceId</returns>
        Task<DeviceModel> GetDeviceAsync(string deviceId);

        /// <summary>
        /// Adds a device asynchronously.
        /// </summary>
        /// <param name="device">The device.</param>
        /// <returns></returns>
        Task<DeviceModel> AddDeviceAsync(DeviceModel device);

        /// <summary>
        /// Updates an existing device in the DocumentDB
        /// Throws a DeviceNotRegisteredException is the device does not already exist in the DocumentDB
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        Task<DeviceModel> UpdateDeviceAsync(DeviceModel device);

        Task<DeviceModel> UpdateHubEnabledStateAsync(string deviceId, bool isEnabled);

        /// <summary>
        /// Gets the devices whose device id is part of the given device id
        /// </summary>
        /// <param name="deviceId">The id of the device or a part of it</param>
        /// <param name="hubEnabledState">The state of the device</param>
        /// <returns></returns>
        Task<IList<DeviceModel>> GetDevicesAsync(string deviceId, HubEnabledState? hubEnabledState = null);
    }
}
