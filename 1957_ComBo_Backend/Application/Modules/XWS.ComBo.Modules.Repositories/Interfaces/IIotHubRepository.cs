﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Shared;
using XWS.ComBo.Modules.Domain;
using XWS.ComBo.Modules.Domain.Commands;

namespace XWS.ComBo.Modules.Repositories.Interfaces
{
    /// <summary>
    /// Interface to expose methods that can be called against the underlying identity repository
    /// </summary>
    public interface IIotHubRepository
    {
        /// <summary>
        /// Gets a device from the iot hub's device registry
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        Task<Device> GetIotHubDeviceAsync(string deviceId);
        Task RemoveDeviceAsync(string deviceId);

        /// <summary>
        ///     Attempts to add the device as a new device and swallows all exceptions
        /// </summary>
        /// <param name="oldIotHubDevice">The IoT Hub Device to add back into the IoT Hub</param>
        /// <returns>true if the device was added successfully, false if there was a problem adding the device</returns>
        Task<bool> TryAddDeviceAsync(Device oldIotHubDevice);

        /// <summary>
        /// Adds the provided device to the IoT hub with the provided security keys
        /// </summary>
        /// <param name="device"></param>
        /// <param name="securityKeys"></param>
        /// <returns></returns>
        Task<DeviceModel> AddDeviceAsync(DeviceModel device, SecurityKeys securityKeys);

        /// <summary>
        /// Attempts to remove the device from the IoT Hub and eats any exceptions that are thrown during the
        /// delete process.
        /// </summary>
        /// <param name="deviceId">ID of the device to remove</param>
        /// <returns>true if the remove was successful and false if the remove was not successful</returns>
        Task<bool> TryRemoveDeviceAsync(string deviceId);

        Task<SecurityKeys> GetDeviceKeysAsync(string deviceId);
        Task<Device> UpdateHubEnabledStateAsync(string deviceId, bool isEnabled);

        Task SendCommand(string deviceId, CommandHistory commandHistory);
    }
}
