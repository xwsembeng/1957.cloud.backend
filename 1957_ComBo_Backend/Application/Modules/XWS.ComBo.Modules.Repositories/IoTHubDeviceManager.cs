﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Shared;
using XWS.BusinessPlatform.Core.Configurations;
using XWS.ComBo.Modules.Domain.FilterObjects;
using XWS.ComBo.Modules.Repositories.Interfaces;

namespace XWS.ComBo.Modules.Repositories
{
    /// <summary>
    ///     Wraps calls to the IoT hub identity store.
    ///     IDisposable is implemented in order to close out the connection to the IoT Hub when this object is no longer in use
    /// </summary>
    public class IoTHubDeviceManager : IIoTHubDeviceManager, IDisposable
    {
        private readonly RegistryManager _deviceManager;
        private readonly ServiceClient _serviceClient;

        private bool _disposed;

      
        public IoTHubDeviceManager(IConfigurationProvider configurationProvider)
        {
            // Temporary code to bypass https cert validation till DNS on IotHub is configured
            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, errors) => true;
            var iotHubConnectionString = configurationProvider.GetConfigurationSettingValue("iotHub.ConnectionString");
            this._deviceManager = RegistryManager.CreateFromConnectionString(iotHubConnectionString);
            this._serviceClient = ServiceClient.CreateFromConnectionString(iotHubConnectionString);
        }

        public async Task<IEnumerable<Twin>> QueryDevicesAsync(ListDevicesFilter filter, int maxDevices = 10000)
        {
            var sqlQuery = filter.GetSQLQuery();
            var deviceQuery = _deviceManager.CreateQuery(sqlQuery);

            var twins = new List<Twin>();
            while (deviceQuery.HasMoreResults && twins.Count < maxDevices)
            {
                twins.AddRange(await deviceQuery.GetNextAsTwinAsync());
            }

            return twins.Take(maxDevices);
        }

        public async Task<Device> AddDeviceAsync(Device device)
        {
            return await _deviceManager.AddDeviceAsync(device);
        }

        public async Task<Device> UpdateDeviceAsync(Device device)
        {
            return await _deviceManager.UpdateDeviceAsync(device);
        }

        public async Task RemoveDeviceAsync(string deviceId)
        {
            await _deviceManager.RemoveDeviceAsync(deviceId);
        }

        public async Task<Device> GetDeviceAsync(string deviceId)
        {
            if(!string.IsNullOrWhiteSpace(deviceId))
                return await _deviceManager.GetDeviceAsync(deviceId);
            return null;
        }

        public async Task<Twin> GetTwinAsync(string deviceId)
        {
            return await _deviceManager.GetTwinAsync(deviceId);
        }

        public async Task<Twin> UpdateTwinAsync(string deviceId, Twin twin)
        {
            return await _deviceManager.UpdateTwinAsync(deviceId, twin, twin.ETag);
        }

        public async Task SendAsync(string deviceId, Message message)
        {
            await this._serviceClient.SendAsync(deviceId, message);
        }

        public async Task<CloudToDeviceMethodResult> InvokeDeviceMethodAsync(string deviceId, CloudToDeviceMethod method)
        {
            return await this._serviceClient.InvokeDeviceMethodAsync(deviceId, method);
        }

        public async Task CloseAsyncService()
        {
            await _deviceManager.CloseAsync();
        }

        public async Task CloseAsyncDevice()
        {
            await this._serviceClient.CloseAsync();
        }
        #region IDispose
        /// <summary>
        ///     Implement the IDisposable interface in order to close the device manager
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this._disposed)
            {
                return;
            }

            if (disposing)
            {
                _deviceManager?.CloseAsync().Wait();
            }

            this._disposed = true;
        }

        ~IoTHubDeviceManager()
        {
            this.Dispose(false);
        }
        #endregion
    }
}
