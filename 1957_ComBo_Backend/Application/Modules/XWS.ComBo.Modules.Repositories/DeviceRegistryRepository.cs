﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Documents;
using XWS.ComBo.Common.Extensions;
using XWS.ComBo.Exceptions;
using XWS.ComBo.Modules.Domain;
using XWS.ComBo.Modules.Domain.Enums;
using XWS.ComBo.Modules.Domain.FilterObjects;
using XWS.ComBo.Modules.Domain.FilterResult;
using XWS.ComBo.Modules.Repositories.Interfaces;
using XWS.Localization;

namespace XWS.ComBo.Modules.Repositories
{
    /// <summary>
    /// Wraps calls to the DocumentDB Device Registry and the IoT hub identity store.
    /// </summary>
    public abstract class DeviceRegistryRepository : IDeviceRegistryRepository
    {
        protected readonly IDocumentDBClient<DeviceModel> _documentClient;

        public DeviceRegistryRepository(IDocumentDBClient<DeviceModel> documentClient)
        {
            _documentClient = documentClient;
        }

        public virtual async Task<ListDevicesFilterResult> GetDevicesAsync(ListDevicesFilter filter)
        {
            List<DeviceModel> deviceList = await this.GetAllDevicesAsync();

            //TODO: Implement querying the filtered devices from DocumentDB if necessary
            //IQueryable<DeviceModel> filteredDevices = FilterHelper.FilterDeviceList(deviceList.AsQueryable<DeviceModel>(), filter.Clauses);

            //IQueryable<DeviceModel> filteredAndSearchedDevices = this.SearchDeviceList(filteredDevices, filter.SearchQuery);

            //IQueryable<DeviceModel> sortedDevices = this.SortDeviceList(filteredAndSearchedDevices, filter.SortColumn, filter.SortOrder);

            //List<DeviceModel> pagedDeviceList = sortedDevices.Skip(filter.Skip).Take(filter.Take).ToList();

            //int filteredCount = filteredAndSearchedDevices.Count();

            return new ListDevicesFilterResult
            {
                Results = deviceList
            };
        }

        /// <summary>
        /// Adds a device to the DocumentDB.
        /// Throws a DeviceAlreadyRegisteredException if a device already exists in the database with the provided deviceId
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        public virtual async Task<DeviceModel> AddDeviceAsync(DeviceModel device)
        {
            if (device == null)
            {
                throw new ArgumentNullException("device");
            }

            if (string.IsNullOrEmpty(device.id))
            {
                device.id = Guid.NewGuid().ToString();
            }
            return await _documentClient.SaveAsync(device);
        }

        /// <summary>
        /// Updates an existing device in the DocumentDB.
        /// Throws a DeviceNotRegisteredException is the device does not already exist in the DocumentDB
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        public virtual async Task<DeviceModel> UpdateDeviceAsync(DeviceModel device)
        {
            if (device == null)
            {
                throw new ArgumentNullException("device");
            }

            if (device.Twin == null)
            {
                throw new DeviceRequiredTwinNotFoundException(
                    StaticResourceManager.GetString("Error", "DeviceRequiredTwinNotFoundMessage"));
            }

            if (string.IsNullOrWhiteSpace(device.Twin.DeviceId))
            {
                throw new DeviceRequiredTwinNotFoundException(
                    StaticResourceManager.GetString("Error", "DeviceRequiredTwinDeviceIdNotFoundMessage"));
            }

            DeviceModel existingDevice = await GetDeviceAsync(device.Twin.DeviceId);
            if (existingDevice == null)
            {
                throw new DeviceNotRegisteredException(device.Twin.DeviceId);
            }

            string incomingRid = device._rid ?? "";

            if (string.IsNullOrWhiteSpace(incomingRid))
            {
                // copy the existing _rid onto the incoming data if needed
                var existingRid = existingDevice._rid ?? "";
                if (string.IsNullOrWhiteSpace(existingRid))
                {
                    throw new InvalidOperationException("Could not find _rid property on existing device");
                }
                device._rid = existingRid;
            }

            string incomingId = device.id ?? "";
          
            if (string.IsNullOrWhiteSpace(incomingId))
            {
                // copy the existing id onto the incoming data if needed
                var existingId = existingDevice.id ?? "";
                if (string.IsNullOrWhiteSpace(existingId))
                {
                    throw new InvalidOperationException("Could not find id property on existing device");
                }
                device.id = existingId;
            }

            //device.DeviceProperties.UpdatedTime = DateTime.UtcNow;
            //DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeSeconds(device._ts);
            //DateTime dateTime = dateTimeOffset.UtcDateTime.ToLocalTime();
            var savedDevice = await this._documentClient.SaveAsync(device);
            return savedDevice;
        }

        public virtual async Task<DeviceModel> UpdateHubEnabledStateAsync(string deviceId, bool isEnabled)
        {
            if (string.IsNullOrEmpty(deviceId))
            {
                throw new ArgumentNullException("deviceId");
            }

            DeviceModel existingDevice = await this.GetDeviceAsync(deviceId);

            if (existingDevice == null)
            {
                throw new DeviceNotRegisteredException(deviceId);
            }

            if (existingDevice.Twin == null)
            {
                throw new DeviceRequiredTwinNotFoundException(StaticResourceManager.GetString("Error", "DeviceRequiredTwinNotFoundMessage"));
            }

            existingDevice.Twin.Tags["HubEnabledState"] = isEnabled ? HubEnabledState.Running.ToString() : HubEnabledState.Disabled.ToString();

            var savedDevice = await this._documentClient.SaveAsync(existingDevice);
            return savedDevice;
        }


        /// <summary>
        /// Removes a device asynchronously.
        /// </summary>
        /// <param name="deviceId">The device identifier.</param>
        /// <returns></returns>
        public async Task RemoveDeviceAsync(string deviceId)
        {
            if (string.IsNullOrEmpty(deviceId))
            {
                throw new ArgumentNullException("deviceId");
            }

            DeviceModel existingDevice = await GetDeviceAsync(deviceId);
            if (existingDevice == null)
            {
                throw new DeviceNotRegisteredException(deviceId);
            }

            await _documentClient.DeleteAsync(existingDevice.id);
        }

        /// <summary>
        /// Queries the DocumentDB and retrieves the device based on its deviceId
        /// </summary>
        /// <param name="deviceId">DeviceID of the device to retrieve</param>
        /// <returns>Device instance if present, null if a device was not found with the provided deviceId</returns>
        public virtual async Task<DeviceModel> GetDeviceAsync(string deviceId)
        {
            if (string.IsNullOrEmpty(deviceId))
            {
                throw new ArgumentException(deviceId);
            }

            var query = await _documentClient.QueryAsync();
            return query.AsEnumerable().FirstOrDefault(x => x.Twin.Get("deviceId") == deviceId);
            
        }

        /// <summary>
        /// Gets the devices whose device id is part of the given device id
        /// </summary>
        /// <param name="deviceId">The id of the device or a part of it</param>
        /// <param name="hubEnabledState"></param>
        /// <returns></returns>
        public async Task<IList<DeviceModel>> GetDevicesAsync(string deviceId, HubEnabledState? hubEnabledState = null)
        {
            var query = await _documentClient.QueryAsync();
            IEnumerable<DeviceModel>deviceModels = query.AsEnumerable().Where(x => ((string) x.Twin.Get("deviceId")).ToLower()
                .Contains(deviceId.ToLower()));
            if(hubEnabledState != null)
                deviceModels = deviceModels.Where(x => (HubEnabledState) x.Twin.Tags["HubEnabledState"] == hubEnabledState);
            return deviceModels.ToList();
        }

        #region Private-Methods

        /// <summary>
        /// Queries the DocumentDB and retrieves all documents in the collection
        /// </summary>
        /// <returns>All documents in the collection</returns>
        private async Task<List<DeviceModel>> GetAllDevicesAsync()
        {
            var devices = await _documentClient.QueryAsync();
            return devices.ToList();
        } 

        #endregion Private-Methods
    }
}
