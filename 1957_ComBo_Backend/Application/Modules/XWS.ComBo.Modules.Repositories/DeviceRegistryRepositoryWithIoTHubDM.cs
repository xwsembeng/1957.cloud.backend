﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Devices.Shared;
using XWS.ComBo.Common.Extensions;
using XWS.ComBo.Modules.Domain;
using XWS.ComBo.Modules.Domain.Enums;
using XWS.ComBo.Modules.Domain.FilterObjects;
using XWS.ComBo.Modules.Domain.FilterResult;
using XWS.ComBo.Modules.Repositories.Interfaces;

namespace XWS.ComBo.Modules.Repositories
{
    /// <summary>
    /// Wraps calls to the Device Managment of the IoT Hub
    /// </summary>
    public class DeviceRegistryRepositoryWithIoTHubDM : DeviceRegistryRepository
    {
        private readonly IIoTHubDeviceManager _deviceManager;

        public DeviceRegistryRepositoryWithIoTHubDM(IDocumentDBClient<DeviceModel> documentClient, IIoTHubDeviceManager deviceManager) :
            base(documentClient)
        {
            _deviceManager = deviceManager;
        }

        public override async Task<ListDevicesFilterResult> GetDevicesAsync(ListDevicesFilter filter)
        {
            // Kick-off DocDB query initializing
            var queryTask = await this._documentClient.QueryAsync();
            
            // Considering all the device properties was copied to IoT Hub twin as tag or
            // reported property, we will only query on the IoT Hub twins. The DocumentDB
            // will not be touched.
            var filteredDevices = (await this._deviceManager.QueryDevicesAsync(filter)).ToList();

            //var sortedDevices = this.SortDeviceList(filteredDevices.AsQueryable(), filter.SortColumn, filter.SortOrder);

            //var pagedDeviceList = sortedDevices.Skip(filter.Skip).Take(filter.Take).ToList();

            // Query on DocDB for traditional device properties, commands and so on
            //var deviceIds = pagedDeviceList.Select(twin => twin.DeviceId);
            var deviceIds = filteredDevices.Select(twin => twin.DeviceId.ToLower()).ToList();
            var devicesFromDocDB = queryTask.AsEnumerable().Where(x => deviceIds.Contains(((string)x.Twin.Get("deviceId")).ToLower()))
                .ToDictionary(d => d.Twin.Get("deviceId"));

            return new ListDevicesFilterResult
            {
                Results = filteredDevices.Select(twin =>
                {
                    DeviceModel deviceModel;
                    if (devicesFromDocDB.TryGetValue(twin.DeviceId, out deviceModel))
                    {
                        //deviceModel.Twin = twin;
                        return deviceModel;
                    }
                    return null;
                }).Where(model => model != null).ToList()
            };
        }

        /// <summary>
        /// Updates the device inside the IoT-Hub and inside the device registry
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        public override async Task<DeviceModel> UpdateDeviceAsync(DeviceModel device)
        {
            // Update the twin if it was changed comparing to the one just retrieved from IoT Hub
            if (device.Twin != null)
            {
                var iotHubTwin = await this._deviceManager.GetTwinAsync(device.Twin.DeviceId);
                if (device.Twin.UpdateRequired(iotHubTwin))
                {
                    //Assign updated twin for the device registry to avoid null-values inside the device registry
                    device.Twin = await this._deviceManager.UpdateTwinAsync(device.Twin.DeviceId, device.Twin);
                }

            }
            var result = await base.UpdateDeviceAsync(device);

            return result;
        }

        public override async Task<DeviceModel> UpdateHubEnabledStateAsync(string deviceId, bool isEnabled)
        {
            var result = await base.UpdateHubEnabledStateAsync(deviceId, isEnabled);

            // Update the twin: set status
            await SetHubEnabledStateTag(deviceId, isEnabled);

            return result;
        }

        #region Private-Methods

        private async Task SetHubEnabledStateTag(string deviceId, bool isEnabled)
        {
            var twin = new Twin(deviceId)
            {
                ETag = "*",
                Tags = { ["HubEnabledState"] = isEnabled ? HubEnabledState.Running.ToString() : HubEnabledState.Disabled.ToString() }
            };
            await this._deviceManager.UpdateTwinAsync(deviceId, twin);
        } 

        #endregion Private-Methods
    }

}
