﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using XWS.BusinessPlatform.Core.Configurations;
using XWS.ComBo.Common.Helpers;
using XWS.ComBo.Common.Interfaces;
using XWS.ComBo.Common.Repositories;
using XWS.ComBo.Modules.Domain;
using XWS.ComBo.Modules.Domain.Commands;
using XWS.ComBo.Modules.Domain.Enums;
using XWS.ComBo.Modules.Repositories.Interfaces;

namespace XWS.ComBo.Modules.Repositories
{
    /// <summary>
    /// Class for working with persistence of Device Twin (Tag and Property)
    /// and Method list defined for all devices. Note that we store Twin and
    /// Method list in the same table in separate columns. The list maintain
    /// a maximum set of unique tag names, property names and method names.
    /// </summary>
    public class NameCacheRepository : AzureTableStorageClient, INameCacheRepository
    {
        public int MaxBatchSize => 100;

        public NameCacheRepository(IConfigurationProvider configurationProvider)
        {
            var storageAccount = CloudStorageAccount.Parse(configurationProvider.GetConfigurationSettingValue("device.StorageConnectionString"));
            TableClient = storageAccount.CreateCloudTableClient();
            TableName = configurationProvider.GetConfigurationSettingValueOrDefault("NameCacheTableName", "NameCacheList");
        }

        /// <summary>
        /// Add a set of names in batch
        /// Reminder: Considering it will be called periodically to update the cache, it was
        /// not designed as a realiable routine to reduce complexity
        /// </summary>
        /// <param name="entityType">Type of adding names</param>
        /// <param name="names">Names to be added</param>
        /// <returns>The asychornize task</returns>
        public async Task AddNamesAsync(NameCacheEntityType entityType, IEnumerable<string> names)
        {
            CheckSingleEntityType(entityType);

            var operations = names.Select(name => TableOperation.InsertOrReplace(new NameCacheTableEntity(entityType, name)
            {
                MethodParameters = "null",  // [WORKAROUND] Existing code requires "null" rather than null for tag or properties
                ETag = "*"
            }));

            while (operations.Any())
            {
                var batch = new TableBatchOperation();

                operations.Take(MaxBatchSize).ToList().ForEach(op => batch.Add(op));
                await ExecuteBatchAsync(batch);

                operations = operations.Skip(MaxBatchSize);
            }
        }

        /// <summary>
        /// Save a new name into the repository. Update it if it already exists.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="nameCacheEntity"></param>
        /// <returns>true if succeeded, false if failed.</returns>
        public async Task<bool> AddNameAsync(NameCacheEntityType entityType, NameCacheEntity nameCacheEntity)
        {
            CheckSingleEntityType(entityType);
            NameCacheTableEntity tableEntity = new NameCacheTableEntity(entityType, nameCacheEntity.Name)
            {
                MethodParameters = JsonConvert.SerializeObject(nameCacheEntity.CommandParameters),
                MethodDescription = nameCacheEntity.Description,
                ETag = "*"
            };
            var result = await DoTableInsertOrReplaceAsync(tableEntity, BuildNameCacheFromTableEntity);
            return result.Status == TableStorageResponseStatus.Successful;
        }

        /// <summary>
        /// Get name list for combined NameCacheEntityType flags
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns>a list of combined names</returns>
        public async Task<IEnumerable<NameCacheEntity>> GetNameListAsync(NameCacheEntityType entityType)
        {
            List<string> filters = new List<string>();
            var flags = Enum.GetValues(typeof(NameCacheEntityType));
            foreach (NameCacheEntityType flag in flags)
            {
                //Combined NameCacheEntityTypes are not stored inside NameCacheList => No filter necessary
                if (entityType.HasFlag(flag) && flag != NameCacheEntityType.All && flag != NameCacheEntityType.Property)
                {
                    var condition = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, flag.ToString());
                    filters.Add(condition);
                }
            }

            TableQuery<NameCacheTableEntity> query = new TableQuery<NameCacheTableEntity>().Where(string.Join(" or ", filters));
            var entities = await ExecuteQueryAsync(query);
            return entities.OrderBy(e => e.PartitionKey).ThenBy(e => e.RowKey).
                Select(e => new NameCacheEntity
                {
                    Name = e.Name,
                    CommandParameters = JsonConvert.DeserializeObject<List<CommandParameter>>(e.MethodParameters),
                    Description = e.MethodDescription,
                });
        }


        #region Private-methods
        /// <summary>
        /// Checks if rthe entity type is not a compound entity type
        /// </summary>
        /// <param name="entityType"></param>
        private void CheckSingleEntityType(NameCacheEntityType entityType)
        {
            if (entityType == NameCacheEntityType.DeviceInfo
                || entityType == NameCacheEntityType.Tag
                || entityType == NameCacheEntityType.DesiredProperty
                || entityType == NameCacheEntityType.ReportedProperty
                || entityType == NameCacheEntityType.Method)
                return;
            throw new ArgumentException("Can only pick up one of the flags: DeviceInfo, Tag, DesiredProperty, ReportedProperty, Method");
        }

        private NameCacheEntity BuildNameCacheFromTableEntity(NameCacheTableEntity tableEntity)
        {
            if (tableEntity == null)
            {
                return null;
            }

            var commandParameters = new List<CommandParameter>();
            try
            {
                commandParameters = JsonConvert.DeserializeObject<List<CommandParameter>>(tableEntity.MethodParameters);
            }
            catch (Exception)
            {
                Trace.TraceError("Failed to deserialize object for method parameters: {0}", tableEntity.MethodParameters);
            }

            var nameCacheEntity = new NameCacheEntity
            {
                Name = tableEntity.Name,
                CommandParameters = commandParameters,
                Description = tableEntity.MethodDescription,
            };

            return nameCacheEntity;
        }

        #endregion Private-methods
    }
}
