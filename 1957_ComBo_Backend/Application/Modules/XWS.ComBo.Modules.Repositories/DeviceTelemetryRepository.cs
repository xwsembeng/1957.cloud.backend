﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using NHibernate.Linq;
using XWS.BusinessPlatform.Core.Configurations;
using XWS.BusinessPlatform.Repositories;
using XWS.ComBo.Common.Helpers;
using XWS.ComBo.Common.Repositories;
using XWS.ComBo.Modules.Domain;
using XWS.ComBo.Modules.Repositories.Interfaces;

namespace XWS.ComBo.Modules.Repositories
{

    using StrDict = IDictionary<string, string>;

    /// <summary>
    /// A repository for Device telemetry data.
    /// </summary>
    public class DeviceTelemetryRepository : BlobStorageClient, IDeviceTelemetryRepository
    {
        private readonly string _telemetryDataPrefix;

        /// <summary>
        /// Initializes a new instance of the DeviceTelemetryRepository class.
        /// </summary>
        /// <param name="configurationProvider">
        /// The IConfigurationProvider implementation with which to initialize 
        /// the new instance.
        /// </param>
        public DeviceTelemetryRepository(IConfigurationProvider configurationProvider)
        {
            var storageAccount = CloudStorageAccount.Parse(configurationProvider.GetConfigurationSettingValue("device.StorageConnectionString"));
            BlobClient = storageAccount.CreateCloudBlobClient();
            ContainerName = configurationProvider.GetConfigurationSettingValue("TelemetryStoreContainerName");
            _telemetryDataPrefix = configurationProvider.GetConfigurationSettingValue("TelemetryDataPrefix");
        }
        /// <summary>
        /// Loads the most recent Device telemetry.
        /// </summary>
        /// <param name="deviceId">
        /// The ID of the Device for which telemetry should be returned.
        /// </param>
        /// <param name="telemetry">The different types of telemetry data</param>
        /// <param name="minTime">
        /// The minimum time of record of the telemetry that should be returned.
        /// </param>
        /// <returns>
        /// Telemetry for the Device specified by deviceId, inclusively since 
        /// minTime.
        /// </returns>
        public async Task<IEnumerable<DeviceTelemetryModel>> LoadLatestDeviceTelemetryAsync(string deviceId,
            IList<Telemetry> telemetry, DateTime minTime)
        {
            IEnumerable<DeviceTelemetryModel> result = new DeviceTelemetryModel[0];

            var telemetryBlobReader = await GetReader(_telemetryDataPrefix, minTime);
            foreach (var telemetryStream in telemetryBlobReader)
            {
                //List which represents the telemetry data of the different blobs 
                IEnumerable<DeviceTelemetryModel> deviceTelemetryModels;
                try
                {
                    deviceTelemetryModels = LoadBlobTelemetryModels(telemetryStream.Data, telemetry);
                }
                catch
                {
                    continue;
                }

                if (deviceTelemetryModels == null)
                {
                    break;
                }

                int preFilterCount = deviceTelemetryModels.Count();

                deviceTelemetryModels =
                    deviceTelemetryModels.Where(
                        deviceTelemetryModel =>
                            deviceTelemetryModel?.Timestamp != null && deviceTelemetryModel.Timestamp.Value >= minTime);

                if (preFilterCount == 0)
                {
                    break;
                }

                result = result.Concat(deviceTelemetryModels);
            }

            if (!string.IsNullOrEmpty(deviceId))
            {
                result = result.Where(t => t.DeviceId == deviceId);
            }

            return result;
        }

        /// <summary>
        /// Populates the telemetry model with the data from the blob storage
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="telemetry"></param>
        /// <returns></returns>
        private static List<DeviceTelemetryModel> LoadBlobTelemetryModels(Stream stream, IList<Telemetry> telemetry)
        {
            Debug.Assert(stream != null, "stream is a null reference.");

            List<DeviceTelemetryModel> deviceTelemetryModels = new List<DeviceTelemetryModel>();

            stream.Position = 0;
            using (var reader = new StreamReader(stream))
            {
                //List of dictionaries which contains the data of a single blob
                IEnumerable<StrDict> strdicts = ParsingHelper.ParseCsv(reader).ToDictionaries();
                foreach (StrDict strdict in strdicts)
                {
                    var deviceTelemetryModel = new DeviceTelemetryModel();

                    string str;
                    if (strdict.TryGetValue("deviceid", out str))
                    {
                        deviceTelemetryModel.DeviceId = str;
                    }

                    deviceTelemetryModel.Timestamp = DateTime.Parse(
                        strdict["eventenqueuedutctime"],
                        CultureInfo.InvariantCulture,
                        DateTimeStyles.AllowWhiteSpaces);

                    IEnumerable<Telemetry> tmpTelemetry;

                    if (telemetry != null && telemetry.Count > 0)
                    {
                        tmpTelemetry = telemetry;
                    }
                    else
                    {
                        List<string> reservedColumns = new List<string>
                        {
                            "DeviceId",
                            "EventEnqueuedUtcTime",
                            "EventProcessedUtcTime",
                            "IoTHub",
                            "PartitionId"
                        };
                        //Add a default telemetry type if there is no one available but stored inside the storage
                        tmpTelemetry = strdict.Keys
                            .Where(key => !reservedColumns.Contains(key))
                            .Select(name => new Telemetry
                            {
                                Name = name,
                                Type = "double"
                            });
                    }

                    foreach (var tmpTelemetryField in tmpTelemetry)
                    {
                        if (strdict.TryGetValue(tmpTelemetryField.Name, out str))
                        {
                            switch (tmpTelemetryField.Type.ToUpperInvariant())
                            {
                                case "INT":
                                case "INT16":
                                case "INT32":
                                case "INT64":
                                case "SBYTE":
                                case "BYTE":
                                    int intValue;
                                    if (
                                        int.TryParse(
                                            str,
                                            NumberStyles.Integer,
                                            CultureInfo.InvariantCulture,
                                            out intValue) &&
                                        !deviceTelemetryModel.Values.ContainsKey(tmpTelemetryField.Name))
                                    {
                                        deviceTelemetryModel.Values.Add(tmpTelemetryField.Name, intValue);
                                    }
                                    break;

                                case "DOUBLE":
                                case "DECIMAL":
                                case "SINGLE":
                                    double dblValue;
                                    if (
                                        double.TryParse(
                                            str,
                                            NumberStyles.Float,
                                            CultureInfo.InvariantCulture,
                                            out dblValue) &&
                                        !deviceTelemetryModel.Values.ContainsKey(tmpTelemetryField.Name))
                                    {
                                        deviceTelemetryModel.Values.Add(tmpTelemetryField.Name, dblValue);
                                    }
                                    break;
                            }
                        }
                    }

                    deviceTelemetryModels.Add(deviceTelemetryModel);
                }
            }
            

            return deviceTelemetryModels;
        }
    }
}
