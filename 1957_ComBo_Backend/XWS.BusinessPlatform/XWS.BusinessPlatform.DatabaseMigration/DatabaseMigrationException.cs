﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.BusinessPlatform.DatabaseMigration
{
    /// <summary>
    /// Description of DatabaseMigrationException
    /// </summary>
    public class DatabaseMigrationException : Exception
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseMigrationException"/> class.
        /// </summary>
        public DatabaseMigrationException(string message) : base(message)
        {
        }
        #endregion Constructors
    }
}
