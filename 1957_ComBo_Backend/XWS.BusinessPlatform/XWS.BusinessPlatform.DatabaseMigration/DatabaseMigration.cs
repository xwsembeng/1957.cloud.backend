﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Migrator.Framework;

namespace XWS.BusinessPlatform.DatabaseMigration
{
    public abstract class DatabaseMigration : Migration
    {
        
        protected string NewIdentifier => Guid.NewGuid().ToString();

        protected DataView ExecuteQuery(string sql)
        {
            DataTable dt = new DataTable();
            dt.Load(Database.ExecuteQuery(sql));
            return dt.DefaultView;
        }

        /// <summary>
        /// Gets the id value (primary key)
        /// </summary>
        /// <param name="table">The table to get the id value from</param>
        /// <param name="column">The column to compare the value to</param>
        /// <param name="value">The value to compare</param>
        /// <returns></returns>
        protected string GetId(string table, string column, string value)
        {
            object id = Database.SelectScalar("Id", table, $"{column}='{value.Replace("'", "''")}'");

            return id?.ToString();
        }

        protected void ExecuteResourceScript(string filename)
        {
            string resourceScript = String.Empty;

            Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(
                $"XWS.BusinessPlatform.DatabaseMigration.ScriptResources.{filename}");
            if (stream != null)
            {
                StreamReader sr = new StreamReader(stream);
                resourceScript = sr.ReadToEnd();
                sr.Close();
                stream.Close();
            }

            if (!String.IsNullOrEmpty(resourceScript))
                Database.ExecuteNonQuery(resourceScript);
        }
    }
}
