﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace XWS.BusinessPlatform.DatabaseMigration
{
    /// <summary>
    /// Description of DatabaseMigrator
    /// </summary>
    public class DatabaseMigrator
    {
        #region Member Variables
        private const string DatabaseProvider = "SQLServer";
        private const string ConnectionStringName = "ComBoSQLDB";
        private Migrator.Migrator _migrator;
        #endregion Member Variables

        #region Properties

        #region Private Properties
        private static string _connectionString;
        private static string ConnectionString
        {
            get
            {
                if (!String.IsNullOrEmpty(_connectionString))
                    return _connectionString;
                else
                    return ConfigurationManager.ConnectionStrings[ConnectionStringName].ConnectionString;
                    //return ConfigurationManager.AppSettings[ConnectionStringName];
            }
            set
            {
                _connectionString = value;
            }
        }

        private Migrator.Migrator Migrator
        {
            get
            {
                if (_migrator == null)
                {
                    if (String.IsNullOrEmpty(ConnectionString))
                    {
                        throw new Exception("ConnectionString was not found in configuration file.");
                    }

                    Assembly migrationAssembly = Assembly.GetExecutingAssembly();
                   
                    _migrator = new Migrator.Migrator(DatabaseProvider, ConnectionString, migrationAssembly);
                }

                return _migrator;
            }
        }
        #endregion Private Properties


        #region Protected Properties
        #endregion Protected Properties


        #region Public Properties
        /// <summary>
        /// Gets the current applied migration version.
        /// </summary>
        /// <value>The current applied migration version.</value>
        public long CurrentAppliedMigration
        {
            get { return Migrator.AppliedMigrations.Count > 0 ? Migrator.AppliedMigrations.Max() : 0; }
        }

        /// <summary>
        /// Gets a collection of the applied migrations.
        /// </summary>
        /// <value>The applied migrations.</value>
        public IList<long> AppliedMigrations
        {
            get { return Migrator.AppliedMigrations; }
        }

        public bool DoDryRun
        {
            get { return Migrator.DryRun; }
            set { Migrator.DryRun = value; }
        }
        #endregion public properties


        #region Static Properties
        #endregion Static properties

        #endregion Properties


        #region Constructors
        #endregion Constructors


        #region Methods

        #region Private Methods
        #endregion Private Methods


        #region Protected Methods
        #endregion Protected Methods


        #region Public Methods
        /// <summary>
        /// Migrates to latest version.
        /// </summary>
        /// <returns>The version the database was migrated to.</returns>
        public long MigrateToLatestVersion()
        {
            Migrator.MigrateToLastVersion();
            return CurrentAppliedMigration;
        }

        /// <summary>
        /// Migrates to a specific version.
        /// </summary>
        /// <returns>The version the database was migrated to.</returns>
        public long MigrateToVersion(long version)
        {
            Migrator.MigrateTo(version);
            return CurrentAppliedMigration;
        }

        /// <summary>
        /// Migrates to the previous version.
        /// </summary>
        /// <returns>The version the database was migrated to.</returns>
        public long MigrateToPreviousVersion()
        {
            if (Migrator.AppliedMigrations.Count() == 0)
            {
                return 0;
            }

            int indexOfCurrentAppliedMigration = Migrator.AppliedMigrations.IndexOf(CurrentAppliedMigration);

            long previousVersion = Migrator.AppliedMigrations[indexOfCurrentAppliedMigration - 1];
            MigrateToVersion(previousVersion);

            return previousVersion;
        }
        #endregion Public Methods


        #region Static Methods
        #endregion Static Methods

        #endregion Methods
    }
}
