﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Migrator.Framework;

namespace XWS.BusinessPlatform.DatabaseMigration.Migrations
{
    [Migration(201703281311)]
    public class Task201703281311_InsertInitalData_Security : DatabaseMigration
    {
        public override void Up()
        {
            ExecuteResourceScript("InsertInitialData_Security.sql");
        }

        public override void Down()
        {
        }
    }
}
