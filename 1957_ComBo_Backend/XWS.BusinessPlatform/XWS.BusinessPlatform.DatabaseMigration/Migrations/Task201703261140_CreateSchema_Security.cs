﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Migrator.Framework;


namespace XWS.BusinessPlatform.DatabaseMigration.Migrations
{
    [Migration(201703261140)]
    public class Task201703261140_CreateSchema_Security : DatabaseMigration
    {
        public override void Up()
        {
            ExecuteResourceScript("CreateSchema_Security.sql");
        }

        public override void Down()
        {
          
        }
    }
}
