/* Begin Administration Module */
INSERT INTO BP_Module (Id,RowVersion,Code,DisplayName,Description,SortIndex,ModuleType,ParentId) VALUES ('f6a40f5e-4750-4962-9cac-78fd6493bf4f',1,'Administration','Administration',null,0,3,null);
INSERT INTO BP_Module (Id,RowVersion,Code,DisplayName,Description,SortIndex,ModuleType,ParentId) VALUES ('066d3fac-9f49-4b03-9db1-7e0302fa7252',1,'Administration.Global','Global',null,0,0,'f6a40f5e-4750-4962-9cac-78fd6493bf4f');
INSERT INTO BP_Profile (Id,RowVersion,Code,DisplayName,Description,ProfileType,IsImmutable) VALUES ('c1b2889c-e97d-49c7-9dd4-0902902194b2',1,'Administration.Profile.Modify','Administrate profiles',null,0,1);
INSERT INTO BP_Profile_Module (ModuleId,ProfileId) VALUES ('066d3fac-9f49-4b03-9db1-7e0302fa7252','c1b2889c-e97d-49c7-9dd4-0902902194b2');
INSERT INTO BP_Profile (Id,RowVersion,Code,DisplayName,Description,ProfileType,IsImmutable) VALUES ('edfb5856-0974-4aba-98f5-5dce7773c1ea',1,'Administration.StructureUnit.Modify','Administrate structure units ',null,0,1);
INSERT INTO BP_Profile_Module (ModuleId,ProfileId) VALUES ('066d3fac-9f49-4b03-9db1-7e0302fa7252','edfb5856-0974-4aba-98f5-5dce7773c1ea');
INSERT INTO BP_Profile (Id,RowVersion,Code,DisplayName,Description,ProfileType,IsImmutable) VALUES ('68cef752-bee5-43de-9dff-739abf7a8b70',1,'Administration.Profile.Assign','Assign profiles (roles)',null,0,1);
INSERT INTO BP_Profile_Module (ModuleId,ProfileId) VALUES ('066d3fac-9f49-4b03-9db1-7e0302fa7252','68cef752-bee5-43de-9dff-739abf7a8b70');
INSERT INTO BP_Profile (Id,RowVersion,Code,DisplayName,Description,ProfileType,IsImmutable) VALUES ('9cf6d932-cdf6-4819-b444-74058869082a',1,'Administration.AppUser.Modify','Administrate application users',null,0,1);
INSERT INTO BP_Profile_Module (ModuleId,ProfileId) VALUES ('066d3fac-9f49-4b03-9db1-7e0302fa7252','9cf6d932-cdf6-4819-b444-74058869082a');
INSERT INTO BP_Profile (Id,RowVersion,Code,DisplayName,Description,ProfileType,IsImmutable) VALUES ('8fb54d72-5502-432f-b40c-3f34d9b2111d',1,'SuperAdmin','Super Admin',null,1,1);
INSERT INTO BP_Profile_Module (ModuleId,ProfileId) VALUES ('066d3fac-9f49-4b03-9db1-7e0302fa7252','8fb54d72-5502-432f-b40c-3f34d9b2111d');
/*End Administration  Module*/

/* Begin FrontOffice-Module */
INSERT INTO BP_Module (Id,RowVersion,Code,DisplayName,Description,SortIndex,ModuleType,ParentId) VALUES ('270458b7-adec-4e20-a655-1acfc9565f9f',1,'FrontOffice','Frontoffice',null,1,3,null);
INSERT INTO BP_Module (Id,RowVersion,Code,DisplayName,Description,SortIndex,ModuleType,ParentId) VALUES ('e162e6b4-6223-4285-853f-b46e13d80edb',1,'FrontOffice.Global','Global',null,0,0,'270458b7-adec-4e20-a655-1acfc9565f9f');

/* End FrontOffice-Module */

/* Begin BackOffice-Module */
INSERT INTO BP_Module (Id,RowVersion,Code,DisplayName,Description,SortIndex,ModuleType,ParentId) VALUES ('deb93015-f6c1-49b2-8e3c-379c0905f1d0',1,'BackOffice','Backoffice',null,2,3,null);
INSERT INTO BP_Module (Id,RowVersion,Code,DisplayName,Description,SortIndex,ModuleType,ParentId) VALUES ('05e299db-0930-4fcb-adc0-61c5d03cab86',16,'BackOffice.Global','Global',null,0,0,'deb93015-f6c1-49b2-8e3c-379c0905f1d0');

/* End BackOffice-Module */

/* Root StructureUnit*/
INSERT INTO BP_StructureUnit (Id,RowVersion,Name,IsActive,Description,ParentId) VALUES ('3d12d781-531b-4930-bf8c-47b21505176e',1,'Root',1,null,null);

/* AppUser SuperAdmin*/
INSERT INTO BP_AppUser ([Id],[RowVersion],[Login],[FirstName],[LastName],[Active],[Culture],[UICulture],[AutoExpandCurrentProject],[IsObsolete],[StructureUnitId],[IsChecked])
     VALUES  ('d3bdf514-56d8-4c95-8ad6-97eeaffe806e',1,'admin','Admin','Super',1,'en-US','en-US',1,0,'3D12D781-531B-4930-BF8C-47B21505176E',1)

INSERT INTO BP_User_Profile_StructureUnit ([Id],[RowVersion],[StructureUnit_Id],[Profile_Id],[User_Id],[IsDenied],[IsCascading])
     VALUES ('CB9E542D-38A9-4B32-BFB5-01D3FEEB2456',1,'3D12D781-531B-4930-BF8C-47B21505176E','8FB54D72-5502-432F-B40C-3F34D9B2111D','d3bdf514-56d8-4c95-8ad6-97eeaffe806e',0,1)