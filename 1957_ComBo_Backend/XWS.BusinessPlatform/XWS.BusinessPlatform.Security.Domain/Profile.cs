﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Domain;
using XWS.BusinessPlatform.Security.Domain.Collections;

namespace XWS.BusinessPlatform.Security.Domain
{
    public class Profile : DomainObject, IComparable
    {
        private readonly IList<Module> _modules = new List<Module>();
        private ModuleCollection _moduleWrapper;
        internal readonly IList<Profile> _children = new List<Profile>();
        private ChildProfileCollection _childrenWrapper;
        internal readonly IList<Profile> _parents = new List<Profile>();
        private ParentProfileCollection _parentWrapper;
        private bool _isImmutable;
        private string _displayName;
        private string _code;
        private string _description;
        private ProfileType _profileType;

        /// <summary>
        /// Initializes a new instance of the <see cref="Profile"/> class.
        /// </summary>
        protected Profile()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Profile"/> class.
        /// </summary>
        /// <param name="code">The profile code.</param>
        /// <param name="displayName">The display name.</param>
        /// <param name="type">The profile type.</param>
        /// <param name="isImmutable">if set to <c>true</c> this instance is immutable.</param>
        /// <param name="modules">The modules this profile should belong to.</param>
        public Profile(string code, string displayName, ProfileType type, bool isImmutable,
            params Module[] modules)
        {
            foreach (Module module in modules)
            {
                module.Profiles.Add(this);
                Modules.Add(module);
            }

            _code = code;
            _displayName = displayName;
            _profileType = type;
            _isImmutable = isImmutable;
        }

        /// <summary>
        /// Gets a value indicating whether this profile is assignable to application users.
        /// Currently this is possible for all roles, but not for rights
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is assignable to user; otherwise, <c>false</c>.
        /// </value>
        public virtual bool IsAssignableToUsers
        {
            get { return ProfileType == ProfileType.Role; }
        }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>The code.</value>
        public virtual string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        /// <summary>
        /// Gets or sets the display name.
        /// </summary>
        /// <value>The display name.</value>
        public virtual string DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        public virtual string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        /// <summary>
        /// Gets the type of the profile.
        /// </summary>
        /// <value>The type of the profile.</value>
        public virtual ProfileType ProfileType
        {
            get { return _profileType; }
            set { _profileType = value; }
        }

        /// <summary>
        /// Gets the modules.
        /// </summary>
        /// <value>The modules.</value>
        public virtual ModuleCollection Modules
        {
            get
            {
                return _moduleWrapper = _moduleWrapper ??
                  new ModuleCollection(_modules);
            }
        }

        /// <summary>
        /// Gets the child profiles.
        /// </summary>
        /// <value>The child profiles</value>
        public virtual ChildProfileCollection Children
        {
            get
            {
                return _childrenWrapper = _childrenWrapper ??
                    new ChildProfileCollection(_children, this, Modules);
            }
        }

        /// <summary>
        /// Gets the parent profiles.
        /// </summary>
        /// <value>The parent profiles.</value>
        public virtual ParentProfileCollection Parents
        {
            get
            {
                return _parentWrapper = _parentWrapper ??
                    new ParentProfileCollection(_parents, this, Modules);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is immutable.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is immutable; otherwise, <c>false</c>.
        /// </value>
        public virtual bool IsImmutable
        {
            get { return _isImmutable; }
            set { _isImmutable = value; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has chil profiles.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has child profiles; otherwise, <c>false</c>.
        /// </value>
        public virtual bool HasChildren
        {
            get { return Children.Count > 0; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has parent profiles.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has parent profiles; otherwise, <c>false</c>.
        /// </value>
        public virtual bool HasParents
        {
            get { return Parents.Count > 0; }
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            return String.Format("{0} ({1})", this.Code, this.ProfileType);
        }

        /// <summary>
        /// Compares the current instance with another object of the same type and returns an integer that indicates whether the current instance precedes, follows, or occurs in the same position in the sort order as the other object.
        /// </summary>
        /// <param name="obj">An object to compare with this instance.</param>
        /// <returns>
        /// A 32-bit signed integer that indicates the relative order of the objects being compared. The return value has these meanings:
        /// Value
        /// Meaning
        /// Less than zero
        /// This instance is less than <paramref name="obj"/>.
        /// Zero
        /// This instance is equal to <paramref name="obj"/>.
        /// Greater than zero
        /// This instance is greater than <paramref name="obj"/>.
        /// </returns>
        /// <exception cref="T:System.ArgumentException">
        /// 	<paramref name="obj"/> is not the same type as this instance.
        /// </exception>
        public virtual int CompareTo(object obj)
        {
            Profile profileToCompareTo = (Profile)obj;
            if (((int)ProfileType) < ((int)profileToCompareTo.ProfileType))
            {
                return 1;
            }

            if (((int)ProfileType) > ((int)profileToCompareTo.ProfileType))
            {
                return -1;
            }

            return String.Compare(Code, profileToCompareTo.Code);
        }
    }
}
