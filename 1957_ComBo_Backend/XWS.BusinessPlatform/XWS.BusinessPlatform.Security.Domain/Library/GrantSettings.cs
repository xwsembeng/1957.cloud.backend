﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.BusinessPlatform.Security.Domain.Library
{
    public class GrantSettings
    {
        public bool Deny { get; set; }
        public bool Cascading { get; set; }
    }
}
