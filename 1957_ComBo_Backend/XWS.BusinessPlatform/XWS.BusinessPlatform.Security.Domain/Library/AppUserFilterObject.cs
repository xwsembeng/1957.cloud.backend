﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Security.Domain.Interfaces;

namespace XWS.BusinessPlatform.Security.Domain.Library
{
    public class AppUserFilterObject
    {
        public string Login { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EMail { get; set; }
        public string Phone { get; set; }
        public IStructureUnit StructureUnit { get; set; }
        public bool StructureUnitCascading { get; set; }
        public bool? Active { get; set; }

        public Profile Role { get; set; }

        public AppUserFilterObject()
        {
            Active = true;
        }
    }
}
