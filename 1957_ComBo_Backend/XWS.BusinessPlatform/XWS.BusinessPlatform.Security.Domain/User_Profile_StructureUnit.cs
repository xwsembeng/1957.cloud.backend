﻿using System;
using XWS.BusinessPlatform.Domain;
using XWS.BusinessPlatform.Security.Domain.Interfaces;

namespace XWS.BusinessPlatform.Security.Domain
{
    public class User_Profile_StructureUnit : DomainObject
    {
        public virtual IStructureUnit StructureUnit { get; set; }

        public virtual Profile Profile { get; set; }

        public virtual IAppUser User { get; set; }

        public virtual bool IsDenied { get; set; }

        public virtual bool IsCascading { get; set; }

        public User_Profile_StructureUnit() { }

        public User_Profile_StructureUnit(Profile profile, IStructureUnit structureUnit, IAppUser user)
        {
            if (profile == null || structureUnit == null || user == null)
                throw new ArgumentNullException("No null values allowed in constructor: User_Profile_StructureUnit(Profile profile, IStructureUnit structureUnit, IUser user).");

            Profile = profile;
            StructureUnit = structureUnit;
            User = user;
        }
    }
}