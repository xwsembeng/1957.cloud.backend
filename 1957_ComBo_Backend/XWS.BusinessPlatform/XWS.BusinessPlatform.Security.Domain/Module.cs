﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Domain;
using XWS.BusinessPlatform.Security.Domain.Collections;

namespace XWS.BusinessPlatform.Security.Domain
{
    /// <summary>
    /// Handles modules
    /// </summary>
    public class Module : DomainObject
    {
        private readonly IList<Module> _children = new List<Module>();
        private ModuleCollection childrenWrapper;
        private readonly IList<Profile> _profiles = new List<Profile>();
        private ProfileCollection _profileWrapper;
        private string _displayName;
        private int _sortIndex;
        private readonly ModuleType _moduleType;
        private string _code;
        private string _description;

        /// <summary>
        /// Initializes a new instance of the <see cref="Module"/> class.
        /// </summary>
        protected Module()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Module"/> class.
        /// </summary>
        /// <param name="code">The modules code.</param>
        /// <param name="displayName">The display name.</param>
        /// <param name="sortIndex">The sort index.</param>
        /// <param name="moduleType">The module type.</param>
        public Module(string code, string displayName, int sortIndex, ModuleType moduleType)
        {
            _code = code;
            _displayName = displayName;
            _sortIndex = sortIndex;
            _moduleType = moduleType;
        }

        /// <summary>
        /// Gets a value indicating whether profiles in this module are assignable to application users.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if profiles in this module are assignable to users; otherwise, <c>false</c>.
        /// </value>
        public virtual bool IsAssignableToUsers
        {
            get { return ModuleType == ModuleType.Global || ModuleType == ModuleType.StructureUnitDependent; }
        }

        /// <summary>
        /// Gets or sets the module code.
        /// </summary>
        /// <value>The module code.</value>
        public virtual string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        /// <summary>
        /// Gets or sets the display name.
        /// </summary>
        /// <value>The display name.</value>
        public virtual string DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        public virtual string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        /// <summary>
        /// Gets or sets the sort index.
        /// </summary>
        /// <value>The sort index.</value>
        public virtual int SortIndex
        {
            get { return _sortIndex; }
            set { _sortIndex = value; }
        }

        /// <summary>
        /// Gets the type of the module.
        /// </summary>
        /// <value>The type of the module.</value>
        public virtual ModuleType ModuleType
        {
            get { return _moduleType; }
        }

        /// <summary>
        /// Gets the assigned profiles.
        /// </summary>
        /// <value>The assigned profiles.</value>
        public virtual ProfileCollection Profiles
        {
            get
            {
                return _profileWrapper = _profileWrapper ??
                    new ProfileCollection(_profiles, this);
            }
        }

        /// <summary>
        /// Gets the child modules.
        /// </summary>
        /// <value>The child modules.</value>
        public virtual ModuleCollection Children
        {
            get
            {
                return childrenWrapper = childrenWrapper ??
                    new ModuleCollection(_children);
            }
        }

        public override string ToString()
        {
            return string.Format("{0} ({1})", this.Code, this.ModuleType);
        }
    }
}
