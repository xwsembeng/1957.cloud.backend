﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.BusinessPlatform.Security.Domain
{
    public enum ProfileType
    {
        Profile = 0,
        Role = 1
    }
}
