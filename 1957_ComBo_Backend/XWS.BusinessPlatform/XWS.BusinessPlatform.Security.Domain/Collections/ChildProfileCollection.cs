﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Domain.Collections.Generic;
using XWS.BusinessPlatform.Security.Domain.Exceptions;

namespace XWS.BusinessPlatform.Security.Domain.Collections
{
    /// <summary>
    /// Description of ProfileCollection
    /// </summary>
    public class ChildProfileCollection : ReadWriteDomainObjectCollection<Profile>
    {
        private readonly Profile _parent;
        private readonly IEnumerable<Module> _modules;

        #region Member Variables
        #endregion Member Variables


        #region Properties

        #region Private Properties
        #endregion Private Properties


        #region Protected Properties
        #endregion Protected Properties


        #region Public Properties
        #endregion public properties


        #region Static Properties
        #endregion Static properties

        #endregion Properties


        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="ModuleCollection"/> class.
        /// </summary>
        public ChildProfileCollection(IList<Profile> profiles, Profile parent, IEnumerable<Module> modules)
            : base(profiles)
        {
            _parent = parent;
            _modules = modules;
        }

        #endregion Constructors


        #region Methods

        #region Private Methods
        #endregion Private Methods


        #region Protected Methods
        #endregion Protected Methods


        #region Public Methods
        public override void Add(Profile profile)
        {
            if (!_modules.Intersect(profile.Modules).Any())
            {
                throw new ProfileException("The profile '" + profile.Code + "' cannot be added as child to " + _parent.Code + "', because it belongs not to the same module.");
            }

            if (!_collection.Contains(profile))
            {
                _collection.Add(profile);
                profile._parents.Add(_parent);
            }
        }

        public override bool Remove(Profile profile)
        {
            profile._parents.Remove(_parent);
            return _collection.Remove(profile);
        }
        #endregion Public Methods


        #region Static Methods
        #endregion Static Methods

        #endregion Methods
    }
}
