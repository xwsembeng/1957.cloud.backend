﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Domain.Collections;
using XWS.BusinessPlatform.Domain.Collections.Generic;

namespace XWS.BusinessPlatform.Security.Domain.Collections
{
    public class ProfileCollection : ReadWriteDomainObjectCollection<Profile>
    {
        #region Member Variables
        private readonly Module _module;
        #endregion Member Variables


        #region Properties

        #region Private Properties
        #endregion Private Properties


        #region Protected Properties
        #endregion Protected Properties


        #region Public Properties
        #endregion public properties


        #region Static Properties
        #endregion Static properties

        #endregion Properties


        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="ModuleCollection"/> class.
        /// </summary>
        public ProfileCollection(IList<Profile> profiles, Module module)
            : base(profiles)
        {
            _module = module;
        }

        #endregion Constructors


        #region Methods

        #region Private Methods
        #endregion Private Methods


        #region Protected Methods
        #endregion Protected Methods


        #region Public Methods
        public override void Add(Profile profile)
        {
            if (!_collection.Contains(profile))
            {
                _collection.Add(profile);
                profile.Modules.Add(_module);
            }
        }

        public override bool Remove(Profile profile)
        {
            profile.Modules.Remove(_module);
            return _collection.Remove(profile);
        }
        #endregion Public Methods


        #region Static Methods
        #endregion Static Methods

        #endregion Methods
    }
}
