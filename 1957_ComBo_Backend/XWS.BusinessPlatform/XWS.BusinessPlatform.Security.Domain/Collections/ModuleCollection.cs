﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Domain.Collections.Generic;

namespace XWS.BusinessPlatform.Security.Domain.Collections
{
    /// <summary>
    /// Description of ModuleCollection
    /// </summary>
    public class ModuleCollection : ReadWriteDomainObjectCollection<Module>
    {
        #region Member Variables
        #endregion Member Variables


        #region Properties

        #region Private Properties
        #endregion Private Properties


        #region Protected Properties
        #endregion Protected Properties


        #region Public Properties
        #endregion public properties


        #region Static Properties
        #endregion Static properties

        #endregion Properties


        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="ModuleCollection"/> class.
        /// </summary>
        public ModuleCollection(IList<Module> modules)
            : base(modules, true)
        { }
        #endregion Constructors


        #region Methods

        #region Private Methods
        #endregion Private Methods


        #region Protected Methods
        #endregion Protected Methods


        #region Public Methods
        #endregion Public Methods


        #region Static Methods
        #endregion Static Methods

        #endregion Methods
    }
}
