﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Domain;
using XWS.BusinessPlatform.Security.Domain.Interfaces;
using XWS.Localization;

namespace XWS.BusinessPlatform.Security.Domain
{
    public class AppUser : DomainObject, IAppUser
    {
        public virtual string Login { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string Password { get; set; }
        public virtual string EMail { get; set; }
        public virtual string Phone { get; set; }
        public virtual bool Active { get; set; }
        public virtual bool IsObsolete { get; set; }
        public virtual bool IsChecked { get; set; }

        public virtual IStructureUnit StructureUnit { get; set; }

        public virtual CultureInfo Culture { get; set; }
        public virtual CultureInfo UICulture { get; set; }
        public virtual bool AutoExpandCurrentProject { get; set; }

        public override bool Equals(object obj)
        {
            return Equals(Id, ((DomainObject)obj).Id);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public virtual string DisplayName
        {
            get
            {
                if (!string.IsNullOrEmpty(LastName) && !string.IsNullOrEmpty(FirstName))
                {
                    return string.Format(StaticResourceManager.GetString("ComBo", "DisplayNameFormat"), LastName, FirstName);
                }

                return $"{LastName}{FirstName}";
            }
        }

        public override string ToString()
        {
            return DisplayName;
        }

        public virtual MailAddress MailAddress
        {
            get
            {
                MailAddress address = new MailAddress(EMail, DisplayName);
                return address;
            }
        }
    }
}
