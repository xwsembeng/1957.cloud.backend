﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Domain;
using XWS.BusinessPlatform.Security.Domain.Interfaces;

namespace XWS.BusinessPlatform.Security.Domain
{
    [Serializable]
    public class StructureUnit : DomainObject, IStructureUnit
    {
        private readonly IList<IStructureUnit> _children = new List<IStructureUnit>();
        private readonly IList<IStructureUnit> _parents = new List<IStructureUnit>();

        public virtual string Name { get; set; }
        public virtual string DisplayName
        {
            get { return Name; }
        }

        public virtual IStructureUnit Parent { get; set; }

        public StructureUnit() { }

        public StructureUnit(string name)
        {
            Name = name;
        }

        public virtual bool IsRoot
        {
            get { return Parent == null; }
        }

        #region IStructureUnit Members

        public virtual bool IsActive { get; set; }

        public virtual string Description { get; set; }


        public virtual IEnumerable<IStructureUnit> Children
        {
            get { return _children; }
        }

        public virtual IEnumerable<IStructureUnit> Parents
        {
            get { return _parents; }
        }

        public virtual void AddChild(IStructureUnit unit)
        {
            if (Children.Contains(unit))
                return;

            _children.Add(unit);
            unit.Parent = this;
        }

        public virtual void RemoveChild(IStructureUnit unit)
        {
            if (!Children.Contains(unit))
                return;

            _children.Remove(unit);
            unit.Parent = null;
        }

        #endregion

        public override bool Equals(object obj)
        {
            return Equals(((StructureUnit)obj).Id, Id);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override string ToString()
        {
            return Name;
        }

        private static void EnumerateStructureUnits(IStructureUnit parentStructureUnit, ref ICollection<IStructureUnit> allStructureUnits)
        {
            foreach (IStructureUnit structureUnit in parentStructureUnit.Children)
            {
                allStructureUnits.Add(structureUnit);
                EnumerateStructureUnits(structureUnit, ref allStructureUnits);
            }
        }

        private static void EnumerateParentStructureUnits(IStructureUnit parentStructureUnit, ref ICollection<IStructureUnit> allStructureUnits)
        {
            if (parentStructureUnit.Parent != null)
            {
                allStructureUnits.Add(parentStructureUnit.Parent);
                EnumerateParentStructureUnits(parentStructureUnit.Parent, ref allStructureUnits);
            }
        }

        public virtual IEnumerable<IStructureUnit> GetStructureUnitsRecursive()
        {
            ICollection<IStructureUnit> flatStructureUnitCollection = new List<IStructureUnit> { this };

            foreach (IStructureUnit structureUnit in Children)
            {
                flatStructureUnitCollection.Add(structureUnit);
                EnumerateStructureUnits(structureUnit, ref flatStructureUnitCollection);
            }

            return flatStructureUnitCollection;
        }

        public virtual IEnumerable<IStructureUnit> GetParentStructureUnitsRecursive()
        {
            ICollection<IStructureUnit> flatStructureUnitCollection = new List<IStructureUnit> { this };
            EnumerateParentStructureUnits(this, ref flatStructureUnitCollection);

            return flatStructureUnitCollection;
        }
    }
}
