﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Domain;

namespace XWS.BusinessPlatform.Security.Domain.Interfaces
{
    public interface IStructureUnit : ICanBeDeactivated
    {
        string Name { get; set; }
        string Description { get; set; }

        string DisplayName { get; }

        IStructureUnit Parent { get; set; }
        IEnumerable<IStructureUnit> Children { get; }

        void AddChild(IStructureUnit unit);
        void RemoveChild(IStructureUnit unit);

        IEnumerable<IStructureUnit> GetStructureUnitsRecursive();
        IEnumerable<IStructureUnit> GetParentStructureUnitsRecursive();
    }
}
