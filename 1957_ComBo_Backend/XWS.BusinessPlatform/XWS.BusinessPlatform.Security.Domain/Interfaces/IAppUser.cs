﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Domain;
using XWS.BusinessPlatform.Services.Mail.Interfaces;

namespace XWS.BusinessPlatform.Security.Domain.Interfaces
{
    public interface IAppUser : IDomainObject, IMailReceiver
    {
        string Login { set; get; }
        string FirstName { set; get; }
        string LastName { set; get; }
        string Password { get; set; }
        string EMail { set; get; }
        string Phone { get; set; }
        bool Active { set; get; }
        bool IsObsolete { get; set; }
        bool IsChecked { get; set; }
        IStructureUnit StructureUnit { set; get; }
        CultureInfo Culture { get; set; }
        CultureInfo UICulture { get; set; }
        bool AutoExpandCurrentProject { get; set; }
        string DisplayName { get; }
    }
}
