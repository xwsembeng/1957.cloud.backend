﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.BusinessPlatform.Security.Domain
{
    public enum ModuleType
    {
        Global = 0,
        StructureUnitDependent = 1,
        ObjectSpecific = 2,
        ModuleContainer = 3
    }
}
