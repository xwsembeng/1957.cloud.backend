﻿/// Contains the most basic component provided by the framework kernel. 

using NHibernate;

namespace XWS.BusinessPlatform.API.Core
{
    /// <summary>
    /// The ISessionManager provides NHibernate sessions to access the database.
    /// 
    /// The session manager is transaction aware. This means that the session manager creates one session 
    /// per transaction. Therefore each call to query a session within the context of the same local ambient
    /// transaction will always return the same session. If no transaction is available the session manager 
    /// will create a session per operation context. In this case each call to query a session within the same
    /// operation context will return the same session. The returned sessions will automatically be closes
    /// if either the transaction or the operatio context has completed.
    /// </summary>
    public interface ISessionManager
    {
        /// <summary>
        /// Returns a session to the configured database. 
        /// </summary>
        /// <returns>A session to the configured database.</returns>
        ISession GetSession();
    }
}
