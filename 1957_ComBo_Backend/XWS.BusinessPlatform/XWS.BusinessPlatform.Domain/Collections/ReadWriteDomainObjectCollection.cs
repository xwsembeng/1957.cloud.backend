﻿using System.Collections;
using XWS.BusinessPlatform.Domain.Collections.Generic;


namespace XWS.BusinessPlatform.Domain.Collections
{
    /// <summary>
    /// Description of ModifiableGenericDomainObjectCollection
    /// </summary>
    public class ReadWriteDomainObjectCollection : ReadOnlyDomainObjectCollection, IReadWriteDomainObjectCollection
    {
        #region Member Variables
        #endregion Member Variables


        #region Properties

        #region Private Properties
        #endregion Private Properties


        #region Protected Properties
        #endregion Protected Properties


        #region Public Properties
        #endregion public properties


        #region Static Properties
        #endregion Static properties

        #endregion Properties


        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="ReadWriteDomainObjectCollection{T}"/> class.
        /// </summary>
        public ReadWriteDomainObjectCollection(IList collection)
            : base(collection)
        { }
        #endregion Constructors


        #region Methods

        #region Private Methods
        #endregion Private Methods


        #region Protected Methods
        #endregion Protected Methods


        #region Public Methods
        public virtual void Add(object element)
        {
            _collection.Add(element);
        }

        public virtual void Remove(object element)
        {
            _collection.Remove(element);
        }
        #endregion Public Methods


        #region Static Methods
        #endregion Static Methods

        #endregion Methods
    }
}