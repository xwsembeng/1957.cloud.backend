using System.Collections;

namespace XWS.BusinessPlatform.Domain.Collections
{
    public interface IReadOnlyDomainObjectCollection : IEnumerable
    {
        object this[int index] { get; }
        int Count { get; }
        ReadOnlyDomainObjectCollection Clone();
        IEnumerable DeepClone();
        bool Contains(object element);
        void Clear();
        string ToString();

        /// <summary>
        /// Builds a comma separeted list of all values of a certain property
        /// </summary>
        /// <param name="property">The property to build the value list from</param>
        /// <returns>values list as string</returns>
        string ToString(string property);

        string ToString(string property, string separator, string lastSeparator);
    }
}