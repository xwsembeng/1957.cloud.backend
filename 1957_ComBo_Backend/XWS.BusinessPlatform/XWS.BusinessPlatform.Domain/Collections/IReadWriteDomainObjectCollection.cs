﻿namespace XWS.BusinessPlatform.Domain.Collections
{
    /// <summary>
    /// Description of IModifiableDomainObjectCollection
    /// </summary>
    public interface IReadWriteDomainObjectCollection : IReadOnlyDomainObjectCollection
    {
        void Add(object element);
        void Remove(object element);
    }
}