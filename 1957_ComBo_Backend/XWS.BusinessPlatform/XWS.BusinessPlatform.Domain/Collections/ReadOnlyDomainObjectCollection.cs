﻿using System;
using System.Collections;
using System.Text;
using XWS.BusinessPlatform.Domain.Collections.Generic;


namespace XWS.BusinessPlatform.Domain.Collections
{
    /// <summary>
    /// Description of DomainObjectCollection
    /// </summary>
    public class ReadOnlyDomainObjectCollection : IReadOnlyDomainObjectCollection, IEnumerable
    {
        #region Member Variables
        protected readonly IList _collection;
        #endregion Member Variables


        #region Properties

        #region Private Properties
        #endregion Private Properties


        #region Protected Properties
        #endregion Protected Properties


        #region Public Properties
        public object this[int index]
        {
            get { return _collection[index]; }
        }

        public int Count
        {
            get { return _collection.Count; }
        }
        #endregion public properties


        #region Static Properties
        #endregion Static properties

        #endregion Properties


        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="ReadOnlyDomainObjectCollection{T}"/> class.
        /// </summary>
        public ReadOnlyDomainObjectCollection(IList collection)
        {
            _collection = collection;
        }

        protected ReadOnlyDomainObjectCollection()
        {
            _collection = new ArrayList();
        }

        #endregion Constructors


        #region Methods

        #region Private Methods
        #endregion Private Methods


        #region Protected Methods
        #endregion Protected Methods


        #region Public Methods
        public ReadOnlyDomainObjectCollection Clone()
        {
            return new ReadOnlyDomainObjectCollection(new ArrayList(_collection));
        }

        public IEnumerable DeepClone()
        {
            IList clonedList = new ArrayList();

            if (_collection.Count == 0)
            {
                return clonedList;
            }

            foreach (object element in _collection)
            {
                if (!(element is ICloneable))
                {
                    throw new Exception("To use deep clone, all elements in the collection must implement IClonable");
                }

                object clonedElement = ((ICloneable)element).Clone();
                clonedList.Add(clonedElement);
            }

            return clonedList;
        }

        public bool Contains(object element)
        {
            return _collection.Contains(element);
        }

        public void Clear()
        {
            _collection.Clear();
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (object element in _collection)
            {
                stringBuilder.Append(element.ToString() + "; ");
            }

            string result = stringBuilder.ToString();

            if (result.Length > 2)
            {
                result = result.Remove(result.Length - 2, 2);
            }

            return result;
        }

        public IEnumerator GetEnumerator()
        {
            return _collection.GetEnumerator();
        }

        /// <summary>
        /// Builds a comma separeted list of all values of a certain property
        /// </summary>
        /// <param name="property">The property to build the value list from</param>
        /// <returns>values list as string</returns>
        public string ToString(string property)
        {
            return Util.CollectionUtil.GetStringList(_collection, property, ", ");
        }

        public string ToString(string property, string separator, string lastSeparator)
        {
            return Util.CollectionUtil.GetStringList(_collection, property, separator, lastSeparator);
        }
        #endregion Public Methods


        #region Static Methods
        #endregion Static Methods

        #endregion Methods
    }
}