﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace XWS.BusinessPlatform.Domain.Collections.Generic
{
    [Serializable]
    public class ReadOnlyDomainObjectCollection<T> : IReadOnlyDomainObjectCollection<T> where T : IDomainObject
    {
        #region Member Variables
        protected readonly IList<T> _collection;
        #endregion Member Variables


        #region Properties

        #region Private Properties
        #endregion Private Properties


        #region Protected Properties
        #endregion Protected Properties


        #region Public Properties
        public T this[Guid id]
        {
            get { return _collection.FirstOrDefault(obj => obj.Id.Equals(id)); }
        }
        public T this[int index]
        {
            get { return _collection[index]; }
        }

        public int Count
        {
            get { return _collection.Count; }
        }
        #endregion public properties


        #region Static Properties
        #endregion Static properties

        #endregion Properties


        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="ReadOnlyDomainObjectCollection{T}"/> class.
        /// </summary>
        public ReadOnlyDomainObjectCollection(IList<T> collection)
        {
            _collection = collection;
        }

        protected ReadOnlyDomainObjectCollection()
        {
            _collection = new List<T>();
        }

        #endregion Constructors


        #region Methods

        #region Private Methods
        #endregion Private Methods


        #region Protected Methods
        #endregion Protected Methods


        #region Public Methods
        public ReadOnlyDomainObjectCollection<T> Clone()
        {
            return new ReadOnlyDomainObjectCollection<T>(new List<T>(_collection));
        }

        public IEnumerable<T> DeepClone()
        {
            IList<T> clonedList = new List<T>();

            if (_collection.Count == 0)
            {
                return clonedList;
            }

            if (!(_collection.First() is ICloneable))
            {
                throw new Exception("To use deep clone, all elements in the collection must implement IClonable");
            }

            
            foreach (T element in _collection)
            {
                T clonedElement = (T)((ICloneable)element).Clone();
                clonedList.Add(clonedElement);
            }

            return clonedList;
        }

        public virtual IEnumerator<T> GetEnumerator()
        {
            return _collection.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public bool Contains(T element)
        {
            return _collection.Contains(element);
        }

        public virtual void Clear()
        {
            _collection.Clear();
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (T element in _collection)
            {
                stringBuilder.Append(element.ToString() + "; ");
            }

            string result = stringBuilder.ToString();

            if (result.Length > 2)
            {
                result = result.Remove(result.Length - 2, 2);
            }

            return result;
        }

        /// <summary>
        /// Builds a comma separeted list of all values of a certain property
        /// </summary>
        /// <param name="property">The property to build the value list from</param>
        /// <returns>values list as string</returns>
        public string ToString(string property)
        {
            return Util.CollectionUtil.GetStringList(_collection, property, ", ");
        }

        public string ToString(string property, string separator, string lastSeparator)
        {
            return Util.CollectionUtil.GetStringList(_collection, property, separator, lastSeparator);
        }
        #endregion Public Methods


        #region Static Methods
        #endregion Static Methods

        #endregion Methods
    }
}