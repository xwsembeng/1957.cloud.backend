﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace XWS.BusinessPlatform.Domain.Collections.Generic
{
    /// <summary>
    /// Description of ModifiableGenericDomainObjectCollection
    /// </summary>
    public class ReadWriteDomainObjectCollection<T> : ReadOnlyDomainObjectCollection<T>, IReadWriteDomainObjectCollection<T> where T: IDomainObject
    {
        #region Member Variables
        private readonly bool _enforceUniqueItems = false;
        #endregion Member Variables


        #region Properties

        #region Private Properties
        #endregion Private Properties


        #region Protected Properties
        #endregion Protected Properties


        #region Public Properties
        public new T this[Guid id]
        {
            get
            {
                return _collection.FirstOrDefault(obj => obj.Id.Equals(id));
            }
            set
            {
                T item = _collection.FirstOrDefault(obj => obj.Id.Equals(id));
                int index = _collection.IndexOf(item);
                _collection[index] = value;
            }
        }
        #endregion public properties


        #region Static Properties
        #endregion Static properties

        #endregion Properties


        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="ReadWriteDomainObjectCollection{T}"/> class.
        /// </summary>
        public ReadWriteDomainObjectCollection(IList<T> collection) : base(collection)
        { }

        public ReadWriteDomainObjectCollection() : base()
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReadWriteDomainObjectCollection{T}"/> class.
        /// </summary>
        /// <param name="collection">The collection to initialize the instance with.</param>
        /// <param name="enforceUniqueItems">if set to <c>true</c> items which already exist in the list are not added again.</param>
        public ReadWriteDomainObjectCollection(IList<T> collection, bool enforceUniqueItems)
            : base(collection)
        {
            _enforceUniqueItems = enforceUniqueItems;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReadWriteDomainObjectCollection&lt;T&gt;"/> class.
        /// </summary>
        /// <param name="enforceUniqueItems">if set to <c>true</c> items which already exist in the list are not added again.</param>
        public ReadWriteDomainObjectCollection(bool enforceUniqueItems)
            : base()
        {
            _enforceUniqueItems = enforceUniqueItems;
        }

        #endregion Constructors


        #region Methods

        #region Private Methods
        #endregion Private Methods


        #region Protected Methods
        #endregion Protected Methods


        #region Public Methods
        public virtual void Add(T element)
        {
            if (!_enforceUniqueItems || !_collection.Contains(element))
            {
                _collection.Add(element);
            }
        }

        public virtual bool Remove(T element)
        {
            return _collection.Remove(element);
        }
        #endregion Public Methods


        #region Static Methods
        #endregion Static Methods

        #endregion Methods
    }
}