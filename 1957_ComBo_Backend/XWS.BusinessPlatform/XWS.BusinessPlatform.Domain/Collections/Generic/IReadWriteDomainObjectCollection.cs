﻿using System.Collections.Generic;


namespace XWS.BusinessPlatform.Domain.Collections.Generic
{
    /// <summary>
    /// Description of IModifiableDomainObjectCollection
    /// </summary>
    public interface IReadWriteDomainObjectCollection<T> : IReadOnlyDomainObjectCollection<T>, IEnumerable<T> where T : IDomainObject
    {
        void Add(T element);
        bool Remove(T element);
    }
}