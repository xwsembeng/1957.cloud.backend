using System;
using System.Collections.Generic;

namespace XWS.BusinessPlatform.Domain.Collections.Generic
{
    public interface IReadOnlyDomainObjectCollection<T>:IEnumerable<T> where T : IDomainObject
    {
        T this[Guid id] { get; }
        T this[int index] { get; }
        int Count { get; }
        ReadOnlyDomainObjectCollection<T> Clone();
        IEnumerable<T> DeepClone();
        bool Contains(T element);
        void Clear();
        string ToString();

        /// <summary>
        /// Builds a comma separeted list of all values of a certain property
        /// </summary>
        /// <param name="property">The property to build the value list from</param>
        /// <returns>values list as string</returns>
        string ToString(string property);

        string ToString(string property, string separator, string lastSeparator);
    }
}