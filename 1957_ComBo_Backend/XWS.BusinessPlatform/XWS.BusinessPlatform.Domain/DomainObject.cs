﻿using System;
using XWS.BusinessPlatform.Domain.Id;

namespace XWS.BusinessPlatform.Domain
{
    [Serializable]
    public abstract class DomainObject : IDomainObject
    {
        #pragma warning disable 649
        private int? _rowVersion;
        private Guid _id;
        #pragma warning restore 649

        public virtual Guid Id
        {
            get { return _id; }
            protected set { _id = value; }
        }
        
        public virtual int? RowVersion
        {
            get { return _rowVersion; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance was ever persisted.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is transient; otherwise, <c>false</c>.
        /// </value>
        public virtual bool IsTransient
        {
            get { return _rowVersion == null; }
        }

        protected DomainObject()
        {
            _id = SequentialGuidGenerator.GenerateComb();
        }

        /// <summary>
        /// Sets the Id to a new Guid and the RowVersion to Null
        /// </summary>
        public virtual void MakeTransient()
        {
            _id = SequentialGuidGenerator.GenerateComb();
            _rowVersion = null;
        }

        public virtual void OnPreFlush()
        {
        }

        public override bool Equals(object obj)
        {
            return Id.Equals(((DomainObject)obj).Id);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}