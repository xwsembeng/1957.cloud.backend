﻿using System;

namespace XWS.BusinessPlatform.Domain
{
    public interface IDomainObject
    {
        Guid Id { get; }
        int? RowVersion { get; }
        bool IsTransient { get; }
        void MakeTransient();
        void OnPreFlush();
    }
}