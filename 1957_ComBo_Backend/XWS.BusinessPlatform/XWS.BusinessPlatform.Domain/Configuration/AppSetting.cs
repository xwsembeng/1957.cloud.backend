﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.BusinessPlatform.Domain.Configuration
{
    /// <summary>
    /// Description of AppSettings
    /// </summary>
    public class AppSetting : DomainObject
    {
        private string _key;
        private string _value;

        public virtual string Key
        {
            get { return _key; }
            set { _key = value; }
        }

        public virtual string Value
        {
            get { return _value; }
            set { _value = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AppSetting"/> class.
        /// </summary>
        public AppSetting()
        { }

        /// <summary>
        /// Initialize a new instance of the <see cref="AppSetting" /> class with the given key.
        /// </summary>
        /// <param name="key"></param>
        public AppSetting(string key)
        {
            Key = key;
        }
    }
}
