﻿namespace XWS.BusinessPlatform.Domain
{
    /// <summary>
    /// Interface for domain objects which can be deactivated.
    /// Used for FindAll(bool onlyActive) methods.
    /// </summary>
    public interface ICanBeDeactivated : IDomainObject
    {
        bool IsActive { get; set; }
    }
}