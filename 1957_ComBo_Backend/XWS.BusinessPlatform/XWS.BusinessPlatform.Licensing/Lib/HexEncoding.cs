namespace XWS.BusinessPlatform.Licensing.Lib
{
    using System;
    using System.Globalization;
    using System.Linq;

    /// <summary>
    /// Summary description for HexEncoding.
    /// </summary>
    public class HexEncoding
    {
        ///<summary>
        ///</summary>
        ///<param name="hexString"></param>
        ///<returns></returns>
        public static int GetByteCount(string hexString)
        {
            int numHexChars = hexString.Count(IsHexDigit);
            // remove all none A-F, 0-9, characters
            // if odd number of characters, discard last character
            if (numHexChars % 2 != 0)
            {
                numHexChars--;
            }
            return numHexChars / 2; // 2 characters per byte
        }

        /// <summary>
        /// Creates a byte array from the hexadecimal string. Each two characters are combined
        /// to create one byte. First two hexadecimal characters become first byte in returned array.
        /// Non-hexadecimal characters are ignored. 
        /// </summary>
        /// <param name="hexString">string to convert to byte array</param>
        /// <param name="discarded">number of characters in string ignored</param>
        /// <returns>byte array, in the same left-to-right order as the hexString</returns>
        public static byte[] GetBytes(string hexString, out int discarded)
        {
            discarded = 0;
            string newString = "";
            // remove all none A-F, 0-9, characters
            foreach (char c in hexString)
            {
                if (IsHexDigit(c))
                    newString += c;
                else
                    discarded++;
            }
            // if odd number of characters, discard last character
            if (newString.Length % 2 != 0)
            {
                discarded++;
                newString = newString.Substring(0, newString.Length - 1);
            }

            int byteLength = newString.Length / 2;
            byte[] bytes = new byte[byteLength];
            int j = 0;
            for (int i = 0; i < bytes.Length; i++)
            {
                bytes[i] = HexToByte(new String(new[] { newString[j], newString[j + 1] }));
                j += 2;
            }
            return bytes;
        }

        ///<summary>
        ///</summary>
        ///<param name="bytes"></param>
        ///<returns></returns>
        public static string ToString(byte[] bytes)
        {
            string hexString = "";
            for (int i = 0; i < bytes.Length; i++)
            {
                hexString += bytes[i].ToString("X2");
            }
            return hexString;
        }

        /// <summary>
        /// Determines if given string is in proper hexadecimal string format
        /// </summary>
        /// <param name="hexString"></param>
        /// <returns></returns>
        public static bool InHexFormat(string hexString)
        {
            return hexString.All(IsHexDigit);
        }

        /// <summary>
        /// Returns true is c is a hexadecimal digit (A-F, a-f, 0-9)
        /// </summary>
        /// <param name="c">Character to test</param>
        /// <returns>true if hex digit, false if not</returns>
        public static bool IsHexDigit(Char c)
        {
            int numA = Convert.ToInt32('A');
            int num1 = Convert.ToInt32('0');
            int numChar = Convert.ToInt32(Char.ToUpper(c));
            return numChar >= numA && numChar < (numA + 6) || numChar >= num1 && numChar < (num1 + 10);
        }

        /// <summary>
        /// Converts 1 or 2 character string into equivalant byte value
        /// </summary>
        /// <param name="hex">1 or 2 character string</param>
        /// <returns>byte</returns>
        private static byte HexToByte(string hex)
        {
            if (hex.Length > 2 || hex.Length <= 0)
                throw new ArgumentException("hex must be 1 or 2 characters in length");
            return byte.Parse(hex, NumberStyles.HexNumber);
        }
    }
}