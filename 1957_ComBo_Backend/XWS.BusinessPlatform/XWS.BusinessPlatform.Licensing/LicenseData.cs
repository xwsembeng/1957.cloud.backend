﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace XWS.BusinessPlatform.Licensing
{
    public static class LicenseData
    {
        public static string CustomerName { get; private set; }
        public static string CustomerEmail { get; private set; }
        public static IList<LicenseModule> Modules { get; private set; }
        
        internal static void SetData(XDocument xDoc)
        {
            CustomerName = xDoc.Element("LICENSE").Element("NAME").Value;
            CustomerEmail = xDoc.Element("LICENSE").Element("EMAIL").Value;
            Modules = new List<LicenseModule>();
            foreach (XElement xModule in xDoc.Element("LICENSE").Element("MODULES").Elements("MODULE"))
            {
                string name = xModule.Attribute("name").Value;
                int count = Convert.ToInt32(xModule.Attribute("count").Value);
                string sValidThrough = xModule.Attribute("validthrough").Value;
                DateTime validThrough = DateTime.Parse(sValidThrough);
                Modules.Add(new LicenseModule(name, count, validThrough));
            }
        }
    }
}