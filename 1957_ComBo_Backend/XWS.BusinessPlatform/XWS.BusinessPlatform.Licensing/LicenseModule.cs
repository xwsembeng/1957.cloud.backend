﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XWS.BusinessPlatform.Licensing
{
    /// <summary>
    /// This class describes a single module with it's registered licenses, used licenses and free licenses
    /// </summary>
    public class LicenseModule
    {
        internal bool Registered { get; private set; }

        /// <summary>
        /// The ModuleCode to which the object belongs to
        /// </summary>
        public string ModuleCode { get; private set; }

        /// <summary>
        /// The number of licenses registered to this module
        /// </summary>
        public int LicenseCount { get; private set; }

        /// <summary>
        /// The license for this module is valid until this date
        /// </summary>
        public DateTime ValidThrough { get; private set; }

        /// <summary>
        /// The number of licenses which are already used up in this module
        /// </summary>
        public int LicenseUsed { get; private set; }

        /// <summary>
        /// The number of licenses which are free to use
        /// </summary>
        public int LicenseFree { get; private set; }

        /// <summary>
        /// Thsi constructor initializes the module with the ModuleCode and the number of registered licenses
        /// </summary>
        /// <param name="moduleCode">the ModuleCode to which the object belongs to</param>
        /// <param name="count">The number of registered licenses</param>
        public LicenseModule (string moduleCode, int count, DateTime validThrough)
        {
            ModuleCode = moduleCode;
            ValidThrough = validThrough;
            LicenseCount = count;
            LicenseUsed = 0;
            LicenseFree = count;
        }

        /// <summary>
        /// Registeres the number of users and calculates the number of used up licenses and free licenses
        /// </summary>
        /// <param name="active">the number of active users</param>
        /// <param name="inactive">the number of inactive users</param>
        public void RegisterUsers(int active, int inactive)
        {
            LicenseUsed = active;
            LicenseFree = LicenseCount - active;

            Registered = true;
        }

        /// <summary>
        /// Checks if license is valid
        /// </summary>
        /// <returns>true if there are 0 or more licenses left for use</returns>
        public bool IsValid()
        {
            DateTime today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            bool valid = true;

            if (LicenseFree < 0)
                valid = false;

            if (ValidThrough <= today)
                valid = false;

            return valid;
        }

        public override string ToString()
        {
            return String.Format("{0}({1};{2}): Used:{3}; Free:{4}",
                                 ModuleCode, LicenseCount, ValidThrough.ToShortDateString(), LicenseUsed, LicenseFree);
        }
    }
}