﻿using System;
using System.Linq;
using XWS.BusinessPlatform.Core.InversionOfControl;
using XWS.BusinessPlatform.Security.Repository.Interfaces;

namespace XWS.BusinessPlatform.Licensing.Exceptions
{
    public class LicensingException: Exception
    {
        private readonly bool _showDetails;

        public LicensingException(string message)
            : base(message)
        {
            _showDetails = false;
        }

        public LicensingException(string message, Exception innerException)
            : base(message, innerException)
        {
            _showDetails = false;
        }

        public LicensingException(string message, bool showDetails)
            : base(message)
        {
            _showDetails = showDetails;
        }

        public LicensingException(string message, Exception innerException, bool showDetails)
            : base(message, innerException)
        {
            _showDetails = showDetails;
        }

        public string LicenseDetails
        {
            get
            {
                if (_showDetails)
                {
                    string output =
                        $"<b>License Information</b><br /><table cellspacing=\"0\" cellpadding=\"0\"><tr><td>Name:</td><td>{LicenseData.CustomerName}</td></tr><tr><td>EMail:</td><td>{LicenseData.CustomerEmail}</td></tr></table><br />";
                    output +=
                        $"<table cellspacing=\"0\" cellpadding=\"0\"><tr><td>Number of Licenses:</td><td>{LicenseData.Modules.Sum(n => n.LicenseCount)}</td></tr><tr><td>Active Users:</td><td>{IoC.Resolve<IAppUserRepository>().FindAll().Count(n => !n.IsObsolete && n.Active)}</td></tr><tr><td>Inactive Users:</td><td>{IoC.Resolve<IAppUserRepository>().FindAll().Count(n => !n.IsObsolete && !n.Active)}</td></tr></table><br />";
                    output += "<table cellspacing=\"0\" cellpadding=\"0\">";
                    output += "<tr><th>Module&nbsp;</th><th>Licenses&nbsp;</th><th>ValidThrough&nbsp;</th><th>Status</th></tr>";
                    foreach (LicenseModule module in LicenseData.Modules)
                    {
                        output +=
                            $"<tr><td>{module.ModuleCode}</td><td>{module.LicenseCount}</td><td>{module.ValidThrough.ToShortDateString()}</td>";
                        output += (module.IsValid() ? "<td style=\"color:green;\">VALID</td>" : "<td style=\"color:red;\">ERROR</td>");
                        output += "</tr>";
                    }
                    output += "</table>";

                    return output;
                }
                string ret = this.Message;
                if (InnerException != null)
                {
                    ret = InnerException.Message;
                }
                return ret;
            }
        }
    }
}
