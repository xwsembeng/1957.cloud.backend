﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Xml.Linq;
using XWS.BusinessPlatform.Core.InversionOfControl;
using XWS.BusinessPlatform.Licensing.Lib;
using XWS.BusinessPlatform.Security.Domain.Interfaces;
using XWS.BusinessPlatform.Licensing.Exceptions;
using XWS.BusinessPlatform.Security.Domain;
using XWS.BusinessPlatform.Security.Repository.Interfaces;
using XWS.RulesEngine;
using XWS.RulesEngine.Rules;
using XWS.Util;
using Module = XWS.BusinessPlatform.Security.Domain.Module;


namespace XWS.BusinessPlatform.Licensing
{
    public static class License
    {
        public static bool LicenseFileSuccessfullyRead { get; set; }

        public static void ReadLicenseFile()
        {
            try
            {
                // read public key and create ServiceProvider
                Stream pkStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("XWS.BusinessPlatform.Licensing.PublicKey.licensing.public.key.snk");
                BinaryReader br = new BinaryReader(pkStream);
                byte[] publicKey = br.ReadBytes((int)pkStream.Length);
                br.Close();
                RSACryptoServiceProvider verifier = new RSACryptoServiceProvider();
                verifier.ImportParameters(EncryptionUtils.GetRSAParameters(publicKey));

                // read license file
                string filename = HttpRuntime.AppDomainAppPath + "\\license.lic";
                if (!File.Exists(filename))
                    throw new FileNotFoundException("LicenseFile not found in application path.");
                StreamReader sr = new StreamReader(filename);
                XDocument xLic = XDocument.Parse(sr.ReadToEnd());
                sr.Close();

                // extract signature from license file
                string signatureHex = xLic.Element("LICENSE").Element("SIGNATURE").Value;
                xLic.Element("LICENSE").Element("SIGNATURE").Remove();
                ASCIIEncoding ByteConverter = new ASCIIEncoding();
                byte[] data = ByteConverter.GetBytes(xLic.ToString());

                // check signature
                int discarded;
                if (!verifier.VerifyData(data, new SHA1CryptoServiceProvider(), HexEncoding.GetBytes(signatureHex, out discarded)))
                    throw new LicensingException("License File is not valid");

                // set data of the license into object model
                LicenseData.SetData(xLic);

                LicenseFileSuccessfullyRead = true;
            }
            catch (LicensingException le)
            {
                LicenseFileSuccessfullyRead = false;
                throw le;
            }
            catch (Exception exc)
            {
                LicenseFileSuccessfullyRead = false;
                throw new LicensingException("License File Parsing Error", exc);
            }
        }

        public static void Validate()
        {
            bool valid = true;
            foreach (LicenseModule module in LicenseData.Modules)
            {
                Register(module.ModuleCode);

                if (!module.IsValid())
                    valid = false;
            }

            if (valid)
            {
                int licenseCount = LicenseData.Modules.Sum(n => n.LicenseCount);
                IList<IAppUser> userList = IoC.Resolve<IAppUserRepository>().FindAll().Where(n => !n.IsObsolete).ToList();
                int activeUsers = userList.Count(n => n.Active);
                int allUsers = userList.Count;
                if (activeUsers > licenseCount || allUsers > (licenseCount * 2))
                    valid = false;
            }

            if (!valid)
                throw new LicensingException("Not enough licenses", true);
        }

        public static RulesCollection CheckRulesForUser(IAppUser user)
        {
            // Licence-Informationen aktualisieren
            foreach (LicenseModule module in LicenseData.Modules)
                Register(module.ModuleCode);

            RulesCollection rulesCollection = new RulesCollection();

            if (user.IsObsolete)
                return rulesCollection;

            int licenseCount = LicenseData.Modules.Sum(n => n.LicenseCount);

            IList<IAppUser> userList = IoC.Resolve<IAppUserRepository>().FindAll().Where(n => !n.IsObsolete).ToList();
            int activeUsers = userList.Count(n => n.Active);
            int allUsers = userList.Count;

            if (user.Active && activeUsers > licenseCount)
                rulesCollection.AddRule(new CustomRule("License", "Licensing_NotEnoughLicenses", false));

            if (!user.Active && allUsers > (licenseCount * 2))
                rulesCollection.AddRule(new CustomRule("License", "Licensing_NotEnoughLicensesInactive", false));

            if (user.Active && !user.IsTransient)
            {
                IUserProfileStructureUnitRepository userProfileStructureUnitRepository = IoC.Resolve<IUserProfileStructureUnitRepository>();

                IList<User_Profile_StructureUnit> userProfileStructureUnits = userProfileStructureUnitRepository.GetUserProfileStructureUnitByUser(user);

                IEnumerable<Profile> profiles = userProfileStructureUnits.Select(obj => obj.Profile).Distinct();
                IEnumerable<Module> modules = profiles.SelectMany(obj => obj.Modules).Distinct();

                modules.ForEach(obj => rulesCollection.AddRule(new CustomRule("License", "Licensing_NotEnoughLicensesModule",
                                                                   FreeLicenseAvailable(obj.Code), obj.Code)));

            }

            return rulesCollection;
        }

        public static bool CanAddNewUsers()
        {
            // Licenz-Informationen aktualisieren
            foreach (LicenseModule module in LicenseData.Modules)
                Register(module.ModuleCode);

            int licenseCount = LicenseData.Modules.Sum(n => n.LicenseCount);

            IList<IAppUser> userList = IoC.Resolve<IAppUserRepository>().FindAll().Where(n => !n.IsObsolete).ToList();
            int activeUsers = userList.Count(n => n.Active);
            int allUsers = userList.Count;

            // wenn bereits alle Lizenzen mit aktiven Usern verbraucht sind, kann nichts mehr angelegt werden
            if (activeUsers >= licenseCount)
                return false;

            // insgesamt dürfen maximal doppelt soviele User in der Datenbank stehen, wie es Lizenzen gibt
            if (allUsers >= (licenseCount * 2))
                return false;

            // Das heißt, es gibt noch irgendwo eine freie Lizenz.
            return true;
        }

        public static bool LicenseAvailable(string moduleCode, bool defaultUnregisteredModules)
        {
            // wenn beim session start die license noch nicht da ist, wird sie danach auch nicht da sein
            //Register(moduleCode);

            LicenseModule module = GetLicenseModule(moduleCode);
            if (module != null)
                return (module.LicenseCount > 0);

            return defaultUnregisteredModules;
        }

        private static bool FreeLicenseAvailable(string moduleCode)
        {
            LicenseModule module = GetLicenseModule(moduleCode.Split('.').Any() ? moduleCode.Split('.')[0] : moduleCode);

            if (module != null && module.LicenseCount > 0)
                return (module.LicenseFree >= 0);

            return true;
        }

        #region private method handling

        private static LicenseModule GetLicenseModule(string moduleCode)
        {
            return LicenseData.Modules.SingleOrDefault(n => n.ModuleCode.Equals(moduleCode));
        }

        private static void Register(string moduleCode)
        {
            IList<IAppUser> appUsersForModule = GetRegisteredUsers(moduleCode);
            IList<IAppUser> appUsersForAdministration = new List<IAppUser>();
            if (LicenseAvailable(moduleCode, false))
                appUsersForAdministration = GetSuperAdmins();

            foreach (IAppUser user in appUsersForAdministration)
            {
                if (appUsersForModule.All(n => n.Id != user.Id))
                    appUsersForModule.Add(user);
            }

            int activeUserCount = appUsersForModule.Count(n => n.Active);
            int inactiveUserCount = appUsersForModule.Count(n => !n.Active);

            LicenseModule licenseModule = GetLicenseModule(moduleCode);
            licenseModule?.RegisterUsers(activeUserCount, inactiveUserCount);
        }

        private static IList<IAppUser> GetRegisteredUsers(string moduleCode)
        {
     
            Module module = IoC.Resolve<IModuleRepository>().FindByCode(moduleCode);

            IList<Profile> profiles = new List<Profile>();
            if (module != null)
            {
                if(module.ModuleType==ModuleType.ModuleContainer)
                {
                    foreach (Module childModule in module.Children)
                    {
                        //List<Profile> childProfiles = childModule.Profiles;
                        foreach (Profile childProfile in childModule.Profiles)
                        {
                            if(!profiles.Contains(childProfile))
                            {
                                profiles.Add(childProfile);
                            }
                        }
                    }
                }
                else
                {
                    profiles = module.Profiles.ToList();    
                }
                
            }
           
            IList<IAppUser> appUsersForModule = new List<IAppUser>();

            foreach (Profile profile in profiles)
            {
                // profiles to ignore for licensing
                if (profile.Code.Equals("Supplier")
                    || profile.Code.Equals("Assessor"))
                    continue;

                IList<IAppUser> appUsersForProfile = FindUserByProfile(profile);
                foreach (IAppUser appUser in appUsersForProfile)
                {
                    if (!appUsersForModule.Any(n => n.Id.Equals(appUser.Id)))
                        appUsersForModule.Add(appUser);
                }
            }

            return appUsersForModule;
        }

        public static IList<IAppUser> GetSuperAdmins()
        {
            Profile profile = IoC.Resolve<IProfileRepository>().GetByCode("SuperAdmin");
            return FindUserByProfile(profile);
        }

        private static IList<IAppUser> FindUserByProfile(Profile profile)
        {
			//IAppUserRepository userRepository = IoC.Resolve<IAppUserRepository>();
            IUserProfileStructureUnitRepository userProfileStructureUnitRepository = IoC.Resolve<IUserProfileStructureUnitRepository>();
            IList<User_Profile_StructureUnit> userProfileStructureUnits = userProfileStructureUnitRepository.GetUserProfileStructureUnitsByProfile(profile);

            IList<IAppUser> appUsers = new List<IAppUser>();
            foreach (User_Profile_StructureUnit userProfileStructureUnit in userProfileStructureUnits)
            {
                IAppUser appUser = userProfileStructureUnit.User;
                if (appUser != null && !appUser.IsObsolete)
                    appUsers.Add(appUser);
            }
            return appUsers;
        }

        #endregion
    }
}