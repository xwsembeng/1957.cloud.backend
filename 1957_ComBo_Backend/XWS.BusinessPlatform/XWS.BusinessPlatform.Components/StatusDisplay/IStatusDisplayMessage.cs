using System;

namespace XWS.BusinessPlatform.Components.StatusDisplay
{
    public interface IStatusDisplayMessage
    {
        string Text { get; set; }
        MessageLevel Level { get; set; }
        bool IsResourceString { get; }
        bool IsFormatString { get; }
        string[] FormatStringParams { get; }
        DateTime TimeStamp { get; }
        bool IsSticky { get; }
        string ResourceFile { get; set; }
    }
}