﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XWS.RulesEngine;

namespace XWS.BusinessPlatform.Components.StatusDisplay
{
    /// <summary>
    /// Container for Status Messages
    /// </summary>
    [Serializable]
    internal class StatusDisplayContainer : IStatusDisplayContainer
    {
        #region Member Variables
        readonly IList<IStatusDisplayMessage> _messages;
        [field:NonSerialized]
        public event EventHandler MessageListChanged;
        #endregion Member Variables


        #region Properties

        #region Private Properties
        #endregion Private Properties


        #region Protected Properties
        #endregion Protected Properties


        #region Public Properties
        public IEnumerable<IStatusDisplayMessage> Messages
        {
            get { return _messages; }
        }

        public IEnumerable<String> this[MessageLevel level]
        {
            get 
            {
                return (from message in Messages where message.Level == level 
                        select message.Text).ToList<String>();
            }  
        }

        public bool IsEmpty
        {
            get { return (_messages.Count == 0); }
        }

        public bool ContainsMessage(string message)
        {
            return Messages.Any(obj => obj.Text == message);
        }

        public bool ContainsWarningOrErrorMessages
        {
            get
            {
                return (from message in Messages
                        where message.Level == MessageLevel.Warning || message.Level == MessageLevel.Error
                        select message).Any();
            }
        }

        public bool ContainsOnlyStickyMessages
        {
            get
            {
                return !(from message in Messages
                        where message.IsSticky == false
                        select message).Any();
            }
        }

        public bool ContainsStickyMessages
        {
            get { return Messages.Any(message => message.IsSticky); }
        }

        #endregion public properties

        #endregion Properties


        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="StatusDisplayContainer"/> class.
        /// </summary>
        public StatusDisplayContainer()
        {
            _messages = new List<IStatusDisplayMessage>();
        }
        #endregion Constructors


        #region Methods

        #region Private Methods
        private void StatusDisplayContainer_MessageListChanged(EventArgs e)
        {
            if (MessageListChanged != null)
            {
                MessageListChanged(this, e);
            }
        }
        #endregion Private Methods


        #region Protected Methods
        #endregion Protected Methods


        #region Public Methods
        public void AddMessage(string message, MessageLevel level, params string[] formatStringParams)
        {
            AddMessage(message, level, false, false, formatStringParams);
        }

        public void AddMessage(string message, MessageLevel level, bool isResourceString, params string[] formatStringParams)
        {
            AddMessage(message, level, isResourceString, false, formatStringParams);
        }

        public void AddMessage(string message, MessageLevel level, bool isResourceString, bool isSticky, params string[] formatStringParams)
        {
            RemoveNonStickyMessages();

            AppendMessage(message, level, isResourceString, isSticky, formatStringParams);
        }

        public void AddMessage(IStatusDisplayMessage displayMessage)
        {
            AppendMessage(displayMessage);
        }

        public void AppendMessage(string message, MessageLevel level, params string[] formatStringParams)
        {
            AppendMessage(message, level, false, false, formatStringParams);
        }

        public void AppendMessage(string message, MessageLevel level, bool isResourceString, params string[] formatStringParams)
        {
            AppendMessage(message, level, isResourceString, false, formatStringParams);
        }

        public void AppendMessage(string message, MessageLevel level, bool isResourceString, bool isSticky, params string[] formatStringParams)
        {
           AppendMessage(message,level,isResourceString,String.Empty,isSticky,formatStringParams);
        }

        public void AppendMessage(string message, MessageLevel level, bool isResourceString, string resourceFile, bool isSticky, params string[] formatStringParams)
        {
            IStatusDisplayMessage statusDisplayMessage = new StatusDisplayMessage(message, level, isResourceString, isSticky, formatStringParams);

            statusDisplayMessage.ResourceFile = resourceFile;
            
            if (!_messages.Contains(statusDisplayMessage))
            {
                if (statusDisplayMessage.IsSticky)
                {
                    // Add sticky messages on top of other messages
                    _messages.Insert(0, statusDisplayMessage);
                }
                else
                {
                    _messages.Add(statusDisplayMessage);
                }

                StatusDisplayContainer_MessageListChanged(EventArgs.Empty);
            }

        }

        public void AppendMessage(IStatusDisplayMessage displayMessage)
        {
            AppendMessage(displayMessage.Text, displayMessage.Level, displayMessage.IsResourceString,
                          displayMessage.IsSticky, displayMessage.FormatStringParams);
        }


        public void AddBrokenRulesList(BrokenRulesCollection brokenRulesList)
        {
            if (brokenRulesList.IsEmpty)
            {
                return;
            }

            RemoveNonStickyMessages();

            foreach (var brokenRule in brokenRulesList)
            {
                var level = (MessageLevel)Enum.Parse(typeof(MessageLevel), brokenRule.Level.ToString());
                AppendMessage(brokenRule.ResourceString, level, true, brokenRule.ResourceFile, false, brokenRule.FormatStringParams);
            }
        }


        public void RemoveMessage(string message)
        {
            foreach (var m in new List<IStatusDisplayMessage>(Messages))
            {
                if (m.Text == message)
                {
                    _messages.Remove(m);
                }
            }

            StatusDisplayContainer_MessageListChanged(EventArgs.Empty);
        }

        private void RemoveNonStickyMessages()
        {
            foreach (IStatusDisplayMessage m in new List<IStatusDisplayMessage>(_messages))
            {
                if (!m.IsSticky)
                {
                    _messages.Remove(m);
                }
            }
        }

        public void Clear()
        {
            RemoveNonStickyMessages();
        }

        public void ClearAll()
        {
            _messages.Clear();
        }
        #endregion Public Methods

        #endregion Methods


        #region Override ToString
        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            var message = new StringBuilder();

            foreach (var m in _messages)
            {
                message.Append("<p>" + m + "</p>");
            }

            return message.ToString();
        }
        #endregion Override ToString()
    }
}