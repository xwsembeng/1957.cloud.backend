using System;
using System.Collections.Generic;
using XWS.RulesEngine;

namespace XWS.BusinessPlatform.Components.StatusDisplay
{
    public interface IStatusDisplayContainer
    {
        //[field:NonSerialized]
        event EventHandler MessageListChanged;

        IEnumerable<IStatusDisplayMessage> Messages { get; }
        bool IsEmpty { get; }
        IEnumerable<String> this[MessageLevel level] { get; }
        void AddMessage(string message, MessageLevel level, params string[] formatStringParams);
        void AddMessage(string message, MessageLevel level, bool isResourceString, params string[] formatStringParams);
        void AddMessage(string message, MessageLevel level, bool isResourceString, bool isSticky, params string[] formatStringParams);
        void AddMessage(IStatusDisplayMessage displayMessage);
        void AppendMessage(string message, MessageLevel level, params string[] formatStringParams);
        void AppendMessage(string message, MessageLevel level, bool isResourceString, bool isSticky, params string[] formatStringParams);
        void AppendMessage(string message, MessageLevel level, bool isResourceString, params string[] formatStringParams);
        void AppendMessage(IStatusDisplayMessage displayMessage);
        void AddBrokenRulesList(BrokenRulesCollection brokenRulesList);
        
        void RemoveMessage(string message);
        void Clear();
        void ClearAll();
        bool ContainsMessage(string message);
        bool ContainsWarningOrErrorMessages { get; }
        bool ContainsOnlyStickyMessages { get; }
        bool ContainsStickyMessages { get; }
    }
}