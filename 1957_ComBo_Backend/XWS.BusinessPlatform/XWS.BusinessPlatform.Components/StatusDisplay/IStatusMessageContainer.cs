﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.BusinessPlatform.Components.StatusDisplay
{
    public interface IStatusMessageContainer
    {
        string StatusMessage { get; }
        bool IsError { get; }
        Exception Exception { get; }
    }
}
