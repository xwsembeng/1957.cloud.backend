﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.BusinessPlatform.Components.StatusDisplay
{
    /// <summary>
    /// Wrapper for status messages
    /// </summary>
    public class StatusMessageContainer : IStatusMessageContainer
    {
        public StatusMessageContainer(string statusMessage, bool isError, Exception exception = null)
        {
            StatusMessage = statusMessage;
            IsError = isError;
            Exception = exception;
        }
        public string StatusMessage { get; }
        public bool IsError { get;  }
        public Exception Exception { get; }
    }
}
