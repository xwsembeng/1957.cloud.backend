﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Resources;


namespace XWS.BusinessPlatform.Components.StatusDisplay
{
    /// <summary>
    /// Description of StatusDisplayEntry
    /// </summary>
    [Serializable]
    internal class StatusDisplayMessage : IStatusDisplayMessage
    {
        #region Member Variables
        #endregion Member Variables


        #region Properties

        #region Private Properties
        #endregion Private Properties


        #region Protected Properties
        #endregion Protected Properties


        #region Public Properties

        #endregion public properties
        public string Text { get; set; }
        public MessageLevel Level { get; set; }
        public bool IsResourceString
        {
            get;
            private set;
        }

        public string ResourceFile { get; set; }

        public bool IsFormatString
        {
            get { return FormatStringParams != null && FormatStringParams.Length > 0; }
        }

        public string[] FormatStringParams
        {
            get;
            private set;
        }

        public DateTime TimeStamp
        {
            get; private set;
        }

        public bool IsSticky
        {
            get; private set;
        }

        #endregion Properties


        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="StatusDisplayMessage"/> class.
        /// </summary>
        public StatusDisplayMessage(string text, MessageLevel level, bool isResourceString, bool isSticky, params string[] formatStringParams)
        {
            this.Text = text;
            this.Level = level;
            this.IsResourceString = isResourceString;
            this.FormatStringParams = formatStringParams;
            this.TimeStamp = DateTime.Now;
            this.IsSticky = isSticky;
        }
        #endregion Constructors


        #region Methods

        #region Private Methods
        #endregion Private Methods


        #region Protected Methods
        #endregion Protected Methods


        #region Public Methods
        #endregion Public Methods

        #endregion Methods


        #region Overrides

        #region Override ToString
        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        { 
            return Text;
        }
        #endregion Override ToString()


        #region Override Equals
        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>.</param>
        /// <returns>
        /// true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            return (this.GetHashCode() == obj.GetHashCode());
        }
        #endregion Override Equals


        #region Override GetHashCode()
        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        public override int GetHashCode()
        {
            return Text.GetHashCode() ^ FormatStringParams.GetHashCode();
        }
        #endregion Override GetHashCode()

        #endregion Overrides
    }
}