using System;
using System.Collections;
using System.Threading;
using NHibernate;
using XWS.BusinessPlatform.API.Core;
using XWS.BusinessPlatform.Core.InversionOfControl;

namespace XWS.BusinessPlatform.Core.DatabaseManagement
{
    /// <summary>
    /// Manages a NHibernate transaction.
    /// </summary>
    public class TransactionContext : IDisposable
    {
        /// <summary>
        /// True if the context has to clean up after ist disposal, otherwise false.
        /// </summary>
        private bool _cleanUpOnExit;

        /// <summary>
        /// A hashtable with the operation contexts in the threads;
        /// </summary>
        private static Hashtable _contextsInThreads;

        /// <summary>
        /// The managed transaction.
        /// </summary>
        ITransaction _transaction;

        /// <summary>
        /// The static constructor.
        /// </summary>
        static TransactionContext()
        {
            _contextsInThreads = new Hashtable();
        }

        /// <summary>
        /// Creates a new managed transaction.
        /// </summary>
        public TransactionContext(bool clearSession)
        {
            // Hold the context only if not one exists yet.
            if (Current == null)
            {
                // Start the transaction
                ISessionManager sessionManager = IoC.Resolve<ISessionManager>();
                ISession session = sessionManager.GetSession();

                if (clearSession)
                {
                    session.Clear();
                }

                _transaction = session.BeginTransaction();

                lock (_contextsInThreads)
                {
                    _contextsInThreads.Add(Thread.CurrentThread.ManagedThreadId, this);
                    _cleanUpOnExit = true;
                }
            }
        }

        /// <summary>
        /// Creates a new managed transaction.
        /// </summary>
        public TransactionContext()
            : this(false)
        { }


        /// <summary>
        /// Commits the transaction.
        /// </summary>
        public void Commit()
        {
            if (_transaction != null)
                _transaction.Commit();
        }

        /// <summary>
        /// Rollback the transaction
        /// </summary>
        public void Rollback()
        {
            if (_transaction != null)
                _transaction.Rollback();
        }

        /// <summary>
        /// Holds the current database operation context.
        /// </summary>
        public static TransactionContext Current
        {
            get
            {
                return _contextsInThreads[Thread.CurrentThread.ManagedThreadId] as TransactionContext;
            }
        }

        #region IDisposable Members

        /// <summary>
        /// Frees the transaction resource.
        /// </summary>
        public void Dispose()
        {
            if (_cleanUpOnExit)
            {
                if (!_transaction.WasCommitted && !_transaction.WasRolledBack)
                {
                    _transaction.Rollback();
                }

                _transaction.Dispose();

                lock (_contextsInThreads)
                {
                    _contextsInThreads.Remove(Thread.CurrentThread.ManagedThreadId);
                }
            }
        }

        #endregion
    }
}