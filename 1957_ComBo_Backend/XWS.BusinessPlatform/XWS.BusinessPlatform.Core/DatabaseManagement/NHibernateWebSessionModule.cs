﻿using System.Diagnostics;
using Castle.DynamicProxy.Internal;
using NHibernate.Context;

namespace XWS.BusinessPlatform.Core.DatabaseManagement
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Web;
    using NHibernate;

    /// <summary>
    /// You will need to configure this module in the web.config file of your
    /// web and register it with IIS before being able to use it. For more information
    /// see the following link: http://go.microsoft.com/?linkid=8101007
    /// </summary>
    public class NHibernateWebSessionModule : IHttpModule
    {

        public void Init(HttpApplication context)
        {
            context.BeginRequest += context_BeginRequest;
            context.EndRequest += context_EndRequest;
        }

        void context_BeginRequest(object sender, EventArgs e)
        {
           InternalSessionManager.BindNHSession();
        }

        void context_EndRequest(object sender, EventArgs e)
        {
            InternalSessionManager.UnbindNHSession();
        }

        
        public void Dispose()
        {

        }
    }
}