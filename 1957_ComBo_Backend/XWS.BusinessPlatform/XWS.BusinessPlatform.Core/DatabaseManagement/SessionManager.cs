﻿using NHibernate;
using XWS.BusinessPlatform.API.Core;


namespace XWS.BusinessPlatform.Core.DatabaseManagement
{
    /// <summary>
    /// Implements the ISessionManager interface.
    /// </summary>
    public class SessionManager : ISessionManager
    {
        #region ISessionManager Members

        /// <summary>
        /// Returns a new session in the context of the session query. This means only a session is returned 
        /// if the call is in the context of a ambient transaction or an operation context.
        /// </summary>
        /// <returns>A NHibernate session</returns>
        public ISession GetSession()
        {
            return InternalSessionManager.GetSession();
        }
        #endregion
    }
}
