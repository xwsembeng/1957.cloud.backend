﻿using System;
using System.Collections;
using System.ServiceModel;
using System.Transactions;
using NHibernate;


namespace XWS.BusinessPlatform.Core.DatabaseManagement
{
    /// <summary>
    ///     Provides support to access a NHibernate session.
    /// </summary>
    public static class InternalSessionManager
    {

        /// <summary>
        ///     Creates a new instance of the SessionManager.
        /// </summary>
        static InternalSessionManager()
        {

        }


        //public static SessionProviderMode SessionMode { get; set; }

        /// <summary>
        ///     Returns a new session in the context of the session query. This means only a session is returned 
        ///     if the call is in the context of a ambient transaction or an operation context.
        /// </summary>
        /// <returns>A NHibernate session.</returns>
        public static ISession GetSession()
        {

            /*return (SessionMode == SessionProviderMode.WebMode ? WebSessionMode() : NonWebSessionMode()
                );*/
            ISessionFactory factory = DatabaseManager.GetSessionFactory();
            ISession currentSession = factory.GetCurrentSession();
            return currentSession;
        }

        /*public static ISession GetCurrentSession()
        {
            ISessionFactory factory = DatabaseManager.GetSessionFactory();
            ISession currentSession = factory.GetCurrentSession();
            return currentSession;
        }*/


        /*private static ISession NonWebSessionMode()
        {
            return NHibernateNonWebSessionModule.GetCurrentSession();
        }*/


        /// <summary>
        ///     Provides a session in wcf mode. You have to register the NHibernateSessionModule http module in your web config.
        /// </summary>
        /// <returns>A NHibernate session.</returns>
        /*public static ISession WebSessionMode()
        {
            return NHibernateWebSessionModule.GetCurrentSession();
        }*/

        public static void BindNHSession()
        {
            ISessionFactory factory = DatabaseManager.GetSessionFactory();
            HybridWebSessionContext.Bind(factory.OpenSession());
        }


        public static void UnbindNHSession()
        {

            ISessionFactory factory = DatabaseManager.GetSessionFactory();

            ISession session = HybridWebSessionContext.Unbind(factory);

            if (session != null)
            {
                if (session.Transaction != null &&
                    session.Transaction.IsActive)
                {
                    session.Transaction.Rollback();
                }
                session.Close();
            }


        }
    }
}