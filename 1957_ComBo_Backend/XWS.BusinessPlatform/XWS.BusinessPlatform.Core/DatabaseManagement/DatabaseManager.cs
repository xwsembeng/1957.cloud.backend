﻿using System;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using System.Data.SqlClient;

/// All classes in the DatabaseManagement namespace are providing fuctionality related to database management and 
/// database connection. For most they are providing a suited NHibernate abstraction for the framework.
namespace XWS.BusinessPlatform.Core.DatabaseManagement
{
    /// <summary>
    /// The DatabaseManager is a static class to manage the connection to the database.
    /// </summary>
    public static class DatabaseManager
    {
        /// <summary>
        /// The NHibernate configuration.
        /// </summary>
        private static NHibernate.Cfg.Configuration _config;
        /// <summary>
        /// A Trace instance.
        /// </summary>
        //private static Trace Trace { get; set; }
        /// <summary>
        /// The NHibernate session factory to create sessions.
        /// </summary>
        private static ISessionFactory _sessionFactory;

        //private static string _configPath;

        public static void Initialize(string configPath)
        {
            _config = new NHibernate.Cfg.Configuration().Configure(configPath + "\\hibernate.cfg.xml");
            _config.AddAssembly("XWS.ComBo.Modules.Domain");
            _config.AddAssembly("XWS.BusinessPlatform.Security.Domain");
            _config.AddAssembly("XWS.BusinessPlatform.Domain");

            _config.SetProperty("current_session_context_class", typeof(HybridWebSessionContext).AssemblyQualifiedName);
            _sessionFactory = _config.BuildSessionFactory();
        }

        /// <summary>
        /// Initializes the DatabaseManager. 
        /// </summary>
        //public static void InitializeBeforeClassMappings()
        //{
        //    //Trace = Trace.Instance;
        //    //Trace.Info("Initializing DatabaseManager");

        //    //// Create NHibernate configuration
        //    //Trace.Info("Building session factory");
        //    _config = new NHibernate.Cfg.Configuration().Configure(SystemState.ConfigPath + "\\hibernate.cfg.xml");
           
        //}

        /// <summary>
        /// Builds the session factory. This method should be called after all mapping assemblies and other
        /// NHibernate configuration parameters have been set to the NHibernate configuration.
        /// </summary>
        //public static void InitializeAfterClassMappings()
        //{

        //    //if(!String.IsNullOrEmpty(SystemState.ConnectionString))
        //    //{
        //    //    //override connectionstring
        //    //    if(_config.Properties.ContainsKey("connection.connection_string_name"))
        //    //    {
        //    //        _config.Properties.Remove("connection.connection_string_name");
        //    //    }
        //    //    _config.SetProperty("connection.connection_string", SystemState.ConnectionString);

        //    //}
        //    _config.SetProperty("current_session_context_class", typeof (HybridWebSessionContext).AssemblyQualifiedName);
        //    _sessionFactory = _config.BuildSessionFactory();
        //    //_config.AddAssembly();
            
        //}

        /// <summary>
        /// Closes the NHibernate session factory.
        /// </summary>
        public static void Terminate()
        {
            //Trace.Info("Closing session factory");
            _sessionFactory.Close();
        }


        public static ISessionFactory GetSessionFactory()
        {
            return _sessionFactory;
        }

        /// <summary>
        /// Creates a new session to the configured database.
        /// </summary>
        /// <returns>A new session.</returns>
        public static ISession CreateSession()
        {
            if (_sessionFactory != null)
            {
                return _sessionFactory.OpenSession();
            }
            else
            {
                throw new Exception("NHibernate SessionFactory is null. Please restart the application.");
            }
        }

        /// <summary>
        /// Creates the database schema to the configured database.
        /// </summary>
        public static void CreateDatabaseSchema()
        {
            //Trace.Info("Creating database Schema");
            new SchemaExport(_config).Create(true, true);
        }

        public static void UpdateDatabaseSchema()
        {
            //Trace.Info("Creating database Schema");
            new SchemaUpdate(_config).Execute(false, true);
        }

        /// <summary>
        /// Deletes the database schema.
        /// </summary>
        public static void DropDatabaseSchema()
        {
            //Trace.Info("Dropping database Schema");
            new SchemaExport(_config).Drop(true, true);
        }

        /// <summary>
        /// Gets the current configuration.
        /// </summary>
        public static NHibernate.Cfg.Configuration Config
        {
            get
            {
                return _config;
            }
        }

        /// <summary>
        /// Tests wheter the database and the database schema exists. Will be true if database and schema exitst else false.
        /// </summary>
        //public static bool DatabaseExists
        //{
        //    get
        //    {
        //        try
        //        {
        //            ISession session = InternalSessionManager.GetSession();

        //            session.CreateCriteria(typeof(Component)).List();
        //        }
        //        catch (ADOException e)
        //        {
        //            if (e.InnerException != null)
        //            {
        //                if (e.InnerException is SqlException)
        //                {
        //                    SqlException sqlException = e.InnerException as SqlException;

        //                    if (sqlException.ErrorCode == -2146232060)
        //                    {
        //                        throw;
        //                    }
        //                }
        //            }
        //            Trace.Instance.Warning("No log tables available");
        //            return false;
        //        }
    
        //        return true;
        //    }
        //}
    }
}
