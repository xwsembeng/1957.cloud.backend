﻿using System;
using System.Collections;
using System.Xml.Linq;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Castle.Windsor.Configuration.Interpreters;

namespace XWS.BusinessPlatform.Core.InversionOfControl
{
    /// <summary>
    /// Wrapper for Windsor Container
    /// </summary>
    public static class IoC
    {
        #region Member Variables
        private static IWindsorContainer _container;
        #endregion Member Variables

        #region Properties

        #region Static Properties
        public static IWindsorContainer Container
        {
            get { return _container ?? (_container = new WindsorContainer()); }
        }
        #endregion Static properties

        #endregion Properties

        #region Methods

        #region Static Methods
        public static TService Resolve<TService>()
        {
            return Container.Resolve<TService>();
        }


        public static TService Resolve<TService>(IDictionary arguments)
        {
            return Container.Resolve<TService>(arguments);
        }


        public static TService Resolve<TService>(string key)
        {
            return Container.Resolve<TService>(key);
        }


        public static TService Resolve<TService>(Type type)
        {
            return (TService)Container.Resolve(type);
        }

        /// <summary>
        /// Releases the instances
        /// </summary>
        /// <param name="instance"></param>
        public static void Release(object instance)
        {
            if (_container != null)
            {
                _container.Release(instance);
            }
        }


        public static void Configure(string configFile)
        {
            _container = new WindsorContainer(new XmlInterpreter(configFile));
        }

        public static void Dispose()
        {
            _container.Dispose();
        }
        /// <summary>
        /// Checks if the IoC-Container still tracks the component
        /// </summary>
        /// <param name="component"></param>
        /// <returns></returns>
        public static bool ContainerHasTrack(object component)
        {
            return _container.Kernel.ReleasePolicy.HasTrack(component);
        }


        public static void AddChildConfiguration(string configFile)
        {
            XDocument xDoc = XDocument.Load(configFile);

            foreach (XElement component in xDoc.Elements().Elements().Elements())
            {
                string key = component.Attribute("id").Value;
                Type serviceType = Type.GetType(component.Attribute("service").Value);
                Type type = Type.GetType(component.Attribute("type").Value);

                if (key == null)
                {
                    throw new InvalidOperationException("'id' is null");
                }
                if (serviceType == null || type == null)
                {
                    throw new InvalidOperationException(String.Format("error in configuration at key '{0}' (Service = '{1}';Type= '{2}')", key, serviceType, type));
                }

                _container.AddComponent(key, serviceType, type);
            }
        }
        #endregion Static Methods

        #endregion Methods
    }
}