﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Management;
using Microsoft.Azure;
using XWS.BusinessPlatform.Core.Extensions;

namespace XWS.BusinessPlatform.Core.Configurations
{
    public class ConfigurationProvider : IConfigurationProvider, IDisposable
    {
        private readonly Dictionary<string, string> _configuration = new Dictionary<string, string>();
        private EnvironmentDescription _environment;
        const string ConfigToken = "config:";
        bool _disposed = false;


        public ConfigurationProvider() { }

        public string GetConfigurationSettingValue(string configurationSettingName)
        {
            return this.GetConfigurationSettingValueOrDefault(configurationSettingName, string.Empty);
        }

        public string GetConfigurationSettingValueOrDefault(string configurationSettingName, string defaultValue)
        {
            if (!this._configuration.ContainsKey(configurationSettingName))
            {
                string configValue = CloudConfigurationManager.GetSetting(configurationSettingName);
                //bool isEmulated = Environment.CommandLine.Contains("iisexpress.exe") ||
                //                  Environment.CommandLine.Contains("w3wp.exe");
                //                  Process.GetCurrentProcess().GetAncestorNames().Contains("devenv"); // if debugging in VS, devenv will be the parent

                if (configValue != null && configValue.StartsWith(ConfigToken, StringComparison.OrdinalIgnoreCase))
                {
                    if (_environment == null)
                    {
                        LoadEnvironmentConfig();
                    }

                    configValue = _environment.GetSetting(
                        configValue.Substring(configValue.IndexOf(ConfigToken, StringComparison.Ordinal) + ConfigToken.Length));
                }

                try
                {
                    this._configuration.Add(configurationSettingName, configValue);
                }
                catch (ArgumentException)
                {
                    // at this point, this key has already been added on a different
                    // thread, so we're fine to continue
                }
            }

            return this._configuration[configurationSettingName] ?? defaultValue;
        }

        void LoadEnvironmentConfig()
        {
            var executingPath = Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath);

            // Check for build_output
            //int buildLocation = executingPath.IndexOf("Build_Output", StringComparison.OrdinalIgnoreCase);
            //if (buildLocation >= 0)
            //{
            //    string fileName = executingPath.Substring(0, buildLocation) + "local.config.user";
            //    if (File.Exists(fileName))
            //    {
            //        this._environment = new EnvironmentDescription(fileName);
            //        return;
            //    }
            //}

            // Web roles run in there app dir so look relative
            //int location = executingPath.IndexOf("XWS.ComBo.Modules.Web\\bin", StringComparison.OrdinalIgnoreCase);

            //if (location == -1)
            //{
            //    location = executingPath.IndexOf("ComboSimulator.WebJob\\bin", StringComparison.OrdinalIgnoreCase);
            //}
            //if (location >= 0)
            //{
            //    string fileName;
            //    //Search local.config.user-file for web application
            //    if (executingPath.Contains("XWS.ComBo.Modules.Web"))
            //    {
            //        fileName = executingPath.Substring(0, location) + "..\\..\\local.config.user";
            //    }
            //    //Search local.config.user-file for web jobs application
            //    else
            //    {
            //        fileName = executingPath.Substring(0, location) + "..\\local.config.user";
            //    }
                
            //    if (File.Exists(fileName))
            //    {
            //        this._environment = new EnvironmentDescription(fileName);
            //        return;
            //    }
            //}
            int location;
            string fileName;
            //Local web application
            if (executingPath.Contains("XWS.ComBo.Modules.Web\\bin"))
            {
                location = executingPath.IndexOf("XWS.ComBo.Modules.Web\\bin", StringComparison.OrdinalIgnoreCase);
                fileName = executingPath.Substring(0, location) + "..\\..\\local.config.user";
            }
            //Published web application
            else if(executingPath.Contains("wwwroot\\bin"))
            {
                location = executingPath.IndexOf("bin", StringComparison.OrdinalIgnoreCase);
                fileName = executingPath.Substring(0, location) + "\\local.config.user";
            }
            else if (executingPath.Contains("ComboSimulator"))
            {
                //Local web job
                if (executingPath.Contains("ComboSimulator.WebJob\\bin"))
                {
                    location = executingPath.IndexOf("ComboSimulator.WebJob\\bin", StringComparison.OrdinalIgnoreCase);
                    fileName = executingPath.Substring(0, location) + "..\\local.config.user";
                }
                //Published web job
                else
                {
                    location = executingPath.IndexOf("ComboSimulator-WebJob", StringComparison.OrdinalIgnoreCase);
                    fileName = executingPath.Substring(0, location) +
                               "..\\..\\..\\..\\..\\home\\site\\wwwroot\\local.config.user";
                }

            }
            else
            {
                //Local web job
                if (executingPath.Contains("XWS.ComBo.EventProcessor.WebJob\\bin"))
                {
                    location = executingPath.IndexOf("XWS.ComBo.EventProcessor.WebJob\\bin",
                        StringComparison.OrdinalIgnoreCase);
                    fileName = executingPath.Substring(0, location) + "..\\..\\local.config.user";
                }
                //Published web job
                else
                {
                    location = executingPath.IndexOf("ComboEventProcessor-WebJob", StringComparison.OrdinalIgnoreCase);
                    fileName = executingPath.Substring(0, location) +
                               "..\\..\\..\\..\\..\\home\\site\\wwwroot\\local.config.user";
                }

            }

            if (File.Exists(fileName))
            {
                this._environment = new EnvironmentDescription(fileName);
                return;
            }
            Trace.TraceInformation("File not found");

            throw new ArgumentException("Unable to locate local.config.user file.  Make sure you have run 'build.cmd local'.");
        }

        #region IDispose 
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                if (_environment != null)
                {
                    _environment.Dispose();
                }
            }

            _disposed = true;
        }

        ~ConfigurationProvider()
        {
            Dispose(false);
        }
        #endregion
    }
}
