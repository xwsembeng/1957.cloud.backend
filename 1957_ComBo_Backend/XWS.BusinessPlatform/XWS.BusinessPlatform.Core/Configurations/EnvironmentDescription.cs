﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.XPath;

namespace XWS.BusinessPlatform.Core.Configurations
{
    public class EnvironmentDescription : IDisposable
    {
        bool isDisposed = false;
        private readonly XmlDocument _document;
        private XPathNavigator _navigator;
        private string _fileName;
        private int _updatedValuesCount = 0;
        private const string ValueAttributeName = "value";
        private const string SettingXpath = "//setting[@name='{0}']";

        public EnvironmentDescription(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                throw new ArgumentNullException("fileName");
            }

            this._fileName = fileName;
            this._document = new XmlDocument();
            using (XmlReader reader = XmlReader.Create(fileName))
            {
                this._document.Load(reader);
            }
            //this._navigator = this._document.CreateNavigator();
        }

        public string GetSetting(string settingName, bool errorOnNull = true)
        {
            if (string.IsNullOrEmpty(settingName))
            {
                throw new ArgumentNullException("settingName");
            }

            string result = string.Empty;
            XmlNode node = this.GetSettingNode(settingName.Trim());
            if (node != null)
            {
                result = node.Attributes[ValueAttributeName].Value;
            }
            else
            {
                if (errorOnNull)
                {
                    var message = string.Format(CultureInfo.InvariantCulture, "{0} was not found", settingName);
                    throw new ArgumentException(message);
                }
            }
            return result;
        }

        private XmlNode GetSettingNode(string settingName)
        {
            string xpath = string.Format(CultureInfo.InvariantCulture, SettingXpath, settingName);
            return this._document.SelectSingleNode(xpath);
        }

        public void Dispose()
        {
            if (!this.isDisposed)
            {
                this.Dispose(true);
                GC.SuppressFinalize(this);
            }
        }

        void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.isDisposed = true;
                if (this._updatedValuesCount > 0)
                {
                    _document.Save(_fileName);
                    Console.Out.WriteLine("Successfully updated {0} mapping(s) in {1}", _updatedValuesCount, Path.GetFileName(_fileName).Split('.')[0]);
                }
            }
        }
    }
}
