﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Domain;

namespace XWS.BusinessPlatform.Repositories
{
    public class GenericInterfaceRepository<T> : GenericRepository<T> where T : IDomainObject
    {
        public override T FindById(Guid id)
        {
            return FindByProperty("Id", id).SingleOrDefault();
        }
    }
}
