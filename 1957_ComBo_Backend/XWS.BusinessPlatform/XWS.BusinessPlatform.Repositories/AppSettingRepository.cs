﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Linq;
using XWS.BusinessPlatform.Domain.Configuration;
using XWS.BusinessPlatform.Repositories.Interfaces;

namespace XWS.BusinessPlatform.Repositories
{
    internal class AppSettingRepository : GenericRepository<AppSetting>, IAppSettingRepository
    {
        #region IAppSettingRepository Members

        public AppSetting GetByKey(string key)
        {
            return Session.Query<AppSetting>().FirstOrDefault(obj => obj.Key == key);
        }

        public string GetValueByKey(string key)
        {
            AppSetting appSetting = GetByKey(key);

            return appSetting?.Value;
        }

        #endregion IAppSettingRepository Members
    }
}
