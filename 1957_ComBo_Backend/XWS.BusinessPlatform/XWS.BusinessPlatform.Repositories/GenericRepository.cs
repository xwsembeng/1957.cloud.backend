﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Criterion;
using XWS.BusinessPlatform.API.Core;
using XWS.BusinessPlatform.Core.InversionOfControl;
using XWS.BusinessPlatform.Domain;
using XWS.BusinessPlatform.Repositories.Interfaces;


namespace XWS.BusinessPlatform.Repositories
{
	/// <summary>
	///     This class provieds a default implementation of the IGenericRepository interface.
	/// </summary>
	/// <typeparam name = "T">The type of the business object the repository is intended for.</typeparam>
	public class GenericRepository<T> : IGenericRepository<T> where T : IDomainObject
	{
		private readonly ISessionManager _sessionManager;


		/// <summary>
		///     Creates a new generic repository instance.
		/// </summary>
		public GenericRepository()
		{
			_sessionManager = IoC.Resolve<ISessionManager>();
		}

		#region IGenericRepository<T> Members
		/// <summary>
		///     The session the repository works with.
		/// </summary>
		//protected ISession Session;
		public ISession Session
		{
			get
			{
				return _sessionManager.GetSession();
			}
		}


		public void ClearSession()
		{
			Session.Clear();
		}


		public void Evict(T instance)
		{
			try
			{
				Session.Evict(instance);
			}
			catch (KeyNotFoundException)
			{
				// Exception here means the instance does not exist in the session, 
				// therefore there is nothing to evict. 
			}
		}


		/// <summary>
		///     Returns a list of all entities of the type of the repository.
		/// </summary>
		/// <returns>A list with entities.</returns>
		public IList<T> FindAll()
		{
			return Session.CreateCriteria(typeof(T)).List<T>();
		}


		/// <summary>
		///     Returns a list of all entities of the type of the repository.
		///     Attention: The type must implement ICanBeDeactivated to use this function.
		/// </summary>
		/// <param name = "onlyActive">if set to <c>true</c> returns only active objects.</param>
		/// <returns>A list with entities.</returns>
		public IList<T> FindAll(bool onlyActive)
		{
			IList<T> result = Session.CreateCriteria(typeof(T)).List<T>();

			if (result.Count > 0 && !(result.First() is ICanBeDeactivated))
			{
				throw new Exception("To use this function, " + typeof(T) + " must implement the interface ICanBeDeactivated");
			}

			return onlyActive ? result.Where(obj => ((ICanBeDeactivated)obj).IsActive).ToList() : result;
		}


		public IList<T> FindAll(bool? isInUse, bool? isArchived)
		{
			ICriteria criteria = Session.CreateCriteria(typeof(T));
			if (isInUse != null)
				criteria.Add(Restrictions.Eq("IsInUse", isInUse));

			if (isArchived != null)
				criteria.Add(Restrictions.Eq("IsArchived", isArchived));

			return criteria.List<T>();
		}


		/// <summary>
		///     Returns a list of all entities of the type of the repository
		///     Attention: The type must implement ICanBeDeactivated to use this function.
		/// </summary>
		/// <param name = "onlyActive">Get only active objects</param>
		/// <param name = "domainObjectToAddIfNotInResultList">The domain object to add if it not in result list.</param>
		/// <returns>A list with entities.</returns>
		public IList<T> FindAll(bool onlyActive, T domainObjectToAddIfNotInResultList)
		{
			IList<T> result = FindAll(onlyActive);

			if (!result.Contains(domainObjectToAddIfNotInResultList))
			{
				result.Add(domainObjectToAddIfNotInResultList);
			}

			return result;
		}


		/// <summary>
		///     Finds one entity with a specific ID.
		/// </summary>
		/// <param name = "id">The id to look for.</param>
		/// <returns>The entity with the defined id or null if no enitity was found.</returns>
		public virtual T FindById(Guid id)
		{
			//return FindByProperty("Id", id).SingleOrDefault();
			return Session.Get<T>(id);
		}


		/// <summary>
		///     Reattaches a detached object to the nhibernate session.
		///     Warning: Not persisted changes in the object may be lost.
		/// </summary>
		/// <param name = "obj">The object to reattach</param>
		/// <returns></returns>
		public T Reload(T obj)
		{
			return FindById(obj.Id);
		}


		/// <summary>
		///     Gets the last persisted state of an object instance.
		///     Removes the object also from the nhibernate session cache.
		/// </summary>
		/// <param name = "obj"></param>
		/// <returns></returns>
		public T Revert(T obj)
		{
			Evict(obj);
			T result = Reload(obj);
			Evict(result);
			return result;
		}


		public virtual void Save(T instance)
		{
			Session.SaveOrUpdate(instance);
		}

	    /*public virtual void Merge(T instance)
	    {
	        Session.Merge(instance);
	    }*/


	    public virtual void Delete(T instance)
		{
			Session.Delete(instance);
		}


		/// <summary>
		///     Returns a list of all entities of the type of the repository sorted by a provided property.
		/// </summary>
		/// <param name = "propertyToSort">The property used for sorting</param>
		/// <param name = "sortAscending">determines the sorting direction</param>
		/// <returns></returns>
		public IList<T> FindAll(string propertyToSort, bool sortAscending)
		{
			return Session.CreateCriteria(typeof(T)).AddOrder(new Order(propertyToSort, sortAscending)).List<T>();
		}
		#endregion

		/// <summary>
		///     Finds an object by property. If more objects are found, the first is returned.
		/// </summary>
		/// <param name = "property">The property to find the object for</param>
		/// <param name = "value">The value to find the object for</param>
		/// <returns>The found object or null</returns>
		protected T FindObjectByProperty(string property, object value)
		{
			return FindObjectByCriterion(Restrictions.Eq(property, value));
		}


		/// <summary>
		///     Finds entities where a specific popertiy matches a specific value.
		/// </summary>
		/// <param name = "propertyName">The name of the property to look for.</param>
		/// <param name = "value">The value the specified property has to match.</param>
		/// <returns>A list of entities matching the property value.</returns>
		protected IList<T> FindByProperty(String propertyName, Object value)
		{
			return Session.CreateCriteria(typeof(T)).Add(Restrictions.Eq(propertyName, value)).List<T>();
		}


	    private IList<T> FindByCriterion(params ICriterion[] criterion)
		{
			ICriteria criteria = Session.CreateCriteria(typeof(T));

			foreach (ICriterion criterium in criterion)
			{
				criteria.Add(criterium);
			}

			return criteria.List<T>();
		}


		/// <summary>
		///     Finds an object by criterions. If more objects are found, the first is returned
		/// </summary>
		/// <param name = "criterion">The criterions to find the object for</param>
		/// <returns>The found object or null</returns>
		protected T FindObjectByCriterion(params ICriterion[] criterion)
		{
			return FindByCriterion(criterion).FirstOrDefault();
		}


		/// <summary>
		///     Finds entities where a specific popertiy matches an specific value and orders the result.
		/// </summary>
		/// <param name = "propertyName">The name of the property to look for.</param>
		/// <param name = "value">The value the specified property has to match.</param>
		/// <param name = "orderProperty">The order property.</param>
		/// <param name = "ascending">The order.</param>
		/// <returns>A ordered list of entities matching the property value.</returns>
		protected IList<T> FindByProperty(String propertyName, Object value, String orderProperty, bool ascending)
		{
			return Session.CreateCriteria(typeof(T)).Add(Restrictions.Eq(propertyName, value)).AddOrder(new Order(orderProperty, ascending)).List<T>();
		}

	    public IQueryable<T> Query()
        {
            return Session.Query<T>();
        }

	    /// <summary>
		///     Deletes all entities from the session where a specific property matches a specific value
		/// </summary>
		/// <param name = "propertyName">the name of the property to look for</param>
		/// <param name = "value">the value the specific property has to match</param>
		protected void DeleteByProperty(String propertyName, Object value)
		{
			IList<T> objects = FindByProperty(propertyName, value);
			foreach (T obj in objects)
				Delete(obj);
		}


	}
}