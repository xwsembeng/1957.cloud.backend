﻿using System.Linq;
using System;
using System.Collections.Generic;
using XWS.BusinessPlatform.Domain;
using NHibernate;

namespace XWS.BusinessPlatform.Repositories.Interfaces
{
   


    /// <summary>
    /// The IGenericRepository interface provides the most general functionality of a repository. You can use this
    /// repository for your own business objects repositories by deriving from it. A default implementation is provided
    /// in the Generic namespace.
    /// </summary>
    /// <typeparam name="T">The type of the entity.</typeparam>
    public interface IGenericRepository<T> where T : IDomainObject
    {
        ISession Session { get; }


        /// <summary>
        /// Returns a list of all entities of the type of the repository.
        /// </summary>
        /// <returns>A list with entities.</returns>
        IList<T> FindAll();


        /// <summary>
        /// Returns a list of all entities of the type of the repository sorted by a provided property.		
        /// <param name="propertyToSort">The property used for sorting</param>		
        /// <param name="sortAscending">determines the sorting direction</param>
        /// <returns>A list of sorted IBusinessObject entities.</returns>
        /// </summary>
        IList<T> FindAll(string propertyToSort, bool sortAscending);


        /// <summary>
        /// Finds one entity with a specific ID.
        /// </summary>
        /// <param name="id">The id to look for.</param>
        /// <returns>The entity with the defined id or null if no enitity was found.</returns>
        T FindById(Guid id);


        /// <summary>
        /// Returns a list of all entities of the type of the repository.
        /// Attention: The type must implement ICanBeDeactivated to use this function.
        /// </summary>
        /// <param name="onlyActive">if set to <c>true</c> returns only active objects.</param>
        /// <returns>A list with entities.</returns>
        IList<T> FindAll(bool onlyActive);


        /// <summary>
        /// Returns a list of all entities of the type of the repository
        /// Attention: The type must implement ICanBeDeactivated to use this function.
        /// </summary>
        /// <param name="onlyActive">Get only active objects</param>
        /// <param name="domainObjectToAddIfNotInResultList">The domain object to add if it not in result list.</param>
        /// <returns>A list with entities.</returns>
        IList<T> FindAll(bool onlyActive, T domainObjectToAddIfNotInResultList);


        IList<T> FindAll(bool? isInUse, bool? isArchived);

        void Delete(T instance);

        void Save(T instance);

        void ClearSession();

        void Evict(T instance);

        T Reload(T instance);
        T Revert(T instance);
        IQueryable<T> Query();
       // void Merge(T instance);
    }
}