﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Domain.Configuration;

namespace XWS.BusinessPlatform.Repositories.Interfaces
{
    public interface IAppSettingRepository : IGenericRepository<AppSetting>
    {
        AppSetting GetByKey(string key);
        string GetValueByKey(string key);
    }
}
