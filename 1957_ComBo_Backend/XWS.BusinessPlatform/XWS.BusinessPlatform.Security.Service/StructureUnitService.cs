﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Core.InversionOfControl;
using XWS.BusinessPlatform.Security.Domain;
using XWS.BusinessPlatform.Security.Domain.Interfaces;
using XWS.BusinessPlatform.Security.Repository.Interfaces;
using XWS.BusinessPlatform.Security.Service.Interfaces;

namespace XWS.BusinessPlatform.Security.Service
{
    internal class StructureUnitService : IStructureUnitService
    {
        private readonly IStructureUnitRepository _structureUnitRepository;
        private readonly ISecurityService _securityService;
        private readonly IProfileService _profileService;

        public StructureUnitService(IStructureUnitRepository structureUnitRepository, ISecurityService securityService,
            IProfileService profileService)
        {
            _structureUnitRepository = structureUnitRepository;
            _securityService = securityService;
            _profileService = profileService;
        }

        public IStructureUnit FindStructureUnitById(Guid structureUnitId)
        {
            return _structureUnitRepository.FindById(structureUnitId);
        }

        public IStructureUnit GetRootStructureUnit()
        {
            return _structureUnitRepository.GetRootStructureUnit();
        }

        public IList<IStructureUnit> GetAllStructureUnitsWithAssignedProfilesForUsers(IList<IAppUser> users, Module module)
        {
            return _structureUnitRepository.GetAllStructureUnitsWithAssignedProfilesForUsers(users, module);
        }

        public IList<IStructureUnit> GetAllStructureUnitsWithAssignedProfilesForUser(IAppUser user, Module module)
        {
            return !IoC.Resolve<ISecurityService>().IsSuperAdmin(user) ? _structureUnitRepository.GetAllStructureUnitsWithAssignedProfilesForUser(user, module) : GetAllStructureUnits();
        }

        public IEnumerable<IStructureUnit> GetStructureUnitsWithAssignedProfilesForUser(IAppUser user, string profileCode)
        {
            if (String.IsNullOrEmpty(profileCode))
            {
                throw new ArgumentNullException("profileCode", "Parameter profileCode must not be null or empty");
            }

            Profile profile = _profileService.FindProfileByCode(profileCode);

            if (profile == null)
            {
                throw new ArgumentException("A profile with profile code '" + profileCode + "' does not exist.");
            }

            return GetStructureUnitsWithAssignedProfilesForUser(user, profile);
        }

        public IEnumerable<IStructureUnit> GetStructureUnitsWithAssignedProfilesForUser(IAppUser user, Profile profile)
        {
            if (_securityService.IsSuperAdmin(user))
            {
                return GetAllStructureUnits();
            }

            return _structureUnitRepository.GetStructureUnitsWithAssignedProfilesForUser(user, profile);
        }

        public IList<IStructureUnit> GetAllStructureUnits()
        {
            return _structureUnitRepository.FindAll();
        }

        public IList<IStructureUnit> GetAllDescendantsForStructureUnit(IStructureUnit unit)
        {
            IList<IStructureUnit> ret = new List<IStructureUnit>();
            foreach (IStructureUnit child in unit.Children)
            {
                GetAllChildrenForStructureUnitRecursive(ref ret, child);
            }
            return ret;
        }

        public IList<IStructureUnit> GetAllAncestorsForStructureUnit(IStructureUnit unit)
        {
            IList<IStructureUnit> ret = new List<IStructureUnit>();
            while (unit.Parent != null)
            {
                unit = unit.Parent;
                ret.Add(unit);

            }
            return ret;
        }

        public IList<IStructureUnit> GetAllAncestorsForStructureUnits(IList<IStructureUnit> units)
        {
            IList<IStructureUnit> ret = new List<IStructureUnit>();
            foreach (IStructureUnit su in units)
            {
                foreach (IStructureUnit sa in GetAllAncestorsForStructureUnit(su))
                {
                    if (!ret.Contains(sa))
                    {
                        ret.Add(sa);
                    }

                }

            }
            return ret;
        }

        private void GetAllChildrenForStructureUnitRecursive(ref IList<IStructureUnit> children, IStructureUnit unit)
        {
            children.Add(unit);
            foreach (IStructureUnit child in unit.Children)
            {
                GetAllChildrenForStructureUnitRecursive(ref children, child);
            }
        }


        public void SaveStructureUnit(IStructureUnit su)
        {
            _structureUnitRepository.Save(su);
        }

        public void RemoveStructureUnit(Guid structureUnitId)
        {
            IStructureUnit su = _structureUnitRepository.FindById(structureUnitId);
            IStructureUnit suParent = su.Parent;
            suParent.RemoveChild(su);
            _structureUnitRepository.Delete(su);
        }

        public IStructureUnit AddChildStructureUnit(Guid parentStructureUnitId, string defaultName)
        {
            IStructureUnit suChild = new StructureUnit();
            IStructureUnit suParent = _structureUnitRepository.FindById(parentStructureUnitId);
            suChild.Name = defaultName;
            _structureUnitRepository.Save(suChild);
            suParent.AddChild(suChild);
            return suChild;
        }

        public void MoveStructureUnit(Guid structureUnitId, Guid newParentStructureUnitId)
        {
            IStructureUnit su = _structureUnitRepository.FindById(structureUnitId);
            IStructureUnit suOldParent = su.Parent;
            IStructureUnit suNewParent = _structureUnitRepository.FindById(newParentStructureUnitId);
            su.Parent = suNewParent;
            suOldParent.RemoveChild(su);
            suNewParent.AddChild(su);
            _structureUnitRepository.Save(su);
            _structureUnitRepository.Save(suOldParent);
            _structureUnitRepository.Save(suNewParent);
        }
    }
}
