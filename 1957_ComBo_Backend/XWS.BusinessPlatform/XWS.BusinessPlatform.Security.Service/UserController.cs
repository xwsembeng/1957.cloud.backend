﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Security.Domain;
using XWS.BusinessPlatform.Security.Domain.Interfaces;
using XWS.BusinessPlatform.Security.Service.Interfaces;
using XWS.BusinessPlatform.Services.Session;

namespace XWS.BusinessPlatform.Security.Service
{
    internal class UserController : IUserController
    {
        private readonly IUserManagementService _userManagementService;


        #region IUserController Members
        public IAppUser CurrentUser
        {
            get { return SessionService.Session["CurrentUser"] as AppUser; }
            private set { SessionService.Session["CurrentUser"] = value; }
        }

        /// <summary>
        ///     Checks if the user represented by the given account is authorized
        ///     and fills the CurrentUser property with the authorized user object
        /// </summary>
        /// <param name = "account">The account to check authorization for</param>
        /// <returns>The authorized app user object or null</returns>
        public IAppUser TryToAuthenticate(string account)
        {
            IAppUser user = _userManagementService.FindUserByLogin(account);
            if (user == null)
            {
                return CurrentUser = null;
            }

            return TryToAuthenticate(user);
        }


        public IAppUser TryToAuthenticate(string account, string password)
        {
            IAppUser user = _userManagementService.FindUserByLoginPassword(account, password);
            return TryToAuthenticate(user);
        }
        #endregion


        #region Constructors
        /// <summary>
        ///     Initializes a new instance of the <see cref = "UserController" /> class.
        /// </summary>
        public UserController(IUserManagementService userManagementService)
        {
            _userManagementService = userManagementService;
            _userManagementService.CurrentUserSettingsChanged += _userManagementService_CurrentUserSettingsChanged;
        }

        #endregion Constructors


        #region Private-Methods
        private void _userManagementService_CurrentUserSettingsChanged(IAppUser appUser)
        {
            CurrentUser = appUser;
        }

        private IAppUser TryToAuthenticate(IAppUser user)
        {
            if (user == null)
            {
                return CurrentUser = null;
            }

            if (user.Active && user.IsChecked)
            {
                return CurrentUser = user;
            }

            return CurrentUser = null;
        } 
        #endregion Private-Methods
    }
}
