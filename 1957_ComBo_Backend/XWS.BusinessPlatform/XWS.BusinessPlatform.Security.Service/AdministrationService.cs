﻿using System;
using System.Collections;
using System.Collections.Generic;
using XWS.BusinessPlatform.Core.InversionOfControl;
using XWS.BusinessPlatform.Domain;
using XWS.BusinessPlatform.Licensing;
using XWS.BusinessPlatform.Security.Domain;
using XWS.BusinessPlatform.Security.Domain.Interfaces;
using XWS.BusinessPlatform.Security.Domain.Library;
using XWS.BusinessPlatform.Security.Repository.Interfaces;
using XWS.BusinessPlatform.Security.Service.Interfaces;
using XWS.RulesEngine;
using XWS.RulesEngine.Exceptions;

namespace XWS.BusinessPlatform.Security.Service
{
    internal class AdministrationService : IAdministrationService
    {
        private readonly IUserProfileStructureUnitRepository _profileStructureUnitRepository;

        public AdministrationService(IUserProfileStructureUnitRepository profileStructureUnitRepository)//ISessionManager sessionManager)
        {
            this._profileStructureUnitRepository = profileStructureUnitRepository;
        }

        public void Grant(IAppUser user, Profile profile, IStructureUnit unit, bool isCascading, bool isDenied)
        {
            if (profile.Code == "SuperAdmin" && !CanChangeSuperadmin())
                throw new Exception("Cannot change Superadmin without being Superadmin");

            User_Profile_StructureUnit userProfileStructureUnit = _profileStructureUnitRepository.GetUserProfileStructureUnit(user, profile, unit);
            if (userProfileStructureUnit != null)
            {
                // do nothing if nothing has been changed
                if (userProfileStructureUnit.IsCascading == isCascading &&
                    userProfileStructureUnit.IsDenied == isDenied)
                    return;

                // set new grant settings to the object
                userProfileStructureUnit.IsCascading = isCascading;
                userProfileStructureUnit.IsDenied = isDenied;
            }
            else
            {
                // no object exists, so create a new object
                userProfileStructureUnit = new User_Profile_StructureUnit(profile, unit, user)
                {
                    IsCascading = isCascading,
                    IsDenied = isDenied
                };
            }

            BrokenRulesCollection brokenRulesCollection = new BrokenRulesCollection();
            _profileStructureUnitRepository.Save(userProfileStructureUnit);

            if (!IsValidToSave(userProfileStructureUnit, ref brokenRulesCollection))
                throw new BrokenRulesException(brokenRulesCollection);
        }

        public GrantSettings GetGrantSettings(IAppUser user, Profile profile, IStructureUnit unit)
        {
            User_Profile_StructureUnit userProfileStructureUnit = _profileStructureUnitRepository.GetUserProfileStructureUnit(user, profile, unit);

            if (userProfileStructureUnit != null && !userProfileStructureUnit.IsTransient)
            {
                return new GrantSettings { Deny = userProfileStructureUnit.IsDenied, Cascading = userProfileStructureUnit.IsCascading };
            }
            return null;
        }


        /*
        *    Revoke Cascading: Unit and Profile
        * 
        * 
        * if an entry not directly exists,
        * there can be entries below (regarding to profile or unit) 
        * which have to be attended. 
        *    e.g. : Find (user1,p1,u4)  --> Entry not directly exists                                                   
        *   
        *      Structure of Profiles:                  structure of units:
        *              p1                                      u1          cascading                                                                                            
        *         p2        p3                                  |---u2 
        *                     p4                                |   |---u21
        *                                                       |   |---u22   not cascading & deny (if cascading is true just this unit is denied)
        *                                                       |       |
        *                                                       |       u221
        *                                                       |---u3
        *      But there are 2 entries existing 
        *      (user1,p3,u1,cascading) and 
        *      (user1,p4,u22,deny)
        *      which may correspondent to our request...
        *      so u1 cascading means:
        *      every Unit below (until deny-element is reached and is marked as not cascading , as at u22 is so). 
        *          
        *      1. u1,u2,u3,u21 have to be integrated in our search
        *      2. p2,p3,p4 have to be integrated in our search
        *      => delete every entry which following condition is true: findEntries(user1, u1|u2|u3|u21, p1|p2|p3|p4)
        *      
        *      
        *              
        */
        /// <summary>
        /// Revoke a Profile from User and Structure Unit 
        /// </summary>
        public void Revoke(IAppUser user, Profile profile, IStructureUnit unit, bool profileCascading, bool unitsCascading)
        {

            if (!IoC.Resolve<ISecurityService>().HasProfile(user, profile, unit))
                return;

            if (profile.Code == "SuperAdmin" && !CanChangeSuperadmin())
                throw new Exception("Cannot change Superadmin without being Superadmin");

            if (!profileCascading && !unitsCascading)
            {
                RevokeSingle(user, profile, unit);
            }
            if (!profileCascading && unitsCascading)
            {
                RevokeCascadingUnit(user, profile, unit);
            }
            if (profileCascading && unitsCascading)
            {
                //RevokeCascadingProfileAndCasdingUnit(user, profile, unit);
            }
            if (profileCascading && !unitsCascading)
            {
                RevokeCascadingProfile(user, profile, unit);
            }
        }


        private void RevokeSingle(IAppUser user, Profile profile, IStructureUnit unit)
        {
            BrokenRulesCollection brokenRulesCollection = new BrokenRulesCollection();
            foreach (User_Profile_StructureUnit userProfileStructureUnit in _profileStructureUnitRepository.GetUserProfileStructureUnitList(user, profile, unit))
            {
                IsValidToSave(userProfileStructureUnit, ref brokenRulesCollection);
                _profileStructureUnitRepository.Delete(userProfileStructureUnit);
            }
        }

        private void RevokeCascadingProfile(IAppUser user, Profile profile, IStructureUnit unit)
        {

            //TODO:FindChildrenRecursive für Profile ?

            IList<Profile> profiles = new List<Profile> { profile };
            foreach (Profile childprofile in profile.Children)
            {
                profiles.Add(childprofile);
            }

            foreach (User_Profile_StructureUnit userProfileStructureUnit in _profileStructureUnitRepository.GetUserProfileStructureUnitsList(user, unit, profiles))
            {
                _profileStructureUnitRepository.Delete(userProfileStructureUnit);
            }
        }

        private void RevokeCascadingUnit(IAppUser user, Profile profile, IStructureUnit unit)
        {
            IList<IStructureUnit> units = new List<IStructureUnit> { unit };
            FindChildrenRecursive(ref units, unit);

            foreach (User_Profile_StructureUnit userProfileStructureUnit in _profileStructureUnitRepository.GetUserProfileStructureUnitList(user, profile, units))
            {
                _profileStructureUnitRepository.Delete(userProfileStructureUnit);
            }
        }

        private static void FindChildrenRecursive(ref IList<IStructureUnit> units, IStructureUnit initialUnit)
        {
            foreach (StructureUnit unit in initialUnit.Children)
            {
                if (!units.Contains(unit))
                    units.Add(unit);

                FindChildrenRecursive(ref units, unit);
            }
        }

        private bool IsValidToSave(User_Profile_StructureUnit userProfileStructureUnit, ref BrokenRulesCollection brokenRulesCollection)
        {
            RulesCollection rulesCollection = new RulesCollection();

            rulesCollection.AddRulesCollection(License.CheckRulesForUser(userProfileStructureUnit.User));

            brokenRulesCollection = rulesCollection.GetBrokenRules();
            return brokenRulesCollection.IsEmpty;
        }

        private bool CanChangeSuperadmin()
        {
            IAppUser currentUser = IoC.Resolve<IUserController>().CurrentUser;
            if (currentUser == null)
            {
                // Dann sind wir wohl beim Initialisieren
                return true;
            }
            IList<User_Profile_StructureUnit> userProfileStructureUnits =
                IoC.Resolve<IUserProfileStructureUnitRepository>().GetUserProfileStructureUnitList(
                    currentUser, IoC.Resolve<IProfileService>().FindProfileByCode("SuperAdmin"));

            if (userProfileStructureUnits == null || userProfileStructureUnits.Count == 0)
                return false;

            return true;
        }
    }
}