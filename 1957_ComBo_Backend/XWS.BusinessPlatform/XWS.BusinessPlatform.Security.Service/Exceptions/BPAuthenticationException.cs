﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.BusinessPlatform.Security.Service.Exceptions
{
    public class BPAuthenticationException : Exception
    {
        public BPAuthenticationException(): base(){ }
        public BPAuthenticationException(string message): base(message){ }
        public BPAuthenticationException(string message, Exception innerException) : base(message, innerException) { }
    }
}
