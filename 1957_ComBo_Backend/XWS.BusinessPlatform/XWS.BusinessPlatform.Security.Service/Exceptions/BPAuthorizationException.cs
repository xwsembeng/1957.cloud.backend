﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.BusinessPlatform.Security.Service.Exceptions
{
    public class BPAuthorizationException : Exception
    {
        public BPAuthorizationException() : base() { }
        public BPAuthorizationException(string message) : base(message) { }
        public BPAuthorizationException(string message, Exception innerException) : base(message, innerException) { }
    }
}
