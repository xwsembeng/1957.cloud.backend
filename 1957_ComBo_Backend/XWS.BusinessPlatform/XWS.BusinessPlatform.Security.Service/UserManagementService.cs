﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Core.InversionOfControl;
using XWS.BusinessPlatform.Licensing;
using XWS.BusinessPlatform.Security.Domain;
using XWS.BusinessPlatform.Security.Domain.Interfaces;
using XWS.BusinessPlatform.Security.Domain.Library;
using XWS.BusinessPlatform.Security.Repository.Interfaces;
using XWS.BusinessPlatform.Security.Service.Interfaces;
using XWS.BusinessPlatform.Services.Configuration;
using XWS.Localization;
using XWS.RulesEngine;
using XWS.RulesEngine.Exceptions;
using XWS.RulesEngine.Rules;
using XWS.Util;

namespace XWS.BusinessPlatform.Security.Service
{
    internal class UserManagementService : IUserManagementService
    {
        private readonly IAppUserRepository _appUserRepository;


        public UserManagementService(IAppUserRepository appUserRepository)
        {
            _appUserRepository = appUserRepository;
        }

        #region IUserManagementService Members
        public event Action<IAppUser> CurrentUserSettingsChanged;
        /// <summary>
        /// Invokes a method inside the resource service which changes the data of the connected 
        /// connected resource if the appusers's data is changed
        /// </summary>
        public event Action<IAppUser> UserSettingsChanged;


        public void DeleteUser(Guid userId)
        {
            IAppUser user = FindUserById(userId);
            _appUserRepository.Delete(user);
        }


        public void DeleteUserLogical(Guid userId)
        {
            _appUserRepository.ClearSession();

            IAppUser user = FindUserById(userId);
            if (user != null)
            {
                user.IsObsolete = true;
                user.Login = $"DELETED{_appUserRepository.FindNumberOfDeletedLogins(user.Login)}_{user.Login}";
                _appUserRepository.Save(user);
            }
        }


        public void SaveUser(IAppUser user)
        {
            if (!user.IsTransient)
                user.IsChecked = true;

            _appUserRepository.Save(user);

            IUserController userController = IoC.Resolve<IUserController>();
            if (userController.CurrentUser != null && user.Equals(userController.CurrentUser) && CurrentUserSettingsChanged != null)
            {
                CurrentUserSettingsChanged(user);
            }
            if (UserSettingsChanged != null)
                UserSettingsChanged(user);
        }


        public IList<IAppUser> GetAllUsers()
        {
            return _appUserRepository.FindAll().Where(n => !n.IsObsolete).ToList();
        }


        public IList<IAppUser> GetUsersByFilter(AppUserFilterObject obj)
        {
            return GetUsersByFilter(obj, true);
        }


        public IList<IAppUser> GetUsersByFilter(AppUserFilterObject obj, bool ignoreSuperAdmin)
        {
            IList<IAppUser> users = _appUserRepository.GetUsersByFilter(obj);

            if (obj.Role == null)
                return users;

            IList<IAppUser> usersWithRole = new List<IAppUser>();
            foreach (IAppUser user in users)
            {
                ISecurityService securityService = IoC.Resolve<ISecurityService>();
                securityService.IgnoreSuperAdmin = ignoreSuperAdmin;

                if (securityService.HasProfile(user, obj.Role.Code))
                {
                    usersWithRole.Add(user);
                }
                securityService.IgnoreSuperAdmin = false;
            }

            return usersWithRole;
        }


        public IAppUser CreateAppUser(bool isChecked)
        {
            return new AppUser
            {
                StructureUnit = IoC.Resolve<IStructureUnitService>().GetRootStructureUnit(),
                UICulture = new CultureInfo(ConfigurationService.AppSettings["defaultUICultureName"]),
                Culture = new CultureInfo(ConfigurationService.AppSettings["defaultCultureName"]),
                AutoExpandCurrentProject = true,
                IsChecked = isChecked
            };
        }


        public IAppUser FindUserByLogin(string login)
        {
            return _appUserRepository.FindByAccount(login);
        }


        public IAppUser FindUserByLogin(string login, bool ignoreObsolete)
        {
            return _appUserRepository.FindByAccount(login, ignoreObsolete);
        }


        public IAppUser FindUserByLoginPassword(string login, string password)
        {
            IAppUser user = _appUserRepository.FindByAccount(login);
            if (user == null)
                return null;

            // User bei denen kein Passwort gesetzt ist, dürfen sich auf jeden Fall nicht einloggen
            // ebenso ist es nicht möglich sich einzuloggen, wenn man kein Passwort eingegeben hat.
            if (String.IsNullOrEmpty(user.Password) || String.IsNullOrEmpty(password))
                return null;

            // ansonsten überprüfen wir eben, ob das Passwort übereinstimmt
            return BCrypt.CheckPassword(password, user.Password) ? user : null;
        }


        public void SetPassword(IAppUser user, string password)
        {
            user.Password = String.IsNullOrEmpty(password) ? null : BCrypt.HashPassword(password, BCrypt.GenerateSalt(12));
        }


        public IAppUser FindUserById(Guid userId)
        {
            IAppUser appUser = _appUserRepository.FindById(userId);
            return appUser;
        }

        public IList<IAppUser> FindUserByProfile(Profile profile)
        {
            IUserProfileStructureUnitRepository userProfileStructureUnitRepository = IoC.Resolve<IUserProfileStructureUnitRepository>();
            IList<User_Profile_StructureUnit> userProfileStructureUnits = userProfileStructureUnitRepository.GetUserProfileStructureUnitsByProfile(profile);

            return (from userProfileStructureUnit in userProfileStructureUnits
                    where userProfileStructureUnit.User != null
                    select userProfileStructureUnit.User).ToList();
        }


        public bool IsPasswordValid(string login, string password)
        {
            return IsPasswordValid(FindUserByLogin(login), password);
        }


        public bool IsPasswordValid(string password)
        {
            return IsPasswordValid(IoC.Resolve<IUserController>().CurrentUser, password);
        }


        public bool IsPasswordValid(IAppUser user, string password)
        {
            if (user == null)
                throw new InvalidOperationException("cannot check user password because user is null");

            // User bei denen kein Passwort gesetzt ist, dürfen sich auf jeden Fall nicht einloggen
            // ebenso ist es nicht möglich sich einzuloggen, wenn man kein Passwort eingegeben hat.
            if (String.IsNullOrEmpty(user.Password) || String.IsNullOrEmpty(password))
                return false;

            return BCrypt.CheckPassword(password, user.Password);
        }


        public IList<IAppUser> GetUncheckedUsers()
        {
            return _appUserRepository.FindUnchecked();
        }
        #endregion

        public bool IsValidToSave(IAppUser appUser, ref BrokenRulesCollection brokenRulesCollection)
        {
            RulesCollection rulesCollection = new RulesCollection();

            IAppUser appUserFromDb = _appUserRepository.FindByAccount(appUser.Login);
            bool loginNameExists = appUserFromDb != null && appUserFromDb.Id != appUser.Id;
            rulesCollection.AddRule(new CustomRule("Account", "App User Login name already exists.", "LoginNameExists", !loginNameExists, appUser.Login));

            rulesCollection.AddRule(new RequiredStringIsValidRule(StaticResourceManager.GetString("Security", "Login"), appUser.Login, 50)); //"Account"
            rulesCollection.AddRule(new RequiredStringIsValidRule(StaticResourceManager.GetString("Security", "FirstName"), appUser.FirstName, 50));
            rulesCollection.AddRule(new RequiredStringIsValidRule(StaticResourceManager.GetString("Security", "LastName"), appUser.LastName, 50)); //LastName

            RequiredStringIsValidRule emailIsFilledRule = new RequiredStringIsValidRule("Email", appUser.EMail, 50);

            rulesCollection.AddRule(emailIsFilledRule);

            if (emailIsFilledRule.IsValid)
            {
                rulesCollection.AddRule(new MailAddressIsValidRule("Email", appUser.EMail));
            }

            rulesCollection.AddRulesCollection(License.CheckRulesForUser(appUser));

            brokenRulesCollection = rulesCollection.GetBrokenRules();
            return brokenRulesCollection.IsEmpty;
        }
    }
}
