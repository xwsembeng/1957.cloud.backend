﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Security.Domain;
using XWS.BusinessPlatform.Security.Repository.Interfaces;
using XWS.BusinessPlatform.Security.Service.Interfaces;
using XWS.Localization;
using XWS.RulesEngine;
using XWS.RulesEngine.Exceptions;
using XWS.RulesEngine.Rules;

namespace XWS.BusinessPlatform.Security.Service
{
    internal class ProfileService : IProfileService
    {
        private readonly IProfileRepository _profileRepository;

        public ProfileService(IProfileRepository profileRepository)
        {
            _profileRepository = profileRepository;
        }

        public IEnumerable<Profile> GetAllProfiles()
        {
            return _profileRepository.FindAll();
        }

        public IEnumerable<Profile> GetProfilesByType(Module module, ProfileType type)
        {
            return _profileRepository.GetProfilesByType(module, type);

        }

        public IEnumerable<Profile> GetProfilesSortedByTypeAndCode(Module module)
        {
            return _profileRepository.GetProfilesSortedByTypeAndCode(module);
        }

        public Profile FindProfileById(Guid ProfileId)
        {
            return _profileRepository.FindById(ProfileId);
        }

        public Profile FindProfileByCode(string code)
        {
            return _profileRepository.GetByCode(code);
        }



        protected bool IsCodeInUse(Profile profile)
        {
            Profile dbProfile = FindProfileByCode(profile.Code);
            _profileRepository.Evict(dbProfile);
            return dbProfile != null && dbProfile.Id != profile.Id;
        }

        public bool IsValidToSave(Profile profile, ref BrokenRulesCollection brokenRulesCollection)
        {
            RulesCollection rulesCollection = new RulesCollection();


            rulesCollection.AddRule(new CustomRule("Code", "ProfileCodeNotUnique", !IsCodeInUse(profile), profile.Code));

            rulesCollection.AddRule(new RequiredStringIsValidRule(StaticResourceManager.GetString("Security", "Name"), profile.DisplayName, 50));    //"Account"
            rulesCollection.AddRule(new RequiredStringIsValidRule(StaticResourceManager.GetString("Security", "Code"), profile.Code, 50));

            brokenRulesCollection = rulesCollection.GetBrokenRules();
            return brokenRulesCollection.IsEmpty;
        }

        public void SaveProfile(Profile profile)
        {
            BrokenRulesCollection brokenRulesCollection = new BrokenRulesCollection();
            if (!IsValidToSave(profile, ref brokenRulesCollection))
            {
                throw new BrokenRulesException(brokenRulesCollection);
            }

            _profileRepository.Save(profile as Profile);
        }

        public void DeleteProfile(Profile profile)
        {
            if (profile.ProfileType == ProfileType.Role)
            {
                IList<Profile> profileLinks = new List<Profile>();
                foreach (Profile child in profile.Children)
                    profileLinks.Add(child);

                foreach (Profile child in profileLinks)
                    profile.Children.Remove(child);
            }
            else
            {
                IList<Profile> roles = new List<Profile>();
                foreach (Profile parent in profile.Parents)
                    roles.Add(parent);

                foreach (Profile role in roles)
                {
                    role.Children.Remove(profile);
                    _profileRepository.Save(role);
                }
            }

            _profileRepository.Delete(profile);
        }

        public bool IsInProfileTree(Profile treeProfile, Profile profileToSearchInTree)
        {
            if (treeProfile.Id == profileToSearchInTree.Id)
                return true;

            bool found = false;
            foreach (Profile parent in treeProfile.Parents)
            {
                if (IsInProfileTree(parent, profileToSearchInTree))
                {
                    found = true;
                    break;
                }
            }
            return found;
        }
    }
}
