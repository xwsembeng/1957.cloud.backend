﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Security.Domain;
using XWS.BusinessPlatform.Security.Repository.Interfaces;
using XWS.BusinessPlatform.Security.Service.Interfaces;

namespace XWS.BusinessPlatform.Security.Service
{
    public class ModuleService : IModuleService
    {
        private readonly IModuleRepository _moduleRepository;

        public ModuleService(IModuleRepository moduleRepository)
        {
            _moduleRepository = moduleRepository;
        }

        public IList<Module> GetAllModules()
        {
            return _moduleRepository.FindAll("SortIndex", true);
        }

        public IList<Module> GetAllUserAssignmentModules()
        {
            return _moduleRepository.FindAllUserAssignmentModules();
        }

        public Module FindModuleById(Guid userId)
        {
            return _moduleRepository.FindById(userId);
        }

        public Module FindModuleByName(string displayName)
        {
            return _moduleRepository.FindByName(displayName);
        }

        public Module FindModuleByCode(string code)
        {
            return _moduleRepository.FindByCode(code);
        }

        public void SaveModule(Module instance)
        {
            _moduleRepository.Save(instance);
        }

        public void DeleteModule(Module instance)
        {
            _moduleRepository.Delete(instance);
        }
    }
}
