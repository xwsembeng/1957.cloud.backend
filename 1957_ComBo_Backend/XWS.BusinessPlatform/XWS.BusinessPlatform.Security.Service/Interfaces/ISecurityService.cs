﻿using XWS.BusinessPlatform.Security.Domain;
using XWS.BusinessPlatform.Security.Domain.Interfaces;

namespace XWS.BusinessPlatform.Security.Service.Interfaces
{
    public interface ISecurityService
    {
        bool HasProfile(IAppUser appUser, string profileCode, IStructureUnit structureUnit);
        bool HasProfile(IAppUser appUser, Profile profile, IStructureUnit structureUnit);
        bool HasProfile(IAppUser appUser, string profileCode);
        bool HasProfile(IAppUser appUser, Profile profile);
        bool IsSuperAdmin(IAppUser appUser);
        bool IgnoreSuperAdmin { get; set; }
        bool HasProfile(IAppUser appUser, string profileCode, Module module);
    }
}