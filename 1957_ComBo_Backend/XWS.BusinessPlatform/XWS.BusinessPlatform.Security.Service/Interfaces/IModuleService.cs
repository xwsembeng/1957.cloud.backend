﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Security.Domain;

namespace XWS.BusinessPlatform.Security.Service.Interfaces
{
    public interface IModuleService
    {
        Module FindModuleById(Guid userId);
        Module FindModuleByName(string name);
        Module FindModuleByCode(string code);
        IList<Module> GetAllModules();
        IList<Module> GetAllUserAssignmentModules();
        void SaveModule(Module instance);
        void DeleteModule(Module instance);
    }
}
