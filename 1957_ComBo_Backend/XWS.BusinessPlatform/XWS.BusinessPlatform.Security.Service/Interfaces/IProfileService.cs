﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Security.Domain;

namespace XWS.BusinessPlatform.Security.Service.Interfaces
{
    public interface IProfileService
    {
        IEnumerable<Profile> GetAllProfiles();
        IEnumerable<Profile> GetProfilesByType(Module module, ProfileType type);
        IEnumerable<Profile> GetProfilesSortedByTypeAndCode(Module module);
        Profile FindProfileById(Guid ProfileId);
        Profile FindProfileByCode(string code);
        void SaveProfile(Profile Profile);
        void DeleteProfile(Profile ProfileId);
        bool IsInProfileTree(Profile treeProfile, Profile profileToSearchInTree);
    }
}
