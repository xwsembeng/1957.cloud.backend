﻿using XWS.BusinessPlatform.Security.Domain;
using XWS.BusinessPlatform.Security.Domain.Interfaces;
using XWS.BusinessPlatform.Security.Domain.Library;

namespace XWS.BusinessPlatform.Security.Service.Interfaces
{
    public interface IAdministrationService
    {
        GrantSettings GetGrantSettings(IAppUser user, Profile profile, IStructureUnit unit);
        void Grant(IAppUser user, Profile profile, IStructureUnit unit, bool isCascading, bool isDenied);
        void Revoke(IAppUser user, Profile profile, IStructureUnit unit, bool profileCascading, bool unitsCascading);
    }
}
