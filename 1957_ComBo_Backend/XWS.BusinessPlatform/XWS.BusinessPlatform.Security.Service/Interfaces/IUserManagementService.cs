﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Security.Domain;
using XWS.BusinessPlatform.Security.Domain.Interfaces;
using XWS.BusinessPlatform.Security.Domain.Library;
using XWS.RulesEngine;

namespace XWS.BusinessPlatform.Security.Service.Interfaces
{
    public interface IUserManagementService
    {
        IAppUser FindUserByLogin(string login);
        IAppUser FindUserByLogin(string login, bool ignoreObsolete);
        IAppUser FindUserByLoginPassword(string login, string password);
        IAppUser FindUserById(Guid userId);
        IAppUser CreateAppUser(bool isChecked);
        IList<IAppUser> GetUsersByFilter(AppUserFilterObject obj);
        IList<IAppUser> GetUsersByFilter(AppUserFilterObject obj, bool ignoreSuperAdmin);
        IList<IAppUser> GetAllUsers();
        IList<IAppUser> FindUserByProfile(Profile profile);
        IList<IAppUser> GetUncheckedUsers();

        event Action<IAppUser> CurrentUserSettingsChanged;

        void DeleteUser(Guid userId);
        void DeleteUserLogical(Guid userId);
        void SaveUser(IAppUser user);

        bool IsPasswordValid(string login, string password);
        bool IsPasswordValid(IAppUser user, string password);
        bool IsPasswordValid(string password);
        void SetPassword(IAppUser user, string password);
        event Action<IAppUser> UserSettingsChanged;
        bool IsValidToSave(IAppUser appUser, ref BrokenRulesCollection brokenRulesCollection);
    }
}
