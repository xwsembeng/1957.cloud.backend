﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Security.Domain.Interfaces;

namespace XWS.BusinessPlatform.Security.Service.Interfaces
{
    public interface IUserController
    {
        IAppUser CurrentUser { get; }
        IAppUser TryToAuthenticate(string account);
        IAppUser TryToAuthenticate(string account, string password);
    }
}
