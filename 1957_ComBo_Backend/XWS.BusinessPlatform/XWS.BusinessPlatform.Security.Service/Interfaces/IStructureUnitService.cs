﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Security.Domain;
using XWS.BusinessPlatform.Security.Domain.Interfaces;

namespace XWS.BusinessPlatform.Security.Service.Interfaces
{
    public interface IStructureUnitService
    {
        IList<IStructureUnit> GetAllStructureUnitsWithAssignedProfilesForUsers(IList<IAppUser> users, Module module);
        IList<IStructureUnit> GetAllStructureUnitsWithAssignedProfilesForUser(IAppUser user, Module module);
        IEnumerable<IStructureUnit> GetStructureUnitsWithAssignedProfilesForUser(IAppUser user, Profile profile);
        IEnumerable<IStructureUnit> GetStructureUnitsWithAssignedProfilesForUser(IAppUser user, string profileCode);
        IList<IStructureUnit> GetAllStructureUnits();
        /// <summary>
        /// gets all children for the StructureUnit recursive
        /// </summary>
        /// <param name="unit"></param>
        /// <returns></returns>
        IList<IStructureUnit> GetAllDescendantsForStructureUnit(IStructureUnit unit);
        IList<IStructureUnit> GetAllAncestorsForStructureUnit(IStructureUnit unit);
        IList<IStructureUnit> GetAllAncestorsForStructureUnits(IList<IStructureUnit> units);

        IStructureUnit FindStructureUnitById(Guid structureUnitId);
        IStructureUnit GetRootStructureUnit();
        IStructureUnit AddChildStructureUnit(Guid parentStructureUnitId, string defaultName);

        void SaveStructureUnit(IStructureUnit obj);
        void RemoveStructureUnit(Guid structureUnitId);
        void MoveStructureUnit(Guid structureUnitId, Guid newParentStructureUnitId);
    }
}
