﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using XWS.BusinessPlatform.Security.Domain;
using XWS.BusinessPlatform.Security.Domain.Interfaces;
using XWS.BusinessPlatform.Security.Repository.Interfaces;
using XWS.BusinessPlatform.Security.Service.Exceptions;
using XWS.BusinessPlatform.Security.Service.Interfaces;

namespace XWS.BusinessPlatform.Security.Service
{
    internal class SecurityService : ISecurityService
    {
        private readonly IProfileRepository _profileRepository;
        private readonly IUserProfileStructureUnitRepository _profileStructureUnitRepository;

        public SecurityService(IProfileRepository profileRepository,
            IUserProfileStructureUnitRepository profileStructureUnitRepository)
        {
            _profileRepository = profileRepository;
            _profileStructureUnitRepository = profileStructureUnitRepository;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the super admin role should be ignored,
        /// means the super user is treated as a "normal" user 
        /// </summary>
        /// <value><c>true</c> if the superadmin role should be ignored; otherwise, <c>false</c>.</value>
        public bool IgnoreSuperAdmin
        {
            get;
            set;
        }

        public bool HasProfile(IAppUser appUser, string profileCode, Module module)
        {
            if (appUser == null)
            {
                throw new BPAuthenticationException(HttpContext.Current.User.Identity.Name);
            }

            if (!IsSuperAdmin(appUser) || IgnoreSuperAdmin)
            {
                IList<Profile> profiles = new List<Profile>();

                if (profileCode == null)
                {
                    throw new ApplicationException("Parameter profile must not be null.");
                }
                Profile profile = GetProfileByCode(profileCode);
                GetAllProfiles(ref profiles, profile);

                if (module.ModuleType == ModuleType.ModuleContainer)
                {
                    profiles = profiles.Where(n => n.Modules.Intersect(module.Children).Count() > 0).ToList();
                }
                else
                {
                    profiles = profiles.Where(n => n.Modules.Contains(module)).ToList();
                }

                IList<User_Profile_StructureUnit> userProfileStructureUnits =
                    _profileStructureUnitRepository.GetUserProfileStructureUnitList(appUser, profiles);

                foreach (User_Profile_StructureUnit userProfileStructureUnit in userProfileStructureUnits)
                {
                    if (profiles.Contains(userProfileStructureUnit.Profile))
                    {
                        return true;
                    }
                }
                return false;
            }
            return true;
        }

        public bool HasProfile(IAppUser appUser, string profileCode)
        {
            if (String.IsNullOrEmpty(profileCode))
            {
                throw new ArgumentNullException("profileCode", "Parameter profileCode must not be null or empty.");
            }

            Profile profile = GetProfileByCode(profileCode);
            return HasProfile(appUser, profile);
        }

        public bool HasProfile(IAppUser appUser, Profile profile)
        {
            if (appUser == null)
            {
                throw new BPAuthenticationException(HttpContext.Current.User.Identity.Name);
            }

            if (!IsSuperAdmin(appUser) || IgnoreSuperAdmin)
            {
                IList<Profile> profiles = new List<Profile>();

                if (profile == null)
                {
                    throw new ApplicationException("Parameter profile must not be null.");
                }

                GetAllProfiles(ref profiles, profile);

                IList<User_Profile_StructureUnit> userProfileStructureUnits =
                    _profileStructureUnitRepository.GetUserProfileStructureUnitList(appUser, profiles);

                foreach (User_Profile_StructureUnit userProfileStructureUnit in userProfileStructureUnits)
                {
                    if (profiles.Contains(userProfileStructureUnit.Profile))
                    {
                        return true;
                    }
                }
                return false;
            }
            return true;
        }

        public bool HasProfile(IAppUser appUser, string profileCode, IStructureUnit structureUnit)
        {
            if (appUser == null)
                throw new BPAuthenticationException(HttpContext.Current.User.Identity.Name);

            if (String.IsNullOrEmpty(profileCode))
            {
                throw new ArgumentNullException("profileCode", "profileCode must not be null or empty.");
            }

            Profile profile = GetProfileByCode(profileCode);

            if (profile == null)
            {
                throw new ApplicationException("Profile with code '" + profileCode + "' does not exist.");
            }

            return HasProfile(appUser, profile, structureUnit);
        }

        public bool HasProfile(IAppUser appUser, Profile profile, IStructureUnit structureUnit)
        {
            if (appUser == null)
                throw new BPAuthenticationException(HttpContext.Current.User.Identity.Name);

            if (profile == null)
            {
                throw new ArgumentNullException("profile", "Parameter 'profile' must not be null");
            }

            if (structureUnit == null)
            {
                throw new ArgumentNullException("structureUnit", "Parameter 'structureUnit' must not be null");
            }

            if (!IsSuperAdmin(appUser))
            {
                IList<Profile> profiles = new List<Profile>();
                GetAllProfiles(ref profiles, profile as Profile);
                IList<User_Profile_StructureUnit> userProfileStructureUnits =
                    _profileStructureUnitRepository.GetUserProfileStructureUnitList(appUser, profiles);

                foreach (User_Profile_StructureUnit userProfileStructureUnit in userProfileStructureUnits)
                {
                    if (userProfileStructureUnit.IsCascading && !userProfileStructureUnit.IsDenied)
                    {
                        foreach (IStructureUnit structureUnitsRecursive in
                            userProfileStructureUnit.StructureUnit.GetStructureUnitsRecursive())
                        {
                            if (structureUnitsRecursive.Equals(structureUnit))
                                return true;
                        }
                    }
                    else if (structureUnit.Id == userProfileStructureUnit.StructureUnit.Id && !userProfileStructureUnit.IsDenied)
                        return true;
                }
                return false;
            }
            return true;
        }

        public bool IsSuperAdmin(IAppUser user)
        {
            if (user == null)
                return false;

            IList<Profile> profiles = new List<Profile>();
            GetAllProfiles(ref profiles, GetProfileByCode("SuperAdmin"));
            IList<User_Profile_StructureUnit> userProfileStructureUnits = _profileStructureUnitRepository.GetUserProfileStructureUnitList(user, profiles);

            if (userProfileStructureUnits.Count > 0)
            {
                foreach (User_Profile_StructureUnit userProfileStructureUnit in userProfileStructureUnits)
                {
                    if (profiles.Contains(userProfileStructureUnit.Profile))
                        return true;
                }
            }
            return false;
        }

        private static void GetAllProfiles(ref IList<Profile> profiles, Profile searchProfile)
        {
            if (searchProfile.HasParents)
            {
                foreach (Profile profile in searchProfile.Parents)
                {
                    GetAllProfiles(ref profiles, profile);
                }
            }
            if (!profiles.Contains(searchProfile))
            {
                profiles.Add(searchProfile);
            }
        }

        private static object[] ExtractProperty(ICollection<Profile> profiles)
        {
            ArrayList properties = new ArrayList(profiles.Count);
            foreach (Profile profile in profiles)
            {
                properties.Add(profile.Id);
            }
            return properties.ToArray();
        }

        private Profile GetProfileByCode(string profileCode)
        {
            return _profileRepository.GetByCode(profileCode);
        }
    }
}
