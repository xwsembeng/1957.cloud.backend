﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Core.InversionOfControl;

namespace XWS.BusinessPlatform.Services.Session
{
    public static class SessionService
    {
        #region Member Variables
        #endregion Member Variables


        #region Properties

        #region Private Properties
        #endregion Private Properties


        #region Protected Properties
        #endregion Protected Properties


        #region Public Properties
        #endregion public properties


        #region Static Properties
        public static ISessionProvider Session
        {
            get { return IoC.Resolve<ISessionProvider>(); }
        }
        #endregion Static properties

        #endregion Properties

        #region Methods

        #region Private Methods
        #endregion Private Methods


        #region Protected Methods
        #endregion Protected Methods


        #region Public Methods
        #endregion Public Methods


        #region Static Methods
        #endregion Static Methods

        #endregion Methods
    }
}
