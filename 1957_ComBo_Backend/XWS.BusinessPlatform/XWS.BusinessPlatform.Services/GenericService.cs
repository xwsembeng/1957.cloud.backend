﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XWS.BusinessPlatform.Domain;
using XWS.BusinessPlatform.Repositories.Interfaces;


namespace XWS.BusinessPlatform.Services
{
    public class GenericService<T> : IGenericService<T> where T : IDomainObject
    {
        private readonly IGenericRepository<T> _repository;

        protected GenericService(IGenericRepository<T> repository)
        {
            _repository = repository;
        }

        public virtual T FindById(Guid id)
        {
            return _repository.FindById(id);
        }

        public virtual IList<T> FindAll()
        {
            return _repository.FindAll();
        }

        public virtual IList<T> FindAll(bool onlyActive)
        {
            return _repository.FindAll(onlyActive);
        }

        public virtual IList<T> FindAll(string propertyToSort, bool sortAscending)
        {
            return _repository.FindAll(propertyToSort, sortAscending);
        }

        public virtual void Save(T instance)
        {
            _repository.Save(instance);
        }

        public virtual void Delete(T instance)
        {
            _repository.Delete(instance);
        }
    }
}
