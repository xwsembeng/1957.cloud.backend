﻿using System;
using System.Collections.Generic;
using XWS.BusinessPlatform.Domain;


namespace XWS.BusinessPlatform.Services
{
	public interface IGenericService<T> where T : IDomainObject
	{
		T FindById(Guid id);
		IList<T> FindAll();
		IList<T> FindAll(bool onlyActive);
		IList<T> FindAll(string propertyToSort, bool sortAscending);
		void Save(T instance);
		void Delete(T instance);
	}
}