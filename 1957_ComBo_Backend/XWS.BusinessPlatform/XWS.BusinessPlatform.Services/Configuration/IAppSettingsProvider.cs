﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.BusinessPlatform.Services.Configuration
{
    public interface IAppSettingsProvider
    {
        string this[string key] { get; set; }
    }
}
