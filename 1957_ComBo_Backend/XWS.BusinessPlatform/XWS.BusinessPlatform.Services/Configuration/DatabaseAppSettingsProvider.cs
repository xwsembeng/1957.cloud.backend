﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Core.InversionOfControl;
using XWS.BusinessPlatform.Domain.Configuration;
using XWS.BusinessPlatform.Repositories.Interfaces;

namespace XWS.BusinessPlatform.Services.Configuration
{
    /// <summary>
    /// Provider class for retrieving app setting values 
    /// from the database
    /// </summary>
    internal class DatabaseAppSettingsProvider : IAppSettingsProvider
    {
        public string this[string key]
        {
            get
            {
                try
                {
                    return IoC.Resolve<IAppSettingRepository>().GetValueByKey(key);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                AppSetting setting = IoC.Resolve<IAppSettingRepository>().GetByKey(key);
                if (setting == null)
                    setting = new AppSetting(key);
                setting.Value = value;

                IoC.Resolve<IAppSettingRepository>().Save(setting);
            }
        }
    }
}
