﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.BusinessPlatform.Services.Configuration
{
    /// <summary>
	///     Provider class for retrieving app setting values 
	///     from the application configuration file
	/// </summary>
    internal class WebConfigAppSettingsProvider : IAppSettingsProvider
    {
        #region IAppSettingsProvider Members

        public string this[string key]
        {
            get { return ConfigurationManager.AppSettings[key]; }
            set { throw new NotSupportedException("writing into web.config is not supported"); }
        }

        #endregion IAppSettingsProvider Members



    }
}
