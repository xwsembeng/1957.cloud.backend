﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XWS.BusinessPlatform.Services.Configuration
{
    /// <summary>
    /// Description of DatabaseWithWebConfigFallbackProvider
    /// </summary>
    internal class DatabaseWithWebConfigFallbackProvider : IAppSettingsProvider
    {
        private readonly IAppSettingsProvider _databaseAppSettingsProvider;
        private readonly IAppSettingsProvider _webConfigAppSettingsProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseWithWebConfigFallbackProvider"/> class.
        /// </summary>
        public DatabaseWithWebConfigFallbackProvider()
        {
            _databaseAppSettingsProvider = new DatabaseAppSettingsProvider();
            _webConfigAppSettingsProvider = new WebConfigAppSettingsProvider();
        }

        public string this[string key]
        {
            get
            {
                string value = _databaseAppSettingsProvider[key];

                if (!String.IsNullOrEmpty(value))
                {
                    return value;
                }

                return _webConfigAppSettingsProvider[key];
            }
            set
            {
                _databaseAppSettingsProvider[key] = value;
            }
        }

       
    }
}
