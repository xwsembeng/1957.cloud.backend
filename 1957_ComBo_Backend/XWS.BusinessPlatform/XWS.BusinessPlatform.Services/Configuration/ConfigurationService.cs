﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Core.InversionOfControl;

namespace XWS.BusinessPlatform.Services.Configuration
{
    public static class ConfigurationService
    { /// <summary>
      /// Gets an App Settings value
      /// </summary>
      /// <value>The app settings.</value>
        public static IAppSettingsProvider AppSettings => IoC.Resolve<IAppSettingsProvider>();
    }
}
