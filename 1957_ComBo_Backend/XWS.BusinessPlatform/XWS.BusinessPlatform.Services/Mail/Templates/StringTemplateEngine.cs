﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Antlr3.ST;


namespace XWS.BusinessPlatform.Services.Mail.Templates
{
    /// <summary>
    /// Description of StringTemplateEngine
    /// </summary>
    internal class StringTemplateEngine : ITemplateEngine
    {
        #region Member Variables
        private StringTemplate _template = null;
        #endregion Member Variables


        #region Properties

        #region Private Properties
        #endregion Private Properties


        #region Protected Properties
        #endregion Protected Properties


        #region Public Properties
        public string Template
        {
            set 
            {
                _template = new StringTemplate(value);
            }
        }


        public string Output
        {
            get { return _template.ToString(); }
        }
        #endregion public properties

        #endregion Properties


        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="StringTemplateEngine"/> class.
        /// </summary>
        public StringTemplateEngine()
        { }
        #endregion Constructors


        #region Methods

        #region Private Methods
        #endregion Private Methods


        #region Protected Methods
        #endregion Protected Methods


        #region Public Methods
        public void SetAttribute(string name, object value)
        {
            if (_template == null)
            {
                throw new ArgumentNullException("Template property must be set before using SetAttribute method.");
            }

            _template.SetAttribute(name, value);
        }


        public void SetAttribute(string name, params object[] values)
        {
            if (_template == null)
            {
                throw new ArgumentNullException("Template property must be set before using SetAttribute method.");
            }

            _template.SetAttribute(name, values);
        }
        #endregion Public Methods

        #endregion Methods


        #region Overrides

        #region Override ToString
        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            return Output;
        }
        #endregion Override ToString()

        #endregion Overrides
    }
}