﻿namespace XWS.BusinessPlatform.Services.Mail.Templates
{
    public interface ITemplateEngine
    {
        string Template { set; }
        void SetAttribute(string name, object value);
        void SetAttribute(string name, params object[] values);
        string Output { get; }
        string ToString();
    }
}