﻿using System.Collections;
using System.Configuration;
using System.Reflection;
using System.Web;
using System.Xml;

namespace XWS.BusinessPlatform.Services.Mail
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Net.Mail;
	using System.Text;
	using Interfaces;
	using Util;


	/// <summary>
	///     Description of Mail
	/// </summary>
	internal class Mail : IMail
	{
		#region Member Variables
		private readonly IList<Attachment> _attachmentList = new List<Attachment>();
		private readonly IList<IMailReceiver> _ccList = new List<IMailReceiver>();
		//private readonly ILogManager _logManager;
		private MailAddress _sender;
		private readonly IList<IMailReceiver> _toList = new List<IMailReceiver>();
		private Exception _lastException;
		private IMailTemplate _mailTemplate;
		#endregion Member Variables

        private bool IsWebMode
        {
            get { return HttpContext.Current != null; }
        }

	    private static string GetScheduledConfigPath()
        {
            string sPath = Assembly.GetExecutingAssembly().Location;
            string sBuff = sPath.Substring(0, sPath.LastIndexOf("\\"));
            sBuff = sBuff.Substring(0, sBuff.LastIndexOf("\\"));
            return sBuff;
        }

        /// <summary>
        /// used for scheduler
        /// </summary>
        /// <returns></returns>
        private static string GetMailTemplatesPath()
        {
            return GetScheduledConfigPath() + "\\MailTemplates";
        }

        /// <summary>
        /// used for scheduler
        /// </summary>
        /// <returns></returns>
        private static string WebConfigPath
        {
            get { return GetScheduledConfigPath() + "\\web.config"; }
        }

        /// <summary>
        /// used for scheduler
        /// </summary>
        /// <returns></returns>
        private static Hashtable ReadConfig()
        {
            string sFileName = WebConfigPath;

            Hashtable ht = new Hashtable();

            XmlDocument objXMLdoc = new XmlDocument();
            objXMLdoc.Load(sFileName);
            XmlElement appSettings = (XmlElement)objXMLdoc.GetElementsByTagName("appSettings")[0];

            foreach (XmlNode node in appSettings.ChildNodes)
            {
                if (node is XmlElement)
                {
                    object key = ((XmlElement)node).GetAttribute("key");
                    object val = ((XmlElement)node).GetAttribute("value");

                    ht.Add(key, val);
                }
            }
            return ht;
        }


		#region Properties


		#region Private Properties
		#endregion Private Properties


		#region Protected Properties
		#endregion Protected Properties


		#region Public Properties
		public bool IsTestMailActive
		{
			get
			{
			    string mailTestEnabled=String.Empty;
                if (IsWebMode)
                {
                    mailTestEnabled = ConfigurationManager.AppSettings["mailTestEnabled"];
                
                }
                else
                {
                    mailTestEnabled=ReadConfig()["mailTestEnabled"].ToString();
                }

                return Convert.ToBoolean(mailTestEnabled);
			}
		}


		public bool IsMailSendingActive
		{
			get
			{
			    string mailSendingActive = String.Empty;
                if (IsWebMode)
                {
                    mailSendingActive=ConfigurationManager.AppSettings["SendingEnabled"];
                }
                else
                {
                    mailSendingActive = ReadConfig()["SendingEnabled"].ToString();
                }
			    return Convert.ToBoolean(mailSendingActive);
			}
		}


		public MailAddress TestMailAddress
		{
			get
			{
			    string testMailAddress = String.Empty;

                if (IsWebMode)
                {
                   testMailAddress=ConfigurationManager.AppSettings["testMailAddress"];
                }
                else
                {
                    testMailAddress = ReadConfig()["testMailAddress"].ToString();
                }
			    try
				{
					return new MailAddress(testMailAddress);
				}
				catch
				{
					throw new Exception("Error: Value for key 'testMailAddress' in Web.Config is not a valid email address.");
				}
			}
		}


		public MailAddress Sender
		{
			get { return _sender; }
            set { _sender = value; }
		}


		public IEnumerable<IMailReceiver> Recipients
		{
			get { return _toList; }
		}


		public IEnumerable<IMailReceiver> CarbonCopies
		{
			get { return _ccList; }
		}


		public Exception LastException
		{
			get { return _lastException; }
		}
		#endregion public properties


		#endregion Properties


		#region Constructors
		public Mail(IMailReceiver mailSender) : this(mailSender.MailAddress) { }


		public Mail(MailAddress mailSender)
		{
			_sender = mailSender;
			//_logManager = IoC.Resolve<ILogManager>();
		}


		public Mail()
		{

            if (HttpContext.Current != null)
            {
                _sender = new MailAddress(ConfigurationManager.AppSettings["DefaultApplicationMailSenderAddress"]);
            }
		    //_sender = new MailAddress(AppSettingsHandler.Mail.DefaultApplicationMailSenderAddress);
			//_logManager = IoC.Resolve<ILogManager>();
		}
		#endregion Constructors


		#region Methods


		#region Private Methods
		private bool ContainsMailAddress(MailAddress mailAddress, IEnumerable<IMailReceiver> list)
		{
			return list.Any(mailRecipient => mailRecipient.MailAddress.Address == mailAddress.Address);
		}


		private string GetToList(MailMessage mailMessage)
		{
			return mailMessage.To.Aggregate(String.Empty, (current, address) => current + (address.Address + "; "));
		}


		private string GetCcList(MailMessage mailMessage)
		{
			return mailMessage.CC.Aggregate(String.Empty, (current, address) => current + (address.Address + "; "));
		}


		private string GetDebugHeader(MailMessage mailMessage)
		{
			var debugHeader = new StringBuilder();
			debugHeader.AppendLine("<div>");
			debugHeader.AppendLine("-------------------- DEBUG HEADER --------------------");
			debugHeader.AppendLine("<table>");
			debugHeader.AppendLine("<tr><td valign=\"top\">To:</td><td>" + GetToList(mailMessage) + "</td></tr>");
			debugHeader.AppendLine("<tr><td valign=\"top\">Cc:</td><td>" + GetCcList(mailMessage) + "</td></tr>");
			debugHeader.AppendLine("</table>");
			debugHeader.AppendLine("-------------------- DEBUG HEADER --------------------");
			debugHeader.AppendLine("</div><br>");

			return debugHeader.ToString();
		}
		#endregion Private Methods


		#region Protected Methods
		#endregion Protected Methods


		#region Public Methods
		public string Subject { get; set; }


		public string Body { get; set; }


		public IMailTemplate MailTemplate
		{
			get { return _mailTemplate; }
			set
			{
				_mailTemplate = value;
				Subject = value.Subject;
				Body = value.Body;
			}
		}


		public void AddRecipient(IMailReceiver mailReceiver)
		{
			if (mailReceiver == null)
			{
				return;
			}

			if (!ContainsMailAddress(mailReceiver.MailAddress, _toList) && !ContainsMailAddress(mailReceiver.MailAddress, _ccList))
			{
				_toList.Add(mailReceiver);
			}
		}


		public void AddRecipients(IEnumerable<IMailReceiver> mailReceiver)
		{
			if (mailReceiver == null)
			{
				return;
			}

			foreach (IMailReceiver m in mailReceiver)
			{
				AddRecipient(m);
			}
		}


		public void AddCarbonCopy(IMailReceiver mailReceiver)
		{
			if (mailReceiver == null)
			{
				return;
			}

			if (!ContainsMailAddress(mailReceiver.MailAddress, _toList) && !ContainsMailAddress(mailReceiver.MailAddress, _ccList))
			{
				_ccList.Add(mailReceiver);
			}
		}


		public void AddCarbonCopies(IEnumerable<IMailReceiver> mailReceiver)
		{
			if (mailReceiver == null)
			{
				return;
			}

			foreach (IMailReceiver m in mailReceiver)
			{
				AddCarbonCopy(m);
			}
		}


		public void ClearRecipientsList()
		{
			_toList.Clear();
		}


		public void ClearCarbonCopiesList()
		{
			_ccList.Clear();
		}


		/// <summary>
		///     Adds an attachement to the E-Mail
		/// </summary>
		/// <param name = "attachment">The attachment to add</param>
		public void AddAttachment(Attachment attachment)
		{
			if (attachment != null)
			{
				_attachmentList.Add(attachment);
			}
		}


		/// <summary>
		///     Adds attachements to the E-Mail
		/// </summary>
		/// <param name = "attachmentlist">The attachments to add</param>
		public void AddAttachments(IEnumerable<Attachment> attachmentlist)
		{
			if (attachmentlist == null)
			{
				return;
			}

			foreach (Attachment attachment in attachmentlist)
			{
				AddAttachment(attachment);
			}
		}

        public bool Send()
        {
            return Send(false);
        }


	    public bool Send(bool isHtmlMail)
		{
			var mailMessage = new MailMessage();
			string recipients = String.Empty;
			try
			{
				if (!IsMailSendingActive)
				{
					//_logManager.LogMessage(LogLevel.Info, "mail sending is deactivated in web.config");
					return true;
				}

				if (Sender == null)
				{
					throw new MailSendingException("No mail sender has been specified.");
				}

				if (String.IsNullOrEmpty(Subject) || String.IsNullOrEmpty(Body))
				{
					throw new MailSendingException("Mail subject or body is empty.");
				}

				mailMessage.IsBodyHtml = isHtmlMail;
				mailMessage.From = Sender;
				mailMessage.Subject = Subject;
				mailMessage.Body = Body;

				foreach (IMailReceiver mailReceiver in Recipients)
				{
					mailMessage.To.Add(mailReceiver.MailAddress);
				}

				foreach (IMailReceiver mailReceiver in CarbonCopies)
				{
					mailMessage.CC.Add(mailReceiver.MailAddress);
				}

				if (mailMessage.To.Count == 0)
				{
					if (mailMessage.CC.Count == 0)
					{
						throw new MailSendingException("To and Cc list are empty.");
					}

					// To list is empty, move an user from Cc list to To list.
					mailMessage.To.Add(mailMessage.CC[0]);
					mailMessage.CC.RemoveAt(0);
				}

				recipients = "To: " + GetToList(mailMessage);
				if (!String.IsNullOrEmpty(GetCcList(mailMessage)))
				{
					recipients += "; Cc: " + GetCcList(mailMessage);
				}

				if (IsTestMailActive)
				{
					mailMessage.Body = GetDebugHeader(mailMessage) + mailMessage.Body;
					mailMessage.To.Clear();
					mailMessage.CC.Clear();
					mailMessage.To.Add(TestMailAddress);
				}

				foreach (Attachment attachment in _attachmentList)
				{
					mailMessage.Attachments.Add(attachment);
				}

                
			    string smtpServer = String.Empty;
                if(IsWebMode)
                {
                    smtpServer = ConfigurationManager.AppSettings["mailServer"];
                }
                else
                {
                    smtpServer = ReadConfig()["mailServer"].ToString();
                }
			    SmtpClient smtpClient=new SmtpClient(smtpServer);
				smtpClient.Send(mailMessage);

				//_logManager.LogMessage(LogLevel.Info, "Mail '{0}' has been successfully sent ({1}).", Subject, recipients);

				mailMessage.Dispose();

				return true;
			}
			catch (Exception e)
			{
				//_logManager.LogMessage(LogLevel.Error, "Mail '{0}' sending has been failed ({1}).", Subject, recipients);
				//_logManager.LogException("Mail", "Send()", e);
				_lastException = e;
				return false;
			}
		}
		#endregion Public Methods


		#endregion Methods


		#region Overrides


		#region Override ToString
		/// <summary>
		///     Returns a <see cref = "T:System.String" /> that represents the current <see cref = "T:System.Object" />.
		/// </summary>
		/// <returns>
		///     A <see cref = "T:System.String" /> that represents the current <see cref = "T:System.Object" />.
		/// </returns>
		public override string ToString()
		{
			return String.Format("To: {0}\nCc: {1}\nSubject: {2}\nBody: {3}", CollectionUtil.GetStringList(Recipients, "MailAddress", ", "), CollectionUtil.GetStringList(CarbonCopies, "MailAddress", ", "), Subject, Body);
		}
		#endregion Override ToString()


		#endregion Overrides
	}
}