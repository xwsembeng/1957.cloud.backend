﻿using System;

namespace XWS.BusinessPlatform.Services.Mail
{
    public class MailTemplateException : Exception
    {
        public MailTemplateException(string errorMessage) : base(errorMessage)
        {
        }
    }
}