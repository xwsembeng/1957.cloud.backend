﻿using System;

namespace XWS.BusinessPlatform.Services.Mail
{
    public class MailSendingException : Exception
    {
        public MailSendingException(string errorMessage) : base(errorMessage)
        {
        }
    }
}