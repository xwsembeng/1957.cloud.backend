﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using XWS.BusinessPlatform.Services.Mail.Interfaces;
using XWS.BusinessPlatform.Services.Mail.Templates;

namespace XWS.BusinessPlatform.Services.Mail
{
    /// <summary>
    /// Stores MailTemplates - can be serialized to XML
    /// </summary>
    [XmlRoot("MailTemplate", Namespace = "", IsNullable = false)]
    public class MailTemplate : IMailTemplate
    {
        #region Member Variables
        private ITemplateEngine _templateEngine;
        private IDictionary<string,object> _attributes = new Dictionary<string, object>();
        [XmlAttribute("Name")] public string _templateName;
        [XmlElement("Subject")] public string _originalSubject;
        [XmlElement("Body")] public string _originalBody;
        #endregion Member Variables


        #region Properties

        #region Private Properties
        #endregion Private Properties


        #region Protected Properties
        #endregion Protected Properties


        #region Public Properties
        public string TemplateName
        {
            get { return _templateName; }
        }

        public string Subject
        {
            get 
            {
                return Prepare(Translate(_originalSubject));
            }
        }

        public string Body
        {
            get 
            {
                return Prepare(Translate(_originalBody));
            }
        }

        public ITemplateEngine TemplateEngine
        {
            set 
            { 
                _templateEngine = value;
            }
        }
        #endregion public properties

        #endregion Properties


        #region Constructors
        public MailTemplate(string templateName, string subject, string body, ITemplateEngine templateEngine)
        {
            _templateName = templateName;
            _originalSubject = subject;
            _originalBody = body;
            _templateEngine = templateEngine;
        }

        private MailTemplate()
        { }
        #endregion Constructors


        #region Methods

        #region Private Methods
        private string Prepare(string text)
        {
            Regex regex = new Regex(@"^(\t| )+", RegexOptions.Multiline);
            text = text.Trim();
            text = regex.Replace(text, String.Empty);
            return text;
        }

        private string Translate(string text)
        {
            _templateEngine.Template = text;
            foreach(var attribute in _attributes)
            {
                _templateEngine.SetAttribute(attribute.Key, attribute.Value);
            }

            return _templateEngine.Output;
        }

        #endregion Private Methods


        #region Protected Methods
        #endregion Protected Methods


        #region Public Methods
        public void SetAttribute(string name, object value)
        {
            _attributes.Add(name, value);
        }


        public void SetAttribute(string name, params object[] values)
        {
            foreach (object value in values)
            {
                SetAttribute(name, value);
            }
        }
        #endregion Public Methods

        #endregion Methods


        #region Overrides

        

        #region Override ToString
        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            return TemplateName;
        }
        #endregion Override ToString()


        #region Override Equals
        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>.</param>
        /// <returns>
        /// true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            return (this.GetHashCode() == obj.GetHashCode());
        }
        #endregion Override Equals


        #region Override GetHashCode()
        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        public override int GetHashCode()
        {
            return TemplateName.GetHashCode();
        }
        #endregion Override GetHashCode()

        #endregion Overrides
    }
}