﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using XWS.BusinessPlatform.Services.Mail.Interfaces;
using XWS.BusinessPlatform.Services.Mail.Templates;
using System.Web;
using System.Configuration;

namespace XWS.BusinessPlatform.Services.Mail
{
    /// <summary>
    /// Description of IMailTemplateService
    /// </summary>
    internal class MailTemplateService : IMailTemplateService
    {
        #region Member Variables

        private static readonly IList<IMailTemplate> _mailTemplates = new List<IMailTemplate>();
        private ITemplateEngine _templateEngine;
        private DirectoryInfo _templatePath;

        #endregion Member Variables

        #region Properties

        #region Private Properties

        #endregion Private Properties

        #region Protected Properties

        #endregion Protected Properties

        #region Public Properties

        public DirectoryInfo TemplatePath
        {
            get { return _templatePath; }
        }

        public IEnumerable<IMailTemplate> MailTemplates
        {
            get { return _mailTemplates; }
        }

        #endregion public properties

        #endregion Properties

        #region Constructors

        public MailTemplateService(ITemplateEngine templateEngine)
        {
            if (HttpContext.Current != null)
            {
                string templatePath =
                    HttpContext.Current.Request.MapPath(HttpContext.Current.Request.ApplicationPath +
                                                        ConfigurationManager.AppSettings["mailTemplatesPath"]);

                if (String.IsNullOrEmpty(templatePath))
                {
                    throw new Exception(
                        "'MailTemplatesPath' key does not exist in Web.config or the value is null or empty.");
                }

                Initialize(templateEngine, new DirectoryInfo(templatePath));
            }
        }


        public MailTemplateService(ITemplateEngine templateEngine, DirectoryInfo templatePath)
        {
            Initialize(templateEngine, templatePath);
        }


        public MailTemplateService(ITemplateEngine templateEngine, string templatePath)
        {
            if (String.IsNullOrEmpty(templatePath))
            {
                throw new ArgumentNullException("templatePath", "Argument must not be null or empty.");
            }

            Initialize(templateEngine, new DirectoryInfo(templatePath));
        }

        #endregion Constructors

        #region Methods

        #region Private Methods

        /// <summary>
        /// only necessary if called outside the webapplication
        /// </summary>
        /// <param name="templateEngine"></param>
        /// <param name="templatePath"></param>
        public void Initialize(ITemplateEngine templateEngine, DirectoryInfo templatePath)
        {
            if (templatePath == null)
            {
                throw new ArgumentNullException("templatePath", "Argument must not be null.");
            }

            if (!templatePath.Exists)
            {
                throw new ArgumentException("Mail template directory " + templatePath + " does not exist.",
                                            "templatePath");
            }

            if (templateEngine == null)
            {
                throw new ArgumentNullException("templateEngine", "Argument must not be null.");
            }

            _templateEngine = templateEngine;
            _templatePath = templatePath;

            ParseTemplateDirectory(templatePath);
        }


        private void ParseTemplateDirectory(DirectoryInfo directory)
        {
            foreach (FileInfo file in directory.GetFiles("*.xml"))
            {
                IMailTemplate template = LoadTemplate(file);

                if (_mailTemplates.Contains(template))
                {
                    _mailTemplates.Remove(template);
                }
                _mailTemplates.Add(template);
            }

            foreach (DirectoryInfo dir in directory.GetDirectories())
            {
                ParseTemplateDirectory(dir);
            }
        }


        private void ValidateTemplate(FileInfo templateFile)
        {
            const string schemaFile = "MailTemplate.xsd";
            var schemaSet = new XmlSchemaSet();
            //Stream xsdStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("XWS.SQMPortal.Services.Mails.MailTemplate.xsd");
            Stream xsdStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(typeof(MailTemplateService),
                                                                                         schemaFile);

            if (xsdStream == null)
            {
                throw new MailTemplateException("XML Schema file '" + schemaFile + "' does not exist.");
            }

            schemaSet.Add(XmlSchema.Read(xsdStream, null));
            var xDocument = new XDocument(XElement.Load(templateFile.FullName, LoadOptions.None));
            xDocument.Validate(schemaSet,
                               (source, args) =>
                               {
                                   throw new Exception("Mail template file '" + templateFile + "' is not valid:" +
                                                       args.Message);
                               });
        }

        #endregion Private Methods

        #region Protected Methods

        #endregion Protected Methods

        #region Public Methods

        public IMailTemplate GetTemplate(string templateName)
        {
            // ToDo: Optimize performance - Build template collection only if any template file has been changed on disc
            ParseTemplateDirectory(_templatePath);

            IMailTemplate mailTemplate = _mailTemplates
                .Where(template => template.TemplateName == templateName).SingleOrDefault();

            return mailTemplate ?? new NullMailTemplate(templateName);
        }


        public IMailTemplate CreateTemplate(string templateName, string subject, string body)
        {
            return new MailTemplate(templateName, subject, body, _templateEngine);
        }


        public IMailTemplate LoadTemplate(FileInfo templateFile)
        {
            //ValidateTemplate(templateFile);

            var fs = new FileStream(templateFile.FullName, FileMode.Open, FileAccess.Read);

            var xs = new XmlSerializer(typeof(MailTemplate));
            var template = (MailTemplate)xs.Deserialize(fs);
            fs.Close();
            template.TemplateEngine = _templateEngine;
            return template;
        }


        public void SaveTemplate(FileInfo templateFile, IMailTemplate template)
        {
            var fs = new FileStream(templateFile.FullName, FileMode.Create);
            var xs = new XmlSerializer(typeof(MailTemplate));
            xs.Serialize(fs, template);
            fs.Close();
        }

        #endregion Public Methods

        #endregion Methods
    }
}