﻿using System.IO;
using XWS.BusinessPlatform.Services.Mail.Templates;

namespace XWS.BusinessPlatform.Services.Mail.Interfaces
{
    public interface IMailTemplateService
    {
        IMailTemplate CreateTemplate(string templateName, string subject, string body);
        IMailTemplate GetTemplate(string templateName);
        void SaveTemplate(FileInfo templateFile, IMailTemplate template);

        /// <summary>
        /// only necessary if called outside the webapplication
        /// </summary>
        /// <param name="templateEngine"></param>
        /// <param name="templatePath"></param>
        void Initialize(ITemplateEngine templateEngine, DirectoryInfo templatePath);
    }
}