﻿namespace XWS.BusinessPlatform.Services.Mail.Interfaces
{
    public interface IMailTemplate
    {
        string TemplateName { get; }
        string Subject { get; }
        string Body { get; }
        void SetAttribute(string name, object value);
        void SetAttribute(string name, object[] values);
    }
}