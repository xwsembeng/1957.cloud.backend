﻿using System.Net.Mail;

namespace XWS.BusinessPlatform.Services.Mail.Interfaces
{
    public interface IMailReceiver
    {
        MailAddress MailAddress { get; }
    }
}