using System;
using System.Collections.Generic;
using System.Net.Mail;

namespace XWS.BusinessPlatform.Services.Mail.Interfaces
{
    public interface IMail
    {
        bool IsTestMailActive { get; }
        bool IsMailSendingActive { get; }
        MailAddress TestMailAddress { get; }
        MailAddress Sender { get; set; }
        IEnumerable<IMailReceiver> Recipients { get; }
        IEnumerable<IMailReceiver> CarbonCopies { get; }
        Exception LastException { get; }
        string Subject { get; set; }
        string Body { get; set; }
        IMailTemplate MailTemplate { get; set; }
        void AddRecipient(IMailReceiver mailReceiver);
        void AddRecipients(IEnumerable<IMailReceiver> mailReceiver);
        void AddCarbonCopy(IMailReceiver mailReceiver);
        void AddCarbonCopies(IEnumerable<IMailReceiver> mailReceiver);
        void ClearRecipientsList();
        void ClearCarbonCopiesList();

        /// <summary>
        /// Adds an attachement to the E-Mail
        /// </summary>
        /// <param name="attachment">The attachment to add</param>
        void AddAttachment(Attachment attachment);

        /// <summary>
        /// Adds attachements to the E-Mail
        /// </summary>
        /// <param name="attachmentlist">The attachments to add</param>
        void AddAttachments(IEnumerable<Attachment> attachmentlist);

        bool Send();

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        string ToString();

        bool Send(bool isHtmlMail);
    }
}