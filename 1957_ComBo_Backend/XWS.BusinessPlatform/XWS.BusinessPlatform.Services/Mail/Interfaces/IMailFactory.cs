using System.Collections.Generic;
using System.Net.Mail;

namespace XWS.BusinessPlatform.Services.Mail.Interfaces
{
    public interface IMailFactory
    {
        IMail CreateMail();
        IMail CreateMail(IMailReceiver user);
        IMail CreateMail(MailAddress address);

        IMail CreateMail(
            IMailReceiver sender,
            IEnumerable<IMailReceiver> to,
            IEnumerable<IMailReceiver> cc,
            string subject,
            string body,
            IEnumerable<Attachment> attachments);
    }
}