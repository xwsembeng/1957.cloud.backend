﻿using System.Collections.Generic;
using System.Net.Mail;
using XWS.BusinessPlatform.Services.Mail.Interfaces;

namespace XWS.BusinessPlatform.Services.Mail
{
    internal class MailFactory : IMailFactory
    {
        public IMail CreateMail(IMailReceiver sender, IEnumerable<IMailReceiver> to, IEnumerable<IMailReceiver> cc,
                                string subject, string body, IEnumerable<Attachment> attachments)
        {
            IMail mail = new Mail(sender);

            mail.AddRecipients(to);
            mail.AddCarbonCopies(cc);

            mail.Subject = subject;
            mail.Body = body;
            mail.AddAttachments(attachments);

            return mail;
        }

        public IMail CreateMail()
        {
            return new Mail();
        }

        public IMail CreateMail(IMailReceiver user)
        {
            return new Mail(user);
        }

        public IMail CreateMail(MailAddress address)
        {
            return new Mail(address);
        }
    }
}