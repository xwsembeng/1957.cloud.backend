﻿using System;
using XWS.BusinessPlatform.Services.Mail.Interfaces;

namespace XWS.BusinessPlatform.Services.Mail
{
    /// <summary>
    /// Description of NullMailTemplate
    /// </summary>
    internal class NullMailTemplate : IMailTemplate
    {
        #region Member Variables

        private const string text = "Requested mail template '{0}' does not exist";
        private readonly string _templateName;

        #endregion Member Variables

        #region Properties

        #region Private Properties

        #endregion Private Properties

        #region Protected Properties

        #endregion Protected Properties

        #region Public Properties

        public string TemplateName
        {
            get { return _templateName; }
        }

        public string Subject
        {
            get { return String.Format(text, TemplateName); }
        }

        public string Body
        {
            get { return String.Format(text, TemplateName); }
        }

        #endregion public properties

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="NullMailTemplate"/> class.
        /// </summary>
        public NullMailTemplate(string templateName)
        {
            _templateName = templateName;
        }

        #endregion Constructors

        #region Methods

        #region Private Methods

        #endregion Private Methods

        #region Protected Methods

        #endregion Protected Methods

        #region Public Methods

        public void SetAttribute(string name, object value)
        {
            // do nothing
        }

        public void SetAttribute(string name, object[] values)
        {
            // do nothing
        }

        #endregion Public Methods

        #endregion Methods

        #region Overrides

        #region Override ToString

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            return base.ToString();
        }

        #endregion Override ToString()

        #region Override GetHashCode()

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #endregion Override GetHashCode()

        #endregion Overrides
    }
}