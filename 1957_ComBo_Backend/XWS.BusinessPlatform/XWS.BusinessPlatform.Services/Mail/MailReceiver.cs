using System;
using System.Net.Mail;
using XWS.BusinessPlatform.Services.Mail.Interfaces;

namespace XWS.BusinessPlatform.Services.Mail
{
    public class MailReceiver : IMailReceiver
    {
        private string _mailAddress;

        public MailReceiver(string mailAddress)
        {
            _mailAddress = mailAddress;
        }

        public MailAddress MailAddress
        {
            get { return new MailAddress(_mailAddress);}
        }
    }
}