﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using XWS.BusinessPlatform.Repositories;
using XWS.BusinessPlatform.Security.Domain;
using XWS.BusinessPlatform.Security.Domain.Interfaces;
using XWS.BusinessPlatform.Security.Domain.Library;
using XWS.BusinessPlatform.Security.Repository.Interfaces;

namespace XWS.BusinessPlatform.Security.Repository
{
    public class AppUserRepository : GenericInterfaceRepository<IAppUser>, IAppUserRepository
    {
        public IList<IAppUser> GetUsersByFilter(AppUserFilterObject obj)
        {

            ICriteria criteria = Session.CreateCriteria(typeof(AppUser));

            // do not load obsolete users
            criteria.Add(Restrictions.Eq("IsObsolete", false));

            if (!string.IsNullOrEmpty(obj.Login))
            {
                criteria.Add(Restrictions.InsensitiveLike("Login", ParseInsensitiveLikeString(obj.Login)));
            }
            if (!string.IsNullOrEmpty(obj.FirstName))
            {
                criteria.Add(Restrictions.InsensitiveLike("FirstName", ParseInsensitiveLikeString(obj.FirstName)));
            }
            if (!string.IsNullOrEmpty(obj.LastName))
            {
                criteria.Add(Restrictions.InsensitiveLike("LastName", ParseInsensitiveLikeString(obj.LastName)));
            }
            if (!string.IsNullOrEmpty(obj.EMail))
            {
                criteria.Add(Restrictions.InsensitiveLike("EMail", ParseInsensitiveLikeString(obj.EMail)));
            }
            if (!string.IsNullOrEmpty(obj.Phone))
            {
                criteria.Add(Restrictions.InsensitiveLike("Phone", ParseInsensitiveLikeString(obj.Phone)));
            }
            if (obj.Active != null)
            {
                criteria.Add(Restrictions.Eq("Active", obj.Active));
            }
            if (obj.StructureUnit != null)
            {
                if (!obj.StructureUnitCascading)
                {
                    criteria.Add(Restrictions.Eq("StructureUnit", obj.StructureUnit));
                }
                else
                {
                    IList<IStructureUnit> ret = new List<IStructureUnit>();
                    GetAllChildrenForStructureUnitRecursive(ref ret, obj.StructureUnit);
                    criteria.Add(Restrictions.InG<IStructureUnit>("StructureUnit", ret));
                }
            }
            IList<IAppUser> userList = criteria.List<IAppUser>();
            return userList;
        }

        public IAppUser FindByAccount(string account)
        {
            return FindByAccount(account, false);
        }

        public IAppUser FindByAccount(string account, bool ignoreObsolete)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(AppUser))
            .Add(Restrictions.Eq("Login", account));

            if (!ignoreObsolete)
                criteria.Add(Restrictions.Eq("IsObsolete", false));

            return criteria.List<IAppUser>()
                .SingleOrDefault();
        }

        public IList<IAppUser> FindUnchecked()
        {
            return FindByProperty("IsChecked", false);
        }

        private void GetAllChildrenForStructureUnitRecursive(ref IList<IStructureUnit> children, IStructureUnit unit)
        {
            children.Add(unit);
            foreach (IStructureUnit child in unit.Children)
            {
                GetAllChildrenForStructureUnitRecursive(ref children, child);
            }
        }

        private string ParseInsensitiveLikeString(string value)
        {
            if (value.Contains('%'))
                return value;

            return "%" + value + "%";
        }

        public int FindNumberOfDeletedLogins(string login)
        {
            return Session.CreateCriteria(typeof(AppUser))
                .Add(Restrictions.Like("Login", "DELETED%_" + login))
                .SetProjection(Projections.Count("Id"))
                .UniqueResult<int>();
        }
    }
}
