﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Repositories.Interfaces;
using XWS.BusinessPlatform.Security.Domain;

namespace XWS.BusinessPlatform.Security.Repository.Interfaces
{
    public interface IModuleRepository : IGenericRepository<Module>
    {
        Module FindByName(string name);
        Module FindByCode(string code);
        IList<Module> FindAllUserAssignmentModules();
    }
}
