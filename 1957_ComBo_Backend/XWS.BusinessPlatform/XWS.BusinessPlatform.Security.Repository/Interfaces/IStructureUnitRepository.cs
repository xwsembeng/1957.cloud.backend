﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Repositories.Interfaces;
using XWS.BusinessPlatform.Security.Domain;
using XWS.BusinessPlatform.Security.Domain.Interfaces;

namespace XWS.BusinessPlatform.Security.Repository.Interfaces
{
    public interface IStructureUnitRepository : IGenericRepository<IStructureUnit>
    {
        IStructureUnit GetRootStructureUnit();
        IList<IStructureUnit> GetAllStructureUnitsWithAssignedProfilesForUsers(IList<IAppUser> users, Module module);
        IList<IStructureUnit> GetAllStructureUnitsWithAssignedProfilesForUser(IAppUser user, Module module);
        IList<IStructureUnit> GetStructureUnitsWithAssignedProfilesForUser(IAppUser user, Profile profile);
    }
}
