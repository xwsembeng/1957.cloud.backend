﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Repositories.Interfaces;
using XWS.BusinessPlatform.Security.Domain;

namespace XWS.BusinessPlatform.Security.Repository.Interfaces
{
    public interface IProfileRepository : IGenericRepository<Profile>
    {
        IEnumerable<Profile> GetProfilesByType(Module module, ProfileType type);
        IEnumerable<Profile> GetProfilesSortedByTypeAndCode(Module module);
        Profile GetByCode(string code);
    }
}
