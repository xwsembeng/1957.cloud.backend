﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Repositories.Interfaces;
using XWS.BusinessPlatform.Security.Domain;
using XWS.BusinessPlatform.Security.Domain.Interfaces;

namespace XWS.BusinessPlatform.Security.Repository.Interfaces
{
    public interface IUserProfileStructureUnitRepository : IGenericRepository<User_Profile_StructureUnit>
    {
        IList<User_Profile_StructureUnit> GetUserProfileStructureUnitList(IAppUser user, Profile profile);
        IList<User_Profile_StructureUnit> GetUserProfileStructureUnitList(IAppUser user, IList<Profile> profiles);
        IList<User_Profile_StructureUnit> GetUserProfileStructureUnitList(IAppUser user, Profile profile, IStructureUnit unit);
        IList<User_Profile_StructureUnit> GetUserProfileStructureUnitList(IAppUser user, Profile profile, IList<IStructureUnit> units);
        IList<User_Profile_StructureUnit> GetUserProfileStructureUnitsList(IAppUser user, IStructureUnit unit, IList<Profile> profiles);
        IList<User_Profile_StructureUnit> GetUserProfileStructureUnitsByProfile(Profile profile);
        IList<User_Profile_StructureUnit> GetUserProfileStructureUnitByUser(IAppUser user);
        User_Profile_StructureUnit GetUserProfileStructureUnit(IAppUser user, Profile profile, IStructureUnit unit);
    }
}
