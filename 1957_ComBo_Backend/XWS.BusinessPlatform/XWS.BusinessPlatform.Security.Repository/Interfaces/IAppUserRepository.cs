﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWS.BusinessPlatform.Repositories.Interfaces;
using XWS.BusinessPlatform.Security.Domain.Interfaces;
using XWS.BusinessPlatform.Security.Domain.Library;

namespace XWS.BusinessPlatform.Security.Repository.Interfaces
{
    public interface IAppUserRepository : IGenericRepository<IAppUser>
    {
        IList<IAppUser> GetUsersByFilter(AppUserFilterObject appUserFilterObject);
        IAppUser FindByAccount(string account);
        IAppUser FindByAccount(string account, bool ignoreObsolete);
        IList<IAppUser> FindUnchecked();
        int FindNumberOfDeletedLogins(string login);
    }
}
