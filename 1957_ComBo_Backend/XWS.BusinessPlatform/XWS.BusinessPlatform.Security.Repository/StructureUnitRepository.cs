﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using XWS.BusinessPlatform.Repositories;
using XWS.BusinessPlatform.Security.Domain;
using XWS.BusinessPlatform.Security.Domain.Interfaces;
using XWS.BusinessPlatform.Security.Repository.Interfaces;

namespace XWS.BusinessPlatform.Security.Repository
{
    internal class StructureUnitRepository : GenericInterfaceRepository<IStructureUnit>, IStructureUnitRepository
    {
        private readonly IProfileRepository profileRepository;

        public StructureUnitRepository(IProfileRepository repository)
        {
            profileRepository = repository;
        }

        public StructureUnit Create()
        {
            StructureUnit newObject = new StructureUnit();
            Session.Save(newObject);
            return newObject;
        }

        public IStructureUnit GetRootStructureUnit()
        {
            IList<StructureUnit> rootUnits = Session.CreateCriteria(typeof(StructureUnit)).Add(Restrictions.IsNull("Parent")).List<StructureUnit>();
            return rootUnits[0];
        }

        /// <summary>
        /// Return all StructureUnit for according module for more user
        /// </summary>
        /// <param name="users"></param>
        /// <param name="module"></param>
        /// <returns></returns>
        public IList<IStructureUnit> GetAllStructureUnitsWithAssignedProfilesForUsers(IList<IAppUser> users, Module module)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(User_Profile_StructureUnit));

            criteria.Add(Restrictions.InG<IAppUser>("User", users));

            IList<Profile> profiles = new List<Profile>();
            if (module != null)
            {
                foreach (Profile p in profileRepository.GetProfilesSortedByTypeAndCode(module))
                    profiles.Add(p);

                criteria.Add(Restrictions.InG<Profile>("Profile", profiles));
            }

            IList<IStructureUnit> ret = new List<IStructureUnit>();
            foreach (User_Profile_StructureUnit ups in criteria.List<User_Profile_StructureUnit>())
            {
                if (!ret.Contains(ups.StructureUnit))
                    ret.Add(ups.StructureUnit);
            }

            return ret;
        }

        /// <summary>
        /// Return all StructureUnit for according module for one user account
        /// </summary>
        /// <param name="user"></param>
        /// <param name="module"></param>
        /// <returns></returns>
        public IList<IStructureUnit> GetAllStructureUnitsWithAssignedProfilesForUser(IAppUser user, Module module)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(User_Profile_StructureUnit));
            criteria.Add(Restrictions.Eq("User", user));

            if (module != null)
            {

                IList<Profile> profiles = new List<Profile>();

                if (module.ModuleType == ModuleType.ModuleContainer)
                {
                    foreach (Module childmodule in module.Children)
                    {
                        foreach (Profile profile in profileRepository.GetProfilesSortedByTypeAndCode(childmodule))
                        {
                            if (!profiles.Contains(profile))
                                profiles.Add(profile);
                        }

                    }
                }
                else
                {
                    foreach (Profile profile in profileRepository.GetProfilesSortedByTypeAndCode(module))
                    {
                        if (!profiles.Contains(profile))
                        {
                            profiles.Add(profile);
                        }
                    }
                }


                criteria.Add(Restrictions.InG("Profile", profiles));
            }

            IList<IStructureUnit> ret = new List<IStructureUnit>();
            foreach (User_Profile_StructureUnit ups in criteria.List<User_Profile_StructureUnit>())
            {
                if (!ret.Contains(ups.StructureUnit))
                    ret.Add(ups.StructureUnit);
            }

            return ret;
        }

        /// <summary>
        /// Return StructureUnits list for according Profile 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="profile"></param>
        /// <returns></returns>
        public IList<IStructureUnit> GetStructureUnitsWithAssignedProfilesForUser(IAppUser user, Profile profile)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(User_Profile_StructureUnit));

            //search criteria for user
            criteria.Add(Restrictions.Eq("User", user));

            IList<IStructureUnit> ret = new List<IStructureUnit>();
            foreach (User_Profile_StructureUnit userProfileStructureUnit in criteria.List<User_Profile_StructureUnit>())
            {
                foreach (Profile child in userProfileStructureUnit.Profile.Children)
                {
                    if (profile != null && child.Id == profile.Id)
                    {
                        if (!ret.Contains(userProfileStructureUnit.StructureUnit))
                        {
                            if (userProfileStructureUnit.IsCascading)
                                AddStructureUnitsByCascading(ref ret, userProfileStructureUnit.StructureUnit);
                            else
                                ret.Add(userProfileStructureUnit.StructureUnit);
                        }
                    }
                }
            }

            return ret;
        }

        private void AddStructureUnitsByCascading(ref IList<IStructureUnit> structureUnits, IStructureUnit structureUnit)
        {
            if (structureUnit.Children != null && ((IList<IStructureUnit>)structureUnit.Children).Count > 0)
            {
                foreach (IStructureUnit child in structureUnit.Children)
                {
                    AddStructureUnitsByCascading(ref structureUnits, child);
                }
            }
            if (!structureUnits.Contains(structureUnit))
                structureUnits.Add(structureUnit);
        }
    }
}
