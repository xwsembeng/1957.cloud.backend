﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Criterion;
using XWS.BusinessPlatform.Repositories;
using XWS.BusinessPlatform.Security.Domain;
using XWS.BusinessPlatform.Security.Domain.Interfaces;
using XWS.BusinessPlatform.Security.Repository.Interfaces;

namespace XWS.BusinessPlatform.Security.Repository
{
    internal class UserProfileStructureUnitRepository : GenericRepository<User_Profile_StructureUnit>, IUserProfileStructureUnitRepository
    {
        public IList<User_Profile_StructureUnit> GetUserProfileStructureUnitList(IAppUser user, Profile profile)
        {
            return Session.CreateCriteria(typeof(User_Profile_StructureUnit))
                .Add(Restrictions.Eq("User", user))
                .Add(Restrictions.Eq("Profile", profile))
                .List<User_Profile_StructureUnit>();
        }

        public IList<User_Profile_StructureUnit> GetUserProfileStructureUnitList(IAppUser user, IList<Profile> profiles)
        {
            return Session.CreateCriteria(typeof(User_Profile_StructureUnit))
                .Add(Restrictions.Eq("User", user))
                .Add(Restrictions.InG("Profile", profiles))
                .List<User_Profile_StructureUnit>();
        }

        public IList<User_Profile_StructureUnit> GetUserProfileStructureUnitList(IAppUser user, Profile profile, IStructureUnit unit)
        {
            return Session.CreateCriteria(typeof(User_Profile_StructureUnit))
                .Add(Restrictions.Eq("StructureUnit", unit))
                .Add(Restrictions.Eq("Profile", profile))
                .Add(Restrictions.Eq("User", user))
                .List<User_Profile_StructureUnit>();
        }

        public IList<User_Profile_StructureUnit> GetUserProfileStructureUnitList(IAppUser user, Profile profile, IList<IStructureUnit> units)
        {
            return Session.CreateCriteria(typeof(User_Profile_StructureUnit))
                .Add(Restrictions.Eq("User", user))
                .Add(Restrictions.Eq("Profile", profile))
                .Add(Restrictions.InG("StructureUnit", units))
                .List<User_Profile_StructureUnit>();
        }
        public IList<User_Profile_StructureUnit> GetUserProfileStructureUnitsList(IAppUser user, IStructureUnit unit, IList<Profile> profiles)
        {
            return Session.CreateCriteria(typeof(User_Profile_StructureUnit))
                .Add(Restrictions.Eq("User", user))
                .Add(Restrictions.InG("Profile", profiles))
                .Add(Restrictions.Eq("StructureUnit", unit))
                .List<User_Profile_StructureUnit>();
        }
        public User_Profile_StructureUnit GetUserProfileStructureUnit(IAppUser user, Profile profile, IStructureUnit unit)
        {
            return Session.CreateCriteria(typeof(User_Profile_StructureUnit))
                .Add(Restrictions.Eq("User", user))
                .Add(Restrictions.Eq("Profile", profile))
                .Add(Restrictions.Eq("StructureUnit", unit))
                .UniqueResult<User_Profile_StructureUnit>();
        }
        public IList<User_Profile_StructureUnit> GetUserProfileStructureUnitsByProfile(Profile profile)
        {
            return Session.CreateCriteria(typeof(User_Profile_StructureUnit))
                .Add(Restrictions.Eq("Profile", profile))
                .List<User_Profile_StructureUnit>();
        }
        public IList<User_Profile_StructureUnit> GetUserProfileStructureUnitByUser(IAppUser user)
        {
            return Session.CreateCriteria(typeof(User_Profile_StructureUnit))
                .Add(Restrictions.Eq("User", user))
                .List<User_Profile_StructureUnit>();
        }
    }
}
