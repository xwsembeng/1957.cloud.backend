﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Linq;
using XWS.BusinessPlatform.Repositories;
using XWS.BusinessPlatform.Security.Domain;
using XWS.BusinessPlatform.Security.Repository.Interfaces;

namespace XWS.BusinessPlatform.Security.Repository
{
    public class ProfileRepository : GenericRepository<Profile>, IProfileRepository
    {
        public IEnumerable<Profile> GetProfilesByType(Module module, ProfileType profileType)
        {
            if (module == null)
            {
                throw new ArgumentNullException("module", "Parameter 'module' must not be null.");
            }

            IQueryable<Profile> query = Session.Query<Profile>();

            return query.Where(obj => obj.Modules.Contains(module) && obj.ProfileType == profileType)
                .OrderBy(obj => obj.Code)
                .ToList();
        }

        public IEnumerable<Profile> GetProfilesSortedByTypeAndCode(Module module)
        {
            if (module == null)
            {
                throw new ArgumentNullException("module", "Parameter 'module' must not be null.");
            }

            IQueryable<Profile> query = Session.Query<Profile>();

            return query
                .Where(obj => obj.Modules.Contains(module))
                .OrderByDescending(obj => obj.ProfileType)
                .ThenBy(obj => obj.Code)
                .ToList();
        }

        public Profile GetByCode(string code)
        {
            return FindObjectByProperty("Code", code);
        }
    }
}
