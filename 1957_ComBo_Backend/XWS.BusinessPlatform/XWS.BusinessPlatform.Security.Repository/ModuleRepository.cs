﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Criterion;
using XWS.BusinessPlatform.Repositories;
using XWS.BusinessPlatform.Security.Domain;
using XWS.BusinessPlatform.Security.Repository.Interfaces;

namespace XWS.BusinessPlatform.Security.Repository
{
    internal class ModuleRepository: GenericInterfaceRepository<Module>, IModuleRepository
    {
        public Module FindByName(string name)
        {
            return FindObjectByProperty("DisplayName", name);
        }

        public Module FindByCode(string code)
        {
            return FindObjectByProperty("Code", code);
        }

        public IList<Module> FindAllUserAssignmentModules()
        {
            return Session.CreateCriteria(typeof(Module))
                .Add(Restrictions.Eq("AllowUserProfileAssignment", true))
                .List<Module>();
        }
    }
}
